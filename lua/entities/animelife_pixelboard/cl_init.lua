include("shared.lua")

function ENT:Initialize()
    self.Canvas = pixeldraw:CreateCanvas(5112, 1324, Color(31, 31, 31), 24)
    self.CurrentColor = Color(0, 0, 0)
    self.NextPlace = CurTime()

    net.Start("animelife.pixelboard.request")
    net.SendToServer()
end

function ENT:Draw()
    -- self:DrawModel()
end

function ENT:DrawTranslucent()
    -- fucking hell. 3d2d within :DrawTranslucent() eats itself and overall works like total shit
    -- using hook instead.
end

local max_dist = 1024^2
hook.Add("PostDrawTranslucentRenderables", "animelife.pixelboards.render", function()
    for _, board in ipairs(ents.FindByClass("animelife_pixelboard")) do
        if board:IsDormant() then return end
        local dist = LocalPlayer():GetPos():DistToSqr(board:GetPos())
        if dist > max_dist then return end

        local canvas = board.Canvas
        if canvas then
            local pos = board:GetPos()
            local ang = board:GetAngles()
            cam.Start3D2D(pos, ang, 0.05)
                local alpha = 1 - (1 * (dist / max_dist))
                surface.SetAlphaMultiplier(alpha)
                    pixeldraw:Render(canvas, 0, 0, 5112, 1324, board.CurrentColor, nil, pos, ang, 0.05)
                    pixeldraw:RenderPalette(32, 32, 32)

                    local can_use = dist < 470^2
                    local click_skip = false
                    if (LocalPlayer():KeyDown(IN_ATTACK) or LocalPlayer():KeyDown(IN_USE)) and can_use and (board.NextPlace or 0) < CurTime() then
                        local px, py, psize = 32, 32, 32
                        local x, y = pixeldraw:CursorPos(canvas, 3, nil, pos, ang, 0.05)
                        for i = 1, #pixeldraw.palette_colors do
                            -- in button bounds
                            if (x >= px - 16 and y >= py - 16) and (x <= px + psize + 16 and y <= py + psize + 16) then
                                local clr = pixeldraw.palette_colors[i]

                                board.CurrentColor = clr
                                click_skip = true
                            end

                            px = px + psize + 16
                        end

                        if !click_skip then
                            pixeldraw:PlacePixel(canvas, x, y, board.CurrentColor)

                            net.Start("animelife.pixelboard.applypixel")
                                net.WriteInt(x, 16)
                                net.WriteInt(y, 16)
                                net.WriteColor(board.CurrentColor)
                            net.SendToServer()

                            board.NextPlace = CurTime() + 5
                        end
                    end
                surface.SetAlphaMultiplier(1)
            cam.End3D2D()
        end
    end
end)

net.Receive("animelife.pixelboard.receivepixel", function()
    local pixelboard = ents.FindByClass("animelife_pixelboard")[1]
    if !IsValid(pixelboard) then return end
    if !pixelboard.Canvas then return end

    local x = net.ReadInt(16)
    local y = net.ReadInt(16)
    local col = net.ReadColor()

    pixeldraw:PlacePixel(pixelboard.Canvas, x, y, col)
end)

net.Receive("animelife.pixelboard.restore", function()
    local pixelboard = ents.FindByClass("animelife_pixelboard")[1]
    if !IsValid(pixelboard) then return end
    if !pixelboard.Canvas then return end

    local size = net.ReadInt(32)
    local data = net.ReadData(size)
    data = util.Decompress(data)
    data = util.JSONToTable(data)

    pixeldraw:GetCanvas(pixelboard.Canvas).Pixels = data
end)