AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")

include("shared.lua")

util.AddNetworkString("animelife.pixelboard.applypixel")
util.AddNetworkString("animelife.pixelboard.receivepixel")
util.AddNetworkString("animelife.pixelboard.restore")
util.AddNetworkString("animelife.pixelboard.request")

function ENT:Initialize()
    self:SetModel(self.Model)
    -- self:PhysicsInit(SOLID_VPHYSICS)

    local phys = self:GetPhysicsObject()
    if IsValid(phys) then
        phys:EnableMotion(false)
    end

    self.Canvas = pixeldraw:NewCanvas("ALPixelArtGlobal", 5112, 1324, Color(31, 31, 31), 24)
    pixeldraw:RestoreCanvasData(self.Canvas)
end

net.Receive("animelife.pixelboard.applypixel", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    -- do basic checks
    if ply:GetPos():Distance(ents.FindByClass("animelife_pixelboard")[1]:GetPos()) > 1024^2 then return end
    if (ply.PixelBoardNextPlace or 0) > SysTime() then return end

    local x = net.ReadInt(16)
    local y = net.ReadInt(16)
    local color = net.ReadColor()

    local changes = pixeldraw:PlacePixel("ALPixelArtGlobal", x, y, color)
    if changes then
        -- kinda went good, now send pixel to everyone
        net.Start("animelife.pixelboard.receivepixel")
            net.WriteInt(x, 16)
            net.WriteInt(y, 16)
            net.WriteColor(color)
        net.Broadcast()
    end

    pixeldraw:SaveCanvas("ALPixelArtGlobal")

    ply.PixelBoardNextPlace = SysTime() + 5
end)

net.Receive("animelife.pixelboard.request", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if table.IsEmpty(pixeldraw.canvas_list) then return end

    for index, canvas in pairs(pixeldraw.canvas_list) do
        local pixels = canvas.Pixels
        if pixels == nil or table.IsEmpty(pixels) then
            continue
        end

        pixels = util.TableToJSON(pixels)
        pixels = util.Compress(pixels)
        local len = pixels:len()

        net.Start("animelife.pixelboard.restore")
            net.WriteInt(len, 32)
            net.WriteData(pixels, len)
        net.Send(ply)
    end
end)

local spawn_pos = {Vector(-8290, -342, 172), Angle(0, 0, 90)}
hook.Add("PostGamemodeLoaded", "animelife.pixelboards.spawn", function()
    local pixelboard = ents.Create("animelife_pixelboard")
    pixelboard:SetPos(spawn_pos[1])
    pixelboard:SetAngles(spawn_pos[2])
    pixelboard:Spawn()
end)