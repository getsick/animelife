--[[---------------------------------------------------------------------------
DarkRP custom entities
---------------------------------------------------------------------------

This file contains your custom entities.
This file should also contain entities from DarkRP that you edited.

Note: If you want to edit a default DarkRP entity, first disable it in darkrp_config/disabled_defaults.lua
    Once you've done that, copy and paste the entity to this file and edit it.

The default entities can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/addentities.lua

For examples and explanation please visit this wiki page:
https://darkrp.miraheze.org/wiki/DarkRP:CustomEntityFields

Add entities under the following line:
---------------------------------------------------------------------------]]

DarkRP.createEntity("Common Hentai Printer", {
    ent = "animelife_hentaiprinter",
    model = "models/props_c17/consolebox03a.mdl",
    price = 3000,
    max = 2,
    cmd = "buycommonprinter",
    description = "Самый базовый Hentai Printer из доступных.",
    spawn = function(ply, tr, tblEnt) 
        -- local pos = ply:GetShootPos() + (ply:GetForward() * 100)
        local pos = tr.HitPos
        local printer = ents.Create("animelife_hentaiprinter")
        printer:SetPos(pos)
        printer:Spawn()
        printer:SetGirl("Common")
        printer:SetPrinterOwner(ply)
        return printer
    end,
})

DarkRP.createEntity("Uncommon Hentai Printer", {
    ent = "animelife_hentaiprinter",
    model = "models/props_c17/consolebox03a.mdl",
    price = 6000,
    max = 2,
    cmd = "buyuncommonprinter",
    description = "Немного более продвинутый Hentai Printer.",
    spawn = function(ply, tr, tblEnt) 
        -- local pos = ply:GetShootPos() + (ply:GetForward() * 100)
        local pos = tr.HitPos
        local printer = ents.Create("animelife_hentaiprinter")
        printer:SetPos(pos)
        printer:Spawn()
        printer:SetGirl("Uncommon")
        printer:SetColor(GLOBALS_HENTAIPRINTERS_GIRLS[printer:GetGirl()].Color)
        printer:SetPrinterOwner(ply)
        return printer
    end,
})

DarkRP.createEntity("Rare Hentai Printer", {
    ent = "animelife_hentaiprinter",
    model = "models/props_c17/consolebox03a.mdl",
    price = 10000,
    max = 2,
    cmd = "buyrareprinter",
    description = "Из категории принтеров для крутых.",
    spawn = function(ply, tr, tblEnt) 
        -- local pos = ply:GetShootPos() + (ply:GetForward() * 100)
        local pos = tr.HitPos
        local printer = ents.Create("animelife_hentaiprinter")
        printer:SetPos(pos)
        printer:Spawn()
        printer:SetGirl("Rare")
        printer:SetColor(GLOBALS_HENTAIPRINTERS_GIRLS[printer:GetGirl()].Color)
        printer:SetPrinterOwner(ply)
        return printer
    end,
})

DarkRP.createEntity("Immortal Hentai Printer", {
    ent = "animelife_hentaiprinter",
    model = "models/props_c17/consolebox03a.mdl",
    price = 15000,
    max = 2,
    cmd = "buyimmortalprinter",
    description = "Второй по недоступности Hentai Printer. Теперь точно для крутых.",
    spawn = function(ply, tr, tblEnt) 
        -- local pos = ply:GetShootPos() + (ply:GetForward() * 100)
        local pos = tr.HitPos
        local printer = ents.Create("animelife_hentaiprinter")
        printer:SetPos(pos)
        printer:Spawn()
        printer:SetGirl("Immortal")
        printer:SetColor(GLOBALS_HENTAIPRINTERS_GIRLS[printer:GetGirl()].Color)
        printer:SetPrinterOwner(ply)
        return printer
    end,
})

DarkRP.createEntity("Immortal+ Hentai Printer (MEMBER+)", {
    ent = "animelife_hentaiprinter",
    model = "models/props_c17/consolebox03a.mdl",
    price = 17000,
    max = 2,
    cmd = "buyimmortalplusprinter",
    description = "Первый. Для крутых и VIP.",
    customCheck = function(ply) return ply:IsVIP() end, -- is vip check
    CustomCheckFailMsg = function(ply, entTable) return "Этот Hentai Printer доступен только для MEMBER+!" end,
    spawn = function(ply, tr, tblEnt) 
        -- local pos = ply:GetShootPos() + (ply:GetForward() * 100)
        local pos = tr.HitPos
        local printer = ents.Create("animelife_hentaiprinter")
        printer:SetPos(pos)
        printer:Spawn()
        printer:SetGirl("Immortal+")
        printer:SetColor(GLOBALS_HENTAIPRINTERS_GIRLS[printer:GetGirl()].Color)
        printer:SetPrinterOwner(ply)
        return printer
    end,
})

DarkRP.createEntity("Телевизор", {
    ent = "mediaplayer_tv",
    model = "models/gmod_tower/suitetv_large.mdl",
    price = 10000,
    max = 1,
    cmd = "buymediaplayer",
    description = "Устройство для просмотра видео.",
})