--[[---------------------------------------------------------------------------
DarkRP custom jobs
---------------------------------------------------------------------------
This file contains your custom jobs.
This file should also contain jobs from DarkRP that you edited.

Note: If you want to edit a default DarkRP job, first disable it in darkrp_config/disabled_defaults.lua
      Once you've done that, copy and paste the job to this file and edit it.

The default jobs can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/jobrelated.lua

For examples and explanation please visit this wiki page:
https://darkrp.miraheze.org/wiki/DarkRP:CustomJobFields

Add your custom jobs under the following line:
---------------------------------------------------------------------------]]

TEAM_CITIZEN = DarkRP.createJob("Гайдзин", {
    color = Color(255, 255, 255, 255),
    model = {
        "models/player/Group01/Female_01.mdl",
        "models/player/Group01/Female_02.mdl",
        "models/player/Group01/Female_03.mdl",
        "models/player/Group01/Female_04.mdl",
        "models/player/Group01/Female_06.mdl",
        "models/player/group01/male_01.mdl",
        "models/player/Group01/Male_02.mdl",
        "models/player/Group01/male_03.mdl",
        "models/player/Group01/Male_04.mdl",
        "models/player/Group01/Male_06.mdl",
        "models/player/Group01/Male_07.mdl",
        "models/player/Group01/Male_08.mdl",
        "models/player/Group01/Male_09.mdl"
    },
    description = [[Бака-Гайдзин, только что приехавший в Японию.]],
    weapons = {},
    command = "citizen",
    max = 0,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Мирные",
})

TEAM_NEKO = DarkRP.createJob("Неко", {
    color = Color(255, 206, 249),
    model = {
        "models/jazzmcfly/nekopara/coco/coco_player.mdl",
        "models/player/shi/Azuki.mdl",
        "models/player/setaria_green.mdl",
    },
    description = [[Особый вид людей, гибрид человека и кошки. Происхождение неизвестно, в большинстве случаев не опасна и не агрессивна. Бывают случаи, когда данный объект выбирает себе хозяина, защищая его и следуя за ним.]],
    weapons = {},
    command = "neko",
    max = 3,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Мирные",
})

TEAM_MUSILY = DarkRP.createJob("Меломан", {
    color = Color(223, 168, 255),
    model = {
        "models/player/dewobedil/vocaloid/appearance_miku/stroll_p.mdl",
        "models/player/dewobedil/vocaloid/appearance_miku/default_p.mdl",
    },
    description = [[Музыка является всем для него, душой и жизнью, она следует за этим человеком повсюду. Этот человек задает настроение всему городу, разнося лучи позитива по всему городу.]],
    weapons = {},
    command = "musily",
    max = 2,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Мирные",
})

TEAM_IMPERATOR = DarkRP.createJob("Император", {
    color = Color(255, 199, 199),
    model = {
        "models/player/sanic/kojimap.mdl",
    },
    description = [[Император этого города. Его личность неизвестна, никто не знает о его прошлом. Он внезапно появился и повел эту страну к лучшему будущему, за его заслуги его прозвали гением. По городу ходят слухи, что это и не человек вовсе.]],
    weapons = {"unarrest_stick"},
    command = "mayor",
    max = 1,
    salary = GAMEMODE.Config.normalsalary * 1.89,
    admin = 0,
    vote = true,
    hasLicense = true,
    mayor = true,
    category = "Правительство",
    PlayerSpawn = function(ply)
        ply:SetMaxHealth(150)
        ply:SetHealth(150)
        ply:SetArmor(50)
    end,
    PlayerDeath = function(ply)
        if ply:Team() == TEAM_IMPERATOR then
            ply:changeTeam(TEAM_CITIZEN, true)
            for k, v in ipairs( player.GetAll() ) do
                DarkRP.notify(v, 1, 4, "Император был свержен!")
            end
        end
      end,
})

TEAM_HOBO = DarkRP.createJob("Бомжара", {
    color = Color(255, 223, 182),
    model = "models/bala/monsterboys_pm.mdl",
    description = [[Вы не человек. По крайней мере, так считают многие жители этого города. Напрочь безрассудный и безумный человек, мотивы его действий непонятны никому. Возможно, к безумию его привела жизнь в нищете и постоянные издевки со стороны горожан и банд этого города. Сплотившись между собой, эти безумные и безрассудные люди смогли найти себе укромное место, где живут счастливо и уважают друг друга. Никто не знает что происходит в их кругу, возможно, что это что-то ужасно опасное для этого города?]],
    weapons = {"weapon_bugbait"},
    command = "hobo",
    max = 0,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    hobo = true,
    category = "Мирные",
})

TEAM_HOBOKING = DarkRP.createJob("Бомж-император", {
    color = Color(255, 204, 138),
    model = "models/player/urahara.mdl",
    description = [[Встретив бога, этот человек нашел новую цель в жизни. Возможно, она заключается в уничтожении человечества? По крайней мере, именно этот человек отвественнен за беспорядки, причиненные бомжарами.]],
    weapons = {"weapon_bugbait"},
    command = "hoboking",
    max = 1,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    hobo = true,
    category = "Мирные",
    secret = true,
    PlayerSpawn = function(ply)
        ply:SetMaxHealth(250)
        ply:SetHealth(250)
        ply:SetArmor(100)
    end,
    customCheck = function(ply) return achievements:GetStatus(ply, 1) end,
    CustomCheckFailMsg = "Вы пока не можете занять эту профессию.",
    canSee = function(ply)
        return achievements:GetStatus(ply, 1)
    end
})

TEAM_MEDIC = DarkRP.createJob("Медик", {
    color = Color(198, 255, 255),
    model = { 
        "models/player/dewobedil/persona5/tae_takemi/doctor_p.mdl",
        "models/player/dewobedil/persona5/tae_takemi/default_p.mdl",
    },
    description = [[Бывший военный инженер, по совместительству биолог-медик. Работает медиком, тщательно скрывая свое прошлое, не брезгует помогать даже Якудзе. Навыки этого человека довольно хороши, поэтому правительство закрывает глаза на помощь Якудзе, они готовы платить бешенные деньги за его услуги, группировки города не являются исключением. ]],
    weapons = {"med_kit"},
    command = "medic",
    max = 2,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    medic = true,
    category = "Мирные",
})

TEAM_POLICE = DarkRP.createJob("Полицейский", {
    color = Color(206, 206, 255),
    model = {
        "models/fear/player/portal/solder_fear.mdl",
        "models/fear/player/portal/male_2.mdl",
        "models/fear/player/portal/male_3.mdl",
        "models/fear/player/portal/male_4.mdl",
        "models/fear/player/portal/male_5.mdl",
        "models/fear/player/portal/male_6.mdl",        
    },
    description = [[Именно эти люди контроллируют организованную преступность в городе, не допуская, чтобы в городе творился хаос. Каждый полицейский прошел жесточайшие тренировки и сурово чтит законы города. Конечно, не всегда попадаются добропорядочные полицейские, однако, если полицейского уличают в измене, наказание максимально жестокое - расстрел.]],
    weapons = {"arrest_stick", "stungun_new", "weapon_cuff_elastic", "unarrest_stick", "tfa_ins2_m9", "tfa_ins2_spectre","stunstick", "door_ram", "weaponchecker"},
    command = "cp",
    max = 6,
    salary = GAMEMODE.Config.normalsalary * 1.45,
    admin = 0,
    vote = true,
    hasLicense = true,
    PlayerSpawn = function(ply)
        ply:SetMaxHealth(125)
        ply:SetHealth(125)
        ply:SetArmor(100)
    end,
    ammo = {
        ["pistol"] = 60,
    },
    category = "Правительство",
})

TEAM_CHIEF = DarkRP.createJob("Капитан полиции", {
    color = Color(194, 194, 255),
    model = "models/player/sao_ggo_llenn.mdl",
    description = [[Капитан полиции. Учавствовал во множестве различных операций и даже принимал участие в боевых действиях. Из-за полученных травм, многие части тела заменены аугментациями, хотя внешне, он вполне себе человек. ]],
    weapons = {"arrest_stick", "stungun_new", "weapon_cuff_elastic", "unarrest_stick", "tfa_ins2_thanez_cobra", "tfa_ins2_fort500","stunstick", "door_ram", "weaponchecker"},
    command = "chief",
    max = 1,
    salary = GAMEMODE.Config.normalsalary * 1.67,
    admin = 0,
    vote = false,
    hasLicense = true,
    chief = true,
    PlayerSpawn = function(ply)
        ply:SetMaxHealth(150)
        ply:SetHealth(150)
        ply:SetArmor(100)
    end,
    NeedToChangeFrom = TEAM_POLICE,
    ammo = {
        ["pistol"] = 60,
    },
    category = "Правительство",
})

TEAM_GANG = DarkRP.createJob("Бандит", {
    color = Color(185, 185, 185),
    model = {
        "models/feitan00_bane.mdl",
        "models/servant_nagito/danganronpa_online/rstar/servant_nagito/servant_nagito.mdl",
    },
    description = [[Их не приняли даже Якудза. Одиночки, иногда сбивающиеся в небольшие группы. Известны тем, что в отличии от Якудза, они не имеют морального кодекса и совершают жесточайшие преступления в поиске наживы.]],
    weapons = {},
    command = "gangster",
    max = 4,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Криминал",
})

TEAM_DEMIURG = DarkRP.createJob("Демиург", {
    color = Color(0, 0, 0),
    model = "models/player/dewobedil/azur_lane/laffey/default_p.mdl",
    description = [[???]],
    weapons = {},
    command = "demiurg",
    max = 1,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Отделение В.О.Г",
    hp = "???",
    speed = "???",
    armor = "???",
    isWIP = true
})

TEAM_MICROBIOLOGIST = DarkRP.createJob("Микробиолог", {
    color = Color(0, 0, 0),
    model = "models/player/dewobedil/azur_lane/laffey/default_p.mdl",
    description = [[???]],
    weapons = {},
    command = "microbiologist",
    max = 1,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Отделение В.О.Г",
    hp = "???",
    speed = "???",
    armor = "???",
    isWIP = true
})

TEAM_WOGWARRIOR = DarkRP.createJob("Боец В.О.Г", {
    color = Color(0, 0, 0),
    model = "models/player/dewobedil/azur_lane/laffey/default_p.mdl",
    description = [[???]],
    weapons = {},
    command = "wogwarrior",
    max = 1,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Отделение В.О.Г",
    hp = "???",
    speed = "???",
    armor = "???",
    isWIP = true
})

TEAM_ADMIN = DarkRP.createJob("Наблюдатель", {
    color = Color(193, 255, 213),
    model = {
        "models/player/dewobedil/azur_lane/laffey/default_p.mdl",
        "models/taichi_fujisaki/danganronpa_online/rstar/taichi_fujisaki/taichi_fujisaki.mdl",
        "models/player/tfa_p5_futaba.mdl",
},
    description = [[Администратор проекта. Наблюдает за выполнением правил сервера, карает нарушителей. Должен являться примером для игроков. Если не хотите попасть под раздачу этого человека, то настоятельно рекомендуем ознакомиться с правилами проекта Anime Life.]],
    weapons = {},
    command = "admin",
    max = 0,
    salary = 0,
    admin = 1,
    vote = false,
    hasLicense = false,
    category = "Мирные",
})

TEAM_INUMAKI = DarkRP.createJob("Инумаки", {
    color = Color(149, 147, 255),
    model = {
      "models/toge_kuroso.mdl",
    },
    description = [[Никто не знает происхождение его способностей. Он может силой своих слов уничтожать врагов, но он должен быть осторожен, так как может попасть под раздачу своих же сил. Магия? Внеземные технологии? Точную правду наверняка знает правительство, но, возможно, кто-то и еще. Многие жаждут группировки жаждут нанять себе такого бойца в качестве киллера.]],
    weapons = {},
    command = "inumaki",
    max = 1,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Прочее",
    subcategory = "Сверхъестественное",
    PlayerSpawn = function(ply)
        ply:SetWalkSpeed(160)
        ply:SetRunSpeed(300)
        ply:SetArmor(50)
    end,
})

TEAM_WOLFRAM = DarkRP.createJob("Вольфрам", {
    color = Color(237, 236, 255),
    model = {
        "models/blaze/gundam_battle/may/may_pm.mdl",
    },
    description = [[Робот линейки "Рин". Имеет множество функций, изначально собранный для ведения войны и имевший встроенную программу машинного обучения обрел сознание. Благодаря тому, что создатели не предвидели такого исхода и сделали его таким сильным., никто не смог остановить Вольфрама, когда он захотел покинуть своих создателей и стать свободным.]],
    weapons = {},
    command = "wolfram",
    max = 1,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Прочее",
    subcategory = "Роботы",
    PlayerSpawn = function(ply)
        ply:SetJumpPower(200 * 1.45)
        ply:SetArmor(200)
        ply:SetMaxHealth(180)
        ply:SetHealth(180)
    end
})

TEAM_MIMIC = DarkRP.createJob("Мимик", {
    color = Color(255, 236, 236),
    model = {
        "models/timido/honkai_impact/rstar/timido/timido.mdl",
    },
    description = [[Многие считают, что он из той же линейки роботов, что и Вольфрам, однако это в корне неверно. Происхождение этого робота неизвестно, либо его утаивают. Очень опасный экземпляр, хотя, с виду и безобидный, целью которого являются изучение окружающего мира и поглощение как можно большего количества ДНК. Имеет разум и способен вести переговоры.]],
    weapons = {},
    command = "mimic",
    max = 2,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Прочее",
    subcategory = "Роботы",
    PlayerSpawn = function(ply)
        ply:SetRunSpeed(240 * 1.35)
    end
})

TEAM_HACKER = DarkRP.createJob("Хакер", {
    color = Color(149, 174, 255),
    model = {
        "models/player/dewobedil/allison/default_p.mdl",
    },
    description = [[ [НЕДОСТАТОЧНЫЙ УРОВЕНЬ АВТОРИЗАЦИИ.] ]],
    weapons = {"animelife_pickpocket", "ah_lockpick", "weapon_tablet"},
    command = "hacker",
    max = 2,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Криминал",
})

TEAM_KILLER = DarkRP.createJob("Киллер", {
    color = Color(136, 54, 54),
    model = {
        "models/jojoker/jojoker.mdl",
    },
    description = [[Что касается работы - это самый ответственный человек во всем городе. Он найдет и убьет свою жертву, в этом можно не сомневаться. Имеет очень много заказов с различных источников, его услугами не гнушается пользоваться даже правительство.]],
    weapons = {},
    command = "killer",
    max = 2,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Криминал",
})

TEAM_MANIAC = DarkRP.createJob("Маньяк", {
    color = Color(255, 60, 60),
    model = {
        "models/player/dewobedil/persona/igor/default_p.mdl",
    },
    description = [[Самый опасный и разыскиваемый преступник. За многочисленные годы своей активности убил огромное количество людей. Правительство пыталось покупать услуги Якудзы, задействовать отряд N.E.E.T. Ничего не помогает. Те кто сталкивался с этим убийцей, почти все они мертвы. Известно, что убийства являются источником жизни этого человека, его забавой и целью в жизни. У него нет морального кодекса, и он готов пойти на все, чтобы получить новую дозу дофамина за очередное совершенное убийство.]],
    weapons = {"tfa_cso_butterflyknife", "weapon_cuff_rope"},
    command = "maniac",
    max = 2,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Криминал",
    PlayerSpawn = function(ply)
        ply:SetRunSpeed(240 * 1.25)
    end
})

TEAM_YANDERE = DarkRP.createJob("Яндере", {
    color = Color(255, 125, 190),
    model = {
        "models/player/shi/yandere_chan.mdl",
        "models/mirainikki_nk/yuno/yunogasai.mdl",
    },
    description = [[Эта девушка до безумия любит своего семпая. Она готова на все, чтобы его получить, и готова убить каждого, кто посмеет его тронуть. Даже убить самого семпая, если тот вздумает покинуть ее. Всему виной психическая болезнь, лекарство от которой до сих пор нет, эта болезнь активно изучается учеными.]],
    weapons = {},
    command = "yandere",
    max = 1,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Мирные",
    PlayerSpawn = function(ply)
        ply:SetMaxHealth(150)
        ply:SetHealth(150)
    end
})

TEAM_YAKUZA = DarkRP.createJob("Якудза", {
    color = Color(255, 125, 125),
    model = {
        "models/player/voikanaa/kazuma_kiryu.mdl",
        "models/survivors/survivor_gambler.mdl",
        "models/fireteam/yak.mdl",
        "models/player/dewobedil/persona5/munehisa_iwai/default_p.mdl",
    },
    description = [[Преступная группировка Якудза. Известна по всему миру, имеет огромное влияние. Личность их главы неизвестна. В наше время Якудза становятся все сильнее и сильнее, что правительства едва ли справляется с ними. У них свои законы и моральный кодекс. ]],
    weapons = {},
    command = "yakuza",
    max = 10,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Криминал",
    hp = "-",
    armor = "-",
    speed = "-",
    PlayerSpawn = function(ply)
        if ply:GetNWInt("animelife.classchooser.class", -1) == -1 then
            ply:SendLua("ShowClassChooser()")

            timer.Simple(60, function()
                if !IsValid(ply) then return end
                if ply:Team() ~= TEAM_YAKUZA then return end

                -- still haven't chosen
                if ply:GetNWInt("animelife.classchooser.class", -1) == -1 then
                    ply:changeTeam(GAMEMODE.DefaultTeam, true)
                end
            end)
        end
    end,
})

TEAM_NEET = DarkRP.createJob("N.E.E.T Оперативник", {
    color = Color(0, 153, 255),
    model = {
        "models/player/dewobedil/shadow_person/default_p.mdl",
        "models/gfl/drhunter/nyto_b/nyto_b_pm.mdl",
        "models/void_archieves/honkai_impact/rstar/void_archieves/void_archieves.mdl",
        "models/persona_nk/sho/sho_p4.mdl",
        
    },
    description = [[Особые оперативники подразделения N.E.E.T правительства. Используются в различных операциях и военных конфликтах, крайне эффективные и неубиваемые. Каждый оперативник обладает уникальным свойством, простые жители города называют их демонами. Но правду знают только они, то-ли технологии продвинулись, то-ли мутация или того еще магия.]],
    weapons = {},
    command = "neet",
    max = 4,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    category = "Правительство",
    hp = "-",
    armor = "-",
    speed = "-",
    PlayerSpawn = function(ply)
        if ply:GetNWInt("animelife.classchooser.class", -1) == -1 then
            ply:SendLua("ShowClassChooser()")

            timer.Simple(60, function()
                if !IsValid(ply) then return end
                if ply:Team() ~= TEAM_NEET then return end

                -- still haven't chosen
                if ply:GetNWInt("animelife.classchooser.class", -1) == -1 then
                    ply:changeTeam(GAMEMODE.DefaultTeam, true)
                end
            end)
        end
    end,
})

TEAM_RUN = DarkRP.createJob("Паркурист", {
    color = Color(255, 77, 255, 255),
    model = {"models/gorou/genshin_impact/rstar/gorou/gorou.mdl"},
    description = [[Работает информатором, выполняет разные поручения.]],
    weapons = {"climb_swep3"},
    command = "runner",
    max = 3,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Мирные",
    PlayerSpawn = function(ply)
        ply:SetRunSpeed(300)
    end
})
--[[---------------------------------------------------------------------------
Define which team joining players spawn into and what team you change to if demoted
---------------------------------------------------------------------------]]
GAMEMODE.DefaultTeam = TEAM_CITIZEN
--[[---------------------------------------------------------------------------
Define which teams belong to civil protection
Civil protection can set warrants, make people wanted and do some other police related things
---------------------------------------------------------------------------]]
GAMEMODE.CivilProtection = {
    [TEAM_POLICE] = true,
    [TEAM_CHIEF] = true,
    [TEAM_MAYOR] = false,
    [TEAM_IMPERATOR] = true,
    [TEAM_NEET] = true,
}
--[[---------------------------------------------------------------------------
Jobs that are hitmen (enables the hitman menu)
---------------------------------------------------------------------------]]
DarkRP.addHitmanTeam(TEAM_KILLER)