SWEP.PrintName = "Hentai Printer Tablet"
SWEP.Author = "animelife"

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.Weight = 5
SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false

SWEP.Slot = 1
SWEP.SlotPos = 2
SWEP.DrawAmmo = false
SWEP.DrawCrosshair = true

SWEP.UseHands = true

SWEP.ViewModel			= "models/weapons/Steam_Deck_anims.mdl"
SWEP.WorldModel			= "models/weapons/Steam_Deck.mdl"

SWEP.ShootSound = Sound("Metal.SawbladeStick")

function SWEP:Initialize()
    self:SetHoldType("slam")
end

function SWEP:AttackFunction()
	local anim = "1"

    local vm = self.Owner:GetViewModel()
    if IsValid(vm) then
        vm:SendViewModelMatchingSequence(vm:LookupSequence(anim))
    end

    if SERVER then -- ?
        net.Start("animelife.hentaitablet.animate")
        net.Send(self.Owner)
    end

    -- print(vm:LookupSequence(anim))
end

function SWEP:PrimaryAttack()
    -- they should be literally the same
    self:AttackFunction()
end

function SWEP:Holster()
    local vm = self.Owner:GetViewModel()
    if IsValid(vm) then
        vm:SendViewModelMatchingSequence(vm:LookupSequence("holster"))
    end

    return true
end

function SWEP:SecondaryAttack()
    self:AttackFunction()
end