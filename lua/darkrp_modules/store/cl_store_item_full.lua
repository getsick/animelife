local PANEL = {}

AccessorFunc(PANEL, "storeitem", "StoreItem", FORCE_STRING)
AccessorFunc(PANEL, "storecategory", "StoreCategory", FORCE_NUMBER)

local detail_header = Material("animelife/globalstore/full/detail_header.png")
local icon_purchase = Material("animelife/globalstore/full/icon_purchase.png")
local icon_close = Material("animelife/globalstore/full/icon_close.png")

local icon_xp = Material("animelife/globalstore/icon_point.png")
local icon_yen = Material("animelife/globalstore/icon_yen.png")
local icon_donate = Material("animelife/globalstore/icon_donate.png")

surface.CreateFont("animelife.globalstore.full.category", {font = "Exo 2 Bold", size = 14, weight = 700, extended = true})
surface.CreateFont("animelife.globalstore.full.name", {font = "Exo 2 SemiBold", size = 22, weight = 600, extended = true})
surface.CreateFont("animelife.globalstore.full.subtitle", {font = "Exo 2 SemiBold", size = 17, weight = 600, extended = true})
surface.CreateFont("animelife.globalstore.full.description", {font = "Exo 2 SemiBold", size = 20, weight = 600, extended = true})
surface.CreateFont("animelife.globalstore.full.price", {font = "Exo 2 SemiBold", size = 21, weight = 600, extended = true})

function PANEL:Init()
    self:SetStoreItem("none")
    self:SetStoreCategory(1)

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("")
    self.CloseButton.Paint = function(pnl, w, h)
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(icon_close)
        surface.DrawTexturedRect(0, 0, w, h)
    end
    self.CloseButton.DoClick = function()
        self:Remove()
        self = nil
    end

    self.PurchaseButton = vgui.Create("DButton", self)
    self.PurchaseButton:SetText("")
    self.PurchaseButton.Paint = function(pnl, w, h)
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(icon_purchase)
        surface.DrawTexturedRect(0, 0, w, h)
    end
    self.PurchaseButton.DoClick = function()
        if self:GetStoreItem() == "none" then return end
        local dmenu = DermaMenu(nil, self)

        local item = InventoryItems[self:GetStoreItem()]
        if item.Price.Yen ~= -1 then
            dmenu:AddOption("Купить за иены (" .. string.Comma(item.Price.Yen) .. ")", function()
                net.Start("animelife.globalstore.buythings")
                    net.WriteInt(1, 3)
                    net.WriteString(self:GetStoreItem())
                net.SendToServer()
            end)
        end
        if item.Price.XP ~= -1 then
            dmenu:AddOption("Купить за очки опыта (" .. string.Comma(item.Price.XP) .. ")", function()
                net.Start("animelife.globalstore.buythings")
                    net.WriteInt(2, 3)
                    net.WriteString(self:GetStoreItem())
                net.SendToServer()
            end)
        end
        if item.Price.Donate ~= -1 then
            dmenu:AddOption("Купить за донат-валюту (" .. string.Comma(item.Price.Donate) .. ")", function()
                net.Start("animelife.globalstore.buythings")
                    net.WriteInt(3, 3)
                    net.WriteString(self:GetStoreItem())
                net.SendToServer()
            end)
        end

        dmenu:Open()
    end

    self.Preview = vgui.Create("DModelPanel", self)
    self.Preview:SetModel("models/error.mdl")
    self.Preview:SetFOV(45)
    self.Preview.LayoutEntity = function() end
    self.Preview.OnCursorEntered = function()
        if InventoryItems[self:GetStoreItem()].Items then
            self:CreatePreview(InventoryItems[self:GetStoreItem()].Items)
        end
    end
    self.Preview.OnCursorExited = function()
        if InventoryItems[self:GetStoreItem()].Items then
            self:RemovePreview()
        end
    end
end

function PANEL:CreatePreview(items)
    if !ValidPanel(self.ItemPreview) then
        self.ItemPreview = vgui.Create("DPanel", self)
        self.ItemPreview:SetAlpha(0)
        self.ItemPreview:AlphaTo(255, 0.5)
        self.ItemPreview.Paint = function(pnl, w, h)
            surface.SetDrawColor(0, 0, 0)
            surface.DrawRect(0, 0, w, h)

            draw.SimpleText("Предметы в коробке", "animelife.globalstore.full.name", w / 2, 12, Color(255, 255, 255), 1)
        end

        self.ItemPreviewLayout = vgui.Create("DIconLayout", self.ItemPreview)
        self.ItemPreviewLayout:SetSpaceX(8)
        self.ItemPreviewLayout:SetSpaceY(8)
        self.ItemPreviewLayout.Items = {}

        for _, item in pairs(items or {}) do
            local pnl = vgui.Create("DPanel", self.ItemPreviewLayout)
            pnl.Paint = function(slf, w, h)
                surface.SetDrawColor(31, 31, 31)
                surface.DrawRect(0, 0, w, h)

                draw.SimpleText(InventoryItems[item[1]].Name, "animelife.globalstore.full.subtitle", 84, h / 2, Color(255, 255, 255), nil, 1)
            end

            local icon = vgui.Create("SpawnIcon", pnl)
            icon:SetModel(InventoryItems[item[1]].Model)

            table.insert(self.ItemPreviewLayout.Items, pnl)
        end
    end
end

function PANEL:RemovePreview()
    if ValidPanel(self.ItemPreview) then
        self.ItemPreview:Remove()
        self.ItemPreview = nil
    end
end

function PANEL:PerformLayout(w, h)
    self.CloseButton:SetPos(w - ((w - 951) / 2) - 33 - 27, 28)
    self.CloseButton:SetSize(27, 27)

    self.PurchaseButton:SetPos(w - ((w - 951) / 2) - 46 - 48, 219)
    self.PurchaseButton:SetSize(48, 48)

    local mn, mx = self.Preview.Entity:GetRenderBounds()
    local size = 0
    size = math.max(size, math.abs(mn.x) + math.abs(mx.x))
    size = math.max(size, math.abs(mn.y) + math.abs(mx.y))
    size = math.max(size, math.abs(mn.z) + math.abs(mx.z))

    self.Preview:SetCamPos(Vector(size, size, size))
    self.Preview:SetLookAt((mn + mx) * 0.5)

    local item = self:GetStoreItem()
    if item ~= "none" then
        self.Preview:SetModel(InventoryItems[self:GetStoreItem()].Model)
    end

    self.Preview:SetPos((w - 139) / 2, 30)
    self.Preview:SetSize(139, 139)

    if ValidPanel(self.ItemPreview) then
        local cx, cy = self:CursorPos()
        self.ItemPreview:SetSize(w, 350)
        self.ItemPreview:SetPos(0, cy + 64)

        self.ItemPreviewLayout:SetPos(0, 48)
        self.ItemPreviewLayout:SetSize(w, 350 - 48)

        for _, icon in pairs(self.ItemPreviewLayout.Items) do
            icon:SetSize(225, 64)
        end
    end
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(0, 0, 0, 225)
    surface.DrawRect(0, 0, w, h)

    surface.SetDrawColor(48, 42, 42)
    surface.DrawRect((w - 951) / 2, 0, 951, h)

    local cat = self:GetStoreCategory()
    local categories = {[1] = "Оружие", [2] = "Скины", [3] = "Аксессуары", [4] = "Донат-предметы"}
    local base_x = ((w - 951) / 2) + 71
    draw.WordBox(8, base_x, 216, categories[cat], "animelife.globalstore.full.category", Color(255, 150, 73), Color(255, 255, 255))

    local item = self:GetStoreItem()
    local item_name = item ~= "none" and InventoryItems[item].Name or "!!! Item not set !!!"
    draw.SimpleText(item_name, "animelife.globalstore.full.name", base_x, 253, Color(255, 255, 255))

    surface.SetDrawColor(0, 0, 0, 75)
    surface.DrawRect((w - 951) / 2, 285, 951, 57)
    
    local pricing_x = base_x + 25
    if InventoryItems[item].Price.Yen ~= -1 then
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(icon_yen)
        surface.DrawTexturedRect(pricing_x - 24, 307, 17, 17)

        draw.SimpleText(string.Comma(InventoryItems[item].Price.Yen), "animelife.globalstore.full.price", pricing_x, 304, Color(255, 255, 255))
        pricing_x = pricing_x + 175
    end

    if InventoryItems[item].Price.XP ~= -1 then
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(icon_xp)
        surface.DrawTexturedRect(pricing_x - 24, 309, 15, 15)

        draw.SimpleText(string.Comma(InventoryItems[item].Price.XP), "animelife.globalstore.full.price", pricing_x, 304, Color(255, 255, 255))
        pricing_x = pricing_x + 175
    end

    if InventoryItems[item].Price.Donate ~= -1 then
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(icon_donate)
        surface.DrawTexturedRect(pricing_x - 24, 306, 19, 19)

        draw.SimpleText(string.Comma(InventoryItems[item].Price.Donate), "animelife.globalstore.full.price", pricing_x, 304, Color(255, 255, 255))
        pricing_x = pricing_x + 175
    end

    surface.SetDrawColor(255, 255, 255)
    surface.SetMaterial(detail_header)
    surface.DrawTexturedRect((w - 951) / 2, 0, 966, 253)

    draw.SimpleText("Описание", "animelife.globalstore.full.subtitle", base_x, 351, Color(209, 209, 209))

    local item_desc = item ~= "none" and InventoryItems[item].Description or "Couldn't retrieve item description."
    local desc_markup = markup.Parse("<font=animelife.globalstore.full.description>" .. item_desc .. "</font>", 812)
    desc_markup:Draw(base_x, 378)
end

function PANEL:Think()
    if input.IsKeyDown(KEY_ESCAPE) then
        self:Remove()
        self = nil
    end
end

vgui.Register("animelife.globalstore.full", PANEL, "EditablePanel")

function GlobalStoreFullPanel(item, cat)
    local full = vgui.Create("animelife.globalstore.full")
    full:SetSize(ScrW(), ScrH())
    full:MakePopup()
    full:SetAlpha(0)
    full:AlphaTo(255, 0.25)
    full:SetStoreItem(item)
    full:SetStoreCategory(cat)
end