module("globalstore", package.seeall)

-- 4: DonateShop
timer.Simple(0.0, function()
    StoreType[4] = {
        "donate_tfa_volk", "donate_memberplus", "donate_sponsor", "donate_tfa_doom_ssg", "donate_tfa_ak117geo", "donate_tfa_ins2_codol_free", "donate_tfa_hailstorm", "donate_tfa_bo3_m8a7", "donate_tfa_cso_skull2", "donate_tfa_cso_tomahawk", "donate_tfa_cso_ruyi", "donate_tfa_cso_dreadnova", "donate_tfa_pack1", "donate_tfa_pack_melee", "donate_tfa_destiny_devils_ruin", "donate_tfa_destiny_duality", "donate_tfa_destiny_trinity_ghoul", "donate_tfa_destiny_vex_mythoclast2", "donate_tfa_destiny_whisper", "donate_tfa_pack2"
    }
end)