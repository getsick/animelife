module("globalstore", package.seeall)

-- 1: Weapons
timer.Simple(0.0, function()
    StoreType[1] = {
        "weapon_qsz", "weapon_lockpick", "weapon_glock19", "weapon_m9", "weapon_knife", "weapon_cobra", "weapon_nova", "weapon_spas", "weapon_m500", "weapon_fort", "weapon_ak12", "weapon_ak400", "weapon_famas", "weapon_lynx", "weapon_mk18", "weapon_akm", "weapon_kriss", "weapon_mp5k", "weapon_mp7", "weapon_spectre", "weapon_remington", "weapon_sks", "weapon_msr", "weapon_magnum", "weapon_vape", "weapon_vape1", "weapon_vape2"
    }
end)