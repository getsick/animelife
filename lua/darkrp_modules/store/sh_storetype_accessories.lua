module("globalstore", package.seeall)

-- 3: Accessories
timer.Simple(0.0, function()
    StoreType[3] = {
        "accessories_hatllenn", "accessories_blackwidow", "accessories_raven", "accessories_vampire", "accessories_samuraiultra", "accessories_moth", "accessories_darkviking", "accessories_ghoul", "accessories_katana", "accessories_disco", "accessories_samuraiblue", "accessories_guitar", "accessories_garageband", "accessories_afro", "accessories_nojiggle", "accessories_neko", "accessories_headphones", "accessories_strawhat", "accessories_bunnyears", "accessories_sunhat", "accessories_ducktube", "accessories_crown", "accessories_glasses04", "accessories_glasses02", "accessories_3dglasses", "accessories_snowboard", "accessories_starglasses"
    }
end)