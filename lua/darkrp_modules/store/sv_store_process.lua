util.AddNetworkString("animelife.globalstore.buythings")

module("globalstore", package.seeall)

function Purchase(ply, item, days)
    local empty_slot, amount = inventory:FindEmptySlot(ply, item)
    -- couldn't find one
    if empty_slot == false then
        DarkRP.notify(ply, 1, 5, "Инвентарь заполнен.") 
        return false
    end

    inventory:AddItem(ply, empty_slot, item, isnumber(amount) and amount + 1 or 1, days, true)

    return true
end

net.Receive("animelife.globalstore.buythings", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local buys_for = net.ReadInt(3)
    local item = net.ReadString()
    if !isstring(item) or item == "none" then return end
    if !InventoryItems[item] then return end

    local item_data = InventoryItems[item]
    -- buys for x yens
    if buys_for == 1 then
        if item_data.Price.Yen ~= -1 then
            if ply:canAfford(item_data.Price.Yen) then
                local success = Purchase(ply, item, item_data.Duration)
                if success then
                    ply:addMoney(-item_data.Price.Yen)

                    DarkRP.notify(ply, 0, 5, "Вы купили " .. item_data.Name .. "!")
                    DarkRP.notify(ply, 0, 5, "Предмет доставлен в инвентарь (I)")
                end
            else
                DarkRP.notify(ply, 1, 5, "Сожалею, не по карману.")
            end
        end
    elseif buys_for == 2 then -- buys for x xp points
        if item_data.Price.XP ~= -1 then
            if ply:GetTreeXP() >= item_data.Price.XP then
                local success = Purchase(ply, item, item_data.Duration)
                if success then
                    ply:AddTreeXP(-item_data.Price.XP)

                    DarkRP.notify(ply, 0, 5, "Вы купили " .. item_data.Name .. "!")
                    DarkRP.notify(ply, 0, 5, "Предмет доставлен в инвентарь (I)")
                end
            else
                DarkRP.notify(ply, 1, 5, "Сожалею, не по карману.")
            end
        end
    elseif buys_for == 3 then -- buys for x donate points
        if item_data.Price.Donate ~= -1 then
            if ply:GetDonationPoints() >= item_data.Price.Donate then
                local success = Purchase(ply, item, item_data.Duration)
                if success then
                    ply:AddIGSFunds(-item_data.Price.Donate, nil, function(new_bal)
                        ply:SetDonationPoints(new_bal)
                    end)

                    DarkRP.notify(ply, 0, 5, "Вы купили " .. item_data.Name .. "!")
                    DarkRP.notify(ply, 0, 5, "Предмет доставлен в инвентарь (I)")
                end
            else
                DarkRP.notify(ply, 1, 5, "Сожалею, не по карману.")
            end
        end
    end
end)