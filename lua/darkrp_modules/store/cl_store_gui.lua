local PANEL = {}

local detail_header = Material("animelife/globalstore/detail_header.png")
local wallet_holder = Material("animelife/globalstore/wallet_holder.png")
local icon_close = Material("animelife/globalstore/full/icon_close.png")

surface.CreateFont("animelife.globalstore.wallet", {font = "Exo 2 SemiBold", size = 17, weight = 600, extended = true})
surface.CreateFont("animelife.globalstore.button", {font = "Exo 2 SemiBold", size = 16, weight = 600, extended = true})
surface.CreateFont("animelife.globalstore.button.funds", {font = "Exo 2 SemiBold", size = 20, weight = 600, extended = true})

function PANEL:Init()
    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("")
    self.CloseButton.Paint = function(pnl, w, h)
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(icon_close)
        surface.DrawTexturedRect(0, 0, w, h)
    end
    self.CloseButton.DoClick = function()
        self:Remove()
        self = nil
    end

    self.DepositFundsButton = vgui.Create("DButton", self)
    self.DepositFundsButton:SetText("")
    self.DepositFundsButton.Paint = function(pnl, w, h)
        draw.SimpleText("Пополнить баланс донат-валюты", "animelife.globalstore.button.funds", w / 2, h / 2, Color(145, 145, 145), 1, 1)
    end
    self.DepositFundsButton.DoClick = function()
        if isfunction(IGS.WIN.Deposit) then
            IGS.WIN.Deposit(1)
        end
    end

    self.NavBarButtons = {}
    self.CurrentStore = 1

    for i, v in pairs({[1] = "Оружие", [2] = "Скины", [3] = "Аксессуары", [4] = "Донат-предметы"}) do
        local btn = vgui.Create("DButton", self)
        btn:SetText("")
        btn.Paint = function(pnl, w, h)
            draw.SimpleText(v, "animelife.globalstore.button", w / 2, h / 2, self.CurrentStore == i and Color(255, 255, 255) or Color(219, 219, 219), 1, 1)
        end
        btn.DoClick = function()
            self.CurrentStore = i
            self:GenerateItems(i)
        end

        table.insert(self.NavBarButtons, btn)
    end

    self.Scroll = vgui.Create("DScrollPanel", self)
    self.Scroll.Items = {}
    self:PaintScrollbar()
    -- self.Layout = vgui.Create("DIconLayout", self.Scroll)
    -- self.Layout:SetBorder(32)
    -- self.Layout:SetSpaceX(48)
    -- self.Layout:SetSpaceY(24)

    -- self.Layout.Items = {}
    self:GenerateItems(self.CurrentStore)
end

function PANEL:PerformLayout(w, h)
    self.CloseButton:SetPos(w - ((w - 951) / 2) - 33 - 27, 64)
    self.CloseButton:SetSize(27, 27)

    self.DepositFundsButton:SetPos(w - ((w - 951) / 2) - 225 - 32, 410)
    self.DepositFundsButton:SetSize(225, 24)

    -- position buttons
    local bx = (w - 951) / 2
    for _, btn in pairs(self.NavBarButtons) do
        btn:SetSize(166, 49)
        btn:SetPos(bx, 445)

        bx = bx + 166
    end

    self.Scroll:SetPos((w - 951) / 2, 445 + 49)
    self.Scroll:SetSize(951, h - (445 + 49))

    -- self.Layout:Dock(FILL)

    local x, y = 72, 32
    for i, pnl in pairs(self.Scroll:GetCanvas():GetChildren()) do
        pnl:SetSize(250, 188)
        pnl:SetPos(x, y)

        if i % 3 == 0 then
            x = 72
            y = y + 188 + 24
        else
            x = x + 250 + 24
        end
    end

    -- self.Layout:Layout()
end

function PANEL:PaintScrollbar()
    local vertical_bar = self.Scroll:GetVBar()
    vertical_bar:SetWide(6)

    vertical_bar.Paint = function() end
    vertical_bar.btnGrip.Paint = function(pnl, w, h)
        draw.RoundedBox(6, 0, 0, w, h, Color(0, 0, 0, 150))
    end
    vertical_bar.btnUp.Paint = function() end
    vertical_bar.btnDown.Paint = function() end
end

function PANEL:GenerateItems(store_id)
    -- remove old ones
    -- for _, pnl in pairs(self.Scroll.Items) do
    --     if ValidPanel(pnl) then
    --         pnl:Remove()
    --     end
    -- end
    -- self.Scroll.Items = {}
    self.Scroll:Clear()

    -- create new ones
    for _, item in pairs(globalstore.StoreType[store_id] or {}) do
        local pnl = vgui.Create("animelife.globalstore.mini", self.Scroll)
        pnl:SetSize(250, 188)
        pnl:SetStoreItem(item)
        pnl:SetStoreCategory(store_id)

        self.Scroll:AddItem(pnl)
        -- table.insert(self.Layout.Items, pnl)
    end
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(48, 42, 42)
    surface.DrawRect((w - 951) / 2, 0, 951, h)

    surface.SetDrawColor(255, 255, 255)
    surface.SetMaterial(detail_header)
    surface.DrawTexturedRect((w - 1295) / 2 + 40.5, 0, 1295, 399)

    surface.SetMaterial(wallet_holder)
    surface.DrawTexturedRect((w - 259) / 2, 330, 259, 102)

    local xp = LocalPlayer():GetTreeXP() or 0
    local money = LocalPlayer():getDarkRPVar("money") or 0
    local donation_pts = LocalPlayer():GetDonationPoints() or 0
    local wallet = string.format("Опыт: %s\nДонат-валюта: %s\nБаланс: ¥%s", string.Comma(xp), string.Comma(donation_pts), string.Comma(money))

    draw.DrawText(wallet, "animelife.globalstore.wallet", w / 2, 348, Color(48, 42, 42), TEXT_ALIGN_CENTER)

    surface.SetDrawColor(0, 0, 0, 75)
    surface.DrawRect((w - 955) / 2, 445, 955, 49)
end

function PANEL:Think()
    if input.IsKeyDown(KEY_ESCAPE) then
        self:Remove()
        self = nil
    end
end

vgui.Register("animelife.globalstore.gui", PANEL, "EditablePanel")

concommand.Add("al_globalstore", function()
    GLOBALS_GLOBALSTORE = vgui.Create("animelife.globalstore.gui")
    GLOBALS_GLOBALSTORE:SetSize(ScrW(), ScrH())
    GLOBALS_GLOBALSTORE:MakePopup()
    GLOBALS_GLOBALSTORE:SetAlpha(0)
    GLOBALS_GLOBALSTORE:AlphaTo(255, 0.25)
end)