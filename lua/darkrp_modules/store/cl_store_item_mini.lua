local PANEL = {}

AccessorFunc(PANEL, "storeitem", "StoreItem", FORCE_STRING)
AccessorFunc(PANEL, "storecategory", "StoreCategory", FORCE_NUMBER)

local icon_purchase = Material("animelife/globalstore/icon_purchase.png")
local icon_xp = Material("animelife/globalstore/icon_point.png")
local icon_yen = Material("animelife/globalstore/icon_yen.png")
local icon_donate = Material("animelife/globalstore/icon_donate.png")

surface.CreateFont("animelife.globalstore.mini.name", {font = "Exo 2 SemiBold", size = 19, weight = 600, extended = true})
surface.CreateFont("animelife.globalstore.mini.price", {font = "Exo 2 SemiBold", size = 21, weight = 600, extended = true})

function PANEL:Init()
    self:SetStoreItem("none")
    self:SetStoreCategory(1)

    self.InfoButton = vgui.Create("DButton", self)
    self.InfoButton:SetText("")
    self.InfoButton.Paint = function(pnl, w, h)
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(icon_purchase)
        surface.DrawTexturedRect(0, 0, w, h)
    end
    self.InfoButton.DoClick = function()
        GlobalStoreFullPanel(self:GetStoreItem(), self:GetStoreCategory())
    end
end

function PANEL:PerformLayout(w, h)
    self.InfoButton:SetPos((w - 32) / 2, h - 32 - 14)
    self.InfoButton:SetSize(32, 32)
end

function PANEL:Paint(w, h)
    draw.RoundedBox(16, 0, 0, w, h, Color(0, 0, 0, 50))

    surface.SetDrawColor(0, 0, 0, 95)
    surface.DrawRect(0, 51, w, 79)

    local item = self:GetStoreItem()
    local item_name = item ~= "none" and InventoryItems[item].Name or "!!! Item not set !!!"
    local item_price = item ~= "none" and InventoryItems[item].Price or {Yen = 0, XP = 0, Donate = 0}

    draw.SimpleText(item_name, "animelife.globalstore.mini.name", w / 2, 16, Color(255, 255, 255), 1)

    local pricing_x = 64
    local pricing_y = 66
    if item_price.Yen ~= -1 then
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(icon_yen)
        surface.DrawTexturedRect(pricing_x - 24, pricing_y + 3, 17, 17)

        draw.SimpleText(string.Comma(item_price.Yen), "animelife.globalstore.mini.price", pricing_x, pricing_y, Color(255, 255, 255))
        pricing_y = pricing_y + 24
    end

    if item_price.XP ~= -1 then
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(icon_xp)
        surface.DrawTexturedRect(pricing_x - 24, pricing_y + 5, 15, 15)

        draw.SimpleText(string.Comma(item_price.XP), "animelife.globalstore.mini.price", pricing_x, pricing_y, Color(255, 255, 255))
        pricing_x = pricing_x + 120
        pricing_y = 66
    end

    if item_price.Donate ~= -1 then
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(icon_donate)
        surface.DrawTexturedRect(pricing_x - 24, pricing_y + 2, 19, 19)

        draw.SimpleText(string.Comma(item_price.Donate), "animelife.globalstore.mini.price", pricing_x, pricing_y, Color(255, 255, 255))
    end

    -- draw.DrawText("Yen: " .. item_price.Yen .. "\nXP: " .. item_price.XP .. "\nDonate: " .. item_price.Donate, "animelife.globalstore.mini.name", w / 2, 66, Color(255, 255, 255), 1)
end

vgui.Register("animelife.globalstore.mini", PANEL, "DPanel")