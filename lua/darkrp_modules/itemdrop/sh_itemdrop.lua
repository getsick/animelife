module("itemdrop", package.seeall)

function itemdrop:CanDrop(pos)
    if SERVER then
        if !util.IsInWorld(pos) then
            return false
        end

        if SH_SZ and SH_SZ:IsWithinSafeZone(pos) then
            return false
        end
    end

    return true
end