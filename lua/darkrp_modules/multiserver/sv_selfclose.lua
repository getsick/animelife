-- do return end -- disable selfclose

local allowed_to_join = {
    "76561198116672024", -- hsgr, getsick, zajka
    "76561198217279453", -- DetroitM, Scar, GintokiM
    "76561197998185511", -- Newbie
    "76561199186200838", -- mayshent
    "76561198448635071", -- fstu
    "76561198152898257", -- reyseg
    "76561198278618840", -- snowke
}
hook.Add("CheckPassword", "animelife.multiserver.selfcloser", function(steam_id64)
    if !table.HasValue(allowed_to_join, steam_id64) then
        return false, "animelife временно закрыт в связи с пожарной тревогой. \nСкоро будем! \n(*_ _)人"
    end
end)