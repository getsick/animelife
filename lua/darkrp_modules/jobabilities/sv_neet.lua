local function DoBlinkAbility(ply)
    WDA_CAST_BLINK(ply)

    ply:EmitSound("dishonored/blink_begin.wav")
end

function DoDarkVisionAbility(ply)
    WDA_CAST_DARKVISION(ply)
end

local function DoFarReachAbility(ply)
    WDA_CAST_FARREACH(ply)

    ply:EmitSound("dishonored/farreach_begin.wav")
end

function DoShadowWalkAbility(ply)
    WDA_CAST_SHADOWWALK(ply)
end

hook.Add("PlayerButtonDown", "animelife.jobabilities.neet.instant", function(ply, btn)
    if ply:Team() ~= TEAM_NEET then return end
    if ply:GetNWInt("animelife.classchooser.class", -1) == 1 then
        -- if btn == KEY_F then
        --     DoShadowWalkAbility(ply)
        -- elseif btn == KEY_G then
        --     DoDarkVisionAbility(ply)
        -- elseif btn == KEY_H then
        --     DoFarReachAbility(ply)
        -- end
    elseif ply:GetNWInt("animelife.classchooser.class", -1) == 3 then
        if btn == KEY_F then
            if (ply.NextBlinkAbility or 0) < SysTime() then
                DoBlinkAbility(ply)

                ply.NextBlinkAbility = SysTime() + 1
            end
        end
    end
end)

local melee = {"tfa_cso_dreadnova", "tfa_cso_tomahawk_xmas", "tfa_cso_butterflyknife", "tfa_cso_ruyi",
"tfa_cso_jaydagger", "tfa_cso_tomahawk"}

hook.Add("EntityTakeDamage", "animelife.jobabilities.neet", function(target, dmg)
    if !IsValid(target) or !target:IsPlayer() then return end
    if target:Team() ~= TEAM_NEET then 
        local attacker = dmg:GetAttacker()
        if IsValid(attacker) and attacker:IsPlayer() then
            if attacker:Team() == TEAM_NEET and attacker:GetNWInt("animelife.classchooser.class", -1) == 4 then
                if attacker.TankHealing then
                    dmg:ScaleDamage(0.1)
                end
            end
        end

        return 
    end

    -- shiroyasha
    if target:GetNWInt("animelife.classchooser.class", -1) == 3 then
        -- local inflictor = dmg:GetInflictor()
        -- if IsValid(inflictor) and table.HasValue(melee, inflictor:GetClass()) then
        --     target:SetHealth(math.Clamp(target:Health() + math.ceil(dmg:GetDamage() * 0.6), 0, target:GetMaxHealth()))
        -- end

        local attacker = dmg:GetAttacker()
        if IsValid(attacker) and attacker:IsPlayer() then
            if attacker.Shiroyashd then
                dmg:ScaleDamage(0.7)
            end
        end
    end

    -- tank
    if target:GetNWInt("animelife.classchooser.class", -1) == 4 then
        if target.TankHealing then
            target:SetHealth(math.Clamp(target:Health() + dmg:GetDamage(), 0, target:GetMaxHealth()))
            dmg:SetDamage(0)

            return
        end

        if target.TankArmored then
            target.TankArmoredResistance = (target.TankArmoredResistance or 0) + (8 / 100)
        end

        local extra_resist = target.TankArmoredResistance or 0
        local dmg_scale = 0.65 - extra_resist
        if dmg_scale < 0.1 then dmg_scale = 0.1 end
        dmg:ScaleDamage(0.65 - extra_resist)
    end
end)

hook.Add("EntityTakeDamage", "animelife.jobabilities.neet.shiroyasha", function(target, dmg)
    if !IsValid(target) or !target:IsPlayer() then return end

    local attacker = dmg:GetAttacker()
    if !IsValid(attacker) or !attacker:IsPlayer() then return end
    if attacker:Team() ~= TEAM_NEET then return end
    if attacker:GetNWInt("animelife.classchooser.class", -1) ~= 3 then return end
    if dmg:GetDamageType() ~= DMG_GENERIC and dmg:GetDamageType() ~= DMG_CLUB then return end

    target:AddTimedJobAbility("ShiroyashaDamaged", function(ply)
        ply.Shiroyashd = true

        local normal_walk = ply:GetWalkSpeed()
        local normal_run = ply:GetRunSpeed()
        GAMEMODE:SetPlayerSpeed(ply, normal_walk - (normal_walk * 0.25), normal_run - (normal_run * 0.25))
        ply.ShiroyashdLastSpeed = {normal_walk, normal_run}
    end, function(ply)
        ply.Shiroyashd = false

        local normal_walk = ply.ShiroyashdLastSpeed[1]
        local normal_run = ply.ShiroyashdLastSpeed[2]
        GAMEMODE:SetPlayerSpeed(ply, normal_walk, normal_run)
    end, 5)
end)

hook.Add("ScalePlayerDamage", "animelife.jobabilities.neet", function(ply, hitgroup, dmg)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    
    local attacker = dmg:GetAttacker()
    if !IsValid(attacker) or !attacker:IsPlayer() then return end
    if attacker:Team() ~= TEAM_NEET then return end
    if attacker:GetNWInt("animelife.classchooser.class", -1) ~= 2 then return end -- sniper

    if hitgroup == HITGROUP_HEAD then
        dmg:ScaleDamage(2.5)
    end
end)

hook.Add("PlayerPostThink", "animelife.jobabilities.neet.shiroyasha", function(ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if ply:Team() ~= TEAM_NEET then return end
    if ply:GetNWInt("animelife.classchooser.class", -1) ~= 3 then return end
    if (ply.NextShiroyashaHeal or 0) > SysTime() then return end

    if ply:Health() < ply:GetMaxHealth() then
        ply:SetHealth(math.Clamp(ply:Health() + 3, 0, ply:GetMaxHealth()))
    end

    ply.NextShiroyashaHeal = SysTime() + 1
end)

if !timer.Exists("animelife.jobabilities.neet.shiroyasha.heal") then
    timer.Create("animelife.jobabilities.neet.shiroyasha.heal", 1.5, 0, function()
        for _, ply in ipairs(player.GetAll()) do
            if ply:Team() ~= TEAM_NEET then continue end
            if ply:GetNWInt("animelife.classchooser.class", -1) ~= 3 then continue end

            if ply:Health() < ply:GetMaxHealth() then
                ply:SetHealth(math.Clamp(ply:Health() + 1, 0, ply:GetMaxHealth()))
            end
        end
    end)
end