hook.Add("EntityTakeDamage", "animelife.jobabilities.maniac", function(target, dmg)
    if !IsValid(target) then return end

    local attacker = dmg:GetAttacker()
    if !IsValid(attacker) or !attacker:IsPlayer() then return end
    if attacker:Team() ~= TEAM_MANIAC then return end

    target:SetNWBool("animelife.jobabilities.maniac.footsteps", true)

    if !timer.Exists("animelife.jobabilities.maniac.footsteps") then
        timer.Create("animelife.jobabilities.maniac.footsteps", 15, 1, function()
            if IsValid(target) then
                target:SetNWBool("animelife.jobabilities.maniac.footsteps", false)
            end

            timer.Remove("animelife.jobabilities.maniac.footsteps")
        end)
    end
end)

hook.Add("PlayerSpawn", "animelife.jobabilities.maniac", function(ply)
    ply:SetNWBool("animelife.jobabilities.maniac.footsteps", false)
end)

hook.Add("EntityTakeDamage", "animelife.jobabilities.maniac2", function(target, dmg)
    if !IsValid(target) or !target:IsPlayer() then return end

    local attacker = dmg:GetAttacker()
    if !IsValid(attacker) or !attacker:IsPlayer() then return end
    if target == attacker then return end

    if attacker:Team() ~= TEAM_MANIAC then return end

    if math.abs(math.AngleDifference(target:GetAngles().y, attacker:GetAngles().y)) <= 50 then -- from behind
        if target:Health() < 150 then
            dmg:SetDamage(150)
            target:Kill()
        else
            if (target.NextParalisys or 0) < SysTime() then
                -- not really a timed ability tho lol
                -- target:AddTimedJobAbility("ManiacStabbed", function(ply)
                --     -- save them for returning
                --     ply.ManiacStabLastWeapons = ply:GetWeapons()

                --     ply:StripWeapons()
                -- end, function()
                --     -- return weapons to the owner
                --     for _, wep in pairs(ply.ManiacStabLastWeapons or {}) do
                --         ply:Give(wep)
                --     end
                -- end, 3)
                
                target:SelectWeapon("keys")

                -- cooldown
                target.NextParalisys = SysTime() + 15
            end
        end

        achievements:MarkCompleted(attacker, 11)
    end
end)

hook.Add("PlayerDeath", "animelife.jobabilities.maniac", function(victim, inflictor, attacker)
    if !IsValid(victim) or !victim:IsPlayer() then return end
    if !IsValid(attacker) or !attacker:IsPlayer() then return end
    if attacker == victim then return end

    if attacker:Team() ~= TEAM_MANIAC then return end

    attacker:SetHealth(attacker:GetMaxHealth())
    attacker:SetArmor(100)
end)