module("skills", package.seeall)

local icon_fix = Material("animelife/skillsystem/skills/fix.png")
local icon_bigger = Material("animelife/skillsystem/skills/bigger.png")
local icon_smaller = Material("animelife/skillsystem/skills/smaller.png")
local icon_chance = Material("animelife/skillsystem/skills/second_chance.png")
local icon_invisible = Material("animelife/skillsystem/skills/invisible.png")
local icon_mask = Material("animelife/skillsystem/skills/mask.png")
local icon_eye = Material("animelife/skillsystem/skills/nightvision.png")
local icon_blink = Material("animelife/skillsystem/skills/blink.png")
local icon_shadowwalk = Material("animelife/skillsystem/skills/shadowwalk.png")
local icon_tankarmored = Material("animelife/skillsystem/skills/tankarmored.png")
local icon_tankhealing = Material("animelife/skillsystem/skills/tankhealing.png")
local icon_blastaway = Material("animelife/skillsystem/skills/blastaway.png")
local icon_death = Material("animelife/skillsystem/skills/death.png")
local icon_dontmove = Material("animelife/skillsystem/skills/dontmove.png")
local icon_runaway = Material("animelife/skillsystem/skills/runaway.png")

List = {}

timer.Simple(0, function()
    List = {
        [TEAM_WOLFRAM] = {
            [1] = {
                Name = "Ремонтный набор",
                Description = "Восстанавливает 70% здоровья",
                Cooldown = 60 * 3,
                Icon = icon_fix,
                OnPressed = function(ply)
                    local new_health = ply:Health() + (ply:GetMaxHealth() * 0.7)
                    new_health = math.Clamp(new_health, 0, ply:GetMaxHealth())
                    ply:SetHealth(new_health)
                end,
            },
            [2] = {
                Name = "Стать больше",
                Description = "Делает вас больше",
                Cooldown = 1,
                Icon = icon_bigger,
                OnPressed = function(ply)
                    local max_size = 2
                    if ply:GetModelScale() < max_size then
                        local size = ply:GetModelScale() + 0.1
                        ply:SetModelScale(size)
                        ply:SetViewOffset(Vector(0, 0, 64 * size))
                        ply:SetViewOffsetDucked(Vector(0, 0, 28 * size))
                    else
                        ply:ChatPrint("Достигнут максимальный размер модели.")
                        achievements:MarkCompleted(ply, 12)
                    end
                end,
            },
            [3] = {
                Name = "Стать меньше",
                Description = "Делает вас меньше",
                Cooldown = 1,
                Icon = icon_smaller,
                OnPressed = function(ply)
                    local min_size = 0.5
                    if ply:GetModelScale() > min_size then
                        local size = ply:GetModelScale() - 0.1
                        ply:SetModelScale(size)
                        ply:SetViewOffset(Vector(0, 0, 64 * size))
                        ply:SetViewOffsetDucked(Vector(0, 0, 28 * size))
                    else
                        ply:ChatPrint("Достигнут минимальный размер модели.")
                    end
                end
            },
        },
        [TEAM_CHIEF] = {
            [1] = {
                Name = "Второй шанс",
                Description = "Попробуйте снова",
                Cooldown = 60 * 10,
                Icon = icon_chance,
                OnPressed = function(ply)
                    ply.SecondChanceAbility = true
                end,
            },
        },
        [TEAM_NEET] = {
            [1] = {
                Name = "Всевидящий",
                Description = "Видно?",
                Cooldown = 1,
                Icon = icon_eye,
                Class = 1,
                OnPressed = function(ply)
                    if ply:GetNWInt("animelife.classchooser.class", -1) ~= 1 then return end

                    DoDarkVisionAbility(ply)
                end,
            },
            [2] = {
                Name = "Призрачное обличие",
                Description = "Как тучка",
                Cooldown = 2 * 60,
                Icon = icon_shadowwalk,
                Class = 1,
                OnPressed = function(ply)
                    if ply:GetNWInt("animelife.classchooser.class", -1) ~= 1 then return end
                    
                    DoShadowWalkAbility(ply)
                end,
            },
            [3] = {
                Name = "Бронестойкость",
                Description = "Урон увеличивает сопротивление",
                Cooldown = 2 * 60,
                Icon = icon_tankarmored,
                Class = 4,
                OnPressed = function(ply)
                    if ply:GetNWInt("animelife.classchooser.class", -1) ~= 4 then return end

                    if !ply.TankArmored then
                        ply:AddTimedJobAbility("TankArmored", function(ply)
                            ply.TankArmored = true
                        end, function(ply)
                            ply.TankArmored = false
                            ply.TankArmoredResistance = 0
                        end, 30)
                    end
                end
            },
            [4] = {
                Name = "Бойня",
                Description = "Наносимый урон пополняет здоровье",
                Cooldown = 2 * 60,
                Icon = icon_tankhealing,
                Class = 4,
                OnPressed = function(ply)
                    if ply:GetNWInt("animelife.classchooser.class", -1) ~= 4 then return end

                    if !ply.TankHealing then
                        ply:AddTimedJobAbility("TankHealing", function(ply)
                            ply.TankHealing = true
                        end, function(ply)
                            ply.TankHealing = false
                        end, 10)
                    end
                end
            },
        },
        [TEAM_MIMIC] = {
            [1] = {
                Name = "Включить инвиз",
                Description = "Стрельба - выход из невидимости (10 сек.)",
                Cooldown = 20,
                Icon = icon_invisible,
                OnPressed = function(ply)
                    ply:AddTimedJobAbility("MimicInvisible", function(p)
                        p.MimicInvisible = true
                        p:SetNWBool("animelife.jobabilities.mimic_nodraw", p.MimicInvisible)
    
                        p:SetNoDraw(p.MimicInvisible)
    
                        for _, v in ipairs(p:GetWeapons()) do
                            v:SetNoDraw(false)
                        end
                
                        for _, v in ipairs(ents.FindByClass("physgun_beam")) do
                            if v:GetParent() == p then
                                v:SetNoDraw(false)
                            end
                        end
    
                        if p:GetNWEntity("animelife.jobabilities.mimic_masked", game.GetWorld()) ~= game.GetWorld() then
                            local mdl = p:getJobTable().model
                            p:SetModel(isstring(mdl) and mdl or table.Random(mdl))
                            p:SetNWEntity("animelife.jobabilities.mimic_masked", game.GetWorld())
                        end
                    end, function(p)
                        if p.MimicInvisible then
                            p.MimicInvisible = false
                            
                            p:SetNWBool("animelife.jobabilities.mimic_nodraw", false)

                            p:SetNoDraw(false)

                            for _, v in ipairs(p:GetWeapons()) do
                                v:SetNoDraw(false)
                            end

                            for _, v in ipairs(ents.FindByClass("physgun_beam")) do
                                if v:GetParent() == p then
                                    v:SetNoDraw(false)
                                end
                            end
                        end
                    end, 10)
                end,
            },
            [2] = {
                Name = "Мимикрия",
                Description = "Маскировка под человека",
                Cooldown = 5,
                Icon = icon_mask,
                OnPressed = function(ply)
                    local tr = ply:GetEyeTraceNoCursor()
                    if IsValid(tr.Entity) and tr.Entity:IsPlayer() then
                        if tr.Entity:GetPos():DistToSqr(ply:GetPos()) > 512^2 then
                            return
                        end

                        ply:SetNWEntity("animelife.jobabilities.mimic_masked", tr.Entity)

                        ply:SetModel(tr.Entity:GetModel())
                    else
                        if ply:GetNWEntity("animelife.jobabilities.mimic_masked", game.GetWorld()) ~= game.GetWorld() then
                            local mdl = ply:getJobTable().model
                            ply:SetModel(isstring(mdl) and mdl or table.Random(mdl))
                            ply:SetNWEntity("animelife.jobabilities.mimic_masked", game.GetWorld())
                        end
                    end
                end,
            },
        },
        [TEAM_MANIAC] = {
            [1] = {
                Name = "Маскировка",
                Description = "Маскировка под человека",
                Cooldown = 5,
                Icon = icon_mask,
                OnPressed = function(ply)
                    local tr = ply:GetEyeTraceNoCursor()
                    if IsValid(tr.Entity) and tr.Entity:IsPlayer() then
                        if tr.Entity:GetPos():DistToSqr(ply:GetPos()) > 512^2 then
                            return
                        end

                        ply:SetNWEntity("animelife.jobabilities.mimic_masked", tr.Entity)

                        ply:SetModel(tr.Entity:GetModel())
                    else
                        if ply:GetNWEntity("animelife.jobabilities.mimic_masked", game.GetWorld()) ~= game.GetWorld() then
                            local mdl = ply:getJobTable().model
                            ply:SetModel(isstring(mdl) and mdl or table.Random(mdl))
                            ply:SetNWEntity("animelife.jobabilities.mimic_masked", game.GetWorld())
                        end
                    end
                end,
            },
        },
        [TEAM_INUMAKI] = {
            [1] = {
                Name = "Blast Away",
                Description = "50 маны",
                Cooldown = 1,
                Icon = icon_blastaway,
                OnPressed = function(self)
                    InumakiBlastaway(self)
                end,
            },
            [2] = {
                Name = "Run Away",
                Description = "10 маны",
                Cooldown = 16,
                Icon = icon_runaway,
                OnPressed = function(self)
                    InumakiRunaway(self)
                end,
            },
            [3] = {
                Name = "Don't Move",
                Description = "10 маны",
                Cooldown = 1,
                Icon = icon_dontmove,
                OnPressed = function(self)
                    InumakiDontmove(self)
                end,
            },
            [4] = {
                Name = "Death",
                Description = "100 маны",
                Cooldown = 1,
                Icon = icon_death,
                OnPressed = function(self)
                    InumakiDeath(self)
                end,
            },
        },
    }
end)