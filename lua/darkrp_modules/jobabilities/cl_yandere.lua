local background = Material("animelife/yandere/background.png")

surface.CreateFont("animelife.yandere.title", {font = "Exo 2 SemiBold", weight = 600, size = 24, extended = true})
surface.CreateFont("animelife.yandere.point", {font = "Exo 2 Medium", weight = 500, size = 19, extended = true})
surface.CreateFont("animelife.yandere.ask", {font = "Exo 2 SemiBold", weight = 600, size = 19, extended = true})
surface.CreateFont("animelife.yandere.button", {font = "Exo 2 SemiBold", weight = 600, size = 18, extended = true})

local function ShowYandereAsk(yandere)
    if !IsValid(yandere) then return end
    if ValidPanel(GLOBALS_YANDEREASK) then
        return
    end

    GLOBALS_YANDEREASK = vgui.Create("EditablePanel")
    GLOBALS_YANDEREASK:SetSize(ScrW(), ScrH())
    GLOBALS_YANDEREASK:MakePopup()
    GLOBALS_YANDEREASK:SetAlpha(0)
    GLOBALS_YANDEREASK:AlphaTo(255, 0.25)
    GLOBALS_YANDEREASK.Paint = function(self, w, h)
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(background)
        surface.DrawTexturedRect((w - 700) / 2, 284, 700, 417)

        local yandere_nick = "Unknown Player"
        if IsValid(yandere) then
            yandere_nick = yandere:Nick()
        end
        draw.SimpleText(yandere_nick .. " хочет установить с вами связь", "animelife.yandere.title", (w - 700) / 2 + 630, 518, Color(228, 171, 255), TEXT_ALIGN_RIGHT)
        draw.SimpleText(yandere_nick .. " будет видеть вас в любой части карты", "animelife.yandere.point", (w - 700) / 2 + 630, 550, Color(137, 137, 137), TEXT_ALIGN_RIGHT)
        draw.SimpleText("При разрыве связи вы должны убить друг друга", "animelife.yandere.point", (w - 700) / 2 + 630, 574, Color(137, 137, 137), TEXT_ALIGN_RIGHT)
        draw.SimpleText("Вам будут доступны некоторые преимущества в бое", "animelife.yandere.point", (w - 700) / 2 + 630, 598, Color(137, 137, 137), TEXT_ALIGN_RIGHT)
        draw.SimpleText("Установить связь?", "animelife.yandere.ask", (w - 700) / 2 + 630, 622, Color(80, 80, 80), TEXT_ALIGN_RIGHT)
    end

    local y = 548
    for i = 1, 2 do
        local btn = vgui.Create("DButton", GLOBALS_YANDEREASK)
        btn:SetPos((GLOBALS_YANDEREASK:GetWide() - 700) / 2 + 55, y)
        btn:SetSize(181, 44)
        btn:SetText("")
        btn.Paint = function(self, w, h)
            draw.RoundedBox(4, 0, 0, w, h, Color(0, 0, 0, 75))
            draw.RoundedBox(4, 0, 0, w, h - 2, i == 1 and Color(228, 171, 255) or Color(255, 201, 201))

            draw.SimpleText(i == 1 and "согласиться" or "отказать", "animelife.yandere.button", w / 2, h / 2, Color(0, 0, 0, 75), 1, 1)
            draw.SimpleText(i == 1 and "согласиться" or "отказать", "animelife.yandere.button", w / 2, h / 2 - 2, Color(255, 255, 255), 1, 1)
        end
        btn.DoClick = function()
            GLOBALS_YANDEREASK:Remove()

            net.Start(i == 1 and "animelife.jobabilities.yandere.accept" or "animelife.jobabilities.yandere.decline")
                net.WriteEntity(yandere)
            net.SendToServer()
        end

        y = y + 44 + 19
    end
end

net.Receive("animelife.jobabilities.yandere.menu", function()
    local yandere = net.ReadEntity()
    ShowYandereAsk(yandere)
end)

local flux = util.Base64Decode("bG9jYWwgZmx1eCA9IHsgX3ZlcnNpb24gPSAiMC4xLjUiIH0KZmx1eC5fX2luZGV4ID0gZmx1eAoKZmx1eC50d2VlbnMgPSB7fQpmbHV4LmVhc2luZyA9IHsgbGluZWFyID0gZnVuY3Rpb24ocCkgcmV0dXJuIHAgZW5kIH0KCmxvY2FsIGVhc2luZyA9IHsKICBxdWFkICAgID0gInAgKiBwIiwKICBjdWJpYyAgID0gInAgKiBwICogcCIsCiAgcXVhcnQgICA9ICJwICogcCAqIHAgKiBwIiwKICBxdWludCAgID0gInAgKiBwICogcCAqIHAgKiBwIiwKICBleHBvICAgID0gIjIgXiAoMTAgKiAocCAtIDEpKSIsCiAgc2luZSAgICA9ICItbWF0aC5jb3MocCAqIChtYXRoLnBpICogLjUpKSArIDEiLAogIGNpcmMgICAgPSAiLShtYXRoLnNxcnQoMSAtIChwICogcCkpIC0gMSkiLAogIGJhY2sgICAgPSAicCAqIHAgKiAoMi43ICogcCAtIDEuNykiLAogIGVsYXN0aWMgPSAiLSgyXigxMCAqIChwIC0gMSkpICogbWF0aC5zaW4oKHAgLSAxLjA3NSkgKiAobWF0aC5waSAqIDIpIC8gLjMpKSIKfQoKbG9jYWwgbWFrZWZ1bmMgPSBmdW5jdGlvbihzdHIsIGV4cHIpCiAgcmV0dXJuIENvbXBpbGVTdHJpbmcoInJldHVybiBmdW5jdGlvbihwKSAiIC4uIHN0cjpnc3ViKCIlJGUiLCBleHByKSAuLiAiIGVuZCIsICJUZXN0RnVuY3Rpb24iKSgpCmVuZAoKZm9yIGssIHYgaW4gcGFpcnMoZWFzaW5nKSBkbwogIGZsdXguZWFzaW5nW2sgLi4gImluIl0gPSBtYWtlZnVuYygicmV0dXJuICRlIiwgdikKICBmbHV4LmVhc2luZ1trIC4uICJvdXQiXSA9IG1ha2VmdW5jKFtbCiAgICBwID0gMSAtIHAKICAgIHJldHVybiAxIC0gKCRlKQogIF1dLCB2KQogIGZsdXguZWFzaW5nW2sgLi4gImlub3V0Il0gPSBtYWtlZnVuYyhbWwogICAgcCA9IHAgKiAyCiAgICBpZiBwIDwgMSB0aGVuCiAgICAgIHJldHVybiAuNSAqICgkZSkKICAgIGVsc2UKICAgICAgcCA9IDIgLSBwCiAgICAgIHJldHVybiAuNSAqICgxIC0gKCRlKSkgKyAuNQogICAgZW5kCiAgXV0sIHYpCmVuZAoKCgpsb2NhbCB0d2VlbiA9IHt9CnR3ZWVuLl9faW5kZXggPSB0d2VlbgoKbG9jYWwgZnVuY3Rpb24gbWFrZWZzZXR0ZXIoZmllbGQpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBsb2NhbCBtdCA9IGdldG1ldGF0YWJsZSh4KQogICAgaWYgdHlwZSh4KSB+PSAiZnVuY3Rpb24iIGFuZCBub3QgKG10IGFuZCBtdC5fX2NhbGwpIHRoZW4KICAgICAgZXJyb3IoImV4cGVjdGVkIGZ1bmN0aW9uIG9yIGNhbGxhYmxlIiwgMikKICAgIGVuZAogICAgbG9jYWwgb2xkID0gc2VsZltmaWVsZF0KICAgIHNlbGZbZmllbGRdID0gb2xkIGFuZCBmdW5jdGlvbigpIG9sZCgpIHgoKSBlbmQgb3IgeAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgpsb2NhbCBmdW5jdGlvbiBtYWtlc2V0dGVyKGZpZWxkLCBjaGVja2ZuLCBlcnJtc2cpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBpZiBjaGVja2ZuIGFuZCBub3QgY2hlY2tmbih4KSB0aGVuCiAgICAgIGVycm9yKGVycm1zZzpnc3ViKCIlJHgiLCB0b3N0cmluZyh4KSksIDIpCiAgICBlbmQKICAgIHNlbGZbZmllbGRdID0geAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgp0d2Vlbi5lYXNlICA9IG1ha2VzZXR0ZXIoIl9lYXNlIiwKICAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uKHgpIHJldHVybiBmbHV4LmVhc2luZ1t4XSBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGVhc2luZyB0eXBlICckeCciKQp0d2Vlbi5kZWxheSA9IG1ha2VzZXR0ZXIoIl9kZWxheSIsCiAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbih4KSByZXR1cm4gdHlwZSh4KSA9PSAibnVtYmVyIiBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGRlbGF5IHRpbWU7IGV4cGVjdGVkIG51bWJlciIpCnR3ZWVuLm9uc3RhcnQgICAgID0gbWFrZWZzZXR0ZXIoIl9vbnN0YXJ0IikKdHdlZW4ub251cGRhdGUgICAgPSBtYWtlZnNldHRlcigiX29udXBkYXRlIikKdHdlZW4ub25jb21wbGV0ZSAgPSBtYWtlZnNldHRlcigiX29uY29tcGxldGUiKQoKCmZ1bmN0aW9uIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpCiAgbG9jYWwgc2VsZiA9IHNldG1ldGF0YWJsZSh7fSwgdHdlZW4pCiAgc2VsZi5vYmogPSBvYmoKICBzZWxmLnJhdGUgPSB0aW1lID4gMCBhbmQgMSAvIHRpbWUgb3IgMAogIHNlbGYucHJvZ3Jlc3MgPSB0aW1lID4gMCBhbmQgMCBvciAxCiAgc2VsZi5fZGVsYXkgPSAwCiAgc2VsZi5fZWFzZSA9ICJxdWFkb3V0IgogIHNlbGYudmFycyA9IHt9CiAgZm9yIGssIHYgaW4gcGFpcnModmFycykgZG8KICAgIGlmIHR5cGUodikgfj0gIm51bWJlciIgdGhlbgogICAgICBlcnJvcigiYmFkIHZhbHVlIGZvciBrZXkgJyIgLi4gayAuLiAiJzsgZXhwZWN0ZWQgbnVtYmVyIikKICAgIGVuZAogICAgc2VsZi52YXJzW2tdID0gdgogIGVuZAogIHJldHVybiBzZWxmCmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOmluaXQoKQogIGZvciBrLCB2IGluIHBhaXJzKHNlbGYudmFycykgZG8KICAgIGxvY2FsIHggPSBzZWxmLm9ialtrXQogICAgaWYgdHlwZSh4KSB+PSAibnVtYmVyIiB0aGVuCiAgICAgIGVycm9yKCJiYWQgdmFsdWUgb24gb2JqZWN0IGtleSAnIiAuLiBrIC4uICInOyBleHBlY3RlZCBudW1iZXIiKQogICAgZW5kCiAgICBzZWxmLnZhcnNba10gPSB7IHN0YXJ0ID0geCwgZGlmZiA9IHYgLSB4IH0KICBlbmQKICBzZWxmLmluaXRlZCA9IHRydWUKZW5kCgoKZnVuY3Rpb24gdHdlZW46YWZ0ZXIoLi4uKQogIGxvY2FsIHQKICBpZiBzZWxlY3QoIiMiLCAuLi4pID09IDIgdGhlbgogICAgdCA9IHR3ZWVuLm5ldyhzZWxmLm9iaiwgLi4uKQogIGVsc2UKICAgIHQgPSB0d2Vlbi5uZXcoLi4uKQogIGVuZAogIHQucGFyZW50ID0gc2VsZi5wYXJlbnQKICBzZWxmOm9uY29tcGxldGUoZnVuY3Rpb24oKSBmbHV4LmFkZChzZWxmLnBhcmVudCwgdCkgZW5kKQogIHJldHVybiB0CmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOnN0b3AoKQogIGZsdXgucmVtb3ZlKHNlbGYucGFyZW50LCBzZWxmKQplbmQKCgoKZnVuY3Rpb24gZmx1eC5ncm91cCgpCiAgcmV0dXJuIHNldG1ldGF0YWJsZSh7fSwgZmx1eCkKZW5kCgoKZnVuY3Rpb24gZmx1eDp0byhvYmosIHRpbWUsIHZhcnMpCiAgcmV0dXJuIGZsdXguYWRkKHNlbGYsIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpKQplbmQKCgpmdW5jdGlvbiBmbHV4OnVwZGF0ZShkZWx0YXRpbWUpCiAgZm9yIGkgPSAjc2VsZiwgMSwgLTEgZG8KICAgIGxvY2FsIHQgPSBzZWxmW2ldCiAgICBpZiB0Ll9kZWxheSA+IDAgdGhlbgogICAgICB0Ll9kZWxheSA9IHQuX2RlbGF5IC0gZGVsdGF0aW1lCiAgICBlbHNlCiAgICAgIGlmIG5vdCB0LmluaXRlZCB0aGVuCiAgICAgICAgZmx1eC5jbGVhcihzZWxmLCB0Lm9iaiwgdC52YXJzKQogICAgICAgIHQ6aW5pdCgpCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnN0YXJ0IHRoZW4KICAgICAgICB0Ll9vbnN0YXJ0KCkKICAgICAgICB0Ll9vbnN0YXJ0ID0gbmlsCiAgICAgIGVuZAogICAgICB0LnByb2dyZXNzID0gdC5wcm9ncmVzcyArIHQucmF0ZSAqIGRlbHRhdGltZQogICAgICBsb2NhbCBwID0gdC5wcm9ncmVzcwogICAgICBsb2NhbCB4ID0gcCA+PSAxIGFuZCAxIG9yIGZsdXguZWFzaW5nW3QuX2Vhc2VdKHApCiAgICAgIGZvciBrLCB2IGluIHBhaXJzKHQudmFycykgZG8KICAgICAgICB0Lm9ialtrXSA9IHYuc3RhcnQgKyB4ICogdi5kaWZmCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnVwZGF0ZSB0aGVuIHQuX29udXBkYXRlKCkgZW5kCiAgICAgIGlmIHAgPj0gMSB0aGVuCiAgICAgICAgZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgICAgICBpZiB0Ll9vbmNvbXBsZXRlIHRoZW4gdC5fb25jb21wbGV0ZSgpIGVuZAogICAgICBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmNsZWFyKG9iaiwgdmFycykKICBmb3IgdCBpbiBwYWlycyhzZWxmW29ial0pIGRvCiAgICBpZiB0LmluaXRlZCB0aGVuCiAgICAgIGZvciBrIGluIHBhaXJzKHZhcnMpIGRvIHQudmFyc1trXSA9IG5pbCBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmFkZCh0d2VlbikKICAtLSBBZGQgdG8gb2JqZWN0IHRhYmxlLCBjcmVhdGUgdGFibGUgaWYgaXQgZG9lcyBub3QgZXhpc3QKICBsb2NhbCBvYmogPSB0d2Vlbi5vYmoKICBzZWxmW29ial0gPSBzZWxmW29ial0gb3Ige30KICBzZWxmW29ial1bdHdlZW5dID0gdHJ1ZQogIC0tIEFkZCB0byBhcnJheQogIHRhYmxlLmluc2VydChzZWxmLCB0d2VlbikKICB0d2Vlbi5wYXJlbnQgPSBzZWxmCiAgcmV0dXJuIHR3ZWVuCmVuZAoKCmZ1bmN0aW9uIGZsdXg6cmVtb3ZlKHgpCiAgaWYgdHlwZSh4KSA9PSAibnVtYmVyIiB0aGVuCiAgICAtLSBSZW1vdmUgZnJvbSBvYmplY3QgdGFibGUsIGRlc3Ryb3kgdGFibGUgaWYgaXQgaXMgZW1wdHkKICAgIGxvY2FsIG9iaiA9IHNlbGZbeF0ub2JqCiAgICBzZWxmW29ial1bc2VsZlt4XV0gPSBuaWwKICAgIGlmIG5vdCBuZXh0KHNlbGZbb2JqXSkgdGhlbiBzZWxmW29ial0gPSBuaWwgZW5kCiAgICAtLSBSZW1vdmUgZnJvbSBhcnJheQogICAgc2VsZlt4XSA9IHNlbGZbI3NlbGZdCiAgICByZXR1cm4gdGFibGUucmVtb3ZlKHNlbGYpCiAgZW5kCiAgZm9yIGksIHYgaW4gaXBhaXJzKHNlbGYpIGRvCiAgICBpZiB2ID09IHggdGhlbgogICAgICByZXR1cm4gZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgIGVuZAogIGVuZAplbmQKCgoKbG9jYWwgYm91bmQgPSB7CiAgdG8gICAgICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgudG8oZmx1eC50d2VlbnMsIC4uLikgZW5kLAogIHVwZGF0ZSAgPSBmdW5jdGlvbiguLi4pIHJldHVybiBmbHV4LnVwZGF0ZShmbHV4LnR3ZWVucywgLi4uKSBlbmQsCiAgcmVtb3ZlICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgucmVtb3ZlKGZsdXgudHdlZW5zLCAuLi4pIGVuZCwKfQpzZXRtZXRhdGFibGUoYm91bmQsIGZsdXgpCgpyZXR1cm4gYm91bmQ=")
flux = CompileString(flux, "flux")()

local hud_background = Material("animelife/yandere/hud_background.png")
local hud_keycap = Material("animelife/yandere/hud_keycap.png")

local yandere_hud_anim = {alpha = 0, add_x = 0}

surface.CreateFont("animelife.yandere.hud", {font = "Exo 2 SemiBold", weight = 600, size = 26, extended = true})
hook.Add("HUDPaint", "animelife.jobabilities.yandere.renderhud.helper", function()
    if !IsValid(LocalPlayer()) then return end
    if LocalPlayer():Team() ~= TEAM_YANDERE then return end
    if IsValid(LocalPlayer():GetNWEntity("animelife.jobabilities.yandere.cutie")) then return end

    local tr = LocalPlayer():GetEyeTraceNoCursor()
    if IsValid(tr.Entity) and tr.Entity:IsPlayer() and tr.Entity:GetPos():DistToSqr(LocalPlayer():GetPos()) < 256^2 then
        if yandere_hud_anim.alpha == 0 then
            flux.to(yandere_hud_anim, 0.25, {alpha = 1, add_x = -151})
        end
    else
        if yandere_hud_anim.alpha == 1 then
            flux.to(yandere_hud_anim, 0.25, {alpha = 0, add_x = 0})
        end
    end

    surface.SetDrawColor(255, 255, 255, 255 * yandere_hud_anim.alpha)
    surface.SetMaterial(hud_background)
    surface.DrawTexturedRect(ScrW() + yandere_hud_anim.add_x, (ScrH() - 290) / 2, 151, 290)

    surface.SetMaterial(hud_keycap)
    surface.DrawTexturedRect(ScrW() + yandere_hud_anim.add_x + 116 - 16, (ScrH() - 290) / 2 + 50, 35, 35)

    draw.SimpleText("E", "animelife.yandere.hud", ScrW() + yandere_hud_anim.add_x + 117, (ScrH() - 290) / 2 + 50 + 14, Color(73, 80, 94, 255 * yandere_hud_anim.alpha), 1, 1)

    flux.update(RealFrameTime())
end)

hook.Add("PreDrawHalos", "animelife.jobabilities.yandere", function()
    if !IsValid(LocalPlayer()) then return end
    if !IsHaloEffectEnabled() then return end

    if LocalPlayer():Team() ~= TEAM_YANDERE then
        local yandere = LocalPlayer():GetNWEntity("animelife.jobabilities.yandere")
        if IsValid(yandere) and yandere:GetNWEntity("animelife.jobabilities.yandere.cutie") == LocalPlayer() then
            halo.Add({yandere}, Color(75, 255, 75), 0, 0, 1, true, true)
        end
    end

    if LocalPlayer():Team() ~= TEAM_YANDERE then return end

    local cutie = LocalPlayer():GetNWEntity("animelife.jobabilities.yandere.cutie", game.GetWorld())
    if !IsValid(cutie) then return end

    halo.Add({cutie}, Color(75, 255, 75), 0, 0, 1, true, true)
end)

hook.Add("HUDPaint", "animelife.jobabilities.yandere.renderhud", function()
    if !IsValid(LocalPlayer()) then return end
    if LocalPlayer():Team() ~= TEAM_YANDERE then return end

    local cutie = LocalPlayer():GetNWEntity("animelife.jobabilities.yandere.cutie", game.GetWorld())
    if IsValid(cutie) and cutie:IsPlayer() then
        local cutie_pos = cutie:GetPos() + Vector(0, 0, 172)
        local dist = LocalPlayer():GetPos():Distance(cutie_pos)
        if dist > 256 then
            dist = dist * 1.905 / 100
            dist = math.floor(dist)

            local pos = cutie_pos:ToScreen()
            draw.SimpleText(dist .. "м", "DermaDefault", pos.x, pos.y, Color(255, 255, 255), 1)
        end
    end
end)