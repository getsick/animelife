util.AddNetworkString("animelife.jobabilities.yandere.menu")
util.AddNetworkString("animelife.jobabilities.yandere.decline")
util.AddNetworkString("animelife.jobabilities.yandere.accept")

function SendYandereRequest(yandere, target)
    local cutie = yandere:GetNWEntity("animelife.jobabilities.yandere.cutie", game.GetWorld())
    if IsValid(cutie) then
        return
    end

    if (target.NextYandereAsk or 0) > SysTime() then
        DarkRP.notify(yandere, 0, 5, "Игрок уже недавно отвечал на предложение.")
        return
    end

    net.Start("animelife.jobabilities.yandere.menu")
        net.WriteEntity(yandere)
    net.Send(target)

    DarkRP.notify(yandere, 0, 5, "Запрос на установление связи отправлен " .. target:Nick())

    target.NextYandereAsk = SysTime() + 300
end

net.Receive("animelife.jobabilities.yandere.accept", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if ply:Team() == TEAM_YANDERE then return end

    local yandere = net.ReadEntity()
    if !IsValid(yandere) then return end

    local cutie = yandere:GetNWEntity("animelife.jobabilities.yandere.cutie", game.GetWorld())
    if IsValid(cutie) then
        return
    end

    yandere:SetNWEntity("animelife.jobabilities.yandere.cutie", ply)
    ply:SetNWEntity("animelife.jobabilities.yandere", yandere)

    DarkRP.notify(yandere, 0, 5, ply:Nick() .. " принял ваше предложение.")
    DarkRP.notify(ply, 0, 5, "Установлена связь с яндере " .. yandere:Nick())

    ply.NextYandereAsk = SysTime() + 300

    achievements:MarkCompleted(yandere, 9)
end)

net.Receive("animelife.jobabilities.yandere.decline", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if ply:Team() == TEAM_YANDERE then return end

    local yandere = net.ReadEntity()
    if IsValid(yandere) then
        DarkRP.notify(yandere, 1, 5, ply:Nick() .. " отказался от предложения.")
    end

    ply.NextYandereAsk = SysTime() + 300
end)

hook.Add("PlayerUse", "animelife.jobabilities.yandere", function(ply, ent)
    if ply:Team() ~= TEAM_YANDERE then return end

    if IsValid(ent) and ent:IsPlayer() then
        if (ply.YandereNextUse or 0) < SysTime() then
            SendYandereRequest(ply, ent)

            ply.YandereNextUse = SysTime() + 2
        end
    end
end)

hook.Add("EntityTakeDamage", "animelife.jobabilities.yandere", function(ent, dmg)
    if !IsValid(ent) or !ent:IsPlayer() then return end

    local attacker = dmg:GetAttacker()
    if IsValid(attacker) and attacker:IsPlayer() then
        -- do healing
        local yan = attacker:GetNWEntity("animelife.jobabilities.yandere")
        local damage = dmg:GetDamage()
        if IsValid(yan) then
            yan:SetHealth(math.Clamp(yan:Health() + (damage * 0.3), 0, yan:GetMaxHealth()))
        end

        local cutie = attacker:GetNWEntity("animelife.jobabilities.yandere.cutie")
        if IsValid(cutie) then
            cutie:SetHealth(math.Clamp(cutie:Health() + (damage * 0.3), 0, cutie:GetMaxHealth()))
        end

        -- enragening
        if attacker:Team() == TEAM_YANDERE or ent:Team() == TEAM_YANDERE then
            if attacker.YandereRage or ent.YandereRage then
                dmg:ScaleDamage(1.3)
            end
        end

        local yandere = ent:GetNWEntity("animelife.jobabilities.yandere")
        if !IsValid(yandere) or !yandere:IsPlayer() then return end
        if attacker ~= yandere then
            yandere:AddTimedJobAbility("YandereRage", function(ply)
                local normal_walk = ply:GetWalkSpeed()
                local normal_run = ply:GetRunSpeed()
                GAMEMODE:SetPlayerSpeed(ply, normal_walk + (normal_walk * 0.3), normal_run + (normal_run * 0.3))
                ply.YandereRageLastSpeed = {normal_walk, normal_run}
                ply.YandereRage = true
            end, function(ply) 
                local normal_walk = ply.YandereRageLastSpeed[1]
                local normal_run = ply.YandereRageLastSpeed[2]
                GAMEMODE:SetPlayerSpeed(ply, normal_walk, normal_run)
                ply.YandereRage = false
            end, 6)
        end
    end
end)

concommand.Add("al_yandere_tiescut", function(ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local yan = ply:GetNWEntity("animelife.jobabilities.yandere")
    if IsValid(yan) then
        yan:SetNWEntity("animelife.jobabilities.yandere.cutie", game.GetWorld())
        ply:SetNWEntity("animelife.jobabilities.yandere", game.GetWorld())

        DarkRP.notify(yan, 0, 5, "Связь разорвана.")
    end

    local cutie = ply:GetNWEntity("animelife.jobabilities.yandere.cutie")
    if IsValid(cutie) then
        cutie:SetNWEntity("animelife.jobabilities.yandere", game.GetWorld())
        ply:SetNWEntity("animelife.jobabilities.yandere.cutie", game.GetWorld())

        DarkRP.notify(cutie, 0, 5, "Связь разорвана.")
    end

    DarkRP.notify(ply, 0, 5, "Связь разорвана.")
end)

hook.Add("OnPlayerChangedTeam", "animelife.jobabilities.yandere", function(ply, old, new)
    if old == TEAM_YANDERE and new ~= TEAM_YANDERE then
        local cutie = ply:GetNWEntity("animelife.jobabilities.yandere.cutie")
        if IsValid(cutie) then
            ply:ConCommand("al_yandere_tiescut")
        end
    end
end)