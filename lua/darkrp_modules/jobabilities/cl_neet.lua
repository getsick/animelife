hook.Add("HUDPaint", "animelife.jobabilities.neet.hint", function()
    if !IsValid(LocalPlayer()) then return end
    if LocalPlayer():Team() ~= TEAM_NEET then return end
    if !LocalPlayer():Alive() then return end

    local class = LocalPlayer():GetNWInt("animelife.classchooser.class", -1)
    if class ~= 3 then return end

    local text = "F: Телепорт"

    draw.DrawText(text, "animelife.Global.HUD_Money", ScrW() - 32 + 2, 24, Color(0, 0, 0, 75), TEXT_ALIGN_RIGHT)
    draw.DrawText(text, "animelife.Global.HUD_Money", ScrW() - 32, 24, Color(255, 243, 205, 150), TEXT_ALIGN_RIGHT)
end)