local meta = FindMetaTable("Player")

function meta:AddTimedJobAbility(name, on_call, on_end, duration)
    if !self.TimedJobAbilities then self.TimedJobAbilities = {} end
    if self.TimedJobAbilities[name] then return end -- there's already one

    self.TimedJobAbilities[name] = {
        CallFunction = on_call,
        EndFunction = on_end,
        Duration = duration,
        Time = os.time()
    }

    on_call(self)

    return true
end

-- i hope it's not that heavy
hook.Add("PlayerPostThink", "animelife.jobabilities.timed.think", function(ply)
    if !ply.TimedJobAbilities then return end

    for name, ability in pairs(ply.TimedJobAbilities) do
        if (ability.Time + ability.Duration) < os.time() then
            ability.EndFunction(ply)

            -- we're done, outta loop
            ply.TimedJobAbilities[name] = nil
        end
    end
end)