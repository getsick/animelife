util.AddNetworkString("animelife.hentaiprinters.menu")
util.AddNetworkString("animelife.hentaiprinters.tablet.menu")
util.AddNetworkString("animelife.hentaiprinters.request_money")
util.AddNetworkString("animelife.hentaiprinters.buy_cooling")
util.AddNetworkString("animelife.hentaiprinters.buy_upgrade")

local function is_tablet_on(ply)
    local wep = ply:HasWeapon("weapon_tablet")

    return wep
end

local function get_owned_printers(ply)
    local ret = {}
    for _, printer in pairs(ents.FindByClass("animelife_hentaiprinter")) do
        if printer:GetPrinterOwner() == ply then
            table.insert(ret, printer)
        end
    end

    return ret
end

net.Receive("animelife.hentaiprinters.request_money", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local printer = net.ReadEntity()
    if !IsValid(printer) then return end
    if !is_tablet_on(ply) then
        if ply:Team() == TEAM_HACKER then
            if printer:GetPos():DistToSqr(ply:GetPos()) > 512^2 then return end
        end
    end
    -- if printer:GetPrinterOwner() ~= ply then return end

    local printed = printer:GetPrinted() or 0
    if printed > 0 then
        printer:SetPrinted(0)

        ply:addMoney(printed)

        DarkRP.notify(ply, 0, 2, "Вы собрали " .. string.Comma(printed) .. "¥")
    end
end)

net.Receive("animelife.hentaiprinters.buy_cooling", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local printer = net.ReadEntity()
    if !IsValid(printer) then return end
    if !is_tablet_on(ply) then
        if ply:Team() == TEAM_HACKER then
            if printer:GetPos():DistToSqr(ply:GetPos()) > 512^2 then return end
        end
    end
    -- if printer:GetPrinterOwner() ~= ply then return end

    if ply:canAfford(150) then
        local new_temperature = printer:GetTemperature() - (math.floor(printer:GetTemperature() * 0.75))
        printer:SetTemperature(new_temperature)

        ply:addMoney(-150)

        DarkRP.notify(ply, 0, 2, "Принтер охлажден до " .. new_temperature .. " °C.")
    else
        DarkRP.notify(ply, 1, 2, "Недостаточно средств для покупки охлаждения.")
    end
end)

net.Receive("animelife.hentaiprinters.buy_upgrade", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local printer = net.ReadEntity()
    if !IsValid(printer) then return end
    if !is_tablet_on(ply) then
        if ply:Team() == TEAM_HACKER then
            if printer:GetPos():DistToSqr(ply:GetPos()) > 512^2 then return end
        end
    end
    -- if printer:GetPrinterOwner() ~= ply then return end
    if printer:GetPrinterLevel() >= 10 then
        DarkRP.notify(ply, 1, 2, "Принтер уже имеет самый высокий уровень.")

        return
    end

    local upgrade_price = hentaiprinter_upgradeformula(printer:GetGirl(), printer:GetPrinterLevel())
    if ply:canAfford(upgrade_price) then
        printer:SetPrinterLevel(printer:GetPrinterLevel() + 1)

        ply:addMoney(-upgrade_price)
    else
        DarkRP.notify(ply, 1, 2, "Недостаточно средств для покупки апгрейда.")
    end
end)

local max_printers = 4
hook.Add("canBuyCustomEntity", "animelife.hentaiprinters.limit", function(ply, tbl)
    if tbl.ent == "animelife_hentaiprinter" then
        if #get_owned_printers(ply) >= max_printers then
            return false, false, "Достигнут лимит приобретенных хентай-принтеров."
        end
    end
end)