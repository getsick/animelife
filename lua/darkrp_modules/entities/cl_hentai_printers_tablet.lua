local PANEL = {}
local PANEL_BG_COLOR = Color(40, 42, 52)
local PANEL_MATERIAL_LOGO = Material("animelife/hentaiprinters/tablet/logo.png")
local PANEL_MATERIAL_BACKGROUND = Material("animelife/hentaiprinters/tablet/detail_background.png")
local PANEL_MATERIAL_LINES = Material("animelife/hentaiprinters/tablet/detail_lines.png")

surface.CreateFont("animelife.hentaiprinter.tablet.gui.title", {font = "Exo 2 SemiBold", size = 21, weight = 600, extended = true})
surface.CreateFont("animelife.hentaiprinter.tablet.gui.type", {font = "Exo 2 SemiBold", size = 15, weight = 600, extended = true})

local function GetOwnedPrinters()
    local ret = {}
    for _, printer in pairs(ents.FindByClass("animelife_hentaiprinter")) do
        if printer:GetPrinterOwner() == LocalPlayer() then
            table.insert(ret, printer)
        end
    end

    return ret
end

function PANEL:Init()
    self.PrinterPanels = {}

    self.CanTabletBeClosed = true
    self.NextTabletClose = CurTime() + 1.5

    self.Scroll = vgui.Create("DScrollPanel", self)
    self:PaintScrollbar()

    for _, printer in pairs(GetOwnedPrinters()) do
        local pnl = vgui.Create("DPanel", self.Scroll)
        pnl.Paint = self.PaintPrinterPanel
        pnl.Printer = printer
        pnl.Buttons = {}
        pnl.Think = function(slf)
            if ValidPanel(slf) then
                if !IsValid(slf.Printer) then
                    table.RemoveByValue(self.PrinterPanels, slf)
                    slf:Remove()
                end
            end
        end

        for i = 1, 3 do
            local btn = vgui.Create("DButton", pnl)
            btn:SetText("")
            btn.Type = i
            btn.Paint = self.PaintButton
            btn.DoClick = function()
                local nw = i == 1 and "animelife.hentaiprinters.request_money" or (i == 2 and "animelife.hentaiprinters.buy_cooling" or "animelife.hentaiprinters.buy_upgrade")
                net.Start(nw)
                    net.WriteEntity(pnl.Printer)
                net.SendToServer()

                surface.PlaySound("click02.wav")
            end

            table.insert(pnl.Buttons, btn)
        end

        table.insert(self.PrinterPanels, pnl)
    end
end

function PANEL:PerformLayout(w, h)
    self.Scroll:SetPos(64, 256)
    self.Scroll:SetSize(w - 128, h - 380)

    local y = 0
    for _, pnl in pairs(self.PrinterPanels) do
        pnl:SetPos((self.Scroll:GetWide() - 1079) / 2, y)
        pnl:SetSize(1079, 102)

        local x = 526
        for _, btn in pairs(pnl.Buttons) do
            btn:SetSize(157, 41)
            btn:SetPos(x, 32)

            x = x + 157 + 19
        end

        y = y + 102 + 16
    end
end

function PANEL:Think()
    if !self.CanTabletBeClosed then return end
    if (self.NextTabletClose or 0) < CurTime() then
        if LocalPlayer():KeyDown(IN_FORWARD) or LocalPlayer():KeyDown(IN_BACK) or LocalPlayer():KeyDown(IN_MOVELEFT) or LocalPlayer():KeyDown(IN_MOVERIGHT) then
            if ValidPanel(self) then
                self:Remove()

                gui.EnableScreenClicker(false)
            end
        end
    end
end

function PANEL:Paint(w, h)
    draw.RoundedBox(56, 0, 0, w, h, Color(0, 0, 0))
    draw.RoundedBox(32, (w - 1200) / 2, (h - 736) / 2, 1200, 736, PANEL_BG_COLOR)
    ui.DrawMaterial(PANEL_MATERIAL_BACKGROUND, (w - 1206) / 2, (h - 747) / 2, 1206, 747, Color(255, 255, 255))

    ui.DrawMaterial(PANEL_MATERIAL_LOGO, (w - 102) / 2, 85, 102, 85, Color(255, 255, 255))

    local printers_num = table.Count(GetOwnedPrinters())

    draw.SimpleText("Ваши Hentai принтеры (" .. printers_num .. ")", "animelife.hentaiprinter.tablet.gui.title", w / 2, 195, Color(255, 255, 255), 1)

    local y = 247 + (printers_num * (102 + 16)) + 16
    ui.DrawMaterial(PANEL_MATERIAL_LINES, (w - 875) / 2, y, 875, 3, Color(255, 255, 255))

    local total_sum = 0
    for _, printer in pairs(GetOwnedPrinters()) do
        local printed = printer:GetPrinted() or 0
        total_sum = total_sum + printed
    end

    draw.SimpleText("Итого накоплено: " .. string.Comma(total_sum) .. "¥", "animelife.hentaiprinter.tablet.gui.title", w / 2, h - 80, Color(255, 255, 255), 1, 4)
end

function PANEL:PaintScrollbar()
    local v_bar = self.Scroll:GetVBar()
    v_bar:SetWide(6)

    v_bar.Paint = function() end
    v_bar.btnGrip.Paint = function(pnl, w, h)
        draw.RoundedBox(6, 0, 0, w, h, Color(0, 0, 0, 150))
    end
    v_bar.btnUp.Paint = function() end
    v_bar.btnDown.Paint = function() end
end

function PANEL:PaintPrinterPanel(w, h)
    draw.RoundedBox(16, 0, 0, w, h, Color(0, 0, 0, 66))

    local mat = GLOBALS_HENTAIPRINTERS_GIRLS[self.Printer:GetGirl()].TabletMaterial
    ui.DrawMaterial(mat, 44, (h - 67) / 2 + 2, 67, 67, Color(255, 255, 255))

    draw.SimpleText(self.Printer:GetPrinterLevel() or 1, "animelife.hentaiprinter.tablet.gui.title", 77, h / 2 - 1, Color(216, 250, 255), 1, 1)

    draw.SimpleText("Ваш личный " .. self.Printer:GetGirl() .. " Hentai Printer", "animelife.hentaiprinter.tablet.gui.type", 151, 29, Color(158, 158, 158))
    draw.SimpleText("¥" .. string.Comma(self.Printer:GetPrinted() or 0) .. " :: " .. (self.Printer:GetTemperature() or 0) .. " °C", "animelife.hentaiprinter.tablet.gui.title", 151, 52, Color(255, 255, 255))
end

function PANEL:PaintButton(w, h)
    draw.RoundedBox(16, 0, 0, w, h, Color(46, 51, 61))

    local text = ""
    if self.Type == 1 then
        if IsValid(self:GetParent().Printer) then
            local printed = self:GetParent().Printer:GetPrinted() or 0
            text = "Снять " .. string.Comma(printed) .. "¥"
        end
    elseif self.Type == 2 then
        text = "Купить охлаждение: 150¥"
    elseif self.Type == 3 then
        if IsValid(self:GetParent().Printer) then
            local upgrade_price = hentaiprinter_upgradeformula(self:GetParent().Printer:GetGirl(), self:GetParent().Printer:GetPrinterLevel())
            text = "Апгрейд: " .. string.Comma(upgrade_price) .. "¥"
        end
    end

    draw.SimpleText(text, "animelife.hentaiprinter.tablet.gui.type", w / 2, h / 2, Color(255, 255, 255), 1, 1)
end

vgui.Register("animelife.hentaiprintertablet.gui", PANEL, "EditablePanel")

function ShowHentaiPrinterTabletMenu()
    if ValidPanel(GLOBALS_HENTAIPRINTERTABLET_GUI) then
        GLOBALS_HENTAIPRINTERTABLET_GUI:Remove()
        GLOBALS_HENTAIPRINTERTABLET_GUI = nil 
        
        gui.EnableScreenClicker(false)

        return
    end

    GLOBALS_HENTAIPRINTERTABLET_GUI = vgui.Create("animelife.hentaiprintertablet.gui")
    GLOBALS_HENTAIPRINTERTABLET_GUI:SetSize(1316, 840)
    GLOBALS_HENTAIPRINTERTABLET_GUI:SetPos((ScrW() - 1316) / 2, (ScrH() - 840) / 2 + 32)
    GLOBALS_HENTAIPRINTERTABLET_GUI:MoveTo((ScrW() - 1316) / 2, (ScrH() - 840) / 2, 0.25)
    GLOBALS_HENTAIPRINTERTABLET_GUI:SetAlpha(0)
    GLOBALS_HENTAIPRINTERTABLET_GUI:AlphaTo(255, 0.15)

    gui.EnableScreenClicker(true)
end

net.Receive("animelife.hentaiprinters.tablet.menu", function()
    if LocalPlayer():Team() ~= TEAM_HACKER then return end

    ShowHentaiPrinterTabletMenu()
end)