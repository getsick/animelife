local common_girl = Material("animelife/hentaiprinters/3d/girl_common.png")
local uncommon_girl = Material("animelife/hentaiprinters/3d/girl_uncommon.png")
local rare_girl = Material("animelife/hentaiprinters/3d/girl_rare.png")
local immortal_girl = Material("animelife/hentaiprinters/3d/girl_immortal.png")

local common_tablet = Material("animelife/hentaiprinters/tablet/circle_common.png")
local uncommon_tablet = Material("animelife/hentaiprinters/tablet/circle_uncommon.png")
local rare_tablet = Material("animelife/hentaiprinters/tablet/circle_rare.png")
local immortal_tablet = Material("animelife/hentaiprinters/tablet/circle_immortal.png")

GLOBALS_HENTAIPRINTERS_GIRLS = {
    ["Common"] = {
        Price = 3000,
        Color = Color(255, 104, 104),
        Tier = 1,
        Yield = 105,
        UpgradeAdd = 3,
        Material = common_girl,
        TabletMaterial = common_tablet,
    },
    ["Uncommon"] = {
        Price = 6000,
        Color = Color(128, 125, 255),
        Tier = 2,
        Yield = 135,
        UpgradeAdd = 3,
        Material = uncommon_girl,
        TabletMaterial = uncommon_tablet,
    },
    ["Rare"] = {
        Price = 10000,
        Color = Color(255, 125, 180),
        Tier = 3,
        Yield = 170,
        UpgradeAdd = 5,
        Material = rare_girl,
        TabletMaterial = rare_tablet,
    },
    ["Immortal"] = {
        Price = 15000,
        Color = Color(255, 155, 209),
        Tier = 4,
        Yield = 250,
        UpgradeAdd = 7,
        Material = immortal_girl,
        TabletMaterial = immortal_tablet,
    },
    ["Immortal+"] = {
        Price = 17000,
        Color = Color(255, 155, 209),
        Tier = 4,
        Yield = 255,
        UpgradeAdd = 8,
        Material = immortal_girl,
        TabletMaterial = immortal_tablet,
        VIP = true
    }
}

function hentaiprinter_upgradeformula(tier, lvl)
    tier = GLOBALS_HENTAIPRINTERS_GIRLS[tier].Tier
    return (100 * tier) + (100 * (lvl + 1))
end

local ENT = {}

ENT.Base = "base_anim"
ENT.Type = "anim"

ENT.PrintName = "Hentai Printers!"
ENT.Author = "AnimeLife"

ENT.Model = Model("models/props_c17/consolebox03a.mdl")
ENT.Spawnable = true
ENT.AdminOnly = true

ENT.PrintTime = 60

ENT.RenderGroup = RENDERGROUP_BOTH

function ENT:SetupDataTables()
    self:NetworkVar("String", 0, "Girl")
    self:NetworkVar("Int", 0, "Temperature")
    self:NetworkVar("Int", 1, "Printed")
    self:NetworkVar("Int", 2, "PrinterLevel")
    self:NetworkVar("Entity", 0, "PrinterOwner")
end

function ENT:Initialize()
    if CLIENT then return end

    self:SetModel(self.Model)
    self:PhysicsInit(SOLID_VPHYSICS)

    self:SetUseType(SIMPLE_USE)

    local phys = self:GetPhysicsObject()
    if IsValid(phys) then
        phys:Wake()
    end

    self:SetTemperature(10)
    self:SetGirl("Common")
    self:SetPrinterLevel(1)

    self:SetMaterial("models/debug/debugwhite")

    local girl = self:GetGirl() or "Common"
    self:SetColor(GLOBALS_HENTAIPRINTERS_GIRLS[girl].Color)

    self.NextPrint = SysTime() + self.PrintTime
    self.NextTempAdd = SysTime() + (self.PrintTime / 2)
end

function ENT:StartSound()
    self.MouthSound = CreateSound(self, "animelife/hentaiprinter.mp3")
    self.MouthSound:SetSoundLevel(52)
    self.MouthSound:PlayEx(1, 100)
end

function ENT:OnRemove()
    if self.MouthSound then
        self.MouthSound:Stop()
    end
end

function ENT:Explode()
    if SERVER then
        local pos = self:GetPos()
        local ef = EffectData()
        ef:SetStart(pos)
        ef:SetOrigin(pos)
        ef:SetScale(1)
        util.Effect("balloon_pop", ef)

        self:EmitSound("animelife/hentaiprinter_explode.mp3")

        if IsValid(self:GetPrinterOwner()) then 
            DarkRP.notify(self:GetPrinterOwner(), 1, 4, DarkRP.getPhrase("money_printer_exploded")) 
        end

        SafeRemoveEntity(self)
    end
end

function ENT:OnTakeDamage(dmg)
    if SERVER then
        self:Explode()
    end
end

function ENT:Think()
    if SERVER then
        if self:WaterLevel() > 0 then
            self:Explode()
        end

        if self:GetTemperature() >= 100 then
            self:Explode()
        end

        if self.NextPrint < SysTime() then
            local yield = GLOBALS_HENTAIPRINTERS_GIRLS[self:GetGirl()].Yield or 105
            local add = (GLOBALS_HENTAIPRINTERS_GIRLS[self:GetGirl()].UpgradeAdd or 3) * (self:GetPrinterLevel() - 1)
            self:SetPrinted((self:GetPrinted() or 0) + yield + add)

            self.NextPrint = SysTime() + self.PrintTime
        end

        if self.NextTempAdd < SysTime() then
            self:SetTemperature(self:GetTemperature() + 1)

            self.NextTempAdd = SysTime() + (self.PrintTime / 2)
        end
    end

    self:StartSound()
end

function ENT:Use(ply)
    if !IsValid(ply) then return end

    net.Start("animelife.hentaiprinters.menu")
        net.WriteEntity(self)
    net.Send(ply)
end

if CLIENT then
    surface.CreateFont("animelife.hentaiprinters.type", {font = "Exo 2 SemiBold", size = 32, weight = 600, extended = true})
    surface.CreateFont("animelife.hentaiprinters.owner", {font = "Exo 2 SemiBold", size = 38, weight = 600, extended = true})
    surface.CreateFont("animelife.hentaiprinters.info", {font = "Exo 2 SemiBold", size = 26, weight = 600, extended = true})
    surface.CreateFont("animelife.hentaiprinters.collected", {font = "Exo 2 SemiBold", size = 32, weight = 600, extended = true})

    function ENT:Draw()
        self:DrawModel()
    end

    local circle_mat = Material("animelife/hentaiprinters/circle.png")
    function ENT:DrawTranslucent()
        local pos = self:LocalToWorld(Vector(0, 0, 8))
        local ang = self:LocalToWorldAngles(Angle(0, 90, 0))
        local dist = LocalPlayer():GetPos():DistToSqr(self:GetPos())
        if dist > 1024^2 then return end
        
        cam.Start3D2D(pos, ang, 0.05)
            local girl = self:GetGirl()

            surface.SetDrawColor(255, 255, 255)
            surface.SetMaterial(circle_mat)
            surface.DrawTexturedRect(-170, -72, 396 / 1.7, 396 / 1.7)

            surface.SetMaterial(GLOBALS_HENTAIPRINTERS_GIRLS[girl].Material)
            surface.DrawTexturedRect(-20, -28, 313/ 1.7, 313 / 1.7)

            draw.SimpleText(girl, "animelife.hentaiprinters.type", 0, -165, Color(255, 255, 255), 1)

            local owner = "Отключившийся игрок"
            if IsValid(self:GetPrinterOwner()) then
                owner = utf8.sub(self:GetPrinterOwner():Nick(), 1, 27)
            end
            draw.SimpleText(owner, "animelife.hentaiprinters.owner", 0, -125, Color(255, 255, 255), 1)

            draw.SimpleText("Накоплено", "animelife.hentaiprinters.info", -82, 8, Color(255, 255, 255), 1)
            draw.SimpleText(string.Comma(self:GetPrinted()) .. "¥", "animelife.hentaiprinters.collected", -82, 32 + 8, Color(255, 255, 255), 1)

            draw.SimpleText(self:GetTemperature() .. " °C", "animelife.hentaiprinters.collected", -56, -38, Color(255, 255, 255, 150), 1)

            local lvl = self:GetPrinterLevel() or 1
            draw.SimpleText("Lv " .. lvl, "animelife.hentaiprinters.collected", -72, 90, Color(255, 255, 255, 150), 1)
        cam.End3D2D()
    end
end

scripted_ents.Register(ENT, "animelife_hentaiprinter")