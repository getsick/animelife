local PANEL = {}
local PANEL_BG_COLOR = Color(35, 35, 35)
local PANEL_MATERIAL_CIRCLE = Material("animelife/hentaiprinters/menu/level_circle.png")
local PANEL_MATERIAL_UPGRADE = Material("animelife/hentaiprinters/menu/icon_upgrade.png")

surface.CreateFont("animelife.hentaiprinter.gui.title", {font = "Exo 2 SemiBold", size = 21, weight = 600, extended = true})
surface.CreateFont("animelife.hentaiprinter.gui.info", {font = "Exo 2 SemiBold", size = 17, weight = 600, extended = true})
surface.CreateFont("animelife.hentaiprinter.gui.level", {font = "Exo 2 SemiBold", size = 43, weight = 600, extended = true})
surface.CreateFont("animelife.hentaiprinter.gui.button", {font = "Exo 2 SemiBold", size = 19, weight = 600, extended = true})

function PANEL:Init()
    self.TakeButton = vgui.Create("DButton", self)
    self.TakeButton:SetText("")
    self.TakeButton.Type = 1
    self.TakeButton.Paint = self.PaintButton
    self.TakeButton.DoClick = function()
        net.Start("animelife.hentaiprinters.request_money")
            net.WriteEntity(self.Printer)
        net.SendToServer()

        surface.PlaySound("click02.wav")
    end

    self.CoolButton = vgui.Create("DButton", self)
    self.CoolButton:SetText("")
    self.CoolButton.Type = 2
    self.CoolButton.Paint = self.PaintButton
    self.CoolButton.DoClick = function()
        net.Start("animelife.hentaiprinters.buy_cooling")
            net.WriteEntity(self.Printer)
        net.SendToServer()

        surface.PlaySound("click02.wav")
    end

    self.UpgradeButton = vgui.Create("DButton", self)
    self.UpgradeButton:SetText("")
    self.UpgradeButton.Type = 3
    self.UpgradeButton.Paint = self.PaintButton
    self.UpgradeButton.DoClick = function()
        net.Start("animelife.hentaiprinters.buy_upgrade")
            net.WriteEntity(self.Printer)
        net.SendToServer()

        surface.PlaySound("click02.wav")
    end
end

function PANEL:PerformLayout(w, h)
    self.TakeButton:SetSize(257, 50)
    self.TakeButton:SetPos(47, 366)

    self.CoolButton:SetSize(257, 50)
    self.CoolButton:SetPos(47, 437)

    self.UpgradeButton:SetSize(65, 121)
    self.UpgradeButton:SetPos(331, 366)
end

function PANEL:Think()
    if LocalPlayer():KeyDown(IN_FORWARD) or LocalPlayer():KeyDown(IN_BACK) or LocalPlayer():KeyDown(IN_MOVELEFT) or LocalPlayer():KeyDown(IN_MOVERIGHT) then
        if ValidPanel(self) then
            self:Remove()

            gui.EnableScreenClicker(false)
        end
    end

    if !IsValid(self.Printer) then
        if ValidPanel(self) then
            self:Remove()
            
            gui.EnableScreenClicker(false)
        end
    end
end

function PANEL:Paint(w, h)
    draw.RoundedBox(16, 0, 0, w, h, PANEL_BG_COLOR)

    draw.SimpleText("Hentai Printer (" .. self.Printer:GetGirl() .. ")", "animelife.hentaiprinter.gui.title", 51, 36, Color(255, 255, 255))

    local owner = "Отключившийся игрок"
    if IsValid(self.Printer:GetPrinterOwner()) then
        owner = self.Printer:GetPrinterOwner():Nick()
    end

    local yield = GLOBALS_HENTAIPRINTERS_GIRLS[self.Printer:GetGirl()].Yield or 105
    local add = (GLOBALS_HENTAIPRINTERS_GIRLS[self.Printer:GetGirl()].UpgradeAdd or 3) * (self.Printer:GetPrinterLevel() - 1)

    local iy = 74
    for i = 1, 3 do
        draw.SimpleText(i == 1 and "Владелец: " .. owner or (i == 2 and string.Comma(yield + add) .. "¥/мин." or "+1 °C/30 с."), "animelife.hentaiprinter.gui.info", w / 2, iy, Color(223, 223, 223), 1)

        iy = iy + 17 + 4
    end

    ui.DrawMaterial(PANEL_MATERIAL_CIRCLE, (w - 130) / 2, 154, 130, 130, Color(255, 255, 255))

    local lvl = self.Printer:GetPrinterLevel()

    surface.SetFont("animelife.hentaiprinter.gui.title")
    local tw = surface.GetTextSize("Lv")
    surface.SetFont("animelife.hentaiprinter.gui.level")
    local lw = surface.GetTextSize(lvl .. "/10")
    local tx = (w - (tw + 24 + lw - 16)) / 2

    draw.SimpleText("Lv", "animelife.hentaiprinter.gui.title", tx, 195 + 17, Color(255, 255, 255))
    draw.SimpleText(lvl .. "/10", "animelife.hentaiprinter.gui.level", tx + 24, 195, Color(210, 255, 252))

    draw.SimpleText("Температура: " .. self.Printer:GetTemperature() .. " °C", "animelife.hentaiprinter.gui.title", w / 2, 305, Color(255, 255, 255), 1)
end

function PANEL:PaintButton(w, h)
    draw.RoundedBox(6, 0, 0, w, h, Color(57, 160, 255))

    if self.Type == 1 then
        if IsValid(self:GetParent().Printer) then
            local printed = self:GetParent().Printer:GetPrinted() or 0
            draw.SimpleText("Снять " .. string.Comma(printed) .. "¥", "animelife.hentaiprinter.gui.button", w / 2, h / 2, Color(255, 255, 255), 1, 1)
        end
    elseif self.Type == 2 then
        draw.SimpleText("Купить охлаждение: 150¥", "animelife.hentaiprinter.gui.button", w / 2, h / 2, Color(255, 255, 255), 1, 1)
    elseif self.Type == 3 then
        ui.DrawMaterial(PANEL_MATERIAL_UPGRADE, (w - 40) / 2, 21, 40, 40, Color(255, 255, 255))

        draw.SimpleText("Lv up", "animelife.hentaiprinter.gui.info", w / 2, 68, Color(255, 255, 255), 1)

        if IsValid(self:GetParent().Printer) then
            local upgrade_price = hentaiprinter_upgradeformula(self:GetParent().Printer:GetGirl(), self:GetParent().Printer:GetPrinterLevel())
            draw.SimpleText(string.Comma(upgrade_price) .. "¥", "animelife.hentaiprinter.gui.button", w / 2, 91, Color(255, 255, 255), 1)
        end
    end
end

vgui.Register("animelife.hentaiprinter.gui", PANEL, "EditablePanel")

function ShowHentaiPrinterMenu(printer)
    if ValidPanel(GLOBALS_HENTAIPRINTER_GUI) then
        GLOBALS_HENTAIPRINTER_GUI:Remove()
        GLOBALS_HENTAIPRINTER_GUI = nil 
        
        gui.EnableScreenClicker(false)

        return
    end

    GLOBALS_HENTAIPRINTER_GUI = vgui.Create("animelife.hentaiprinter.gui")
    GLOBALS_HENTAIPRINTER_GUI:SetSize(444, 531)
    GLOBALS_HENTAIPRINTER_GUI:SetPos((ScrW() - 444) / 2, ScrH() - 531)
    GLOBALS_HENTAIPRINTER_GUI:MoveTo((ScrW() - 444) / 2, ScrH() - 531 - 90, 0.25)
    GLOBALS_HENTAIPRINTER_GUI:SetAlpha(0)
    GLOBALS_HENTAIPRINTER_GUI:AlphaTo(255, 0.15)
    GLOBALS_HENTAIPRINTER_GUI.Printer = printer

    gui.EnableScreenClicker(true)
end

net.Receive("animelife.hentaiprinters.menu", function()
    local printer = net.ReadEntity()
    if !IsValid(printer) then return end

    ShowHentaiPrinterMenu(printer)
end)