util.AddNetworkString("animelife.inventory.networking")
util.AddNetworkString("animelife.inventory.useitem")
util.AddNetworkString("animelife.inventory.sellitem")
util.AddNetworkString("animelife.inventory.stashitem")

module("inventory", package.seeall)

DEBUG = true -- should be false on release
MAX_SLOTS_MAIN = 30
MAX_SLOTS_MAIN_DONATED = 45 -- for those who donated
ITEMS = {}

function inventory:InitOnPlayer(ply)
    if istable(ply.Inventory) then
        return 
    end

    ply.Inventory = {
        ["Main Section"] = {},
        ["Accessories Section"] = {}
    }

    local donated = false
    local max_slots = donated and MAX_SLOTS_MAIN_DONATED or MAX_SLOTS_MAIN
    for i = 1, max_slots do
        ply.Inventory["Main Section"][i] = {} -- fill with empty slots
        ply.Inventory["Accessories Section"][i] = {}
    end

    -- TODO: Accessories should be unlimited, have to store things in table ontop of each other
end

function inventory:Network(ply, data, category, slot)
    data = util.TableToJSON(data)
    data = util.Compress(data)
    local len = data:len()
    net.Start("animelife.inventory.networking")
        net.WriteInt(len, 32)
        net.WriteData(data, len)
        net.WriteString(category)
        net.WriteInt(slot, 32)
    net.Send(ply)
end

function inventory:AddItem(ply, slot, item, amount, expires_in, from_store)
    if !istable(ply.Inventory) then
        self:InitOnPlayer(ply)
    end

    local item_cat = InventoryItems[item].Section
    local item_data = InventoryItems[item]

    local job_table = ply.getJobTable and ply:getJobTable() or nil
    if job_table and table.HasValue(job_table.weapons, item_data.Class) and !from_store then
        DarkRP.notify(ply, 1, 5, "Этот предмет нельзя положить в инвентарь.")
        return
    end

    -- should be removed from inventory on the day x
    local date = 0
    if isnumber(expires_in) then
        date = os.time() + (expires_in * 86400)
        -- let's see if user had an exact same item already
        if ply.Inventory[item_cat][slot] then
            if ply.Inventory[item_cat][slot].ExpirationDate and ply.Inventory[item_cat][slot].Class == item then
                -- extra!
                date = date + (ply.Inventory[item_cat][slot].ExpirationDate - os.time())
            end
        end
    end

    ply.Inventory[item_cat][slot] = {
        Class = item,
        Name = item_data.Name or "Unknown Item",
        Tier = item_data.Tier or 0,
        Model = item_data.Model or "models/error.mdl",
        Droppable = item_data.Droppable,
        IsAccessories = item_data.Accessories,
        Amount = amount,
    }

    if isnumber(expires_in) then
        ply.Inventory[item_cat][slot].ExpirationDate = date
    end

    -- add more info to it
    if item_data.Accessories then
        ply.Inventory[item_cat][slot] = {
            AccessoriesCategory = item_data.AccessoriesCategory,
        }
    end

    -- process on client
    self:Network(ply, ply.Inventory[item_cat][slot], item_cat, slot)

    -- save changes in database
    self:Save(ply)

    if DEBUG then
        print("insert new item (" .. item .. ") for player", ply, "into slot", slot)
    end
end

function inventory:RemoveItem(ply, section, slot, sell, expiring_take)
    if !istable(ply.Inventory) then
        self:InitOnPlayer(ply)
    end

    local item = ply.Inventory[section][slot]

    -- accessories are yours forever
    if !expiring_take then
        if !sell and InventoryItems[item.Class].NotDecreasable then return end
    end

    if item.Amount > 1 and !expiring_take then
        ply.Inventory[section][slot].Amount = item.Amount - 1
    else
        ply.Inventory[section][slot] = {}
    end

    -- fill with emptiness ><
    self:Network(ply, ply.Inventory[section][slot], section, slot)

    -- save changes in database
    self:Save(ply)

    if DEBUG then
        print("deleting item for player", ply, "from slot", slot, "in section of", section)
    end
end

function inventory:SlotBusy(ply, section, slot)
    if !istable(ply.Inventory) then
        return true
    end

    return ply.Inventory[section][slot] ~= {}
end

function inventory:FindEmptySlot(ply, item)
    if !istable(ply.Inventory) then
        self:InitOnPlayer(ply)
    end

    local cat = InventoryItems[item].Section

    local function stack_slot()
        for slot, v in pairs(ply.Inventory[cat]) do
            if table.IsEmpty(v) then continue end

            -- found a place! stack 'em
            if v.Class == item then
                return slot, v.Amount
            end
        end

        return false
    end

    local res_slot, res_amount = stack_slot()
    if isnumber(res_slot) then
        return res_slot, res_amount
    end

    local function empty_slot()
        for slot, v in pairs(ply.Inventory[cat]) do
            if table.IsEmpty(v) then return slot end
        end

        return false
    end

    res_slot = empty_slot()
    return res_slot
end

net.Receive("animelife.inventory.useitem", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply.Inventory then return end
    if ply:GetNWBool("animelife.administration.ban", false) or ply:GetNWBool("animelife.administration.jail", false) then
        DarkRP.notify(ply, 1, 5, "Вы наказаны. Доступ к инвентарю ограничен.")
        return
    end

    local cat = net.ReadString()
    local slot = net.ReadInt(32)
    if !inventory:SlotBusy(ply, cat, slot) then return end

    local item = ply.Inventory[cat][slot].Class
    local restricted = TeamRestrictedItems[ply:Team()]
    if restricted and table.HasValue(restricted, item) then
        DarkRP.notify(ply, 1, 5, "Вы не можете использовать этот предмет на этой профессии.")
        return
    end

    if ply:HasWeapon(InventoryItems[item].Class) then return end

    -- ply:Give(InventoryItems[item].Class)
    local status = InventoryItems[item].OnUse(ply)
    if status == "fail" then return end

    inventory:RemoveItem(ply, cat, slot)
end)

net.Receive("animelife.inventory.sellitem", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply.Inventory then return end

    local cat = net.ReadString()
    local slot = net.ReadInt(32)
    if !inventory:SlotBusy(ply, cat, slot) then return end

    local item = ply.Inventory[cat][slot].Class
    if InventoryItems[item].Price.Yen == -1 then return end

    inventory:RemoveItem(ply, cat, slot, true)

    ply:addMoney(InventorySellPrice(InventoryItems[item].Price.Yen))
end)

net.Receive("animelife.inventory.stashitem", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply.Inventory then return end

    local cat = net.ReadString()
    local slot = net.ReadInt(32)
    if !inventory:SlotBusy(ply, cat, slot) then return end

    local item = ply.Inventory[cat][slot].Class

    inventory:RemoveItem(ply, cat, slot, true)
end)

concommand.Add("al_invweapon", function(ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    -- if !ply.Inventory then return end

    local wep = ply:GetActiveWeapon()
    if !IsValid(wep) then return end

    local item = (function(class)
        for key, item in pairs(InventoryItems or {}) do
            if item.Class == class then
                return key
            end
        end

        return nil
    end)(wep:GetClass())
    if item == nil then return end

    local job_table = ply.getJobTable and ply:getJobTable() or nil
    if job_table and table.HasValue(job_table.weapons, wep:GetClass()) then
        DarkRP.notify(ply, 1, 5, "Этот предмет нельзя положить в инвентарь.")
        return
    end

    if classchooser and classchooser.List[ply:Team()] then
        local class = ply:GetNWInt("animelife.classchooser.class", -1)
        if class ~= -1 then
            local class_weapons = classchooser.List[ply:Team()][class].Weapons
            if table.HasValue(class_weapons, wep:GetClass()) then
                DarkRP.notify(ply, 1, 5, "Этот предмет нельзя положить в инвентарь.")
                return
            end
        end
    end

    local empty_slot, amount = inventory:FindEmptySlot(ply, item)
    -- couldn't find one
    if empty_slot == false then
        DarkRP.notify(ply, 1, 5, "Инвентарь заполнен.") 
        return false
    end

    inventory:AddItem(ply, empty_slot, item, isnumber(amount) and amount + 1 or 1)

    ply:StripWeapon(wep:GetClass())

    DarkRP.notify(ply, 0, 5, "Оружие убрано в инвентарь.")
end)

hook.Add("PlayerButtonDown", "animelife.inventory.openmenu", function(ply, btn)
    if btn == KEY_I then
        ply:ConCommand("al_inventory")
    end
end)

hook.Add("PlayerPostThink", "animelife.inventory.expiringitems", function(ply)
    if !IsValid(ply) then return end
    if !ply.Inventory then return end

    for cat, slot in pairs(ply.Inventory) do
        for id, item in pairs(slot) do
            if !item.ExpirationDate then continue end

            if item.ExpirationDate < os.time() then
                -- remove item logic
                inventory:RemoveItem(ply, cat, id, false, true)
            end
        end
    end
end)