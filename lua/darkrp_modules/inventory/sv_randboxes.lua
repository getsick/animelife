function InventoryOpenBox(ply, items)
    if !InventoryItems[items] then return end
    
    local days = InventoryItems[items].Duration
    items = InventoryItems[items].Items

    local function probably(percentage)
        return math.Rand(0, 1) * 100 < percentage
    end   

    local resulting_prize = nil
    for _, prize in RandomPairs(items) do
        if probably(prize[2]) then
            -- print("prize is: ", prize[1])
            resulting_prize = prize[1]
            break
        end
    end

    if !resulting_prize then
        resulting_prize = table.Random(items)[1]
    end

    -- the uselessness of this function is actually a good thing, it seems
    if globalstore then
        globalstore.Purchase(ply, resulting_prize, days)
    end

    return resulting_prize
end