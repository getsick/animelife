ITEM.Name = "Test Weapon"
ITEM.Description = "Weapon to test inventory"
ITEM.Model = "models/props_borealis/bluebarrel001.mdl"

ITEM.IsWeapon = true
ITEM.Class = "lowpoly_ak74"

ITEM.CanTrade = true
ITEM.CanDrop = true

ITEM.Section = "Main Section"

function ITEM:OnUse(ply)
    ply:Give(self.Class)
end