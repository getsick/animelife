module("inventory.accessories", package.seeall)

List = {
    ["accessories_hatllenn"] = {
        Model = Model("models/prop/llenn_hat.mdl"),
        Category = "Head",
        Attachment = "anim_attachment_head",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            scale = 0.71
            pos = pos + (ang:Right() * -4)
            ang:RotateAroundAxis(ang:Forward(), 15)
            ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_afro"] = {
        Model = Model("models/gmod_tower/afro.mdl"),
        Category = "Head",
        Attachment = "anim_attachment_head",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * -5)
            ang:RotateAroundAxis(ang:Forward(), 25)
            ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_nojiggle"] = {
        Model = Model("models/gmod_tower/balloonicorn_nojiggle.mdl"),
        Category = "Head",
        Attachment = "anim_attachment_head",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            scale = 0.2
            pos = pos + (ang:Right() * -10)
            ang:RotateAroundAxis(ang:Forward(), 15)
            ang:RotateAroundAxis(ang:Up(), 0)
            ang:RotateAroundAxis(ang:Right(), 180)

            return pos, ang, scale
        end
    },
    ["accessories_neko"] = {
        Model = Model("models/gmod_tower/catears.mdl"),
        Category = "Head",
        Attachment = "anim_attachment_head",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * -4)
            ang:RotateAroundAxis(ang:Forward(), 25)
            ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_headphones"] = {
        Model = Model("models/gmod_tower/headphones.mdl"),
        Category = "Head",
        Attachment = "anim_attachment_head",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * -2) + (ang:Up() * -1)
            ang:RotateAroundAxis(ang:Forward(), 10)
            ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_santahat"] = {
        Model = Model("models/captainbigbutt/skeyler/hats/santa.mdl"),
        Category = "Head",
        Attachment = "anim_attachment_head",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            scale = 0.7
            pos = pos + (ang:Right() * -5)
            ang:RotateAroundAxis(ang:Forward(), 25)
            ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_starglasses"] = {
        Model = Model("models/gmod_tower/starglasses.mdl"),
        Category = "Eyes",
        Attachment = "eyes",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Forward() * -1.6)

            return pos, ang, scale
        end
    },
    ["accessories_winterholiday"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/winterholiday.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 5)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_snowman"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/snowman.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 5)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_holidaygift"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/tacticalsanta.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 5)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_icecube"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/yeti.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 5)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_blackwidow"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/blackwidow.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 6) + (ang:Forward() * -4)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_raven"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/raven.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 3.5) + (ang:Forward() * -4)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_vampire"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/vampire.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 9) + (ang:Forward() * -8)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_samuraiultra"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/samuraiultra.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 7) + (ang:Forward() * -2)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_moth"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/moth.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 4) + (ang:Forward() * -20)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 268)

            return pos, ang, scale
        end
    },
    ["accessories_darkviking"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/darkviking.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 7) + (ang:Forward() * -2)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_ghoul"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/winterghoul.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 8) + (ang:Forward() * 12)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_katana"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/werewolf.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 3)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_disco"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/exercisemale.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 5)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_samuraiblue"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/samuraiblue.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 3)
            ang:RotateAroundAxis(ang:Forward(), 210)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_guitar"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/hippieguitar.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 5)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_garageband"] = {
        Model = Model("models/konnie/asapgaming/fortnite/backpacks/garagebandmale.mdl"),
        Category = "Spine",
        Bone = "ValveBiped.Bip01_Spine",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * 5)
            ang:RotateAroundAxis(ang:Forward(), 90)
            -- ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_strawhat"] = {
        Model = Model("models/captainbigbutt/skeyler/hats/strawhat.mdl"),
        Category = "Head",
        Attachment = "anim_attachment_head",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * -6) + (ang:Up() * -0.5)
            ang:RotateAroundAxis(ang:Forward(), 10)
            ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_bunnyears"] = {
        Model = Model("models/captainbigbutt/skeyler/hats/bunny_ears.mdl"),
        Category = "Head",
        Attachment = "anim_attachment_head",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * -5)
            ang:RotateAroundAxis(ang:Forward(), 25)
            ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_sunhat"] = {
        Model = Model("models/captainbigbutt/skeyler/hats/sunhat.mdl"),
        Category = "Head",
        Attachment = "anim_attachment_head",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * -2.6) + (ang:Up() * 2.5)
            ang:RotateAroundAxis(ang:Forward(), 10)
            ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_ducktube"] = {
        Model = Model("models/captainbigbutt/skeyler/accessories/duck_tube.mdl"),
        Category = "Head",
        Attachment = "anim_attachment_head",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * -5)
            ang:RotateAroundAxis(ang:Forward(), 25)
            ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_crown"] = {
        Model = Model("models/lordvipes/peachcrown/peachcrown.mdl"),
        Category = "Head",
        Attachment = "anim_attachment_head",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Right() * -4) + (ang:Up() * -2)
            ang:RotateAroundAxis(ang:Forward(), 10)
            ang:RotateAroundAxis(ang:Up(), 90)
            ang:RotateAroundAxis(ang:Right(), 270)

            return pos, ang, scale
        end
    },
    ["accessories_glasses04"] = {
        Model = Model("models/captainbigbutt/skeyler/accessories/glasses04.mdl"),
        Category = "Eyes",
        Attachment = "eyes",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            scale = 0.8
            pos = pos + (ang:Forward() * -1.1) + (ang:Up() * -0.8)

            return pos, ang, scale
        end
    },
    ["accessories_glasses02"] = {
        Model = Model("models/captainbigbutt/skeyler/accessories/glasses02.mdl"),
        Category = "Eyes",
        Attachment = "eyes",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            scale = 0.8
            pos = pos + (ang:Forward() * -1.1) + (ang:Up() * -0.8)

            return pos, ang, scale
        end
    },
    ["accessories_3dglasses"] = {
        Model = Model("models/gmod_tower/3dglasses.mdl"),
        Category = "Eyes",
        Attachment = "eyes",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Forward() * -1.6)

            return pos, ang, scale
        end
    },
    ["accessories_snowboard"] = {
        Model = Model("models/gmod_tower/snowboardgoggles.mdl"),
        Category = "Eyes",
        Attachment = "eyes",
        ModifyClientsideModel = function(ply, pos, ang, scale)
            pos = pos + (ang:Forward() * -4)

            return pos, ang, scale
        end
    },
}