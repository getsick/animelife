local PANEL = {}

AccessorFunc(PANEL, "item", "Item", FORCE_STRING)
AccessorFunc(PANEL, "style", "Style", FORCE_NUMBER)
AccessorFunc(PANEL, "slotnum", "SlotNum", FORCE_NUMBER)
AccessorFunc(PANEL, "invcat", "InventoryCategory", FORCE_STRING)
AccessorFunc(PANEL, "itemamount", "ItemAmount", FORCE_NUMBER)

surface.CreateFont("animelife.inventory.slot.tooltip", {font = "Exo 2 SemiBold", size = 18, weight = 600, extended = true})
surface.CreateFont("animelife.inventory.slot.tooltip_small", {font = "Exo 2 SemiBold", size = 14, weight = 600, extended = true})

local PANEL_STYLES = {
    -- default
    [1] = {
        Outline = Color(230, 197, 255, 73),
        Background = Color(242, 190, 255, 30)
    },
    -- trading
    [2] = {
        Outline = Color(245, 165, 111),
        Background = Color(192, 192, 192, 0.42 * 255)
    }
}

function PANEL:Init()
    self:SetItem("none")
    self:SetStyle(1) -- set default style
    self:SetSlotNum(1)
    self:SetInventoryCategory("Main Section")
    self:SetItemAmount(1)

    self:SetCursor("hand")

    self.PreviewModel = vgui.Create("SpawnIcon", self)
    self.PreviewModel:SetModel("models/error.mdl")
    self.PreviewModel:SetVisible(false)
    self.PreviewModel:SetMouseInputEnabled(false)
    self.PreviewModel:SetTooltip(false)
    self.PreviewModel.PaintOver = function() end -- remove blue outline on hover
end

function PANEL:OnMousePressed(mc)
    if self:GetItem() == "none" then return end
    if mc == MOUSE_RIGHT then
        local dmenu = DermaMenu(nil, self)
        local item = InventoryItems[self:GetItem()]

        dmenu:AddOption("Использовать", function()
            net.Start("animelife.inventory.useitem")
                net.WriteString(self:GetInventoryCategory())
                net.WriteInt(self:GetSlotNum() or 1, 32)
            net.SendToServer()
        end)
        if item.Price.Yen ~= -1 then
            local price = string.Comma(InventorySellPrice(item.Price.Yen))
            dmenu:AddOption("Продать (¥" .. price .. ")", function() 
                Derma_Query("Вы уверены, что хотите продать " .. item.Name .. " за ¥" .. price .. "?", "Продать предмет", "Да", function()
                    net.Start("animelife.inventory.sellitem")
                        net.WriteString(self:GetInventoryCategory())
                        net.WriteInt(self:GetSlotNum() or 1, 32)
                    net.SendToServer()
                end, "Нет", function() end)
            end)
        else
            dmenu:AddOption("Уничтожить", function() 
                Derma_Query("Вы уверены, что хотите уничтожить " .. item.Name .. "?", "Уничтожить предмет", "Да", function()
                    net.Start("animelife.inventory.stashitem")
                        net.WriteString(self:GetInventoryCategory())
                        net.WriteInt(self:GetSlotNum() or 1, 32)
                    net.SendToServer()
                end, "Нет", function() end)
            end)
        end

        dmenu:Open()
    end
end

function PANEL:OnMouseReleased()
    if ValidPanel(self.TempSlot) then
        self.TempSlot:Remove()
        self.TempSlot = nil
    end
end

function PANEL:SetModel(model)
    self.PreviewModel:SetModel(model)
    self.PreviewModel:SetVisible(true)
end

function PANEL:PerformLayout(w, h)
    self.PreviewModel:SetPos(12, 12)
    self.PreviewModel:SetSize(w - 24, h - 24)
end

function PANEL:InitTooltip()
    if self:GetItem() == "none" then return end

    local item_name = InventoryItems[self:GetItem()].Name
    local expiration_date = LocalPlayer().Inventory[self:GetInventoryCategory()][self:GetSlotNum()].ExpirationDate
    if isnumber(expiration_date) then
       item_name = item_name .. " (" .. string.NiceTime(expiration_date - os.time()) .. ")"
    end

    local tooltip = vgui.Create("DPanel", self:GetParent())
    tooltip:SetSize(math.max(100, utf8.len(item_name) * 7), 32)
    tooltip.Paint = function(panel, w, h)
        if self:GetItem() ~= "none" then
            -- draw.RoundedBox(4, 0, h, w, 16, Color(0, 0, 0, 150))
            draw.RoundedBox(4, 0, 0, w, h, Color(0, 0, 0, 225))
            
            draw.SimpleText(item_name, utf8.len(item_name) < 16 and "animelife.inventory.slot.tooltip" or "animelife.inventory.slot.tooltip_small", w / 2, h / 2, Color(165, 166, 189), 1, 1)
        end
    end

    return tooltip
end

function PANEL:OnCursorEntered()
    if !ValidPanel(self.Tooltip) then
        self.Tooltip = self:InitTooltip()
    end
end

function PANEL:OnCursorExited()
    if ValidPanel(self.Tooltip) then
        self.Tooltip:Remove()
        self.Tooltip = nil
    end
end

function PANEL:Think()
    if ValidPanel(self.Tooltip) then
        -- local mx, my = self:CursorPos()
        local mx, my = self:GetPos()
        self.Tooltip:SetPos(mx, my + 66)
    end

    if self:GetItem() ~= "none" then
        if !self.PreviewModel:IsVisible() then
            self.PreviewModel:SetVisible(true)
        end

        if ValidPanel(self.Tooltip) then
            if !self:IsHovered() and self.Tooltip:IsVisible() then
                self.Tooltip:SetVisible(false)
            end
        end
    end
end

local tier_colors = {
    [1] = Color(185, 185, 185, 30), -- gray
    [2] = Color(190, 255, 209, 30), -- green
    [3] = Color(221, 83, 255, 30), -- purple
    [4] = Color(255, 193, 101, 30), -- orange
    [5] = Color(255, 121, 121, 30), -- red
}

function PANEL:Paint(w, h)
    local cur_style = self:GetStyle()

    surface.SetDrawColor(PANEL_STYLES[cur_style].Background)
    surface.DrawRect(0, 0, w, h)

    surface.SetDrawColor(PANEL_STYLES[cur_style].Outline)
    surface.DrawOutlinedRect(0, 0, w, h, 1)

    if self:GetItem() ~= "none" then
        local tier = InventoryItems[self:GetItem()].Tier or 0
        if tier > 0 then
            surface.SetDrawColor(tier_colors[tier])
            surface.DrawRect(0, 0, w, h)
        end

        if self:GetItemAmount() > 1 then
            draw.SimpleText(self:GetItemAmount(), "animelife.inventory.slot.tooltip", w - 8, h - 8, Color(255, 255, 255), TEXT_ALIGN_RIGHT, TEXT_ALIGN_BOTTOM)
        end
    end
end

vgui.Register("animelife.inventory.slot", PANEL, "DPanel")