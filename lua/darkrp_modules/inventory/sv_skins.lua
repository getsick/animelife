function InventorySetSkin(ply, mdl)
    local bounds = Vector(16, 16, 16)
    local inital_pos = ply:GetPos() + Vector(0, 0, 32)
    effects.Bubbles(inital_pos - bounds, inital_pos + bounds, 30, 72, 5, 0)

    ply:EmitSound("physics/plaster/drywall_impact_soft" .. math.random(1, 2) .. ".wav")

    if ply:GetModel() == mdl then
        if ply:Team() == TEAM_YAKUZA or ply:Team() == TEAM_NEET then
            local class = ply:GetNWInt("animelife.classchooser.class", -1)
            if class ~= -1 then
                ply:SetModel(table.Random(classchooser.List[ply:Team()][class].Model))
            end
        else
            ply:SetModel(ply:getPreferredModel(ply:Team()))
        end
        return
    end

    ply:SetModel(mdl)
end