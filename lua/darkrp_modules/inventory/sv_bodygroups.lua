concommand.Add("al_requestbodygroup", function(ply, cmd, args)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if (ply.NextBodygroupChange or 0) > SysTime() then 
        DarkRP.notify(ply, 1, 5, "Скоростное переодевание? Попробуй сбавить темп.")
        return 
    end

    local id = args[1]
    id = tonumber(id)
    if !isnumber(id) then return end
    local val = args[2]

    ply:SetBodygroup(id, val)

    ply.NextBodygroupChange = SysTime() + 1
end)