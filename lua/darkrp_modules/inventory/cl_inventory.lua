local PANEL = {}

local base = Material("animelife/inventory/base.png")
local detail_animelife = Material("animelife/inventory/detail_animelife.png")
local detail_animelife_e = Material("animelife/inventory/detail_animelife_e.png")
local icon_info = Material("animelife/inventory/icon_info.png")

surface.CreateFont("animelife.inventory.wallet", {font = "Exo 2 SemiBold", size = ScreenScale(17) / 3, weight = 600, extended = true})
surface.CreateFont("animelife.inventory.button", {font = "Exo 2 SemiBold", size = ScreenScale(19) / 3, weight = 600, extended = true})

function PANEL:Init()
    self.InvGrid = vgui.Create("animelife.inventory.grid", self)
    self.InvGrid:SetSlotSize(ui.y(58))

    self.PlayerModel = vgui.Create("DModelPanel", self)
    self.PlayerModel:SetFOV(30)
    self.PlayerModel.LayoutEntity = function() end

    self.MenuButtons = {}
    self.CurrentSection = 1

    for i = 1, 2 do
        local btn = vgui.Create("DButton", self)
        btn:SetText("")
        btn.Paint = function(pnl, w, h)
            surface.SetDrawColor(0, 0, 0, self.CurrentSection == i and 125 or 0)
            surface.DrawRect(0, 0, w, h)

            draw.SimpleText(i == 1 and "Предметы" or "Аксессуары", "animelife.inventory.button", w / 2, h / 2 - 2, self.CurrentSection == i and Color(255, 255, 255) or Color(206, 206, 206), 1, 1)
        end
        btn.DoClick = function()
            self.CurrentSection = i

            self:ReloadInventory(i == 1 and "Main Section" or "Accessories Section")
        end

        table.insert(self.MenuButtons, btn)
    end

    self.BodygroupButton = vgui.Create("DButton", self)
    self.BodygroupButton:SetText("")
    self.BodygroupButton.Paint = function(pnl, w, h)
        surface.SetDrawColor(0, 0, 0, 200)
        surface.DrawRect(0, 0, w, h)

        surface.SetDrawColor(194, 161, 255)
        surface.DrawRect(w - 2, 0, 2, h)

        draw.SimpleText("Группы тела", "animelife.inventory.wallet", w / 2, h / 2, color_white, 1, 1)
    end
    self.BodygroupButton.DoClick = function()
        local dmenu = DermaMenu()

        for _, b in pairs(LocalPlayer():GetBodyGroups()) do
            if !b.submodels then continue end
            local sub = dmenu:AddSubMenu(b.name)

            for k, m in pairs(b.submodels) do
                local opt = sub:AddOption(m == "" and "спрятать" or string.StripExtension(m), function()
                    if IsValid(self.PlayerModel.Entity) then
                        self.PlayerModel.Entity:SetBodygroup(b.id, k)
                    end

                    RunConsoleCommand("al_requestbodygroup", b.id, k)
                end)
                if m == "" then opt:SetIcon("icon16/cross.png") else opt:SetIcon("icon16/accept.png") end
            end
        end

        dmenu:Open()
    end
end

function PANEL:Think()
    if ValidPanel(self.PlayerModel) then
        if self.PlayerModel:GetModel() ~= LocalPlayer():GetModel() then
            self.PlayerModel:SetModel(LocalPlayer():GetModel())
        end
    end
end

function PANEL:PerformLayout(w, h)
    self.InvGrid:SetPos(ui.x(874) + ui.x(16), ui.y(244) + ui.y(16))
    self.InvGrid:SetSize(ui.x(1046) - ui.x(32), ui.y(593) - ui.y(32))

    self.PlayerModel:SetPos(ui.x(156), ui.y(244))
    self.PlayerModel:SetSize(ui.x(404), ui.y(593))

    local by = 411
    for _, btn in pairs(self.MenuButtons) do
        btn:SetSize(ui.x(314), ui.y(50))
        btn:SetPos(ui.x(560), ui.y(by))
        
        by = by + 50
    end

    -- load inventory items
    self:ReloadInventory(self.CurrentSection == 1 and "Main Section" or "Accessories Section")

    self.PlayerModel:SetModel(LocalPlayer():GetModel())

    local mn, mx = self.PlayerModel.Entity:GetRenderBounds()
    local size = 0
    size = math.max(size, math.abs(mn.x) + math.abs(mx.x))
    size = math.max(size, math.abs(mn.y) + math.abs(mx.y))
    size = math.max(size, math.abs(mn.z) + math.abs(mx.z))

    self.PlayerModel:SetCamPos(Vector(size, size, size))
    self.PlayerModel:SetLookAt((mn + mx) * 0.5)

    self.BodygroupButton:SetPos(0, h - ui.y(300))
    self.BodygroupButton:SetSize(ui.x(156), ui.y(30))
end

function PANEL:ReloadInventory(section)
    for _, slot in pairs(self.InvGrid.Slots) do
        slot:SetItem("none")
        slot.PreviewModel:SetVisible(false)
    end

    if LocalPlayer().Inventory then
        for slot, item in pairs(LocalPlayer().Inventory[section] or {}) do
            if item == {} then continue end
            if item == nil then continue end
            if !isstring(item.Model) then continue end
            self.InvGrid.Slots[slot]:SetInventoryCategory(section)
            self.InvGrid.Slots[slot]:SetModel(item.Model)
            self.InvGrid.Slots[slot]:SetItem(item.Class)
            self.InvGrid.Slots[slot]:SetItemAmount(item.Amount or 1)
        end
    end
end

function PANEL:Paint(w, h)
    ui.Smooth(true, true)
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(base)
        surface.DrawTexturedRect(0, ui.y(244), w, ui.y(593))

        surface.SetMaterial(detail_animelife)
        surface.DrawTexturedRect(ui.x(218), ui.y(8), ui.x(688), ui.y(357))

        surface.SetMaterial(detail_animelife_e)
        surface.DrawTexturedRect(ui.x(201), ui.y(152), ui.x(120), ui.y(158))
    ui.Smooth(false, true)

    local money = LocalPlayer():getDarkRPVar("money") or 0
    local donation_points = LocalPlayer():GetDonationPoints() or 0
    draw.DrawText("¥" .. string.Comma(money) .. "\n" .. "Донат-валюта: " .. string.Comma(donation_points), "animelife.inventory.wallet", ui.x(687) + (ui.x(62 / 2)), h - ui.y(300), Color(255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)

    draw.SimpleText("Инвентарь", "animelife.inventory.wallet", ui.x(680) + (ui.x(75 / 2)), ui.y(352), Color(206, 206, 206), 1)

    draw.SimpleText("Инвентарь можно скрыть, повторно нажав I", "animelife.Global.HUD_Money", w / 2 + 2, h - ui.y(195), Color(0, 0, 0, 75), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
    draw.SimpleText("Инвентарь можно скрыть, повторно нажав I", "animelife.Global.HUD_Money", w / 2, h - ui.y(195), Color(255, 255, 255, 100), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
end

vgui.Register("animelife.inventory", PANEL, "EditablePanel")

concommand.Add("al_inventory", function()
    if !ValidPanel(GLOBALS_INVENTORY) then
        GLOBALS_INVENTORY = vgui.Create("animelife.inventory")
        GLOBALS_INVENTORY:SetSize(ScrW(), ScrH())
        GLOBALS_INVENTORY:SetVisible(false)
        GLOBALS_INVENTORY:SetAlpha(0)
    end

    if GLOBALS_INVENTORY:IsVisible() then
        GLOBALS_INVENTORY:AlphaTo(0, 0.1)
        GLOBALS_INVENTORY:SetVisible(false)
        gui.EnableScreenClicker(false)
    else
        GLOBALS_INVENTORY:AlphaTo(255, 0.25)
        GLOBALS_INVENTORY:SetVisible(true)
        gui.EnableScreenClicker(true)
    end
end)

net.Receive("animelife.inventory.networking", function()
    local len = net.ReadInt(32)
    local data = net.ReadData(len)
    local category = net.ReadString()
    local slot = net.ReadInt(32)

    data = util.Decompress(data)
    data = util.JSONToTable(data)

    if !LocalPlayer().Inventory then
        LocalPlayer().Inventory = {}
    end

    if !LocalPlayer().Inventory[category] then
        LocalPlayer().Inventory[category] = {}
    end

    LocalPlayer().Inventory[category][slot] = data

    -- print("inventory: Delivered serverdata", len, category, slot, istable(data))

    if ValidPanel(GLOBALS_INVENTORY) then
        GLOBALS_INVENTORY:ReloadInventory(GLOBALS_INVENTORY.CurrentSection == 1 and "Main Section" or "Accessories Section")
    end
end)