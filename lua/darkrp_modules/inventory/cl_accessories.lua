local meta = FindMetaTable("Player")

module("inventory.accessories", package.seeall)

function meta:AddClientsideModel(id)
    if !List[id] then return end

    local item = List[id]

    local mdl = ClientsideModel(item.Model, RENDERGROUP_OPAQUE)
    mdl:SetNoDraw(true)

    if !self.ClientsideModels then self.ClientsideModels = {} end
    if !self.ClientsideModels[item.Category] then self.ClientsideModels[item.Category] = {} end

    self.ClientsideModels[item.Category][id] = mdl
end

function meta:RemoveClientsideModel(id)
    if !List[id] then return end
    if !self.ClientsideModels then return end
    local item = List[id]
    if !self.ClientsideModels[item.Category] then return end
    if !self.ClientsideModels[item.Category][id] then return end

    if IsValid(self.ClientsideModels[item.Category][id]) then
        self.ClientsideModels[item.Category][id]:Remove()
    end

    self.ClientsideModels[item.Category][id] = nil
end

function meta:SetClientsideModelOffset(category, x, y, z, p, yaw, r, scale)
    if !self.OffsetData then self.OffsetData = {} end

    self.OffsetData[category] = {
        x = x, y = y, z = z,
        pitch = p, yaw = yaw, roll = r,
        scale = scale
    }

    return self.OffsetData[category]
end

hook.Add("PostPlayerDraw", "animelife.accessories.postplayerdraw", function(ply)
	if !ply:Alive() then return end
	if !ply.ClientsideModels then return end

    local offset_data = ply.OffsetData or nil
	
	for category, data in pairs(ply.ClientsideModels) do
        for id, mdl in pairs(data) do
            if !List[id] then ply.ClientsideModels[category][id] = nil continue end

            local item = List[id]

            if !item.Attachment and !item.Bone then ply.ClientsideModels[category][id] = nil continue end

            local pos = Vector()
            local ang = Angle()

            if item.Attachment then
                local attach_id = ply:LookupAttachment(item.Attachment)
                if !attach_id then return end

                local attach = ply:GetAttachment(attach_id)
                if !attach then return end

                pos = attach.Pos
                ang = attach.Ang
            else
                local bone_id = ply:LookupBone(item.Bone)
                if !bone_id then return end

                pos, ang = ply:GetBonePosition(bone_id)
            end

            -- add unique accessories offset 
            -- ie we should have an ability to modify `pos, ang, scale` via sh_accessories.lua
            local scale = 1
            if item.ModifyClientsideModel then
                pos, ang, scale = item.ModifyClientsideModel(ply, pos, ang, scale)
            end

            if offset_data ~= nil then
                if ply.OffsetData[category] then
                    offset_data = ply.OffsetData[category]
                    pos = pos + (ang:Forward() * offset_data.x) + (ang:Right() * offset_data.y) + (ang:Up() * offset_data.z)

                    ang:RotateAroundAxis(ang:Forward(), offset_data.pitch)
                    ang:RotateAroundAxis(ang:Right(), offset_data.yaw)
                    ang:RotateAroundAxis(ang:Up(), offset_data.roll)

                    scale = offset_data.scale
                end
            end

            mdl:SetModelScale(scale, 0)

            mdl:SetPos(pos)
            mdl:SetAngles(ang)

            mdl:SetRenderOrigin(pos)
            mdl:SetRenderAngles(ang)
            mdl:SetupBones()
            mdl:DrawModel()
            mdl:SetRenderOrigin()
            mdl:SetRenderAngles()
        end
	end
end)

local function OpenEditorGUI()
    if ValidPanel(GLOBALS_ACCESSORIESEDITOR) then
        GLOBALS_ACCESSORIESEDITOR:Remove()
        GLOBALS_ACCESSORIESEDITOR = nil 
    end

    GLOBALS_ACCESSORIESEDITOR = vgui.Create("animelife.accessories.editor")
    GLOBALS_ACCESSORIESEDITOR:SetSize(ScrW(), 300)
    GLOBALS_ACCESSORIESEDITOR:MakePopup()
end

function EnterEditingMode(ply)
    ply.AccessoriesEditor = true

    OpenEditorGUI()
end

function LeaveEditingMode(ply)
    ply.AccessoriesEditor = false

    if ValidPanel(GLOBALS_ACCESSORIESEDITOR) then
        GLOBALS_ACCESSORIESEDITOR:Remove()
        GLOBALS_ACCESSORIESEDITOR = nil 
    end
end

hook.Add("CalcView", "animelife.accessories.editor", function(ply, pos, ang, fov)
    if !ply.AccessoriesEditor then return end
    if !ValidPanel(GLOBALS_ACCESSORIESEDITOR) then return end
    local head_bone = LocalPlayer():LookupBone("ValveBiped.Bip01_Head1")
    local head_pos = LocalPlayer():LocalToWorld(Vector(54, 0, 78))
    local whoop, whoop2 = 0, 0
    if GLOBALS_ACCESSORIESEDITOR.Category == "Spine" then
        whoop = -120
        whoop2 = -240
    end
    if isnumber(head_bone) then
        head_pos = LocalPlayer():GetBonePosition(head_bone) + LocalPlayer():GetUp() * 20 + LocalPlayer():GetForward() * (40 + whoop) + LocalPlayer():GetRight() * 30
    end

    return {
        origin = head_pos,
        angles = LocalPlayer():LocalToWorldAngles(Angle(15, -220 - whoop2, 8)),
        drawviewer = true
    }
end)

net.Receive("animelife.inventory.accessories.broadcast_offset", function()
    local ply = net.ReadEntity()
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local cat = net.ReadString()
    if !isstring(cat) then return end

    local x = net.ReadFloat()
    local y = net.ReadFloat()
    local z = net.ReadFloat()
    local pitch = net.ReadFloat()
    local yaw = net.ReadFloat()
    local roll = net.ReadFloat()
    local scale = net.ReadFloat()

    -- consider that a reset
    if scale == 1 and x == 0 and y == 0 and z == 0 and pitch == 0 and yaw == 0 and roll == 0 then
        if ply.OffsetData and ply.OffsetData[cat] then
            ply.OffsetData[cat] = nil
        end
        return
    end

    ply:SetClientsideModelOffset(cat, x, y, z, pitch, yaw, roll, scale)
end)

net.Receive("animelife.inventory.accessories.equip_item", function()
    local ply = net.ReadEntity()
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local id = net.ReadString()
    if !isstring(id) then return end

    local equip = net.ReadBool()
    if equip then
        ply:AddClientsideModel(id)
    else
        ply:RemoveClientsideModel(id)
    end
end)