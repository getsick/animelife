util.AddNetworkString("animelife.inventory.root.printinv")

concommand.Add("al_printinv", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end
    if !ply:IsUserGroup("root") then return end

    arg_str = string.Split(arg_str, " ")
    local target = string.Trim(arg_str[1], '"')
    target = player.GetBySteamID(target)
    if !IsValid(target) or !target:IsPlayer() then return end
    if !target.Inventory then return end

    local clean_inv = function(inv)
        local res = ""

        for cat, slot in pairs(inv) do
            if table.IsEmpty(slot) then
                continue
            end

            res = res .. "\n" .. cat .. ":\n"
            for id, item in pairs(slot) do
                if table.IsEmpty(item) then continue end

                res = res .. ("[%s] %s\n"):format(id, item.Class)
            end
        end

        return res
    end

    net.Start("animelife.inventory.root.printinv")
        net.WriteString(clean_inv(target.Inventory))
    net.Send(ply)
end)