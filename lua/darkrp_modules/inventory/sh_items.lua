InventoryItems = InventoryItems or {
    ["weapon_qsz"] = {
        Name = "QSZ-92",
        Description = "Пистолет.",
        Model = "models/weapons/tfa_ins2/w_qsz92.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_qsz92",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 2000, XP = 400, Donate = -1},
        Tier = 5,

        OnUse = function(ply)
            ply:Give("tfa_ins2_qsz92")
        end,
    },
    ["weapon_vape"] = {
        Name = "Разноцветный Вейп",
        Description = "Вейп.",
        Model = "models/swamponions/vape.mdl",

        IsWeapon = true,
        Class = "tfa_weapon_vape",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 70000, XP = 7200, Donate = -1},
        Tier = 5,

        OnUse = function(ply)
            ply:Give("weapon_vape_juicy")
        end,
    },
    ["weapon_lockpick"] = {
        Name = "Отмычка",
        Description = "Отмычка.",
        Model = "models/weapons/w_crowbar.mdl",

        IsWeapon = true,
        Class = "tfa_weapon_lockpick",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 7000, XP = 1200, Donate = -1},
        Tier = 5,

        OnUse = function(ply)
            ply:Give("ah_lockpick")
        end,
    },
    ["weapon_knife"] = {
        Name = "Нож Бабочка",
        Description = "Холодное оружие.",
        Model = "models/weapons/tfa_cso/w_butterflyknife.mdl",

        IsWeapon = true,
        Class = "tfa_cso_butterflyknife",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 2500, XP = 500, Donate = -1},
        Tier = 5,

        OnUse = function(ply)
            ply:Give("tfa_cso_butterflyknife")
        end,
    },
    ["weapon_tomahawk"] = {
        Name = "Новогодний Томагавк",
        Description = "Холодное оружие.",
        Model = "models/weapons/tfa_cso/w_tomahawk_xmas.mdl",

        IsWeapon = true,
        Class = "tfa_weapon_tomahawk",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = -1, XP = 750, Donate = -1},
        Tier = 5,

        OnUse = function(ply)
            ply:Give("tfa_cso_tomahawk_xmas")
        end,
    },
    ["weapon_vape1"] = {
        Name = "Галюциногенный Вейп",
        Description = "Вейп.",
        Model = "models/swamponions/vape.mdl",

        IsWeapon = true,
        Class = "tfa_weapon_vape1",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 70000, XP = 7200, Donate = -1},
        Tier = 5,

        OnUse = function(ply)
            ply:Give("weapon_vape_hallucinogenic")
        end,
    },
    ["weapon_vape2"] = {
        Name = "Бабочка Вейп",
        Description = "Вейп.",
        Model = "models/swamponions/vape.mdl",

        IsWeapon = true,
        Class = "tfa_weapon_vape2",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 70000, XP = 7200, Donate = -1},
        Tier = 5,

        OnUse = function(ply)
            ply:Give("weapon_vape_butterfly")
        end,
    },
    ["weapon_glock19"] = {
        Name = "Glock 19",
        Description = "Пистолет.",
        Model = "models/weapons/tfa_ins2/w_glock19.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_glock_19",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 2000, XP = 400, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_glock_19")
        end,
    },
    ["weapon_m9"] = {
        Name = "M9",
        Description = "Пистолет.",
        Model = "models/weapons/tfa_ins2/w_m9.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_m9",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 2000, XP = 400, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_m9")
        end,
    },
    ["weapon_cobra"] = {
        Name = "King Cobra",
        Description = "Пистолет.",
        Model = "models/weapons/tfa_ins2/w_thanez_cobra.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_thanez_cobra",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 2800, XP = 800, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_thanez_cobra")
        end,
    },
    ["weapon_nova"] = {
        Name = "Nova",
        Description = "Дробовик.",
        Model = "models/weapons/tfa_ins2/w_nova.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_nova",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 8000, XP = 1400, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_nova")
        end,
    },
    ["weapon_spas"] = {
        Name = "SPAS-12",
        Description = "Дробовик.",
        Model = "models/weapons/tfa_ins2/w_spas12_bri.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_spas12",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 8500, XP = 1450, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_spas12")
        end,
    },
    ["weapon_m500"] = {
        Name = "M500",
        Description = "Дробовик.",
        Model = "models/weapons/tfa_ins2/w_m500.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_m500",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 9000, XP = 1500, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_m500")
        end,
    },
    ["weapon_fort"] = {
        Name = "Fort-500",
        Description = "Дробовик.",
        Model = "models/weapons/tfa_ins2/w_fort500.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_fort500",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 10000, XP = 1600, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_fort500")
        end,
    },
    ["weapon_ak12"] = {
        Name = "AK-12",
        Description = "Автомат.",
        Model = "models/weapons/tfa_ins2/w_ak12.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_ak12",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 9000, XP = 1500, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_ak12")
        end,
    },
    ["weapon_ak400"] = {
        Name = "AK-400",
        Description = "Автомат.",
        Model = "models/weapons/tfa_ins2/w_ak400.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_ak400",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 9500, XP = 1600, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_ak400")
        end,
    },
    ["weapon_famas"] = {
        Name = "FAMAS F1",
        Description = "Автомат.",
        Model = "models/weapons/tfa_ins2/w_famas.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_famas",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 9000, XP = 1500, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_famas")
        end,
    },
    ["weapon_lynx"] = {
        Name = "Lynx CQ300",
        Description = "Автомат.",
        Model = "models/weapons/tfa_ins2/w_cq300.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_cq300",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 10000, XP = 1600, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_cq300")
        end,
    },
    ["weapon_mk18"] = {
        Name = "MK-18",
        Description = "Автомат.",
        Model = "models/weapons/tfa_ins2/w_mk18.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_mk18",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 10000, XP = 1600, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_mk18")
        end,
    },
    ["weapon_akm"] = {
        Name = "MOE AKM",
        Description = "Автомат.",
        Model = "models/weapons/tfa_ins2/w_moe_akm.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_moe_akm",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 9000, XP = 1500, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_moe_akm")
        end,
    },
    ["weapon_kriss"] = {
        Name = "KRISS Vector",
        Description = "Пистолет-пулемет",
        Model = "models/weapons/tfa_ins2/w_krissv.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_krissv",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 6000, XP = 1000, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_krissv")
        end,
    },
    ["weapon_mp5k"] = {
        Name = "MP5K",
        Description = "Пистолет-пулемет.",
        Model = "models/weapons/tfa_ins2/w_mp5k.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_mp5k",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 5200, XP = 900, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_mp5k")
        end,
    },
    ["weapon_mp7"] = {
        Name = "MP7",
        Description = "Пистолет-пулемет.",
        Model = "models/weapons/tfa_ins2/w_mp7.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_mp7",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 5500, XP = 950, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_mp7")
        end,
    },
    ["weapon_spectre"] = {
        Name = "Spectre M4",
        Description = "Пистолет-пулемет.",
        Model = "models/weapons/tfa_ins2/w_spectre.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_spectre",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 6000, XP = 1000, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_spectre")
        end,
    },
    ["weapon_remington"] = {
        Name = "Remington M24 SWS",
        Description = "Снайперская винтовка.",
        Model = "models/weapons/smc/m24/w_m24.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_remington_m24_sws",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 11000, XP = 2000, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_remington_m24_sws")
        end,
    },
    ["weapon_sks"] = {
        Name = "SKS",
        Description = "Снайперская винтовка.",
        Model = "models/weapons/tfa_ins2/w_sks.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_sks",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 12000, XP = 2200, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_sks")
        end,
    },
    ["weapon_msr"] = {
        Name = "MSR",
        Description = "Снайперская винтовка.",
        Model = "models/weapons/tfa_ins2/w_codol_msr.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_codol_msr",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 11000, XP = 2000, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_codol_msr")
        end,
    },
    ["weapon_magnum"] = {
        Name = "GOL Magnum",
        Description = "Снайперская винтовка.",
        Model = "models/weapons/tfa_ins2/w_gol.mdl",

        IsWeapon = true,
        Class = "tfa_ins2_gol",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = 11000, XP = 2000, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("tfa_ins2_gol")
        end,
    },

    ["weapon_partypopper"] = {
        Name = "Хлопушка",
        Description = "Хлопушка.",
        Model = "models/zerochain/props_pumpkinnight/zpn_partypopper.mdl",

        IsWeapon = true,
        Class = "zpn_partypopper",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = -1, XP = 25, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("zpn_partypopper")
        end,
    },
    ["weapon_pumpkinslayer"] = {
        Name = "Злая Хлопушка",
        Description = "Хлопушка. Бьет сильно.",
        Model = "models/zerochain/props_pumpkinnight/zpn_partypopper.mdl",

        IsWeapon = true,
        Class = "zpn_partypopper01",

        CanTrade = true,
        CanDrop = true,

        Section = "Main Section",

        Price = {Yen = -1, XP = 100, Donate = -1},
        Tier = 1,

        OnUse = function(ply)
            ply:Give("zpn_partypopper01")
        end,
    },
    ["playermodel_poyo"] = {
        Name = "Poyo",
        Description = "Poyo",
        Model = "models/jazzmcfly/poyo/poyo.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_poyo",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 250000, XP = -1, Donate = -1},
        Tier = 5,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/jazzmcfly/poyo/poyo.mdl")
        end
    },

    ["playermodel_ftsu"] = {
        Name = "dev1",
        Description = "dev1",
        Model = "models/player/mmd/melantha/melantha_player.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_ftsu",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = -1, Donate = -1},
        Tier = 5,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/mmd/melantha/melantha_player.mdl")
        end
    },

    ["playermodel_mays"] = {
        Name = "dev2",
        Description = "dev2",
        Model = "models/player/serto/yaren/yaren.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_mays",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = -1, Donate = -1},
        Tier = 5,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/serto/yaren/yaren.mdl")
        end
    },

    ["playermodel_zhongli"] = {
        Name = "Zhongli",
        Description = "Zhongli",
        Model = "models/cyanblue/genshin_impact/zhongli/zhongli.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_zhongli",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 250000, XP = -1, Donate = -1},
        Tier = 5,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/cyanblue/genshin_impact/zhongli/zhongli.mdl")
        end
    },

    ["playermodel_muffler"] = {
        Name = "Muffler",
        Description = "Muffler",
        Model = "models/jazzmcfly/muffler/muffler.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_muffler",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 250000, XP = -1, Donate = -1},
        Tier = 5,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/jazzmcfly/muffler/muffler.mdl")
        end
    },

    ["playermodel_shishiro"] = {
        Name = "Shishiro",
        Description = "Shishiro",
        Model = "models/pacagma/vtuber/botan/botan_player.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_shishiro",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 250000, XP = -1, Donate = -1},
        Tier = 5,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/pacagma/vtuber/botan/botan_player.mdl")
        end
    },

    ["playermodel_elysia"] = {
        Name = "Elysia",
        Description = "Elysia",
        Model = "models/elysia_maid/honkai_impact/rstar/elysia_maid/elysia_maid.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_elysia",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 250000, XP = -1, Donate = -1},
        Tier = 5,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/elysia_maid/honkai_impact/rstar/elysia_maid/elysia_maid.mdl")
        end
    },

    ["playermodel_sora"] = {
        Name = "Sora",
        Description = "Sora",
        Model = "models/player/arknights_sora_summer.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_sora",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/arknights_sora_summer.mdl")
        end
    },

    ["playermodel_megurine"] = {
        Name = "Megurine Luka",
        Description = "Megurine Luka",
        Model = "models/player/dewobedil/vocaloid/megurine_luka/default_p.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_megurine",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/dewobedil/vocaloid/megurine_luka/default_p.mdl")
        end
    },

    ["playermodel_miku"] = {
        Name = "Colorful Miku",
        Description = "Colorful Miku",
        Model = "models/player/dewobedil/vocaloid/appearance_miku/colorful_p.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_miku",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/dewobedil/vocaloid/appearance_miku/colorful_p.mdl")
        end
    },

    ["playermodel_mikuchristmas"] = {
        Name = "Christmas Miku",
        Description = "Christmas Miku",
        Model = "models/captainbigbutt/vocaloid/christmas_miku.mdl",

        IsWeapon = false,
        Class = "playermodel_mikuchristmas",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = 28800, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/captainbigbutt/vocaloid/christmas_miku.mdl")
        end
    },

    ["playermodel_yowane"] = {
        Name = "Christmas Yowane Haku",
        Description = "Christmas Yowane Haku",
        Model = "models/player/dewobedil/vocaloid/yowane_haku/christmas_p.mdl",

        IsWeapon = false,
        Class = "playermodel_yowane",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = 28800, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/dewobedil/vocaloid/yowane_haku/christmas_p.mdl")
        end
    },

    ["playermodel_shinmyoumaru"] = {
        Name = "Shinmyoumaru",
        Description = "Shinmyoumaru",
        Model = "models/cyanblue/touhou14/shinmyoumaru/shinmyoumaru.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_shinmyoumaru",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/cyanblue/touhou14/shinmyoumaru/shinmyoumaru.mdl")
        end
    },

    ["playermodel_noelle"] = {
        Name = "Noelle",
        Description = "Noelle",
        Model = "models/player/genshin_impact_noelle.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_noelle",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/genshin_impact_noelle.mdl")
        end
    },

    ["playermodel_kira"] = {
        Name = "Yoshikage Kira",
        Description = "Yoshikage Kira",
        Model = "models/player/kir/btdkira/btdkira.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_kira",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/kir/btdkira/btdkira.mdl")
        end
    },

    ["playermodel_adam"] = {
        Name = "Adam",
        Description = "Adam",
        Model = "models/male_character/honkai_impact/rstar/male_character/male_character.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_adam",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/male_character/honkai_impact/rstar/male_character/male_character.mdl")
        end
    },

    ["playermodel_miko"] = {
        Name = "Gyakushinn Miko",
        Description = "Gyakushinn Miko",
        Model = "models/pacagma/houkai_impact_3rd/gyakushinn_miko/gyakushinn_miko_player.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_miko",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/pacagma/houkai_impact_3rd/gyakushinn_miko/gyakushinn_miko_player.mdl")
        end
    },

    ["playermodel_plutia"] = {
        Name = "Plutia",
        Description = "Plutia",
        Model = "models/player/dewobedil/hyperdimension_neptunia/plutia/default_p.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_plutia",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/dewobedil/hyperdimension_neptunia/plutia/default_p.mdl")
        end
    },

    ["playermodel_yang"] = {
        Name = "Welt Yang",
        Description = "Welt Yang",
        Model = "models/welt_yang/honkai_impact/rstar/welt_yang/welt_yang.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_yang",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/welt_yang/honkai_impact/rstar/welt_yang/welt_yang.mdl")
        end
    },

    ["playermodel_ribbon"] = {
        Name = "Ribbon Girl Miku",
        Description = "Ribbon Girl Miku",
        Model = "models/player/dewobedil/vocaloid/ribbon_girl_miku/default_p.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_ribbon",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/dewobedil/vocaloid/ribbon_girl_miku/default_p.mdl")
        end
    },

    ["playermodel_lukaa"] = {
        Name = "Luka Append",
        Description = "Luka Append",
        Model = "models/captainbigbutt/vocaloid/luka_append.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_lukaa",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/captainbigbutt/vocaloid/luka_append.mdl")
        end
    },

    ["playermodel_kanna"] = {
        Name = "Kanna Kamui",
        Description = "Kanna Kamui",
        Model = "models/player/dewobedil/maid_dragon/kanna/default_p.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_kanna",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/dewobedil/maid_dragon/kanna/default_p.mdl")
        end
    },

    ["playermodel_kasen"] = {
        Name = "Kasen Ibaraki",
        Description = "Kasen Ibaraki",
        Model = "models/player/dewobedil/touhou/kasen_ibaraki/default_p.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_kasen",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/dewobedil/touhou/kasen_ibaraki/default_p.mdl")
        end
    },

    ["playermodel_akashi"] = {
        Name = "Akashi",
        Description = "Akashi",
        Model = "models/akashi/azur_lane/rstar/akashi/akashi.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_akashi",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/akashi/azur_lane/rstar/akashi/akashi.mdl")
        end
    },

    ["playermodel_chino"] = {
        Name = "Chino Kafuu",
        Description = "Chino Kafuu",
        Model = "models/player/dewobedil/chino_kafuu/default_p.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_chino",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/dewobedil/chino_kafuu/default_p.mdl")
        end
    },

    ["playermodel_chinoo"] = {
        Name = "Chino Kafuu Maid",
        Description = "Chino Kafuu Maid",
        Model = "models/player/dewobedil/chino_kafuu/maid_p.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_chinoo",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/dewobedil/chino_kafuu/maid_p.mdl")
        end
    },

    ["playermodel_qiqi"] = {
        Name = "Qiqi",
        Description = "Qiqi",
        Model = "models/cyanblue/genshin_impact/qiqi/qiqi.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_qiqi",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 250000, XP = -1, Donate = -1},
        Tier = 5,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/cyanblue/genshin_impact/qiqi/qiqi.mdl")
        end
    },

    ["playermodel_akari"] = {
        Name = "Mirai Akari",
        Description = "Mirai Akari",
        Model = "models/player/dewobedil/mirai_akari/default_p.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_akari",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/dewobedil/mirai_akari/default_p.mdl")
        end
    },

    ["playermodel_hk416"] = {
        Name = "HK416",
        Description = "HK416",
        Model = "models/player/dewobedil/girls_frontline/hk416/default_p.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "playermodel_hk416",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 70000, XP = -1, Donate = -1},
        Tier = 4,

        NotDecreasable = true,

        OnUse = function(ply)
            InventorySetSkin(ply, "models/player/dewobedil/girls_frontline/hk416/default_p.mdl")
        end
    },
    ["donate_memberplus"] = {
        Name = "MEMBER+ (30D)",
        Description = [[MEMBER+ - ваш маленький помощник в самые трудные дни.
Вот же они: преимущества, которые вы получаете, приобретая MEMBER+
- 20 очков опыта, вместо 10 каждую минуту;
- На 55% больше получаемой валюты со всех источников;
- x2 ежедневные награды;
- Уникальный значок в профиле;
- Доступен VIP денежный принтер.
Действует 30 дней.
Внимание, покупая привилегию "Спонсор" вы не получите преимуществ MEMBER+.]],
        Model = "models/Items/item_item_crate.mdl",

        IsWeapon = false,
        Class = "donate_memberplus",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 200},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if !ply:IsVIP() then
                ply:SetVIP(true, 30)

                DarkRP.notify(ply, 0, 5, "Вы теперь Member+. Счастливого пользования!")
            else
                DarkRP.notify(ply, 1, 5, "У вас уже есть активный Member+.")

                return "fail"
            end
        end
    },
    ["donate_sponsor"] = {
        Name = "Спонсор (30D)",
        Description = [[Спонсор - привилегия, доступная для покупки и дающая вам привилегии и функции администрации. Она же называемая "Спонсорка" и "Админка".]],
        Model = "models/Items/item_item_crate.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_sponsor",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 400},
        Tier = 5,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:IsUserGroup("Sponsor") then
                DarkRP.notify(ply, 1, 5, "Спонсор уже включен.")

                return "fail"
            end

            ply:SetUserGroup("Sponsor")
        end
    },
    ["donate_tfa_volk"] = {
        Name = "Volk (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Volk.
        Действует 30 дней.]],
        Model = "models/weapons/eak47/w_volk_iw.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_volk",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 250},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_volk") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_volk", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Volk в спавнменю.")
            end)
        end
    },
    ["donate_tfa_destiny_devils_ruin"] = {
        Name = "Devils Ruin (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Devils Ruin.
        Действует 30 дней.]],
        Model = "models/devils ruin.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_destiny_devils_ruin",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 170},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_destiny_devils_ru") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_destiny_devils_ru", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Devils Ruin из спавнменю.")
            end)
        end
    },
    ["donate_tfa_destiny_duality"] = {
        Name = "Duality (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Duality.
        Действует 30 дней.]],
        Model = "models/duality.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_destiny_duality",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 250},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_destiny_duality") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_destiny_duality", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Duality из спавнменю.")
            end)
        end
    },
    ["donate_tfa_destiny_trinity_ghoul"] = {
        Name = "Trinity Ghoul (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Trinity Ghoul.
        Действует 30 дней.]],
        Model = "models/trinity ghoul.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_destiny_trinity_ghoul",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 170},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_destiny_trinity_g") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_destiny_trinity_g", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Trinity Ghoul из спавнменю.")
            end)
        end
    },
    ["donate_tfa_destiny_vex_mythoclast2"] = {
        Name = "Vex Mythoclast (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Vex Mythoclast.
        Действует 30 дней.]],
        Model = "models/weapons/vexmythoclast/c_vex_mythoclast.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_destiny_vex_mythoclast2",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 250},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_destiny_vex_mytho") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_destiny_vex_mytho", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Vex Mythoclast из спавнменю.")
            end)
        end
    },
    ["donate_tfa_destiny_whisper"] = {
        Name = "Whisper of the worm (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Whisper of the worm.
        Действует 30 дней.]],
        Model = "models/weapons/whisper/c_whisper_between_breaths.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_destiny_whisper",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 250},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_destiny_whisper") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_destiny_whisper", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Whisper of the Worm из спавнменю.")
            end)
        end
    },
    ["donate_tfa_doom_ssg"] = {
        Name = "Super Shotgun (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Super Shotgun.
        Действует 30 дней.]],
        Model = "models/weapons/tfa_doom/w_ssg.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_doom_ssg",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 200},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_doom_ssg") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_doom_ssg", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Super Shotgun из спавнменю.")
            end)
        end
    },
    ["donate_tfa_ak117geo"] = {
        Name = "AK117 Geometric (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие AK117 Geometric.
        Действует 30 дней.]],
        Model = "models/weapons/v_ak117geo.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_ak117geo",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 250},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_ak117geo") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_ak117geo", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать AK117 Geometric из спавнменю.")
            end)
        end
    },
    ["donate_tfa_ins2_codol_free"] = {
        Name = "Freedom SR (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Freedom SR.
        Действует 30 дней.]],
        Model = "models/weapons/tfa_ins2/w_codol_freedom.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_ins2_codol_free",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 250},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_ins2_codol_free") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_ins2_codol_free", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Freedom SR из спавнменю.")
            end)
        end
    },
    ["donate_tfa_hailstorm"] = {
        Name = "Hailstorm (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Hailstorm.
        Действует 30 дней.]],
        Model = "models/weapons/hailstorm_rev/w_iw_hailstorm.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_hailstorm",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 150},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_hailstorm") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_hailstorm", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Hailstorm из спавнменю.")
            end)
        end
    },
    ["donate_tfa_bo3_m8a7"] = {
        Name = "M8A7 (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие M8A7.
        Действует 30 дней.]],
        Model = "models/weapons/w_m8a7_bo3.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_bo3_m8a7",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 200},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_bo3_m8a7") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_bo3_m8a7", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать M8A7 из спавнменю.")
            end)
        end
    },
    ["donate_tfa_cso_skull2"] = {
        Name = "SKULL-2 (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие SKULL-2.
        Действует 30 дней.]],
        Model = "models/weapons/tfa_cso/w_skull2.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_cso_skull2",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 200},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_cso_skull2") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_cso_skull2", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать SKULL-2 из спавнменю.")
            end)
        end
    },
    ["donate_tfa_cso_tomahawk"] = {
        Name = "Томагавк (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Томагавк.
        Действует 30 дней.]],
        Model = "models/weapons/tfa_cso/w_tomahawk.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_cso_tomahawk",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 125},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_cso_tomahawk") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_cso_tomahawk", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Томагавк из спавнменю.")
            end)
        end
    },
    ["donate_tfa_cso_ruyi"] = {
        Name = "Посох Короля Обезьян (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Посох Короля Обезьян.
        Действует 30 дней.]],
        Model = "models/weapons/tfa_cso/w_monkeywpnset3.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_cso_ruyi",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 150},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_cso_ruyi") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_cso_ruyi", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Посох Короля Обезьян из спавнменю.")
            end)
        end
    },
    ["donate_tfa_cso_dreadnova"] = {
        Name = "Нова (30D)",
        Description = [[Покупая данную привилегию, вы получите оружие Нова.
        Действует 30 дней.]],
        Model = "models/weapons/tfa_cso/w_dreadnova_a.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "donate_tfa_cso_dreadnova",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 200},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            if ply:HasPurchase("donate_tfa_cso_dreadnova") then
                DarkRP.notify(ply, 1, 5, "Услуга уже подключена.")

                return "fail"
            end

            IGS.PlayerActivateItem(ply, "donate_tfa_cso_dreadnova", function()
                DarkRP.notify(ply, 0, 5, "Теперь вы можете брать Нова из спавнменю.")
            end)
        end
    },
    ["donate_tfa_pack1"] = {
        Name = "Weapon Pack 1 (30D)",
        Description = [[Покупая данную привилегию, вы получите следующий список оружий:
        - M8A7 (30D); 
        - Hailstorm (30D);
        - Freedom SR (30D);
        - AK117 Geometric (30D);
        - Super Shotgun (30D);
        - Volk (30D);
        - SKULL-2 (30D);
        Действует 30 дней.]],
        Model = "models/Items/item_item_crate.mdl",

        IsWeapon = false,
        Class = "donate_tfa_pack1",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 1250},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            for _, item in pairs({"donate_tfa_bo3_m8a7", "donate_tfa_hailstorm",
            "donate_tfa_ins2_codol_free", "donate_tfa_ak117geo", "donate_tfa_doom_ssg",
            "donate_tfa_volk", "donate_tfa_cso_skull2"}) do
                IGS.PlayerActivateItem(ply, item, function()
                end)
            end

            DarkRP.notify(ply, 0, 5, "Теперь вы можете брать предметы Weapon Pack 1 из спавнменю.")
        end
    },
    ["donate_tfa_pack2"] = {
        Name = "Weapon Pack 2 (30D)",
        Description = [[Покупая данную привилегию, вы получите следующий список оружий:
        - Devils Ruin (30D); 
        - Whisper of the worm (30D);
        - Duality (30D);
        - Trinity Ghoul (30D);
        - Vex Mythoclast (30D);
        Действует 30 дней.]],
        Model = "models/Items/item_item_crate.mdl",

        IsWeapon = false,
        Class = "donate_tfa_pack2",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 920},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            for _, item in pairs({"donate_tfa_destiny_devils_ru", "donate_tfa_destiny_whisper",
            "donate_tfa_destiny_duality", "donate_tfa_destiny_trinity_g", "donate_tfa_destiny_vex_mytho"}) do
                IGS.PlayerActivateItem(ply, item, function()
                end)
            end

            DarkRP.notify(ply, 0, 5, "Теперь вы можете брать предметы Weapon Pack 2 из спавнменю.")
        end
    },
    ["donate_tfa_pack_melee"] = {
        Name = "Weapon Pack 3 (30D)",
        Description = [[Покупая данную привилегию, вы получите следующий список оружий:
        - Нова (30D); 
        - Посох Короля Обезьян (30D);
        - Томагавк (30D);
        Действует 30 дней.]],
        Model = "models/Items/item_item_crate.mdl",

        IsWeapon = false,
        Class = "donate_tfa_pack_melee",

        CanTrade = true,
        CanDrop = false,

        Section = "Main Section",

        Price = {Yen = -1, XP = -1, Donate = 400},
        Tier = 5,

        NotDecreasable = false,

        OnUse = function(ply)
            for _, item in pairs({"donate_tfa_cso_dreadnova", "donate_tfa_cso_tomahawk",
            "donate_tfa_cso_ruyi"}) do
                IGS.PlayerActivateItem(ply, item, function()
                end)
            end

            DarkRP.notify(ply, 0, 5, "Теперь вы можете брать предметы Weapon Pack 3 из спавнменю.")
        end
    },
    ["accessories_hatllenn"] = {
        Name = "Шляпа Коммандора (30D)",
        Description = "Голова.",
        Model = "models/prop/llenn_hat.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_hatllenn",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_hatllenn") then
                ply:RemoveAccessories("accessories_hatllenn")
                return
            end

            ply:SetAccessories("accessories_hatllenn")
        end
    },
    ["accessories_winterholiday"] = {
        Name = "Зимний праздник",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/winterholiday.mdl",

        IsWeapon = false,
        Class = "accessories_winterholiday",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = 28800, Donate = -1},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_winterholiday") then
                ply:RemoveAccessories("accessories_winterholiday")
                return
            end

            ply:SetAccessories("accessories_winterholiday")
        end
    },
    ["accessories_snowman"] = {
        Name = "Снеговик",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/snowman.mdl",

        IsWeapon = false,
        Class = "acessories_snowman",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = 28800, Donate = -1},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_snowman") then
                ply:RemoveAccessories("accessories_snowman")
                return
            end

            ply:SetAccessories("accessories_snowman")
        end
    },
    ["accessories_holidaygift"] = {
        Name = "Новогодний Подарок",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/tacticalsanta.mdl",

        IsWeapon = false,
        Class = "accessories_holidaygift",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = 28800, Donate = -1},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_holidaygift") then
                ply:RemoveAccessories("accessories_holidaygift")
                return
            end

            ply:SetAccessories("accessories_holidaygift")
        end
    },
    ["accessories_icecube"] = {
        Name = "Снежный Куб",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/yeti.mdl",

        IsWeapon = false,
        Class = "accessories_icecube",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = 28800, Donate = -1},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_icecube") then
                ply:RemoveAccessories("accessories_icecube")
                return
            end

            ply:SetAccessories("accessories_icecube")
        end
    },
    ["accessories_blackwidow"] = {
        Name = "Крылья Черной Вдовы (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/blackwidow.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_blackwidow",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_blackwidow") then
                ply:RemoveAccessories("accessories_blackwidow")
                return
            end

            ply:SetAccessories("accessories_blackwidow")
        end
    },
    ["accessories_raven"] = {
        Name = "Синие Крылья (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/raven.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_raven",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_raven") then
                ply:RemoveAccessories("accessories_raven")
                return
            end

            ply:SetAccessories("accessories_raven")
        end
    },
    ["accessories_vampire"] = {
        Name = "Вампирские Крылья (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/vampire.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_vampire",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_vampire") then
                ply:RemoveAccessories("accessories_vampire")
                return
            end

            ply:SetAccessories("accessories_vampire")
        end
    },
    ["accessories_samuraiultra"] = {
        Name = "Крылья Самурая (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/samuraiultra.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_samuraiultra",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_samuraiultra") then
                ply:RemoveAccessories("accessories_samuraiultra")
                return
            end

            ply:SetAccessories("accessories_samuraiultra")
        end
    },
    ["accessories_moth"] = {
        Name = "Крылья Моли (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/moth.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_moth",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_moth") then
                ply:RemoveAccessories("accessories_moth")
                return
            end

            ply:SetAccessories("accessories_moth")
        end
    },
    ["accessories_darkviking"] = {
        Name = "Костяные Крылья (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/darkviking.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_darkviking",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_darkviking") then
                ply:RemoveAccessories("accessories_darkviking")
                return
            end

            ply:SetAccessories("accessories_darkviking")
        end
    },
    ["accessories_ghoul"] = {
        Name = "Черные Крылья (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/winterghoul.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_ghoul",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_ghoul") then
                ply:RemoveAccessories("accessories_ghoul")
                return
            end

            ply:SetAccessories("accessories_ghoul")
        end
    },
    ["accessories_katana"] = {
        Name = "Катана (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/werewolf.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_katana",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_katana") then
                ply:RemoveAccessories("accessories_katana")
                return
            end

            ply:SetAccessories("accessories_katana")
        end
    },
    ["accessories_disco"] = {
        Name = "Плеер (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/exercisemale.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_disco",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_disco") then
                ply:RemoveAccessories("accessories_disco")
                return
            end

            ply:SetAccessories("accessories_disco")
        end
    },
    ["accessories_samuraiblue"] = {
        Name = "Сасимоно (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/samuraiblue.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_samuraiblue",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_samuraiblue") then
                ply:RemoveAccessories("accessories_samuraiblue")
                return
            end

            ply:SetAccessories("accessories_samuraiblue")
        end
    },
    ["accessories_guitar"] = {
        Name = "Гитара (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/hippieguitar.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_guitar",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_guitar") then
                ply:RemoveAccessories("accessories_guitar")
                return
            end

            ply:SetAccessories("accessories_guitar")
        end
    },
    ["accessories_garageband"] = {
        Name = "Гараж Бенд (30D)",
        Description = "Спина.",
        Model = "models/konnie/asapgaming/fortnite/backpacks/garagebandmale.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_garageband",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_garageband") then
                ply:RemoveAccessories("accessories_garageband")
                return
            end

            ply:SetAccessories("accessories_garageband")
        end
    },
    ["accessories_afro"] = {
        Name = "Афро (30D)",
        Description = "Голова.",
        Model = "models/gmod_tower/afro.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_afro",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_afro") then
                ply:RemoveAccessories("accessories_afro")
                return
            end

            ply:SetAccessories("accessories_afro")
        end
    },
    ["accessories_nojiggle"] = {
        Name = "Пони (30D)",
        Description = "Голова.",
        Model = "models/gmod_tower/balloonicorn_nojiggle.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_nojiggle",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_nojiggle") then
                ply:RemoveAccessories("accessories_nojiggle")
                return
            end

            ply:SetAccessories("accessories_nojiggle")
        end
    },
    ["accessories_neko"] = {
        Name = "Кошачьи Уши (30D)",
        Description = "Голова.",
        Model = "models/gmod_tower/catears.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_neko",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_neko") then
                ply:RemoveAccessories("accessories_neko")
                return
            end

            ply:SetAccessories("accessories_neko")
        end
    },
    ["accessories_headphones"] = {
        Name = "Наушники (30D)",
        Description = "Голова.",
        Model = "models/gmod_tower/headphones.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_headphones",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply) 
            if ply:HasAccessories("accessories_headphones") then
                ply:RemoveAccessories("accessories_headphones")
                return
            end

            ply:SetAccessories("accessories_headphones")
        end
    },
    ["accessories_santahat"] = {
        Name = "Шапка Санты",
        Description = "Голова.",
        Model = "models/captainbigbutt/skeyler/hats/santa.mdl",

        IsWeapon = false,
        Class = "accessories_santahat",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = 28800, Donate = -1},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_santahat") then
                ply:RemoveAccessories("accessories_santahat")
                return
            end

            ply:SetAccessories("accessories_santahat")
        end
    },
    ["accessories_strawhat"] = {
        Name = "Соломенная Шляпа (30D)",
        Description = "Голова.",
        Model = "models/captainbigbutt/skeyler/hats/strawhat.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_strawhat",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_strawhat") then
                ply:RemoveAccessories("accessories_strawhat")
                return
            end

            ply:SetAccessories("accessories_strawhat")
        end
    },
    ["accessories_bunnyears"] = {
        Name = "Кроличьи Уши (30D)",
        Description = "Голова.",
        Model = "models/captainbigbutt/skeyler/hats/bunny_ears.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_bunnyears",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_bunnyears") then
                ply:RemoveAccessories("accessories_bunnyears")
                return
            end

            ply:SetAccessories("accessories_bunnyears")
        end
    },
    ["accessories_sunhat"] = {
        Name = "Солнечная Шляпа (30D)",
        Description = "Голова.",
        Model = "models/captainbigbutt/skeyler/hats/sunhat.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_sunhat",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_sunhat") then
                ply:RemoveAccessories("accessories_sunhat")
                return
            end

            ply:SetAccessories("accessories_sunhat")
        end
    },
    ["accessories_ducktube"] = {
        Name = "Уточка (30D)",
        Description = "Голова.",
        Model = "models/captainbigbutt/skeyler/accessories/duck_tube.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_ducktube",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_ducktube") then
                ply:RemoveAccessories("accessories_ducktube")
                return
            end

            ply:SetAccessories("accessories_ducktube")
        end
    },
    ["accessories_crown"] = {
        Name = "Корона (30D)",
        Description = "Голова.",
        Model = "models/lordvipes/peachcrown/peachcrown.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_crown",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 500000, XP = 28800, Donate = 75},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_crown") then
                ply:RemoveAccessories("accessories_crown")
                return
            end

            ply:SetAccessories("accessories_crown")
        end
    },
    ["accessories_glasses04"] = {
        Name = "Авиаторные Очки (30D)",
        Description = "Лицо.",
        Model = "models/captainbigbutt/skeyler/accessories/glasses04.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_glasses04",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 300000, XP = 18800, Donate = 55},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_glasses04") then
                ply:RemoveAccessories("accessories_glasses04")
                return
            end

            ply:SetAccessories("accessories_glasses04")
        end
    },
    ["accessories_glasses02"] = {
        Name = "Черные Очки (30D)",
        Description = "Лицо.",
        Model = "models/captainbigbutt/skeyler/accessories/glasses02.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_glasses02",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 300000, XP = 18800, Donate = 55},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_glasses02") then
                ply:RemoveAccessories("accessories_glasses02")
                return
            end

            ply:SetAccessories("accessories_glasses02")
        end
    },
    ["accessories_3dglasses"] = {
        Name = "3Д Очки (30D)",
        Description = "Лицо.",
        Model = "models/gmod_tower/3dglasses.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_3dglasses",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 300000, XP = 18800, Donate = 55},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_3dglasses") then
                ply:RemoveAccessories("accessories_3dglasses")
                return
            end

            ply:SetAccessories("accessories_3dglasses")
        end
    },
    ["accessories_snowboard"] = {
        Name = "Сноуборд Очки (30D)",
        Description = "Лицо.",
        Model = "models/gmod_tower/snowboardgoggles.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_snowboard",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 300000, XP = 18800, Donate = 55},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_snowboard") then
                ply:RemoveAccessories("accessories_snowboard")
                return
            end

            ply:SetAccessories("accessories_snowboard")
        end
    },
    ["accessories_starglasses"] = {
        Name = "Звездные Очки (30D)",
        Description = "Лицо.",
        Model = "models/gmod_tower/starglasses.mdl",
        Duration = 30,

        IsWeapon = false,
        Class = "accessories_starglasses",

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = 300000, XP = 18800, Donate = 55},
        Tier = 3,

        NotDecreasable = true,

        OnUse = function(ply)
            if ply:HasAccessories("accessories_starglasses") then
                ply:RemoveAccessories("accessories_starglasses")
                return
            end

            ply:SetAccessories("accessories_starglasses")
        end
    },
    ["skin_random"] = {
        Name = "Коробка со скинами (30D) [Общее]",
        Description = "Коробка, из которой вам выпадет случайный скин.\nСписок предметов:\n1. Poyo (Шанс выпадения: 2%)\n2. Zhongli (Шанс выпадения: 2%)\n3. Muffler (Шанс выпадения: 2%)\n4. Shishiro (Шанс выпадения: 2%)\n5. Qiqi (Шанс выпадения: 2%)\n6. Elysia (Шанс выпадения: 2%)\n7. Sora (Шанс выпадения: 5%)\n8. Megurine Luka (Шанс выпадения: 5%)\n9. Colorful Miku (Шанс выпадения: 5%)\n10. Shinmyoumaru (Шанс выпадения: 5%)\n11. Noelle (Шанс выпадения: 5%)\n12. Kira Yoshikage (Шанс выпадения: 5%)\n13. Adam (Шанс выпадения: 5%)\n14. Gyakushinn Miko (Шанс выпадения: 5%)\n15. Plutia (Шанс выпадения: 5%)\n16. Welt Yang (Шанс выпадения: 5%)\n17. Ribbon Girl Miku (Шанс выпадения: 5%)\n18. Luka Append (Шанс выпадения: 5%)\n19. Kanna Kamui (Шанс выпадения: 4%)\n20. Kasen Ibaraki (Шанс выпадения: 4%)\n21. Mirai Akari (Шанс выпадения: 4%)\n22. HK416 (Шанс выпадения: 4%)\n23. Akashi (Шанс выпадения: 4%)\n24. Chino Kafuu (Шанс выпадения: 4%)\n25. Chino Kafuu Maid (Шанс выпадения: 4%)",
        Model = "models/Items/item_item_crate.mdl",

        IsWeapon = false,
        Class = "skin_random",

        Items = {
            {"playermodel_poyo", 2},
            {"playermodel_zhongli", 2},
            {"playermodel_muffler", 2},
            {"playermodel_shishiro", 2},
            {"playermodel_elysia", 2},
            {"playermodel_sora", 5},
            {"playermodel_megurine", 5},
            {"playermodel_miku", 5},
            {"playermodel_shinmyoumaru", 5},
            {"playermodel_noelle", 5},
            {"playermodel_kira", 5},
            {"playermodel_adam", 5},
            {"playermodel_miko", 5},
            {"playermodel_plutia", 5},
            {"playermodel_yang", 5},
            {"playermodel_ribbon", 5},
            {"playermodel_lukaa", 5},
            {"playermodel_kanna", 4},
            {"playermodel_kasen", 4},
            {"playermodel_akari", 4},
            {"playermodel_hk416", 4},
            {"playermodel_akashi", 4},
            {"playermodel_chino", 4},
            {"playermodel_chinoo", 4},
            {"playermodel_qiqi", 2} ,
        },

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = 28800, Donate = 125},
        Tier = 3,

        Duration = 30,

        OnUse = function(ply)
            InventoryOpenBox(ply, "skin_random")
        end
    },

    ["skin_mens"] = {
        Name = "Коробка со скинами (30D) [Мужские]",
        Description = "Коробка, из которой вам выпадет случайный скин.\nСписок предметов:\n1. Zhongli (Шанс выпадения: 9%)\n2. Kira Yoshikage (Шанс выпадения: 9%)\n3. Adam (Шанс выпадения: 41%)\n4. Welt Yang (Шанс выпадения: 41%)",
        Model = "models/Items/item_item_crate.mdl",

        IsWeapon = false,
        Class = "skin_mens",

        Items = {
            {"playermodel_zhongli", 9},
            {"playermodel_kira", 9},
            {"playermodel_adam", 41},
            {"playermodel_yang", 41},
            },

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = 48000, Donate = 175},
        Tier = 3,

        Duration = 30,

        OnUse = function(ply)
            InventoryOpenBox(ply, "skin_mens")
        end
    },

    ["skin_rare"] = {
        Name = "Коробка со скинами (30D) [Редкие]",
        Description = "Коробка, из которой вам выпадет случайный скин.\nСписок предметов:\n1. Poyo (Шанс выпадения: 17%)\n2. Zhongli (Шанс выпадения: 17%)\n3. Muffler (Шанс выпадения: 16%)\n4. Shishiro (Шанс выпадения: 17%)\n5. Elysia (Шанс выпадения: 16%)\n6. Shishiro (Шанс выпадения: 17%)",
        Model = "models/Items/item_item_crate.mdl",

        IsWeapon = false,
        Class = "skin_rare",

        Items = {
            {"playermodel_poyo", 17},
            {"playermodel_zhongli", 17},
            {"playermodel_muffler", 16},
            {"playermodel_shishiro", 17},
            {"playermodel_elysia", 16},
            {"playermodel_qiqi", 17} ,
            },

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = 115200, Donate = 400},
        Tier = 5,

        Duration = 30,

        OnUse = function(ply)
            InventoryOpenBox(ply, "skin_rare")
        end
    },

    ["skin_loli"] = {
        Name = "Коробка со скинами (30D) [Лоли]",
        Description = "Коробка, из которой вам выпадет случайный скин.\nСписок предметов:\n1. Chino Kafuu (Шанс выпадения: 18%)\n2. Chino Kafuu Maid (Шанс выпадения: 18%)\n3. Akashi (Шанс выпадения: 18%)\n4. Kanna Kamui (Шанс выпадения: 18%)\n5. Shinmyoumaru (Шанс выпадения: 21%)\n6. Qiqi (Шанс выпадения: 7%)",
        Model = "models/Items/item_item_crate.mdl",

        IsWeapon = false,
        Class = "skin_loli",

        Items = {
            {"playermodel_chinoo", 18},
            {"playermodel_chino", 18},
            {"playermodel_akashi", 18},
            {"playermodel_kanna", 18},
            {"playermodel_shinmyoumaru", 21},
            {"playermodel_qiqi", 7},
            },

        CanTrade = true,
        CanDrop = false,

        Section = "Accessories Section",

        Price = {Yen = -1, XP = 48000, Donate = 175},
        Tier = 3,

        Duration = 30,

        OnUse = function(ply)
            InventoryOpenBox(ply, "skin_loli")
        end
    },
}
InventorySellPrice = function(store_price)
    return math.floor(store_price / 2.5)
end

local function LoadItem(name, item)
    local uid = string.StripExtension(name)

    InventoryItems[uid] = item

    return item
end

-- hook.Add("Initialize", "animelife.inventory.items", function()
    -- local dir = "addons/animelife/lua/darkrp_modules/inventory/itemlist/"
    -- for _, name in pairs(file.Find(dir .. "*.lua", "GAME")) do
    --     ITEM = {}

    --     if SERVER then
    --         AddCSLuaFile("itemlist/" .. name)
    --     end
        
    --     include("itemlist/" .. name)

    --     LoadItem(name, ITEM)

    --     ITEM = nil
    -- end
-- end)