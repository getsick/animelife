util.AddNetworkString("animelife.inventory.accessories.broadcast_offset")
util.AddNetworkString("animelife.inventory.accessories.equip_item")

local meta = FindMetaTable("Player")

module("inventory.accessories", package.seeall)

LateJoinAccessories = LateJoinAccessories or {}
LateJoinOffsets = LateJoinOffsets or {}

function meta:SetAccessories(id, send)
    if !send then
        send = player.GetHumans()
    end

    net.Start("animelife.inventory.accessories.equip_item")
        net.WriteEntity(self)
        net.WriteString(id)
        net.WriteBool(true)
    net.Send(send)

    -- don't change global table if it's latejoin call
    if istable(send) then
        if !LateJoinAccessories[self:SteamID()] then
            LateJoinAccessories[self:SteamID()] = {}
        end

        LateJoinAccessories[self:SteamID()][id] = true
    end
end

function meta:RemoveAccessories(id, send)
    if !send then
        send = player.GetHumans()
    end

    net.Start("animelife.inventory.accessories.equip_item")
        net.WriteEntity(self)
        net.WriteString(id)
        net.WriteBool(false)
    net.Send(send)

    -- don't change global table if it's latejoin call
    if istable(send) then
        if LateJoinAccessories[self:SteamID()] then
            LateJoinAccessories[self:SteamID()][id] = nil
        end
    end
end

function meta:HasAccessories(id)
    if !LateJoinAccessories[self:SteamID()] then
        return false
    end

    return LateJoinAccessories[self:SteamID()][id]
end

concommand.Add("al_accessories_saveoffset", function(ply, cmd, args)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !isstring(args[8]) then return end

    -- didn't bother to set accessories first? get out.
    if !LateJoinAccessories[ply:SteamID()] then return end

    local cat = args[8]
    -- is it even valid?
    if cat ~= "Head" and cat ~= "Spine" and cat ~= "Eyes" then
        return 
    end

    local save_tbl = {}

    -- does it work correctly at all times? idk.
    for i = 1, 7 do
        if !isnumber(tonumber(args[i])) then return end
        
        table.insert(save_tbl, tonumber(args[i]))
    end

    -- this seems like a complete disaster
    -- por favor mátame c'mon
    net.Start("animelife.inventory.accessories.broadcast_offset")
        net.WriteEntity(ply)
        net.WriteString(cat)
        net.WriteFloat(args[1])
        net.WriteFloat(args[2])
        net.WriteFloat(args[3])
        net.WriteFloat(args[4])
        net.WriteFloat(args[5])
        net.WriteFloat(args[6])
        net.WriteFloat(args[7])
    net.Broadcast()

    if !LateJoinOffsets[ply:SteamID()] then
        LateJoinOffsets[ply:SteamID()] = {}
    end

    LateJoinOffsets[ply:SteamID()][cat] = save_tbl
end)

local function late_join_send_accessories(ply)
    -- whoa, no one's wearing anything atm
    if table.IsEmpty(LateJoinAccessories) then return end

    for id, item in pairs(LateJoinAccessories) do
        local t = player.GetBySteamID(id)
        if !IsValid(t) or !t:IsPlayer() then
            -- old data, we don't need it anymore
            LateJoinAccessories[id] = nil

            continue
        end
        
        for class, valid in pairs(item) do
            if valid then
                t:SetAccessories(class, ply)
            end
        end
    end
end

local function late_join_send_offsets(ply)
    if table.IsEmpty(LateJoinOffsets) then return end

    for id, cat in pairs(LateJoinOffsets) do
        local t = player.GetBySteamID(id)
        if !IsValid(t) or !t:IsPlayer() then
            -- old data, we don't need it anymore
            LateJoinOffsets[id] = nil

            continue 
        end

        for category, val in pairs(cat) do
            -- tous les mêmes
            net.Start("animelife.inventory.accessories.broadcast_offset")
                net.WriteEntity(t)
                net.WriteString(category)
                net.WriteFloat(val[1])
                net.WriteFloat(val[2])
                net.WriteFloat(val[3])
                net.WriteFloat(val[4])
                net.WriteFloat(val[5])
                net.WriteFloat(val[6])
                net.WriteFloat(val[7])
            net.Send(ply)
        end
    end
end

hook.Add("PlayerInitialSpawn", "animelife.inventory.accessories.latejoin", function(ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    -- hopefully player is fully connected and *will* receive accessories data
    late_join_send_accessories(ply)
    late_join_send_offsets(ply)
end)