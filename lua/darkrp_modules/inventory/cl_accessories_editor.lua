local PANEL = {}

surface.CreateFont("animelife.accessorieseditor.title", {font = "Exo 2 SemiBold", size = 19, weight = 500, extended = true})
surface.CreateFont("animelife.accessorieseditor.label", {font = "Exo 2 SemiBold", size = 15, weight = 500, extended = true})

function PANEL:Init()
    self.Sliders = {}
    self.Category = "Head"

    self.SaveValues = {
        x = 0, y = 0, z = 0,
        pitch = 0, yaw = 0, roll = 0,
        scale = 1
    }

    local mins_maxs = {
        [1] = {-10, 10},
        [2] = {-10, 10},
        [3] = {-10, 10},
        [4] = {-360, 360},
        [5] = {-360, 360},
        [6] = {-360, 360},
        [7] = {0.25, 1.75}
    }

    for i, text in pairs{"Vector X", "Vector Y", "Vector Z", "Pitch", "Yaw", "Roll", "Scale"} do
        local slider = vgui.Create("animelife.settings.slider", self)
        slider:SetMin(mins_maxs[i][1])
        slider:SetMax(mins_maxs[i][2])
        slider.OnValueChanged = function(_, new)
            -- efficiency? didn't heard of that
            -- TODO: grab&set the original values beforehand

            local old = {
                x = 0, y = 0, z = 0,
                pitch = 0, yaw = 0, roll = 0,
                scale = 1
            }
            if LocalPlayer().OffsetData then
                old = LocalPlayer().OffsetData[self.Category] or {
                    x = 0, y = 0, z = 0,
                    pitch = 0, yaw = 0, roll = 0,
                    scale = 1
                }
            end

            LocalPlayer():SetClientsideModelOffset(self.Category, 
            (i == 1 and new or old.x), 
            (i == 2 and new or old.y), 
            (i == 3 and new or old.z), 
            (i == 4 and new or old.pitch), 
            (i == 5 and new or old.yaw), 
            (i == 6 and new or old.roll), 
            (i == 7 and new or old.scale))

            self.SaveValues = LocalPlayer().OffsetData[self.Category] or {
                x = 0, y = 0, z = 0,
                pitch = 0, yaw = 0, roll = 0,
                scale = 1
            }
        end

        local label = vgui.Create("DLabel", self)
        label:SetFont("animelife.accessorieseditor.label")
        label:SetText(text)
        label:SetColor(Color(255, 255, 255))

        table.insert(self.Sliders, {label, slider})
    end

    self.Categories = {}

    for i = 1, 3 do
        local btn = vgui.Create("DButton", self)
        btn:SetText("")
        btn.Paint = function(pnl, w, h)
            local cat = (i == 1 and "Head" or (i == 2 and "Eyes" or "Spine"))
            surface.SetDrawColor(255, 153, 230, self.Category == cat and 112 or 32)
            surface.DrawRect(0, 0 ,w, h)
    
            draw.SimpleText(i == 1 and "Голова" or (i == 2 and "Глаза" or "Спина"), "animelife.accessorieseditor.label", w / 2, h / 2, Color(255, 255, 255), 1, 1)
        end
        btn.DoClick = function()
            self.Category = (i == 1 and "Head" or (i == 2 and "Eyes" or "Spine"))
        end

        table.insert(self.Categories, btn)
    end

    self.ResetButton = vgui.Create("DButton", self)
    self.ResetButton:SetText("")
    self.ResetButton.Paint = function(pnl, w, h)
        surface.SetDrawColor(153, 255, 187, 112)
        surface.DrawRect(0, 0 ,w, h)

        draw.SimpleText("Сбросить настройки", "animelife.accessorieseditor.label", w / 2, h / 2, Color(255, 255, 255), 1, 1)
    end
    self.ResetButton.DoClick = function()
        RunConsoleCommand("al_accessories_saveoffset", 0, 0, 0, 0, 0, 0, 1, self.Category)
    end

    self.LeaveButton = vgui.Create("DButton", self)
    self.LeaveButton:SetText("")
    self.LeaveButton.Paint = function(pnl, w, h)
        surface.SetDrawColor(255, 153, 230)
        surface.DrawRect(0, 0 ,w, h)

        draw.SimpleText("Выйти из редактора", "animelife.accessorieseditor.label", w / 2, h / 2, Color(255, 255, 255), 1, 1)
    end
    self.LeaveButton.DoClick = function()
        inventory.accessories.LeaveEditingMode(LocalPlayer())

        RunConsoleCommand("al_accessories_saveoffset", self.SaveValues.x, self.SaveValues.y, self.SaveValues.z, 
        self.SaveValues.pitch, self.SaveValues.yaw, self.SaveValues.roll, self.SaveValues.scale, self.Category)

        if ValidPanel(self) then
            self:Remove()
            self = nil
        end
    end
end

function PANEL:PerformLayout(w, h)
    local x, y = 64, 64
    for i, slider in pairs(self.Sliders) do
        slider[1]:SetPos(x + 16, y - 16)
        slider[1]:SizeToContents()
        slider[2]:SetPos(x, y)
        slider[2]:SetSize(273, 11)

        if i % 3 == 0 then
            y = 64
            x = x + 273 + 64
        else
            y = y + 64
        end
    end

    local btn_y = 64
    for _, cat in pairs(self.Categories) do
        cat:SetSize(128, 32)
        cat:SetPos(w - 128 - 64, btn_y)

        btn_y = btn_y + 32 + 16
    end

    self.ResetButton:SetPos(w - 128 - 64, btn_y)
    self.ResetButton:SetSize(128, 32)

    self.LeaveButton:SetPos((w - 128) / 2, h - 54)
    self.LeaveButton:SetSize(128, 32)
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(0, 0, 0)
    surface.DrawRect(0, 0, w, h)

    draw.SimpleText("Редактор положения аксессуаров", "animelife.accessorieseditor.title", w / 2, 16, Color(255, 255, 255), 1)
end

vgui.Register("animelife.accessories.editor", PANEL, "EditablePanel")