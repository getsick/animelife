MySQLite.begin()

MySQLite.query([[
    CREATE TABLE IF NOT EXISTS al_inventory(
        sid TINYTEXT NOT NULL PRIMARY KEY,
        invdata LONGTEXT NOT NULL,
        size MEDIUMINT NOT NULL
    );
]])

module("inventory", package.seeall)

function inventory:Compress(inv)
    local compressed_inv = inv
    compressed_inv = util.TableToJSON(inv)
    -- compressed_inv = util.Compress(compressed_inv)
    compressed_inv = util.Base64Encode(compressed_inv)

    return compressed_inv
end

function inventory:Decompress(inv, size)
    local decompressed_inv = inv
    -- decompressed_inv = util.Decompress(decompressed_inv, size)
    decompressed_inv = util.Base64Decode(inv)

    return decompressed_inv
end

function inventory:Save(ply)
    local inv_data = ply.Inventory
    inv_data = self:Compress(inv_data)
    local size = inv_data:len()

    MySQLite.query([[SELECT size
    FROM al_inventory
    where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
        if !data then
            MySQLite.query([[REPLACE INTO al_inventory VALUES(]] ..
            MySQLite.SQLStr(ply:SteamID()) .. [[, ]] ..
            MySQLite.SQLStr(inv_data) .. [[, ]] ..
            tonumber(size) .. ");")
        end

        MySQLite.query([[UPDATE al_inventory SET invdata = ]] .. MySQLite.SQLStr(inv_data) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
        MySQLite.query([[UPDATE al_inventory SET size = ]] .. tonumber(size) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
    end, function(e, q) 
        print("Database Error:", e, q)
    end)
end

function inventory:Restore(ply)
    MySQLite.query([[
        SELECT invdata, size
        FROM al_inventory
        where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
            if !data then return end

            local size = data[1].size
            local d = data[1].invdata
            local inv = self:Decompress(d, size)
            inv = util.JSONToTable(inv)
            if !istable(inv) then return end
            if table.IsEmpty(inv) then return end

            -- instantly set it
            ply.Inventory = inv

            -- now network it (slow?)
            for cat, slot in pairs(ply.Inventory or {}) do
                if table.IsEmpty(slot) then continue end -- skip empty slots
                for id, item in pairs(slot or {}) do
                    if table.IsEmpty(item) then continue end -- skip empty slots

                    self:Network(ply, ply.Inventory[cat][id], cat, id)
                end
            end
        end, function(e, q)
            print("Database Error:", e, q)
        end)
end

hook.Add("PlayerInitialSpawn", "animelife.inventory.db", function(ply)
    timer.Simple(5, function()
        if !IsValid(ply) or !ply:IsPlayer() then return end

        inventory:Restore(ply)
    end)
end)