MySQLite.begin()

MySQLite.query([[
    CREATE TABLE IF NOT EXISTS al_achievements(
        sid TINYTEXT NOT NULL PRIMARY KEY,
        achieved MEDIUMTEXT NOT NULL,
        unfinished MEDIUMTEXT NOT NULL
    );
]])

module("achievements", package.seeall)

function SaveData(ply)
    MySQLite.query([[SELECT achieved, unfinished
    FROM al_achievements
    where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
        if !data then
            MySQLite.query([[REPLACE INTO al_achievements VALUES(]] ..
            MySQLite.SQLStr(ply:SteamID()) .. [[, ]] ..
            MySQLite.SQLStr("")  .. [[, ]] ..
            MySQLite.SQLStr("") .. ");")
        end

        local achieved = ply:GetNWString("animelife.achievements", "[]")
        local unfinished = ply:GetNWString("animelife.achievements.unfinished", "[]")

        MySQLite.query([[UPDATE al_achievements SET achieved = ]] .. MySQLite.SQLStr(achieved) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
        MySQLite.query([[UPDATE al_achievements SET unfinished = ]] .. MySQLite.SQLStr(unfinished) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
    end, function(e, q) 
        print("Database Error:", e, q)
    end)
end

function RestoreData(ply)
    MySQLite.query([[
        SELECT achieved, unfinished
        FROM al_achievements
        where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
            if !data then return end
            
            if data[1].achieved then
                ply:SetNWString("animelife.achievements", data[1].achieved)
            end

            if data[1].unfinished then
                ply:SetNWString("animelife.achievements.unfinished", data[1].unfinished)
            end
        end, function(e, q)
            print("Database Error:", e, q)
        end)
end

hook.Add("PlayerInitialSpawn", "animelife.achievements.db", function(ply)
    timer.Simple(3, function()
        if !IsValid(ply) or !ply:IsPlayer() then return end

        RestoreData(ply)
    end)
end)