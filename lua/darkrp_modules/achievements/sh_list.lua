local garb_col = Material("animelife/scoreboard/achievement/garbage_collector.png")
local trees = Material("animelife/scoreboard/achievement/0.png")
local builder = Material("animelife/scoreboard/achievement/builder.png")
local customizer = Material("animelife/scoreboard/achievement/customizer.png")
local ddr = Material("animelife/scoreboard/achievement/ddr.png")
local illusioner = Material("animelife/scoreboard/achievement/illusioner.png")
local stabbed = Material("animelife/scoreboard/achievement/stabbed.png")
local snd_name = Material("animelife/scoreboard/achievement/second_name.png")
local stolen = Material("animelife/scoreboard/achievement/stolen.png")
local yandere = Material("animelife/scoreboard/achievement/yandere.png")
local no_regrets = Material("animelife/scoreboard/achievement/no_regrets.png")
local titan = Material("animelife/scoreboard/achievement/titan.png")
local spell_teller = Material("animelife/scoreboard/achievement/spell_teller.png")

timer.Simple(0.0, function()
    achievements:New(1, "Мусорный король", "Работа вне производства", garb_col, 500, function()

    end)
    -- achievements:New("Среди деревьев", "Полезно быть на улице", trees, 4, function()

    -- end)
    achievements:New(3, "Строитель", "Точность и усидчивость", builder, 1, function()
        
    end)
    achievements:New(4, "Исчезли", "А какое оправдание?", stolen, 50, function()
        
    end)
    achievements:New(5, "Самостоятельный", "Сделаю лучше", customizer, 1, function()
        
    end)
    achievements:New(6, "Dance Dance Revolution", "Наблюдайте за моими движениями", ddr, 1, function()
        
    end)
    achievements:New(7, "Иллюзионист", "Предан работе", illusioner, 25, function()
        
    end)
    achievements:New(8, "Исполнено", "Профессионал своего дела", no_regrets, 25, function()
        
    end)
    achievements:New(9, "Милый", "Связь установлена", yandere, 1, function()
        
    end)
    achievements:New(10, "Второе имя", "На 100% круче предыдущего", snd_name, 1, function()
        
    end)
    achievements:New(11, "Удар в спину", "Так не делают...", stabbed, 1, function()
        
    end)
    achievements:New(12, "Титан", "Какой большой...", titan, 1, function()
        
    end)
    achievements:New(13, "Заклинатель", "Слова = сила", spell_teller, 15, function()
        
    end)
end)