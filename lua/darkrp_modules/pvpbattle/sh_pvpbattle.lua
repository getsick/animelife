module("dueling", package.seeall)

weapon_types = {
    ["full"] = {}, -- fills up auto
    ["pistols"] = {"lowpoly_m1911", "lowpoly_g18", "lowpoly_deagle"},
    ["snipers"] = {"lowpoly_svd", "lowpoly_m82a3"},    
}

for k, cat in pairs(weapon_types) do
    if k == "full" then continue end

    for _, w in pairs(cat) do
        table.insert(weapon_types["full"], w)
    end
end