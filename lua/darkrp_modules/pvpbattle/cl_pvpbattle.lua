local detail_cloud = Material("animelife/pvpbattle/detail_cloud.png")

local color_barbg = Color(0, 0, 0)
local color_bar = Color(255, 255, 255)

hook.Add("HUDPaint", "animelife.pvpbattle.hud", function()
    if !IsValid(LocalPlayer()) then return end
    if ValidPanel(GLOBALS_SCOREBOARD) and GLOBALS_SCOREBOARD:IsVisible() then return end

    local opponent = LocalPlayer():GetNWEntity("animelife.pvpbattle.opponent")
    if IsValid(opponent) and opponent:IsPlayer() then
        local cloud_float = math.sin(CurTime() * 2) * 8

        surface.SetDrawColor(color_white)
        surface.SetMaterial(detail_cloud)
        surface.DrawTexturedRect((ScrW() - 171) / 2, 128 + cloud_float, 171, 98)

        draw.SimpleText(LocalPlayer():Nick(), "animelife.Global.HUD_Money", ScrW() / 2 - 86, 128 + cloud_float + 16, color_white, 2)

        surface.SetDrawColor(color_barbg)
        surface.DrawRect(ScrW() / 2 - 128 - 48, 128 + cloud_float + 42, 128, 8)

        render.SetScissorRect(ScrW() / 2 - 128 - 48, 0, ScrW() / 2 - 128 - 48 + (128 * (LocalPlayer():Health() / LocalPlayer():GetMaxHealth())), ScrH(), true)
            surface.SetDrawColor(color_bar)
            surface.DrawRect(ScrW() / 2 - 128 - 48, 128 + cloud_float + 42, 128, 8)
        render.SetScissorRect(0, 0, 0, 0, false)

        draw.SimpleText(opponent:Nick(), "animelife.Global.HUD_Money", ScrW() / 2 + 86, 128 + cloud_float + 16, color_white)

        surface.SetDrawColor(color_barbg)
        surface.DrawRect(ScrW() / 2 + 48, 128 + cloud_float + 42, 128, 8)

        render.SetScissorRect(ScrW() / 2 + 48, 0, ScrW() / 2 + 48 + (128 * (opponent:Health() / opponent:GetMaxHealth())), ScrH(), true)
            surface.SetDrawColor(color_bar)
            surface.DrawRect(ScrW() / 2 + 48, 128 + cloud_float + 42, 128, 8)
        render.SetScissorRect(0, 0, 0, 0, false)
    end
end)