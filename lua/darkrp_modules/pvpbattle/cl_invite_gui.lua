local PANEL = {}

surface.CreateFont("animelife.pvpbattle.invite.title", {font = "Exo 2 SemiBold", size = 16, weight = 600, extended = true})

local weapon_list = {
    ["full"] = "Полный набор оружия",
    ["full_random"] = "Случайное оружие из полного набора",
    ["pistols"] = "Пистолеты [Комплект оружия]",
    ["pistols_random"] = "Пистолеты [Случайное]",
    ["snipers"] = "Снайперские винтовки [Комплект оружия]",
    ["snipers_random"] = "Снайперские винтовки [Случайное]",
}

function PANEL:Init()
    self.PlayerPick = vgui.Create("DComboBox", self)
    self.PlayerPick:SetValue("Игрок не выбран")

    local player_table = player.GetHumans()
    timer.Simple(0, function()
        for _, ply in pairs(player_table) do
            self.PlayerPick:AddChoice(ply:Nick() .. " [" .. ply:SteamID() .. "]", nil, self.Player == ply)
        end
    end)

    self.WeaponPick = vgui.Create("DComboBox", self)
    self.WeaponPick:SetValue("Комплект не выбран")
    self.WeaponPick.ChoiceList = {}

    for k, w in pairs(weapon_list) do
        self.WeaponPick:AddChoice(w)

        self.WeaponPick.ChoiceList[#self.WeaponPick.ChoiceList + 1] = k
    end

    self.BetInput = vgui.Create("DTextEntry", self)
    
    self.YenCheck = vgui.Create("DCheckBoxLabel", self)
    self.YenCheck:SetText("Иены")
    self.YenCheck:SetValue(true)
    self.YenCheck.OnChange = function(b)
        if b then
            self.XPCheck:SetChecked(false)
        end
    end

    self.XPCheck = vgui.Create("DCheckBoxLabel", self)
    self.XPCheck:SetText("Опыт")
    self.XPCheck:SetValue(false)
    self.XPCheck.OnChange = function(b)
        if b then
            self.YenCheck:SetChecked(false)
        end
    end

    self.SendButton = vgui.Create("DButton", self)
    self.SendButton:SetText("")
    self.SendButton.Paint = function(pnl, w, h)
        draw.RoundedBox(8, 0, 0, w, h, Color(0, 0, 0))

        draw.SimpleText("Отправить приглашение", "animelife.administration.report.title", w / 2, h / 2, color_white, 1, 1)
    end
    self.SendButton.DoClick = function()
        local pick = self.PlayerPick:GetSelectedID()
        if !isnumber(pick) then return end
        local ply = player_table[pick]
        if !IsValid(ply) then return end
        if !self.YenCheck:GetChecked() and !self.XPCheck:GetChecked() then return end
        
        local id = self.WeaponPick:GetSelectedID()
        RunConsoleCommand("al_duelinvite", ply:SteamID(), self.BetInput:GetValue(), self.YenCheck:GetChecked() and 1 or 2, self.WeaponPick.ChoiceList[id])

        self:Remove()
        self = nil
    end

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("")
    self.CloseButton.Paint = function(pnl, w, h)
        draw.SimpleText("x", "animelife.administration.report.title", w / 2, h / 2, color_white, 1, 1)
    end
    self.CloseButton.DoClick = function()
        self:Remove()
        self = nil
    end
end

function PANEL:PerformLayout(w, h)
    self.SendButton:SetPos((w - 252) / 2, h - 50 - 44)
    self.SendButton:SetSize(252, 50)

    local y = 81 + 25
    self.PlayerPick:SetPos((w - 352) / 2, y)
    self.PlayerPick:SetSize(352, 36)
    y = y + 36 + 25

    self.WeaponPick:SetPos((w - 352) / 2, y)
    self.WeaponPick:SetSize(352, 36)
    y = y + 36 + 25

    self.BetInput:SetPos((w - 352) / 2, y)
    self.BetInput:SetSize(216, 36)

    local x = self.BetInput:GetWide() + 16
    self.YenCheck:SetPos((w - 352) / 2 + x, y + 12)
    self.YenCheck:SizeToContents()
    x = x + self.YenCheck:GetWide() + 8

    self.XPCheck:SetPos((w - 352) / 2 + x, y + 12)
    self.XPCheck:SizeToContents()

    self.CloseButton:SetPos(w - 24 - 12, 12)
    self.CloseButton:SetSize(24, 24)
end

function PANEL:Paint(w, h)
    draw.RoundedBox(16, 0, 0, w, h, Color(0, 0, 0, 150))
    draw.RoundedBoxEx(16, 0, 0, w, 47, Color(0, 0, 0), true, true, false, false)

    draw.SimpleText("пригласить на дуэль", "animelife.pvpbattle.invite.title", 16, 16, color_white)

    local y = 81
    draw.SimpleText("Отправить приглашение игроку", "animelife.pvpbattle.invite.title", w / 2, y, color_white, 1)
    y = y + 36 + 36 - 8
    draw.SimpleText("Выберите набор оружия", "animelife.pvpbattle.invite.title", w / 2, y, color_white, 1)
    y = y + 36 + 36 - 10
    draw.SimpleText("Ваша ставка", "animelife.pvpbattle.invite.title", w / 2, y, color_white, 1)
end

function PANEL:Think()
    if input.IsKeyDown(KEY_ESCAPE) then
        self:Remove()
        self = nil
    end
end

vgui.Register("animelife.pvpbattle.invitegui", PANEL, "EditablePanel")

function OpenDuelInviteGUI(ply)
    local pnl = vgui.Create("animelife.pvpbattle.invitegui")
    pnl:SetSize(433, 390)
    pnl:Center()
    pnl:MakePopup()
    pnl:SetAlpha(0)
    pnl:AlphaTo(255, 0.25)
    pnl.Player = ply
end