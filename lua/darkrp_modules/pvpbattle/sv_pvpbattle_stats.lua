MySQLite.begin()

MySQLite.query([[
    CREATE TABLE IF NOT EXISTS al_pvpbattle(
        sid TINYTEXT NOT NULL PRIMARY KEY,
        rpname TINYTEXT NOT NULL,
        wins SMALLINT NOT NULL
    );
]])

module("dueling", package.seeall)

function dueling:DatabaseWin(ply)
    MySQLite.query([[SELECT wins
    FROM al_pvpbattle
    where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
        local wins = 1
        if !data then
            MySQLite.query([[REPLACE INTO al_pvpbattle VALUES(]] ..
            MySQLite.SQLStr(ply:SteamID()) .. [[, ]] ..
            MySQLite.SQLStr(ply:Nick()) .. [[, ]] ..
            wins .. ");")
        else
            if data[1] and data[1].wins then
                wins = tonumber(data[1].wins) + 1
            end
        end

        MySQLite.query([[UPDATE al_pvpbattle SET rpname = ]] .. MySQLite.SQLStr(ply:Nick()) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
        MySQLite.query([[UPDATE al_pvpbattle SET wins = ]] .. tonumber(wins) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
    end, function(e, q) 
        print("Database Error:", e, q)
    end)
end