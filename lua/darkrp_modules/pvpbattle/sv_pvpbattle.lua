module("dueling", package.seeall)

duels = duels or {}
duel_duration = 5 -- x minutes
spawn_positions = {
    [1] = Vector(-12086, 2559, 571),
    [2] = Vector(-12067, 1543, 583)
}

function dueling:CreateDuel(sender, target, bet, bet_type, wep_type)
    if !sender then error('trying to create duel yet the sender is null') end
    if !target then error('trying to create duel yet the target is null') end
    if !bet then bet = 100 end
    if !bet_type then bet_type = 1 end
    if !wep_type then wep_type = "full" end

    local weps = self:GetWeaponPack(wep_type)

    local idx = table.insert(duels, {
        Sender = sender, Target = target, Bet = bet, BetType = bet_type, Weapons = weps,
        StartTime = SysTime()
    })

    self:SpawnParticipants(idx)
    self:PlayerLoadout(idx)

    sender:SetNWEntity("animelife.pvpbattle.opponent", target)
    target:SetNWEntity("animelife.pvpbattle.opponent", sender)

    sender.DuelID = idx
    target.DuelID = idx

    return idx
end

function dueling:GetDuel(idx)
    return duels[idx]
end

function dueling:EndDuel(idx, winner)
    local data = self:GetDuel(idx)
    if !data then return end

    if IsValid(winner) then
        data.Winner = winner 
    end

    table.remove(duels, idx)

    hook.Call("OnDuelEnded", GAMEMODE, idx, data)

    return true
end

function dueling:GetWeaponPack(wep_type)
    if wep_type:find("_random") then
        local rep = string.Replace(wep_type, "_random", "")
        return {weapon_types[rep][math.random(#weapon_types[rep])]}
    end

    local weps = {}

    for _, wep in ipairs(weapon_types[wep_type]) do
        table.insert(weps, wep)
    end

    return weps
end

function dueling:SpawnParticipants(idx)
    local duel = self:GetDuel(idx)
    if !IsValid(duel.Sender) or !IsValid(duel.Target) then return end

    local side = true
    local function select_origin()
        side = !side

        if side then
            return spawn_positions[1], Angle(0, -90, 0)
        else
            return spawn_positions[2], Angle(0, 90, 0)
        end
    end

    local pos_sender, ang_sender = select_origin()
    duel.Sender:SetPos(pos_sender)
    duel.Sender:SetEyeAngles(ang_sender)

    local pos_target, ang_target = select_origin()
    duel.Target:SetPos(pos_target)
    duel.Target:SetEyeAngles(ang_target)
end

function dueling:PlayerLoadout(idx)
    local duel = self:GetDuel(idx)
    if !IsValid(duel.Sender) or !IsValid(duel.Target) then return end

    local function get_current_weapon_classnames(weps)
        local classnames = {}

        for _, wep in pairs(weps) do
            if IsValid(wep) and wep:IsWeapon() then
                table.insert(classnames, wep:GetClass())
            end
        end

        return classnames
    end

    duel.Sender.DuelStoredWeapons = get_current_weapon_classnames(duel.Sender:GetWeapons())
    duel.Target.DuelStoredWeapons = get_current_weapon_classnames(duel.Target:GetWeapons())

    duel.Sender:StripWeapons()
    duel.Target:StripWeapons()

    for _, wep in ipairs(duel.Weapons) do
        duel.Sender:Give(wep)
        duel.Target:Give(wep)
    end

    -- make it a fair fight
    duel.Sender:SetHealth(100)
    duel.Sender:SetArmor(100)
    duel.Target:SetHealth(100)
    duel.Target:SetArmor(100)
end

function dueling:Invite(sender, target, data)
    if !self:CanInvite(sender, target) then DarkRP.notify(sender, 1, 7, "Приглашение на дуэль не может быть отправлено.") return end
    if data.BetType == 1 then
        if !target:canAfford(data.Bet) or !sender:canAfford(data.Bet) then
            DarkRP.notify(sender, 1, 7, "Приглашение на дуэль не может быть отправлено (Высокая ставка).")
            return
        end
    else
        if (target:GetTreeXP() < data.Bet) or (sender:GetTreeXP() < data.Bet) then
            DarkRP.notify(sender, 1, 7, "Приглашение на дуэль не может быть отправлено (Высокая ставка).")
            return
        end
    end

    target.DuelCurrentInvite = sender
    target.DuelCurrentInviteData = data

    sender.NextDuelInvite = SysTime() + 60

    DarkRP.notify(sender, 0, 7, "Приглашение на дуэль отправлено " .. target:Nick())

    local target_notify = ("%s приглашает вас на дуэль. Ставка: %s. Оружие: %s"):format(sender:Nick(), data.Bet .. (data.BetType == 1 and "¥" or "XP"), data.WeaponType)
    DarkRP.notify(target, 0, 10, target_notify)

    target:ChatPrint(target_notify)
    target:ChatPrint("Чтобы принять предложение, введите /duelaccept")

    -- TODO: add invite expiring?
end

function dueling:CanInvite(sender, target)
    if !table.IsEmpty(duels) then return false end -- someone's already dueling
    if self:IsDueling(target) then return false end
    if sender == target then return false end
    if (sender.NextDuelInvite or 0) > SysTime() then return false end

    return true
end

function dueling:IsDueling(ply)
    for _, duel in ipairs(duels) do
        if duel.Sender == ply or duel.Target == ply then
            return true
        end
    end

    return false
end

hook.Add("Think", "animelife.dueling.logic", function()
    for i = 1, #duels do
        local duel = dueling:GetDuel(i)
        -- end duel when target/sender is invalid
        if !IsValid(duel.Sender) or !IsValid(duel.Target) then
            -- try to pick a valid player if possible and end duel
            dueling:EndDuel(i, IsValid(duel.Sender) and duel.Sender or duel.Target)
        end

        -- time runned out
        if (duel.StartTime + (dueling.duel_duration * 60)) < SysTime() then
            dueling:EndDuel(i)
        end
    end
end)

hook.Add("OnDuelEnded", "animelife.dueling.ended", function(index, data)
    local winner = data.Winner
    local ply1 = data.Sender
    local ply2 = data.Target

    -- restore weawpons
    local function restore(ply)
        if !ply.DuelStoredWeapons then return end

        for _, wep in pairs(ply.DuelStoredWeapons) do
            if !ply:HasWeapon(wep) and !table.HasValue(data.Weapons, wep) then
                ply:Give(wep)
            end
        end

        ply.DuelStoredWeapons = nil
    end

    if IsValid(winner) then
        local winner_opponent = winner:GetNWEntity("animelife.pvpbattle.opponent")
        local nick = IsValid(winner_opponent) and winner_opponent:Nick() or "Disconnected Player"
        DarkRP.notify(winner, 0, 10, "Вы выиграли дуэль против " .. nick .. "!")

        if data.BetType == 1 then
            if IsValid(winner_opponent) then
                winner:addMoney(data.Bet)

                local minus = data.Bet
                local money = winner_opponent:getDarkRPVar("money") or 0

                -- abusable, but im lazy to think of a workaround
                if money < minus then
                    minus = money
                end

                winner_opponent:addMoney(-minus)
            end
        elseif data.BetType == 2 then
            if IsValid(winner_opponent) then
                winner:AddTreeXP(data.Bet)

                local minus = data.Bet
                local xp = winner_opponent:GetTreeXP()

                -- abusable, but im lazy to think of a workaround
                if xp < minus then
                    minus = xp
                end

                winner_opponent:AddTreeXP(-minus)
            end
        end

        if data.BetType == 1 and data.Bet >= 5000 then
            dueling:DatabaseWin(winner)
        elseif data.BetType == 2 and data.Bet >= 200 then
            dueling:DatabaseWin(winner)
        end
    end

    if IsValid(ply1) then
        ply1:Spawn()
        ply1:StripWeapons()
        restore(ply1)
        ply1:SetNWEntity("animelife.pvpbattle.opponent", game.GetWorld())
        ply1.DuelID = nil
    end

    if IsValid(ply2) then
        ply2:Spawn()
        ply2:StripWeapons()
        restore(ply2)
        ply2:SetNWEntity("animelife.pvpbattle.opponent", game.GetWorld())
        ply2.DuelID = nil
    end
end)

hook.Add("PlayerDeath", "animelife.dueling.handledeath", function(victim, inflictor, attacker)
    local opponent = victim:GetNWEntity("animelife.pvpbattle.opponent")
    if IsValid(opponent) then
        local winner = nil 
        if IsValid(attacker) then
            winner = opponent == attacker and attacker or nil
        end
        
        dueling:EndDuel(victim.DuelID, winner)
    end
end)

hook.Add("EntityFireBullets", "animelife.dueling.restrictweapons", function(ent, data)
    if IsValid(ent) and ent:IsPlayer() then
        local opponent = ent:GetNWEntity("animelife.pvpbattle.opponent")
        if IsValid(opponent) and ent.DuelID ~= nil then
            local duel_data = dueling.duels[ent.DuelID]
            local holding_wep = ent:GetActiveWeapon()
            if istable(duel_data) and IsValid(holding_wep) and !table.HasValue(duel_data.Weapons, holding_wep:GetClass()) then
                -- supressing bullet did nothing when i was playtesting
                -- so strip it just in case
                if ent.DuelStoredWeapons then
                    -- set it to be restored
                    table.insert(ent.DuelStoredWeapons, holding_wep:GetClass())
                end

                ent:StripWeapon(holding_wep:GetClass())

                -- holding_wep:SetNextPrimaryFire(CurTime() + 5)
                -- holding_wep:SetNextSecondaryFire(CurTime() + 5)

                return false
            end
        end
    end
end)

hook.Add("PlayerSay", "animelife.dueling.inviteaccept", function(ply, msg)
    if msg == "/duelaccept" or msg == "!duelaccept" then
        if IsValid(ply.DuelCurrentInvite) then
            local data = ply.DuelCurrentInviteData
            if data then
                if !ply.DuelID then
                    dueling:CreateDuel(ply.DuelCurrentInvite, ply, data.Bet, data.BetType, data.WepType)

                    ply.DuelCurrentInvite = nil 
                    ply.DuelCurrentInviteData = nil
                else
                    DarkRP.notify(ply, 1, 5, "Вы уже принимаете участие в дуэли.")
                end
            else
                DarkRP.notify(ply, 1, 5, "Нет активных приглашений.")
            end
        else
            DarkRP.notify(ply, 1, 5, "Нет активных приглашений.")
        end

        return ""
    end
end)

concommand.Add("al_duelinvite", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    arg_str = string.Replace(arg_str, '"')
    arg_str = string.Split(arg_str, " ")
    
    local target = arg_str[1]
    target = player.GetBySteamID(target)
    if !IsValid(target) or !target:IsPlayer() then return end

    local bet = arg_str[2]
    bet = tonumber(bet)
    if !isnumber(bet) then return end

    local bet_type = arg_str[3]
    bet_type = tonumber(bet_type)
    if !isnumber(bet_type) then return end

    local function translate_weptype(wep_type)
        local str = ""
        if wep_type:find("pistols") then
            str = "Пистолеты"
        elseif wep_type:find("snipers") then
            str = "Снайперские винтовки"
        elseif wep_type:find("full") then
            str = "Полный комплект оружия"
        end

        if wep_type:find("_random") then
            str = str .. " [Случайные]"
        else
            if !wep_type:find("full") then
                str = str .. " [Комплект оружия]"
            end
        end

        return str == "" and "Unknown" or str
    end

    local wep_type = arg_str[4]
    local weapon_type = translate_weptype(wep_type)

    dueling:Invite(ply, target, {
        Bet = 300, BetType = 1, WeaponType = weapon_type,
        ply, Target = target, WepType = wep_type
    })
end)