sql.Query("CREATE TABLE IF NOT EXISTS animelife_warns(SteamID TINYTEXT, CuratorID TINYTEXT, Reason TINYTEXT, Date SMALLINT)")

util.AddNetworkString("animelife.administration.warnadmin")
util.AddNetworkString("animelife.administration.curation.viewwarns")

module("administration", package.seeall)

function AddWarn(curator, admin, reason, date)
    if !isnumber(date) then return end
    reason = reason or "???"

    sql.Query("INSERT INTO animelife_warns(SteamID, CuratorID, Reason, Date) VALUES("
    .. SQLStr(admin:SteamID()) .. ", "
    .. SQLStr(curator:SteamID()) .. ", "
    .. SQLStr(reason) .. ", "
    .. date .. ")")
end

function GetWarns(admin)
    local query = sql.Query("SELECT * FROM animelife_warns WHERE SteamID = " .. SQLStr(admin:SteamID()))
    if istable(query) and #query > 0 then
        return query
    end

    return {}
end

function ShowWarns(ply, warns)
    if table.IsEmpty(warns) then 
        DarkRP.notify(ply, 1, 3, "Предупреждений не найдено.")
        return 
    end

    warns = util.TableToJSON(warns)
    warns = util.Compress(warns)
    local len = warns:len()
    net.Start("animelife.administration.curation.viewwarns")
        net.WriteString(ply:Nick())
        net.WriteInt(len, 16)
        net.WriteData(warns, len)
    net.Send(ply)
end

timer.Create("animelife.administration.WarnThink", 600, 0, function()
    local t = sql.Query("SELECT * FROM animelife_warns")
    if istable(t) and #t > 0 then
        for _, v in pairs(t) do
            if tonumber(v.Date) < os.time() then
                sql.Query("DELETE FROM animelife_warns WHERE SteamID = " .. SQLStr(v.SteamID) .. " AND Reason = " .. SQLStr(v.Reason) .. " AND Date = " .. v.Date .. " AND CuratorID = " .. SQLStr(v.CuratorID))
            end
        end
    end
end)

net.Receive("animelife.administration.warnadmin", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end
    if !ply:HasActionAccess("addwarn") then return end
    
    local target = net.ReadString()
    target = player.GetBySteamID(target)
    if !IsValid(target) or !target:IsPlayer() then return end
    local reason = net.ReadString()
    local duration = net.ReadString()
    duration = tonumber(duration)
    if !isnumber(duration) then return end

    AddWarn(ply, target, reason, os.time() + (duration * 60 * 1440))

    DarkRP.notify(ply, 0, 3, "Предупреждение выдано.")
    DarkRP.notify(target, 0, 3, ply:Nick() .. " выдал вам предупреждение.")
end)