util.AddNetworkString("animelife.administration.new_report")
util.AddNetworkString("animelife.administration.takereport")
util.AddNetworkString("animelife.administration.reportupdate")
util.AddNetworkString("animelife.administration.closereport")
util.AddNetworkString("animelife.administration.sendreport")

MySQLite.begin()

MySQLite.query([[
    CREATE TABLE IF NOT EXISTS al_adminreports(
        sid TINYTEXT NOT NULL,
        rpname TINYTEXT NOT NULL,
        victim_name TINYTEXT NOT NULL,
        reason TINYTEXT NOT NULL,
        score FLOAT NOT NULL,
        date MEDIUMINT NOT NULL
    );
]])

module("administration", package.seeall)

Reports = Reports or {}

function NewReport(ply, target, reason)
    local idx = table.insert(Reports, {Player = ply, Target = target, Reason = reason, Time = os.time()})

    local admins = GetAdmins()

    net.Start("animelife.administration.new_report")
        net.WriteEntity(ply)
        net.WriteEntity(target)
        net.WriteString(reason)
        net.WriteInt(os.time(), 32)
    net.Send(admins)

    -- timer.Simple(600, function()
    --     table.remove(Reports, idx)
    -- end)
end

function ReportEnded(admin, victim, reason, score)
    MySQLite.query([[INSERT INTO al_adminreports VALUES(]] ..
    MySQLite.SQLStr(admin:SteamID()) .. [[, ]] ..
    MySQLite.SQLStr(admin:Nick()) .. [[, ]] ..
    MySQLite.SQLStr(victim:Nick()) .. [[, ]] ..
    MySQLite.SQLStr(reason) .. [[, ]] ..
    tonumber(score)  .. [[, ]] ..
    os.time() .. ");")

    admin:SetNWInt("animelife.administration.report_count", admin:GetNWInt("animelife.administration.report_count", 0) + 1)
end

function DropLeaderboard()
end

net.Receive("animelife.administration.sendreport", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if ply:IsAdmin() then return end
    if (ply.NextReportAbility or 0) > SysTime() then
        DarkRP.notify(ply, 1, 3, "Вы недавно отправляли жалобу. Подождите немного.")
        return
    end

    local target = net.ReadEntity()
    if !IsValid(target) or !target:IsPlayer() then return end
    if target == ply then return end

    local reason = net.ReadString()

    NewReport(ply, target, reason)

    ply.NextReportAbility = SysTime() + 300
    
    DarkRP.notify(ply, 0, 3, "Жалоба отправлена.")
end)

local function take_reports(admin, reporter, target, reason, time)
    for _, report in pairs(Reports) do
        if IsValid(report.TakenBy) then continue end
        if report.Player == reporter and report.Target == target and report.Reason == reason and report.Time == time then
            -- table.RemoveByValue(Reports, report)
            report.TakenBy = admin
            admin.ReportsWorkInProgress = true
        end
    end

    if admin.ReportsWorkInProgress then
        -- update info on client
        net.Start("animelife.administration.reportupdate")
            net.WriteEntity(admin)
            net.WriteEntity(reporter)
            net.WriteEntity(target)
            net.WriteString(reason)
            net.WriteInt(time, 32)
            net.WriteBool(false)
        net.Send(GetAdmins())

        if IsValid(target) and target:IsPlayer() then
            GotoPlayer(admin, target, admin)
        end

        if IsValid(reporter) and reporter:IsPlayer() then
            GotoPlayer(admin, reporter, admin)
        end
    end
end

local function close_reports(admin)
    local report_ended = false
    local removal = {}
    for _, report in pairs(Reports) do
        if report.TakenBy == admin then
            table.RemoveByValue(Reports, report)
            admin.ReportsWorkInProgress = false
            report_ended = true

            ReportEnded(admin, report.Player, report.Reason, 0)
            removal = {admin, report.Player, report.Target, report.Reason, report.Time}
        end
    end

    if report_ended then
        -- update info on client
        net.Start("animelife.administration.reportupdate")
            net.WriteEntity(removal[1])
            net.WriteEntity(removal[2])
            net.WriteEntity(removal[3])
            net.WriteString(removal[4])
            net.WriteInt(removal[5], 32)
            net.WriteBool(true)
        net.Send(GetAdmins())

        if IsValid(removal[2]) and removal[2]:IsPlayer() then
            Return(admin, removal[2])
        end

        if IsValid(removal[3]) and removal[3]:IsPlayer() then
            Return(admin, removal[3])
        end
    end
end

concommand.Add("al_closereports", function(ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    close_reports(ply)
end)

net.Receive("animelife.administration.takereport", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    if ply.ReportsWorkInProgress then
        DarkRP.notify(ply, 1, 3, "Закройте активную жалобу, прежде чем приступить к новой.")
        return
    end

    local reporter = net.ReadEntity()
    if reporter == ply then return end
    local target = net.ReadEntity()
    if target == ply then return end
    local reason = net.ReadString()
    local time = net.ReadInt(32)

    take_reports(ply, reporter, target, reason, time)
end)

net.Receive("animelife.administration.closereport", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    close_reports(ply)
end)

local function report_counter(ply)
    MySQLite.query([[
        SELECT score
        FROM al_adminreports
        where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
            if !data then return end

            ply:SetNWInt("animelife.administration.report_count", #data)
        end, function(e, q)
            print("Database Error:", e, q)
        end)
end

hook.Add("PlayerInitialSpawn", "animelife.administration.report.db", function(ply)
    timer.Simple(5, function()
        if !IsValid(ply) or !ply:IsPlayer() then return end
        if !ply:IsAdmin() then return end

        report_counter(ply)
    end)
end)