local users = player.GetAll
local max_users = game.MaxPlayers
local count = table.Count
local floor = math.floor

local function kick_em()
    for _, v in pairs(users()) do
        if v:GetNWBool("animelife.administration.ban", false) then
            v:Kick("Стало тесно. Освобождаем пространство.")
        end
    end
end

hook.Add("PlayerInitialSpawn", "animelife.administration.kickbanned", function(ply)
    if count(users()) > (floor(max_users() / 1.2)) then
        kick_em()
    end
end)