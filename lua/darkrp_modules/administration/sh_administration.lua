module("administration", package.seeall)

Actions = {
    ["noclip"] = {
        Name = "noclip",
        Icon = "bullet_blue",
        Function = function(admin, ply)
            Noclip(admin, ply)
        end,
    },
    ["slay"] = {
        Name = "убить",
        Icon = "bullet_red",
        Function = function(admin, ply)
            Slay(admin, ply)
        end,
    },
    ["freeze"] = {
        Name = "заморозить",
        Icon = "bullet_blue",
        Function = function(admin, ply)
            Freeze(admin, ply)
        end,
    },
    ["nodraw"] = {
        Name = "остановить отрисовку",
        Icon = "bullet_blue",
        Function = function(admin, ply)
            NoDraw(admin, ply)
        end,
    },
    ["ban"] = {
        Name = "в бан",
        Icon = "bullet_delete",
        ShouldShow = function(ply)
            return !ply:GetNWBool("animelife.administration.ban", false)
        end,
        Function = function(admin, ply)
        end,
    },
    ["ban_un"] = {
        Name = "снять бан",
        Icon = "bullet_green",
        ShouldShow = function(ply)
            return ply:GetNWBool("animelife.administration.ban", false)
        end,
        Function = function(admin, ply)
            Unban(admin, ply)
        end,
    },
    ["jail"] = {
        Name = "джайл",
        Icon = "bullet_red",
        ShouldShow = function(ply)
            return !ply:GetNWBool("animelife.administration.jail", false)
        end,
        Function = function(admin, ply)
        end,
    },
    ["jail_un"] = {
        Name = "снять джайл",
        Icon = "bullet_green",
        ShouldShow = function(ply)
            return ply:GetNWBool("animelife.administration.jail", false)
        end,
        Function = function(admin, ply)
            Unjail(admin, ply)
        end,
    },
    ["godmode"] = {
        Name = "годмод",
        Icon = "bullet_blue",
        Function = function(admin, ply)
            God(admin, ply)
        end,
    },
    ["return"] = {
        Name = "вернуть назад",
        Icon = "bullet_blue",
        Function = function(admin, ply)
            Return(admin, ply)
        end,
    },
    ["teleport"] = {
        Name = "телепортироваться",
        Icon = "bullet_blue",
        Function = function(admin, ply)
            GotoPlayer(admin, admin, ply)
        end,
    },
    ["teleportto"] = {
        Name = "телепортировать к",
        Icon = "bullet_blue",
        ShowPlayers = true,
        Function = function(admin, ply, ply2)
            GotoPlayer(admin, ply, ply2)
        end,
    },
    ["stripweapons"] = {
        Name = "забрать все оружие",
        Icon = "bullet_blue",
        Function = function(admin, ply)
            StripWeapons(admin, ply)
        end,
    },
    ["setteam"] = {
        Name = "установить профессию",
        Icon = "bullet_blue",
        ShowTeams = true,
        Function = function(admin, ply, idx)
            ChangeTeam(admin, ply, idx)
        end,
    },
    ["giveweapon"] = {
        Name = "выдать оружие",
        Icon = "bullet_blue",
        TakesOneArgument = true,
        Text = "Введите классовое имя оружия (например, weapon_ak47)",
        Function = function(admin, ply, arg)
            GiveWeapon(admin, ply, arg)
        end,
    },
    ["sethp"] = {
        Name = "установить hp",
        Icon = "bullet_blue",
        TakesOneArgument = true,
        Text = "Введите новое значение для установки здоровья",
        Function = function(admin, ply, arg)
            SetHealth(admin, ply, arg)
        end,
    },
    ["setarmor"] = {
        Name = "установить броню",
        Icon = "bullet_blue",
        TakesOneArgument = true,
        Text = "Введите новое значение для установки брони",
        Function = function(admin, ply, arg)
            SetArmor(admin, ply, arg)
        end,
    },
    ["voicemute"] = {
        Name = "заблокировать голосовой чат",
        Icon = "bullet_red",
        TakesOneArgument = true,
        Text = "Введите продолжительность блокировки в минутах",
        ShouldShow = function(ply)
            return !ply:GetNWBool("animelife.administration.voicemute", false)
        end,
        Function = function(admin, ply, arg)
            Mute(admin, ply, "voice", arg)
        end,
    },
    ["voicemute_un"] = {
        Name = "разблокировать голосовой чат",
        Icon = "bullet_green",
        ShouldShow = function(ply)
            return ply:GetNWBool("animelife.administration.voicemute", false)
        end,
        Function = function(admin, ply)
            Unmute(admin, ply, "voice")
        end,
    },
    ["chatmute"] = {
        Name = "заблокировать текстовый чат",
        Icon = "bullet_red",
        TakesOneArgument = true,
        Text = "Введите продолжительность блокировки в минутах",
        ShouldShow = function(ply)
            return !ply:GetNWBool("animelife.administration.chatmute", false)
        end,
        Function = function(admin, ply, arg)
            Mute(admin, ply, "chat", arg)
        end,
    },
    ["chatmute_un"] = {
        Name = "разблокировать текстовый чат",
        Icon = "bullet_green",
        ShouldShow = function(ply)
            return ply:GetNWBool("animelife.administration.chatmute", false)
        end,
        Function = function(admin, ply)
            Unmute(admin, ply, "chat")
        end,
    },
    ["addwarn"] = {
        Name = "выдать предупреждение",
        Icon = "bullet_red",
        ShouldShow = function(ply)
            return ply:IsAdmin()
        end,
        Function = function(curator, admin, reason, time)
            AddWarn(curator, admin, reason, os.time() + (time * 60 * 1440))
        end,
    },
    ["menu_manager"] = {
        Name = "просмотреть предупреждения",
        Icon = "bullet_blue",
        ShouldShow = function(ply)
            return ply:IsAdmin()
        end,
        Function = function(curator, admin)
            ShowWarns(curator, GetWarns(admin))
        end,
    },
    ["setusergroup"] = {
        Name = "установить права доступа",
        Icon = "bullet_blue",
        ShowUsergroups = true,
        Function = function(curator, admin, usergroup)
            if curator:GetUserGroup() ~= "root" and usergroup == "root" then return end

            AddUserToUsergroup(admin:SteamID(), usergroup)
        end,
    },
    ["spectate"] = {
        Name = "наблюдение",
        Icon = "bullet_blue",
        Function = function(admin, ply)
            -- Spectate(admin, ply)

            if admin.FSpectating ~= nil then
                admin:ConCommand("FSpectate_StopSpectating")
            else
                admin:ConCommand("FSpectate " .. ply:SteamID())
            end
        end,
    },
    ["kick"] = {
        Name = "отключить от сервера",
        Icon = "bullet_red",
        Function = function(admin, ply)
            Kick(admin, ply)
        end,
    },
}

Usergroups = {}

function AddNew(name, immunity, allowed_functions)
    Usergroups[name] = {
        Immunity = immunity,
        Allowed = allowed_functions
    }

    return name
end

function GetAdmins()
    local res = {}

    for _, ply in pairs(player.GetAll()) do
        if ply:IsAdmin() then
            table.insert(res, ply)
        end
    end

    return res
end

hook.Add("ShouldCollide", "animelife.administration.collision", function(ent1, ent2)
    if ent1:IsPlayer() and ent2:IsPlayer() then 
        if ent1:GetNWBool("animelife.administration.ban", false) or ent1:GetNWBool("animelife.administration.jail", false) then
            return false 
        end
    end
end)

local meta = FindMetaTable("Player")

function meta:IsAdmin()
    local group = Usergroups[self:GetUserGroup()]
    if !group then
        return false
    end

    if group.Allowed then
        return group.Allowed["admin"]
    end

    return false
end

function meta:CanAct(ply)
    local self_group = Usergroups[self:GetUserGroup()]
    local ply_group = Usergroups[ply:GetUserGroup()]
    if !self_group then return false end
    if ply == self then return true end

    if ply_group then
        if self_group.Immunity <= ply_group.Immunity then
            return false
        end
    end

    return true
end

function meta:HasActionAccess(act)
    local group = Usergroups[self:GetUserGroup()]
    if !group then
        return false
    end

    if group.Allowed then
        return group.Allowed[act]
    end

    return false
end

-- NOTE: Do not remove, rename or edit player usergroup.
AddNew("player", 1)

AddNew("Сurator", 70, {
    ["admin"] = true, -- is admin usergroup
    ["menu_manager"] = true, -- can click admins in admin menu
    ["warns"] = true, -- can give warns
    ["setteam"] = true,
    ["freeze"] = true,
    ["noclip"] = true,
    ["slay"] = true,
    ["nodraw"] = true,
    ["godmode"] = true,
    ["return"] = true,
    ["teleport"] = true,
    ["teleportto"] = true,
    ["stripweapons"] = true,
    ["sethp"] = true,
    ["setarmor"] = true,
    ["voicemute"] = true,
    ["voicemute_un"] = true,
    ["chatmute"] = true,
    ["chatmute_un"] = true,
    ["ban"] = true,
    ["ban_un"] = true,
    ["jail"] = true,
    ["jail_un"] = true,
    ["addwarn"] = true,
    ["spectate"] = true,
    ["kick"] = true,
})
AddNew("Ivent", 35, {
    ["admin"] = true, -- is admin usergroup
    ["setteam"] = true,
    ["freeze"] = true,
    ["noclip"] = true,
    ["slay"] = true,
    ["nodraw"] = true,
    ["godmode"] = true,
    ["return"] = true,
    ["teleport"] = true,
    ["teleportto"] = true,
    ["stripweapons"] = true,
    ["sethp"] = true,
    ["setarmor"] = true,
    ["voicemute"] = true,
    ["voicemute_un"] = true,
    ["chatmute"] = true,
    ["chatmute_un"] = true,
    ["ban"] = true,
    ["ban_un"] = true,
    ["jail"] = true,
    ["jail_un"] = true,
    ["spectate"] = true,
})
AddNew("superadmin", 90, {
    ["admin"] = true, -- is admin usergroup
    ["menu_manager"] = true,
    ["warns"] = true,
    -- actions
    ["setteam"] = true,
    ["freeze"] = true,
    ["noclip"] = true,
    ["slay"] = true,
    ["nodraw"] = true,
    ["godmode"] = true,
    ["return"] = true,
    ["teleport"] = true,
    ["teleportto"] = true,
    ["stripweapons"] = true,
    ["giveweapon"] = true,
    ["sethp"] = true,
    ["setarmor"] = true,
    ["voicemute"] = true,
    ["voicemute_un"] = true,
    ["chatmute"] = true,
    ["chatmute_un"] = true,
    ["ban"] = true,
    ["ban_un"] = true,
    ["jail"] = true,
    ["jail_un"] = true,
    ["addwarn"] = true,
    ["setusergroup"] = true,
    ["spectate"] = true,
    ["kick"] = true,
})
AddNew("root", 100, {
    ["admin"] = true, -- is admin usergroup
    ["menu_manager"] = true,
    ["warns"] = true,
    -- actions
    ["setteam"] = true,
    ["freeze"] = true,
    ["noclip"] = true,
    ["slay"] = true,
    ["nodraw"] = true,
    ["godmode"] = true,
    ["return"] = true,
    ["teleport"] = true,
    ["teleportto"] = true,
    ["stripweapons"] = true,
    ["giveweapon"] = true,
    ["sethp"] = true,
    ["setarmor"] = true,
    ["voicemute"] = true,
    ["voicemute_un"] = true,
    ["chatmute"] = true,
    ["chatmute_un"] = true,
    ["ban"] = true,
    ["ban_un"] = true,
    ["jail"] = true,
    ["jail_un"] = true,
    ["setusergroup"] = true,
    ["spectate"] = true,
    ["kick"] = true,
})
AddNew("Sponsor", 30, {
    ["admin"] = true, -- is admin usergroup
    -- actions
    ["setteam"] = true,
    ["freeze"] = true,
    ["noclip"] = true,
    ["slay"] = true,
    ["nodraw"] = true,
    ["godmode"] = true,
    ["return"] = true,
    ["teleport"] = true,
    ["teleportto"] = true,
    ["stripweapons"] = true,
    ["voicemute"] = true,
    ["voicemute_un"] = true,
    ["chatmute"] = true,
    ["chatmute_un"] = true,
    ["jail"] = true,
    ["jail_un"] = true,
    ["spectate"] = true,
})

AddNew("Admin", 35, {
    ["admin"] = true, -- is admin usergroup
    -- actions
    ["setteam"] = true,
    ["freeze"] = true,
    ["noclip"] = true,
    ["slay"] = true,
    ["nodraw"] = true,
    ["godmode"] = true,
    ["return"] = true,
    ["teleport"] = true,
    ["teleportto"] = true,
    ["stripweapons"] = true,
    ["voicemute"] = true,
    ["voicemute_un"] = true,
    ["chatmute"] = true,
    ["chatmute_un"] = true,
    ["ban"] = true,
    ["ban_un"] = true,
    ["jail"] = true,
    ["jail_un"] = true,
    ["spectate"] = true,
    ["kick"] = true,
})

AddNew("Operator", 40, {
    ["admin"] = true, -- is admin usergroup
    -- actions
    ["setteam"] = true,
    ["freeze"] = true,
    ["noclip"] = true,
    ["slay"] = true,
    ["nodraw"] = true,
    ["godmode"] = true,
    ["return"] = true,
    ["teleport"] = true,
    ["teleportto"] = true,
    ["stripweapons"] = true,
    ["voicemute"] = true,
    ["voicemute_un"] = true,
    ["chatmute"] = true,
    ["chatmute_un"] = true,
    ["ban"] = true,
    ["ban_un"] = true,
    ["jail"] = true,
    ["jail_un"] = true,
    ["spectate"] = true,
    ["kick"] = true,
})