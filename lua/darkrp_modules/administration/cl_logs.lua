module("logs", package.seeall)

List = {}

surface.CreateFont("animelife.logs.title", {font = "Exo 2 SemiBold", weight = 600, size = 24, extended = true})
surface.CreateFont("animelife.logs.time", {font = "Exo 2 SemiBold", weight = 600, size = 18, extended = true})

local columns = {
    ["Chat"] = {
        [1] = "SteamID",
        [2] = "Имя",
        [3] = "Сообщение",
        [4] = "Командный чат?",
        [5] = "Время",
    },
    ["Death"] = {
        [1] = "SteamID Жертвы",
        [2] = "SteamID Убийцы",
        [3] = "Имя жертвы",
        [4] = "Имя убийцы",
        [5] = "Время",
    },
}

function ShowUI(t)
    net.Start("animelife.logs.request_update")
        net.WriteString(t)
    net.SendToServer()

    local frame = vgui.Create("DFrame")
    frame:SetSize(ScrW(), 634)
    frame:Center()
    frame:MakePopup()
    frame.Paint = function(self, w, h)
        draw.RoundedBox(6, 0, 0, w, h, Color(255, 255, 255))
        draw.RoundedBoxEx(6, 0, 0, 128, h, Color(245, 245, 245), true, false, true, false)

        draw.SimpleText("Логи", "animelife.logs.title", 128 + 24, 24, Color(43, 43, 43), nil, 1)

        draw.SimpleText("Время: " .. os.date("%x %H:%M", os.time()), "animelife.logs.time", w / 2, 24, Color(31, 31, 31), 1, 1)
    end

    local list_view = vgui.Create("DListView", frame)
    list_view:SetPos(128, 48)
    list_view:SetSize(frame:GetWide() - 128, frame:GetTall() - 48)
    list_view:SetDataHeight(32)
    list_view:SetMultiSelect(false)

    for column, translation in pairs(columns[t]) do
        list_view:AddColumn(translation)
    end

    timer.Simple(0.5, function()
        local vals = {}
        for _, p in SortedPairsByMemberValue(List[t], "Time", true) do
            list_view:AddLine(p.PlayerID, p.PlayerName or "Couldn't retrieve player name", p.Message, p.Team and "Да" or "Нет", os.date("%x %H:%M", p.Time))
            -- for i, val in pairs(p) do
            --     print(i, val)
            --     table.insert(vals, val)
            -- end
        end

        -- list_view:AddLine(unpack(vals))
    end)
end

-- ShowUI("Chat")

net.Receive("animelife.logs.send_update", function()
    local t = net.ReadString()
    local len = net.ReadInt(32)
    local data = net.ReadData(len)
    data = util.Decompress(data)
    data = util.JSONToTable(data)

    List[t] = data
end)