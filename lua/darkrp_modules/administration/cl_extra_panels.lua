-- Extras: Ban Panel
local PANEL = {}

AccessorFunc(PANEL, "steamid", "SteamID", FORCE_STRING)

surface.CreateFont("animelife.administration.extrapanels.title", {font = "Exo 2 SemiBold", size = 16, weight = 600, extended = true})

function PANEL:Init()
    -- self:SetTitle("animelife administration: ban user")

    self.SteamIDInput = vgui.Create("DTextEntry", self)
    self.SteamIDInput:SetPlaceholderText("Введите или вставьте SteamID игрока")

    self.ReasonInput = vgui.Create("DTextEntry", self)
    self.ReasonInput:SetPlaceholderText("Введите причину бана")

    self.DurationInput = vgui.Create("DTextEntry", self)
    self.DurationInput:SetPlaceholderText("Введите продолжительность бана в минутах")
    
    self.BanButton = vgui.Create("DButton", self)
    self.BanButton:SetText("В бан юзверя")
    self.BanButton.DoClick = function()
        if self.SteamIDInput:GetValue() ~= "" and self.ReasonInput:GetValue() ~= "" and self.DurationInput:GetValue() ~= "" then
            net.Start("animelife.administration.askban")
                net.WriteString(self.SteamIDInput:GetValue())
                net.WriteString(self.ReasonInput:GetValue())
                net.WriteString(self.DurationInput:GetValue())
            net.SendToServer()
        end

        self:Remove()
        self = nil
    end

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("Закрыть окно")
    self.CloseButton.DoClick = function()
        self:Remove()
        self = nil
    end
end

function PANEL:PerformLayout(w, h)
    self.BanButton:SetSize(w - (w / 2) - 32, 61)
    self.BanButton:SetPos(32, h - 61 - 48)

    self.CloseButton:SetSize(w - (w / 2) - 32, 61)
    self.CloseButton:SetPos(w - self.CloseButton:GetWide() - 32, h - 61 - 48)

    self.SteamIDInput:SetSize(386, 42)
    self.SteamIDInput:SetPos((w - 386) / 2, 128)
    self.SteamIDInput:SetText(self:GetSteamID())

    self.ReasonInput:SetSize(386, 42)
    self.ReasonInput:SetPos((w - 386) / 2, 228)

    self.DurationInput:SetSize(386, 42)
    self.DurationInput:SetPos((w - 386) / 2, 328)
end

function PANEL:Think()
    if input.IsKeyDown(KEY_ESCAPE) then
        self:Remove()
        self = nil
    end
end

function PANEL:Paint(w, h)
    draw.RoundedBox(6, 0, 0, w, h, Color(0, 0, 0, 200))

    draw.SimpleText("animelife administration: ban user", "animelife.administration.extrapanels.title", 16, 16, Color(255, 255, 255))

    draw.RoundedBox(6, 32, 46, w - 64, 32, Color(255, 177, 59))
    draw.SimpleText("Это окно получит редизайн когда-нибудь позже. Приоритет низкий.", "animelife.administration.extrapanels.title", 32 + 12, 46 + 6, Color(31, 31, 31))

    draw.SimpleText("SteamID для бана", "animelife.administration.extrapanels.title", w / 2, 100, Color(255, 255, 255), 1)
    draw.SimpleText("Причина бана", "animelife.administration.extrapanels.title", w / 2, 228 - 28, Color(255, 255, 255), 1)
    draw.SimpleText("Продолжительность бана", "animelife.administration.extrapanels.title", w / 2, 328 - 28, Color(255, 255, 255), 1)
end

vgui.Register("animelife.administration.ban", PANEL, "EditablePanel")

-- Extras: Jail Panel
local PANEL = {}

AccessorFunc(PANEL, "steamid", "SteamID", FORCE_STRING)

function PANEL:Init()
    -- self:SetTitle("animelife administration: jail user")

    self.SteamIDInput = vgui.Create("DTextEntry", self)
    self.SteamIDInput:SetPlaceholderText("Введите или вставьте SteamID игрока")

    self.DurationInput = vgui.Create("DTextEntry", self)
    self.DurationInput:SetPlaceholderText("Введите продолжительность джайла в минутах")
    
    self.JailButton = vgui.Create("DButton", self)
    self.JailButton:SetText("В джайл юзверя")
    self.JailButton.DoClick = function()
        if self.SteamIDInput:GetValue() ~= "" and self.DurationInput:GetValue() ~= "" then
            net.Start("animelife.administration.askjail")
                net.WriteString(self.SteamIDInput:GetValue())
                net.WriteString(self.DurationInput:GetValue())
            net.SendToServer()
        end

        self:Remove()
        self = nil
    end

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("Закрыть окно")
    self.CloseButton.DoClick = function()
        self:Remove()
        self = nil
    end
end

function PANEL:PerformLayout(w, h)
    self.JailButton:SetSize(w - (w / 2) - 32, 61)
    self.JailButton:SetPos(32, h - 61 - 48)

    self.CloseButton:SetSize(w - (w / 2) - 32, 61)
    self.CloseButton:SetPos(w - self.CloseButton:GetWide() - 32, h - 61 - 48)

    self.SteamIDInput:SetSize(386, 42)
    self.SteamIDInput:SetPos((w - 386) / 2, 128)
    self.SteamIDInput:SetText(self:GetSteamID())

    self.DurationInput:SetSize(386, 42)
    self.DurationInput:SetPos((w - 386) / 2, 228)
end

function PANEL:Think()
    if input.IsKeyDown(KEY_ESCAPE) then
        self:Remove()
        self = nil
    end
end

function PANEL:Paint(w, h)
    draw.RoundedBox(6, 0, 0, w, h, Color(0, 0, 0, 200))

    draw.SimpleText("animelife administration: jail user", "animelife.administration.extrapanels.title", 16, 16, Color(255, 255, 255))

    draw.RoundedBox(6, 32, 46, w - 64, 32, Color(255, 177, 59))
    draw.SimpleText("Это окно получит редизайн когда-нибудь позже. Приоритет низкий.", "animelife.administration.extrapanels.title", 32 + 12, 46 + 6, Color(31, 31, 31))

    draw.SimpleText("SteamID для джайла", "animelife.administration.extrapanels.title", w / 2, 100, Color(255, 255, 255), 1)
    draw.SimpleText("Продолжительность джайла", "animelife.administration.extrapanels.title", w / 2, 228 - 28, Color(255, 255, 255), 1)
end

vgui.Register("animelife.administration.jail", PANEL, "EditablePanel")

-- Extras: Warn Panel
local PANEL = {}

AccessorFunc(PANEL, "steamid", "SteamID", FORCE_STRING)

function PANEL:Init()
    -- self:SetTitle("animelife administration: ban user")

    self.SteamIDInput = vgui.Create("DTextEntry", self)
    self.SteamIDInput:SetPlaceholderText("Введите или вставьте SteamID администратора")

    self.ReasonInput = vgui.Create("DTextEntry", self)
    self.ReasonInput:SetPlaceholderText("Введите причину варна")

    self.DurationInput = vgui.Create("DTextEntry", self)
    self.DurationInput:SetPlaceholderText("Введите продолжительность предупржедения в днях")
    
    self.BanButton = vgui.Create("DButton", self)
    self.BanButton:SetText("Выдать предупреждение")
    self.BanButton.DoClick = function()
        if self.SteamIDInput:GetValue() ~= "" and self.ReasonInput:GetValue() ~= "" and self.DurationInput:GetValue() ~= "" then
            net.Start("animelife.administration.warnadmin")
                net.WriteString(self.SteamIDInput:GetValue())
                net.WriteString(self.ReasonInput:GetValue())
                net.WriteString(self.DurationInput:GetValue())
            net.SendToServer()
        end

        self:Remove()
        self = nil
    end

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("Закрыть окно")
    self.CloseButton.DoClick = function()
        self:Remove()
        self = nil
    end
end

function PANEL:PerformLayout(w, h)
    self.BanButton:SetSize(w - (w / 2) - 32, 61)
    self.BanButton:SetPos(32, h - 61 - 48)

    self.CloseButton:SetSize(w - (w / 2) - 32, 61)
    self.CloseButton:SetPos(w - self.CloseButton:GetWide() - 32, h - 61 - 48)

    self.SteamIDInput:SetSize(386, 42)
    self.SteamIDInput:SetPos((w - 386) / 2, 128)
    self.SteamIDInput:SetText(self:GetSteamID())

    self.ReasonInput:SetSize(386, 42)
    self.ReasonInput:SetPos((w - 386) / 2, 228)

    self.DurationInput:SetSize(386, 42)
    self.DurationInput:SetPos((w - 386) / 2, 328)
end

function PANEL:Think()
    if input.IsKeyDown(KEY_ESCAPE) then
        self:Remove()
        self = nil
    end
end

function PANEL:Paint(w, h)
    draw.RoundedBox(6, 0, 0, w, h, Color(0, 0, 0, 200))

    draw.SimpleText("animelife administration: warn admins", "animelife.administration.extrapanels.title", 16, 16, Color(255, 255, 255))

    draw.RoundedBox(6, 32, 46, w - 64, 32, Color(255, 177, 59))
    draw.SimpleText("Это окно получит редизайн когда-нибудь позже. Приоритет низкий.", "animelife.administration.extrapanels.title", 32 + 12, 46 + 6, Color(31, 31, 31))

    draw.SimpleText("SteamID администратора", "animelife.administration.extrapanels.title", w / 2, 100, Color(255, 255, 255), 1)
    draw.SimpleText("Причина варна", "animelife.administration.extrapanels.title", w / 2, 228 - 28, Color(255, 255, 255), 1)
    draw.SimpleText("Продолжительность предупреждения", "animelife.administration.extrapanels.title", w / 2, 328 - 28, Color(255, 255, 255), 1)
end

vgui.Register("animelife.administration.warns", PANEL, "EditablePanel")

function OpenBanPanel(target)
    local pnl = vgui.Create("animelife.administration.ban")
    pnl:SetSize(512, 590)
    pnl:Center()
    pnl:MakePopup()
    pnl:SetSteamID(target:SteamID())
end

function OpenJailPanel(target)
    local pnl = vgui.Create("animelife.administration.jail")
    pnl:SetSize(512, 450)
    pnl:Center()
    pnl:MakePopup()
    pnl:SetSteamID(target:SteamID())
end

function OpenWarnPanel(target)
    local pnl = vgui.Create("animelife.administration.warns")
    pnl:SetSize(512, 590)
    pnl:Center()
    pnl:MakePopup()
    pnl:SetSteamID(target:SteamID())
end