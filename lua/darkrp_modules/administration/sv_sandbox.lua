hook.Add("PlayerSpawnSENT", "animelife.administration.adminsent", function(ply)
    return ply:IsUserGroup("superadmin") or ply:IsUserGroup("root")
end)

hook.Add("CanTool", "animelife.administration.restricted_sents", function(ply, tr, tool)
    if tool == "creator" then
        return false
    end
end)

hook.Add("PlayerSpawnSWEP", "animelife.administration.adminswep", function(ply)
    if ply:IsUserGroup("root") then return true end
end)

hook.Add("CanPlayerSuicide", "animelife.administration.suicide", function(ply)
    return ply:IsAdmin()
end)

hook.Add("OnEntityCreated", "animelife.administration.tv", function(ent)
    if ent:GetClass() == "mediaplayer_tv" then
        ent:SetCollisionGroup(COLLISION_GROUP_WEAPON)
    end
end)

-- DarkRP
local restricted_drop = {"weapon_tablet", "tfa_cso_dreadnova", "tfa_cso_ruyi", "tfa_cso_tomahawk", "tfa_cso_jaydagger", "weapon_cuff_rope", "weapon_cuff_elastic", "climb_swep3", "weapon_cuff_tactical", "tfa_destiny_devils_ruin", "tfa_destiny_duality", "tfa_cso_skull2", "tfa_destiny_trinity_ghoul", "tfa_destiny_vex_mythoclast2", "tfa_destiny_whisper", "tfa_ak117geo", "tfa_ins2_codol_free", "tfa_hailstorm", "tfa_doom_ssg", "tfa_volk", "tfa_bo3_m8a7"}
hook.Add("canDropWeapon", "animelife.darkrpmisc.candrop", function(ply, ent)
    if table.HasValue(restricted_drop, ent:GetClass()) then
        return false
    end

    if classchooser and classchooser.List[ply:Team()] then
        local class = ply:GetNWInt("animelife.classchooser.class", -1)
        if class ~= -1 then
            local class_weapons = classchooser.List[ply:Team()].Weapons
            if table.HasValue(class_weapons, ent:GetClass()) then
                return false
            end
        end
    end
end)