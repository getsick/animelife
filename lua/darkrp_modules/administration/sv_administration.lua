sql.Query("CREATE TABLE IF NOT EXISTS animelife_bans(SteamID TINYTEXT, Reason TINYTEXT, Date SMALLINT, Time SMALLINT, Expired TINYINT)")
sql.Query("CREATE TABLE IF NOT EXISTS animelife_jails(SteamID TINYTEXT, Time SMALLINT)")

util.AddNetworkString("animelife.administration.menu")
util.AddNetworkString("animelife.administration.msg")
util.AddNetworkString("animelife.administration.action")
util.AddNetworkString("animelife.administration.askban")
util.AddNetworkString("animelife.administration.askjail")
util.AddNetworkString("animelife.administration.setteam")
util.AddNetworkString("animelife.administration.action2")
util.AddNetworkString("animelife.administration.hideall")

module("administration", package.seeall)

local function Message(admin, target, act)
    local admin_name = "(Unknown Admin)"
    local target_name = "(Unknown Player)"
    act = act or "did something to"

    if IsValid(admin) then
        admin_name = admin:Nick()
    end

    if IsValid(target) then
        target_Name = target:Nick()
    end

    for _, ply in pairs(player.GetAll()) do
        if !ply:IsAdmin() then continue end

        net.Start("animelife.administration.msg")
            net.WriteString(admin_name)
            net.WriteString(target_Name)
            net.WriteString(act)
        net.Send(ply)
    end
end

function Slay(admin, target)
    target:Kill()

    Message(admin, target, "убил")

    return true
end

function God(admin, target)
    if !target.aniadmin_god then
        target:GodEnable()
        target.aniadmin_god = true

        Message(admin, target, "включил режим бога")
    else
        target:GodDisable()
        target.aniadmin_god = false

        Message(admin, target, "выключил режим бога")
    end

    return target.aniadmin_god
end

function StripWeapons(admin, target)
    target:StripWeapons()

    Message(admin, target, "обезоружил")

    return true
end

function NoDraw(admin, target)
    if target.aniadmin_nodraw then
        target:SetNoDraw(false)
        target.aniadmin_nodraw = false

        for _, v in ipairs(target:GetWeapons()) do
            v:SetNoDraw(false)
        end

        for _, v in ipairs(ents.FindByClass("physgun_beam")) do
            if v:GetParent() == target then
                v:SetNoDraw(false)
            end
        end

        Message(admin, target, "снял неведимку с")
    else
        target:SetNoDraw(true)
        target.aniadmin_nodraw = true

        for _, v in ipairs(target:GetWeapons()) do
            v:SetNoDraw(true)
        end

        for _, v in ipairs(ents.FindByClass("physgun_beam")) do
            if v:GetParent() == target then
                v:SetNoDraw(true)
            end
        end

        Message(admin, target, "спрятал")
    end

    return target.aniadmin_nodraw
end

function Freeze(admin, target)
    if target.aniadmin_locked then
        target:UnLock()
        target.aniadmin_locked = false

        Message(admin, target, "разморозил")
    else
        target:Lock()
        target.aniadmin_locked = true

        Message(admin, target, "заморозил")
    end

    return target.aniadmin_locked
end

function GiveWeapon(admin, target, weapon)
    target:Give(weapon)

    Message(admin, target, "выдал " .. weapon)

    return true
end

function ChangeTeam(admin, target, team_index)
    // target:SetTeam(team_index)
    // target:Spawn()

    target:changeTeam(team_index, true)

    Message(admin, target, "сменил профессию")

    return true
end

function SetHealth(admin, target, amount)
    target:SetHealth(amount)

    Message(admin, target, "установил здоровье на " .. amount .. " для")

    return true
end

function SetArmor(admin, target, amount)
    target:SetArmor(amount)

    Message(admin, target, "установил броню на " .. amount .. " для")

    return true
end

function GotoCoords(admin, target, pos)
    target.aniadmin_lastpos = target:GetPos()
    target:SetPos(pos)

    Message(admin, target, "телепортировал по координатам")

    return true
end

function Spectate(admin, target)
    if admin.aniadmin_spectate then
        admin:UnSpectate()
        admin:Spawn()
        admin:SetPos(admin.aniadmin_spectate_pos)

        admin.aniadmin_spectate = false
        admin.aniadmin_spectate_ent = nil
    else
        admin:SpectateEntity(target)
        admin:Spectate(OBS_MODE_CHASE)

        admin.aniadmin_spectate = true
        admin.aniadmin_spectate_ent = target
        admin.aniadmin_spectate_pos = admin:GetPos()
    end

    return true
end

hook.Add("SetupPlayerVisibility", "animelife.administration.spectate", function(ply)
    if ply.aniadmin_spectate and IsValid(ply.aniadmin_spectate_ent) then
        AddOriginToPVS(ply.aniadmin_spectate_ent:GetPos())
    end
end)

function GotoPlayer(admin, target, target2)
    target.aniadmin_lastpos = target:GetPos()
    target:SetPos(target2:GetPos() + Vector(0, 0, 73))

    local target2_nick = "?"
    if IsValid(target2) and target2:IsPlayer() then
        target2_nick = target2:Nick()
    end

    Message(admin, target, "(tp: -> " .. target2_nick .. ")")

    return true
end

function Return(admin, target)
    if isvector(target.aniadmin_lastpos) then
        target:SetPos(target.aniadmin_lastpos)
    end

    Message(admin, target, "вернул")

    return true
end

function Noclip(admin, target)
    if target.aniadmin_noclip then
        target.aniadmin_noclip = false

        Message(admin, target, "выключил noclip")
    else
        target.aniadmin_noclip = true

        Message(admin, target, "включил noclip")
    end

    return target.aniadmin_noclip
end

hook.Add("PlayerNoClip", "animelife.ManageNoClip", function(ply, desiredNoClipState)
    if !desiredNoClipState then return true end
	if ply.aniadmin_noclip and desiredNoClipState then
        return true
    end
end)

function Kick(admin, target)
    Message(admin, target, "отключил от сервера")

    target:Kick("Администратор " .. admin:Nick() .. " отключил вас от сервера ┐('～`;)┌")

    return true
end

function Mute(admin, target, chat_type, time)
    time = (time or 10) * 60
    if chat_type == "chat" then
        target.aniadmin_chatmute = true
        target:SetNWBool("animelife.administration.chatmute", true)
        Message(admin, target, "muted textchat for " .. string.NiceTime(time) .. " for")
    elseif chat_type == "voice" then
        target.aniadmin_voicemute = true
        target:SetNWBool("animelife.administration.voicemute", true)
        Message(admin, target, "muted voicechat for " .. string.NiceTime(time) .. " for")
    end

    local ident = "animelife.mutedplayers-" .. chat_type .. "-" .. target:SteamID()
    if timer.Exists(ident) then
        timer.Remove(ident)
    end

    timer.Create(ident, time, 1, function()
        if !IsValid(target) then return end

        if chat_type == "chat" then
            target.aniadmin_chatmute = false
            target:SetNWBool("animelife.administration.chatmute", false)
        elseif chat_type == "voice" then
            target.aniadmin_voicemute = false
            target:SetNWBool("animelife.administration.voicemute", false)
        end

        timer.Remove(ident)
    end)

    return true
end

function Unmute(admin, target, chat_type)
    if chat_type == "chat" then
        target.aniadmin_chatmute = false
        target:SetNWBool("animelife.administration.chatmute", false)
    elseif chat_type == "voice" then
        target.aniadmin_voicemute = false
        target:SetNWBool("animelife.administration.voicemute", false)
    end

    local ident = "animelife.mutedplayers-" .. chat_type .. "-" .. target:SteamID()
    if timer.Exists(ident) then
        timer.Remove(ident)
    end

    return true
end

function Ban(admin, target, reason, time)
    time = (time or 10) * 60
    reason = reason or "неизвестна"
    local steam_id = target

    if !isstring(target) and IsValid(target) then
        target:changeTeam(TEAM_CITIZEN or 1, true, true)
        -- target:Spawn()
        target:StripWeapons()
        target:SetNoDraw(true)
        -- target:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
        target:SetCustomCollisionCheck(true)

        net.Start("animelife.administration.hideall")
            net.WriteBool(true)
        net.Send(target)
        
        -- netvars
        target:SetNWBool("animelife.administration.ban", true)
        target:SetNWString("animelife.administration.ban_reason", reason)
        target:SetNWInt("animelife.administration.ban_date", os.time() + time)
        target:SetNWFloat("animelife.administration.ban_wait", UnPredictedCurTime() + time)

        target:SetNWInt("animelife.administration.ban_count", target:GetNWInt("animelife.administration.ban_count", 0) + 1)

        -- set steam id
        steam_id = target:SteamID()
    end

    // sql.Query("DELETE FROM animelife_bans WHERE SteamID = " .. SQLStr(steam_id)) -- incase there's a ban already
    sql.Query("UPDATE animelife_bans SET Expired = " .. 1 .. " WHERE SteamID = " .. SQLStr(steam_id))
    sql.Query("INSERT INTO animelife_bans(SteamID, Reason, Date, Time, Expired) VALUES("
    .. SQLStr(steam_id) .. ", "
    .. SQLStr(reason) .. ", "
    .. os.time() + time .. ", "
    .. time .. ", "
    .. 0 .. ")")
end

function Unban(admin, target)
    local steam_id = target
    if !isstring(target) and IsValid(target) then
        if !target:GetNWBool("animelife.administration.jail", false) then
            -- target:changeTeam(target, TEAM_CITIZEN or 1, true)
            target:KillSilent()
            target:SetNoDraw(false)
            -- target:SetCollisionGroup(COLLISION_GROUP_PLAYER)
            target:SetCustomCollisionCheck(false)
        end

        net.Start("animelife.administration.hideall")
            net.WriteBool(false)
        net.Send(target)
        
        -- netvars
        target:SetNWBool("animelife.administration.ban", false)
        target:SetNWString("animelife.administration.ban_reason", nil)
        target:SetNWInt("animelife.administration.ban_date", nil)
        target:SetNWFloat("animelife.administration.ban_wait", nil)

        -- set steam id
        steam_id = target:SteamID()
    end

    // sql.Query("DELETE FROM animelife_bans WHERE SteamID = " .. SQLStr(steam_id))
    sql.Query("UPDATE animelife_bans SET Expired = " .. 1 .. " WHERE SteamID = " .. SQLStr(steam_id))
end

hook.Add("PlayerLoadout", "animelife.administration.PlayerLoadout", function(ply)
    if ply:GetNWBool("animelife.administration.ban", false) or ply:GetNWBool("animelife.administration.jail", false) then
        return true -- prevent loadout
    end
end)

hook.Add("PlayerInitialSpawn", "animelife.administration.RestoreBans", function(ply)
    timer.Simple(2, function()
        if !IsValid(ply) then return end

        local data = sql.Query("SELECT * FROM animelife_bans WHERE SteamID = " .. SQLStr(ply:SteamID()))
        if istable(data) and #data > 0 then
            local last_key = table.GetLastKey(data)
            if tonumber(data[last_key].Expired) < 1 then
                ply:changeTeam(ply, TEAM_CITIZEN or 1, true)
                ply:Spawn()
                ply:StripWeapons()
                ply:SetNoDraw(true)
                -- ply:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
                ply:SetCustomCollisionCheck(true)
                
                -- netvars
                ply:SetNWBool("animelife.administration.ban", true)
                ply:SetNWString("animelife.administration.ban_reason", data[last_key].Reason)
                ply:SetNWInt("animelife.administration.ban_date", data[last_key].Date)
                ply:SetNWFloat("animelife.administration.ban_wait", UnPredictedCurTime() + data[last_key].Time)
            end

            ply:SetNWInt("animelife.administration.ban_count", #data)
        end

        local jails = sql.Query("SELECT * FROM animelife_jails WHERE SteamID = " .. SQLStr(ply:SteamID()))
        if istable(jails) and #jails > 0 then
            ply:SetNWInt("animelife.administration.jail_count", #jails)
        end
    end)
end)

local jail_pos = Vector(-4055, 10911, -1252)
function Jail(admin, target, time)
    time = (time or 10) * 60

    if !target:GetNWBool("animelife.administration.ban", false) then
        target:StripWeapons()
        target:SetCustomCollisionCheck(true)
        target:changeTeam(TEAM_CITIZEN or 1, true, true)
        -- target:Spawn()
        target:SetPos(jail_pos)
    end

    target:SetNWBool("animelife.administration.jail", true)
    target:SetNWFloat("animelife.administration.jail_wait", UnPredictedCurTime() + time)
    target.aniadmin_jailtime = os.time() + time

    Message(admin, target, "jailed for " .. (time / 60) .. " minutes")

    target:SetNWInt("animelife.administration.jail_count", target:GetNWInt("animelife.administration.jail_count", 0) + 1)

    sql.Query("INSERT INTO animelife_jails(SteamID, Time) VALUES("
    .. SQLStr(target:SteamID()) .. ", "
    .. time .. ")")
end

function Unjail(admin, target)
    target:SetNWBool("animelife.administration.jail", false)
    target:SetNWFloat("animelife.administration.jail_wait", nil)
    target.aniadmin_jailtime = 0

    if !target:GetNWBool("animelife.administration.ban", false) then
        -- target:changeTeam(target, TEAM_CITIZEN or 1, true)
        target:Spawn()
        target:SetCustomCollisionCheck(false)
    end

    Message(admin, target, "unjailed")
end

hook.Add("PhysgunPickup", "animelife.administration.PlayerPickup", function(ply, ent)
	if ply:IsAdmin() and ent:IsPlayer() then
        if ply:CanAct(ent) then
		    return true
        end
	end
end)

hook.Add("PlayerSpawn", "animelife.administration.JailBanSpawn", function(ply)
    if ply:GetNWBool("animelife.administration.ban", false) then
        ply:StripWeapons()
        ply:SetNoDraw(true)
        -- ply:SetCollisionGroup(COLLISION_GROUP_PASSABLE_DOOR)
        ply:SetCustomCollisionCheck(true)
    elseif ply:GetNWBool("animelife.administration.jail", false) then
        ply:StripWeapons()
        -- ply:SetCollisionGroup(COLLISION_GROUP_PASSABLE_DOOR)
        ply:SetCustomCollisionCheck(true)
        ply:SetPos(jail_pos)
    end
end)

hook.Add("PlayerUse", "animelife.administration.JailBanUse", function(ply, ent)
    if ply:GetNWBool("animelife.administration.ban", false) or ply:GetNWBool("animelife.administration.jail", false) then
        return false
    end
end)

hook.Add("FindUseEntity", "animelife.administration.JailBanUse", function(ply, ent)
    if ply:GetNWBool("animelife.administration.ban", false) or ply:GetNWBool("animelife.administration.jail", false) then
        return game.GetWorld()
    end
end)

hook.Add("playerBuyDoor", "animelife.administration.JailBanDoors", function(ply, ent)
    if ply:GetNWBool("animelife.administration.ban", false) or ply:GetNWBool("animelife.administration.jail", false) then
        return false, "Вы наказаны. Двери не трогать.", true
    end
end)

hook.Add("PlayerSpawnProp", "animelife.administration.JailBanProps", function(ply)
    if ply:GetNWBool("animelife.administration.ban", false) or ply:GetNWBool("animelife.administration.jail", false) then
        return false
    end
end)

hook.Add("playerCanChangeTeam", "animelife.administration.JailBanJobs", function(ply)
    if ply:GetNWBool("animelife.administration.ban", false) or ply:GetNWBool("animelife.administration.jail", false) then
        return false, "Вы наказаны. Профессии не менять."
    end
end)

hook.Add("EntityTakeDamage", "animelife.administration.JailBanDeath", function(target)
    if IsValid(target) and target:IsPlayer() then
        if target:GetNWBool("animelife.administration.ban", false) then
            return true
        end
    end
end)

hook.Add("PlayerSwitchWeapon", "animelife.administration.nodraw", function(ply, old, new)
    if IsValid(ply) and ply:IsPlayer() then
        if ply.aniadmin_nodraw or ply:GetNoDraw() then
            if IsValid(new) then
                new:SetNoDraw(true)
            end
        end
    end
end)

timer.Create("animelife.JailThink", 2, 0, function()
    for _, ply in pairs(player.GetHumans()) do
        if ply:GetNWBool("animelife.administration.jail", false) then
            if (ply.aniadmin_jailtime or 0) < os.time() then
                Unjail(game.GetWorld(), ply)
            end
        end
    end
end)

timer.Create("animelife.BansThink", 6, 0, function()
    local t = sql.Query("SELECT * FROM animelife_bans")
    if istable(t) and #t > 0 then
        for i = 1, #t do
            local v = t[i]
            if tonumber(v.Expired) == 0 and (tonumber(v.Date) < os.time()) then
                // sql.Query("UPDATE animelife_bans SET Expired = " .. 1 .. "WHERE SteamID = " .. SQLStr(v.SteamID))
                if IsValid(player.GetBySteamID(v.SteamID)) then
                    Unban(game.GetWorld(), player.GetBySteamID(v.SteamID))
                else
                    Unban(game.GetWorld(), v.SteamID)
                end
            end
        end
    end
end)

function OpenMenu(ply)
    if !ply:IsAdmin() then return end
    
    local warns = GetWarns(ply)
    warns = util.TableToJSON(warns)
    warns = util.Compress(warns)

    local len = warns:len()
    net.Start("animelife.administration.menu")
        net.WriteInt(len, 32)
        net.WriteData(warns, len)
    net.Send(ply)
end

net.Receive("animelife.administration.action", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local act = net.ReadString()
    local tgt = net.ReadEntity()
    local tgt2 = net.ReadEntity()

    if !IsValid(tgt) or !tgt:IsPlayer() then return end
    if !ply:HasActionAccess(act) then
        ply:ChatPrint("Это действие недоступно для вашей привилегии.")
        return
    end
    if !ply:CanAct(tgt) and act ~= "teleport" then
        ply:ChatPrint("Это действие неприменимо по отношению к " .. tgt:Nick() .. ".")
        return
    end

    if administration and administration.Actions[act] then
        administration.Actions[act].Function(ply, tgt, tgt2)
    end
end)

net.Receive("animelife.administration.askban", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local target = net.ReadString()
    local reason = net.ReadString()
    local duration = net.ReadString()

    if !isnumber(tonumber(duration)) then
        return
    end

    local target_as_player = player.GetBySteamID(target)

    Ban(ply, IsValid(target_as_player) and target_as_player or target, reason, tonumber(duration))
end)

net.Receive("animelife.administration.askjail", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local target = net.ReadString()
    local duration = net.ReadString()

    if !isnumber(tonumber(duration)) then
        return
    end

    local target_as_player = player.GetBySteamID(target)

    Jail(ply, IsValid(target_as_player) and target_as_player or target, tonumber(duration))
end)

net.Receive("animelife.administration.setteam", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local tgt = net.ReadEntity()
    local job = net.ReadInt(16)

    if !IsValid(tgt) or !tgt:IsPlayer() then return end
    if !ply:HasActionAccess("setteam") then
        ply:ChatPrint("Это действие недоступно для вашей привилегии.")
        return
    end
    if !ply:CanAct(tgt) then
        ply:ChatPrint("Это действие неприменимо по отношению к " .. tgt:Nick() .. ".")
        return
    end

    if administration and administration.Actions["setteam"] then
        administration.Actions["setteam"].Function(ply, tgt, job)
    end
end)

net.Receive("animelife.administration.action2", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local act = net.ReadString()
    local tgt = net.ReadEntity()
    local arg = net.ReadString()

    if !IsValid(tgt) or !tgt:IsPlayer() then return end
    if !ply:HasActionAccess(act) then
        ply:ChatPrint("Это действие недоступно для вашей привилегии.")
        return
    end
    if !ply:CanAct(tgt) then
        ply:ChatPrint("Это действие неприменимо по отношению к " .. tgt:Nick() .. ".")
        return
    end

    if administration and administration.Actions[act] then
        administration.Actions[act].Function(ply, tgt, isnumber(tonumber(arg)) and tonumber(arg) or arg)
    end
end)