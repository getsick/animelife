local PANEL = {}

function PANEL:Init()
    self.List = vgui.Create("DListView", self)
    self.List:AddColumn("SteamID куратора")
    self.List:AddColumn("SteamID админа")
    self.List:AddColumn("Никнейм админа")
    self.List:AddColumn("Причина")
    self.List:AddColumn("Дата окончания")

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("Закрыть окно")
    self.CloseButton.DoClick = function()
        self:Remove()
        self = nil
    end
end

function PANEL:PerformLayout(w, h)
    self.List:Dock(FILL)
    self.List:DockPadding(0, 0, 0, 48)

    self.CloseButton:Dock(BOTTOM)
end

function PANEL:SetData(nick, data)
    for _, row in pairs(data) do
        self.List:AddLine(row.CuratorID, row.SteamID, nick, row.Reason, os.date("%d.%m.%y %H:%M:%S", row.Date))
    end
end

function PANEL:Paint(w, h)
end

vgui.Register("animelife.administration.warnviewer", PANEL, "EditablePanel")

net.Receive("animelife.administration.curation.viewwarns", function()
    local nick = net.ReadString()
    local len = net.ReadInt(16)
    local data = net.ReadData(len)
    data = util.Decompress(data)
    data = util.JSONToTable(data)

    local pnl = vgui.Create("animelife.administration.warnviewer")
    pnl:SetSize(ScrW() / 3, ScrH() / 3)
    pnl:Center()
    pnl:MakePopup()
    pnl:SetData(nick, data)
end)