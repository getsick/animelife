local function findPlayer(obj)
    if isstring(obj) then
        -- SteamID case
        local sid, sid64 = player.GetBySteamID(obj), player.GetBySteamID64(obj)
        if IsValid(sid) then
            return sid
        elseif IsValid(sid64) then
            return sid64
        end

        -- Name case
        for _, ply in pairs(player.GetAll()) do
            if ply:GetName() == obj then
                return ply
            end
        end
    end

    return nil
end

concommand.Add("al_noclip", function(ply, cmd, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local target = findPlayer(arg_str)
    if !IsValid(target) then
        target = ply
    end

    if !ply:HasActionAccess("noclip") then return end
    if !ply:CanAct(target) then return end
    
    administration.Noclip(ply, target)
end)

concommand.Add("al_slay", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local target = findPlayer(arg_str)
    if !IsValid(target) then
        target = ply
    end

    if !ply:HasActionAccess("slay") then return end
    if !ply:CanAct(target) then return end
    
    administration.Slay(ply, target)
end)

concommand.Add("al_god", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local target = findPlayer(arg_str)
    if !IsValid(target) then
        target = ply
    end

    if !ply:HasActionAccess("godmode") then return end
    if !ply:CanAct(target) then return end
    
    administration.God(ply, target)
end)

concommand.Add("al_stripweapons", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local target = findPlayer(arg_str)
    if !IsValid(target) then
        target = ply
    end

    if !ply:HasActionAccess("stripweapons") then return end
    if !ply:CanAct(target) then return end
    
    administration.StripWeapons(ply, target)
end)

concommand.Add("al_nodraw", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local target = findPlayer(arg_str)
    if !IsValid(target) then
        target = ply
    end

    if !ply:HasActionAccess("nodraw") then return end
    if !ply:CanAct(target) then return end
    
    administration.NoDraw(ply, target)
end)

concommand.Add("al_freeze", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local target = findPlayer(arg_str)
    if !IsValid(target) then
        target = ply
    end

    if !ply:HasActionAccess("freeze") then return end
    if !ply:CanAct(target) then return end
    
    administration.Freeze(ply, target)
end)

concommand.Add("al_giveweapon", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    arg_str = string.Split(arg_str, " ")

    local target = findPlayer(arg_str[1])
    if !IsValid(target) then
        target = ply
    end

    if !ply:HasActionAccess("giveweapon") then return end
    if !ply:CanAct(target) then return end
    
    if !isstring(arg_str[2]) then return end
    administration.GiveWeapon(ply, target, arg_str[2])
end)

concommand.Add("al_sethealth", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    arg_str = string.Split(arg_str, " ")

    local target = findPlayer(arg_str[1])
    if !IsValid(target) then
        target = ply
    end

    if !ply:HasActionAccess("sethp") then return end
    if !ply:CanAct(target) then return end
    
    if !isnumber(tonumber(arg_str[2])) then return end
    administration.SetHealth(ply, target, tonumber(arg_str[2]))
end)

concommand.Add("al_setarmor", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    arg_str = string.Split(arg_str, " ")

    local target = findPlayer(arg_str[1])
    if !IsValid(target) then
        target = ply
    end

    if !ply:HasActionAccess("setarmor") then return end
    if !ply:CanAct(target) then return end
    
    if !isnumber(tonumber(arg_str[2])) then return end
    administration.SetArmor(ply, target, tonumber(arg_str[2]))
end)

concommand.Add("al_return", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    local target = findPlayer(arg_str)
    if !IsValid(target) then
        target = ply
    end

    if !ply:HasActionAccess("return") then return end
    if !ply:CanAct(target) then return end

    administration.Return(ply, target)
end)

concommand.Add("al_teleport", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end

    arg_str = string.Split(arg_str, " ")

    local target = findPlayer(string.Trim(arg_str[1], '"'))
    local target2 = findPlayer(string.Trim(arg_str[2], '"'))
    if !IsValid(target) then
        target = ply
    end
    if !IsValid(target2) then
        target2 = ply
    end

    if !ply:HasActionAccess("teleport") then return end
    if !ply:CanAct(target) then return end
    if !ply:CanAct(target2) then return end

    administration.GotoPlayer(ply, target, target2)
end)

concommand.Add("al_ban", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end
    if !ply:HasActionAccess("ban") then return end

    arg_str = string.Split(arg_str, " ")

    local target = findPlayer(arg_str[1])
    local time = tonumber(arg_str[2])
    local reason = table.concat(arg_str, " ", 3, #arg_str)

    if !IsValid(target) then
        target = arg_str[1]
    end

    if !isnumber(time) then
        return
    end

    administration.Ban(ply, target, reason, time)
end)

concommand.Add("al_unban", function(ply, cmd, args, arg_str)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !ply:IsAdmin() then return end
    if !ply:HasActionAccess("ban_un") then return end

    local target = findPlayer(arg_str)
    if !IsValid(target) then
        target = arg_str
    end

    administration.Unban(ply, target)
end)