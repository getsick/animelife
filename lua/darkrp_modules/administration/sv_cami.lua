hook.Add("CAMI.PlayerHasAccess", "animelife.administration.cami", function(access_handler, actor, privilege, cb, target, extra)
    if privilege == "DarkRP_AdminCommands" then
        return ply:IsUserGroup("Curator") or ply:IsUserGroup("superadmin") or ply:IsUserGroup("root")
    end
end)