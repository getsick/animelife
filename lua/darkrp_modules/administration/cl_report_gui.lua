local PANEL = {}

surface.CreateFont("animelife.administration.report.title", {font = "Exo 2 SemiBold", size = 16, weight = 600, extended = true})

function PANEL:Init()
    self.PlayerPick = vgui.Create("DComboBox", self)
    self.PlayerPick:SetValue("Игрок не выбран")

    local player_table = player.GetHumans()
    for _, ply in pairs(player_table) do
        self.PlayerPick:AddChoice(ply:Nick() .. " [" .. ply:SteamID() .. "]")
    end

    self.ReasonInput = vgui.Create("DTextEntry", self)

    self.SendButton = vgui.Create("DButton", self)
    self.SendButton:SetText("")
    self.SendButton.Paint = function(pnl, w, h)
        draw.RoundedBox(8, 0, 0, w, h, Color(0, 0, 0))

        draw.SimpleText("Отправить жалобу", "animelife.administration.report.title", w / 2, h / 2, Color(255, 255, 255), 1, 1)
    end
    self.SendButton.DoClick = function()
        local pick = self.PlayerPick:GetSelectedID()
        if !isnumber(pick) then return end
        local ply = player_table[pick]
        net.Start("animelife.administration.sendreport")
            net.WriteEntity(ply)
            net.WriteString(self.ReasonInput:GetValue())
        net.SendToServer()

        self:Remove()
        self = nil
    end

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("")
    self.CloseButton.Paint = function(pnl, w, h)
        draw.SimpleText("x", "animelife.administration.report.title", w / 2, h / 2, Color(255, 255, 255), 1, 1)
    end
    self.CloseButton.DoClick = function()
        self:Remove()
        self = nil
    end
end

function PANEL:PerformLayout(w, h)
    self.SendButton:SetPos((w - 252) / 2, h - 50 - 44)
    self.SendButton:SetSize(252, 50)

    self.PlayerPick:SetPos((w - 352) / 2, 81 + 25)
    self.PlayerPick:SetSize(352, 36)

    self.ReasonInput:SetPos((w - 352) / 2, 201)
    self.ReasonInput:SetSize(352, 36)

    self.CloseButton:SetPos(w - 24 - 12, 12)
    self.CloseButton:SetSize(24, 24)
end

function PANEL:Paint(w, h)
    draw.RoundedBox(16, 0, 0, w, h, Color(0, 0, 0, 150))
    draw.RoundedBoxEx(16, 0, 0, w, 47, Color(0, 0, 0), true, true, false, false)

    draw.SimpleText("пожаловаться на игрока", "animelife.administration.report.title", 16, 16, Color(255, 255, 255))

    draw.SimpleText("Хочу пожаловаться на", "animelife.administration.report.title", w / 2, 81, Color(255, 255, 255), 1)
    draw.SimpleText("Причина жалобы", "animelife.administration.report.title", w / 2, 176, Color(255, 255, 255), 1)
end

function PANEL:Think()
    if input.IsKeyDown(KEY_ESCAPE) then
        self:Remove()
        self = nil
    end
end

vgui.Register("animelife.administration.reportgui", PANEL, "EditablePanel")

function OpenReportGUI()
    local pnl = vgui.Create("animelife.administration.reportgui")
    pnl:SetSize(433, 390)
    pnl:Center()
    pnl:MakePopup()
    pnl:SetAlpha(0)
    pnl:AlphaTo(255, 0.25)
end