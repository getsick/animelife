MySQLite.begin()

MySQLite.query([[
    CREATE TABLE IF NOT EXISTS al_usergroups(
        sid TINYTEXT NOT NULL PRIMARY KEY,
        usergroup TINYTEXT NOT NULL,
        type TINYINT NOT NULL,
        date MEDIUMINT NOT NULL
    );
]])

module("administration", package.seeall)

function AddUserToUsergroup(steam_id, group, t)
    if !steam_id then return end
    if !group then return end
    if !t then t = 1 end

    if group == "player" or group == "user" then
        MySQLite.query([[DELETE FROM al_usergroups WHERE sid = ]] .. MySQLite.SQLStr(steam_id) .. ";")
    else
        MySQLite.query([[SELECT usergroup
        FROM al_usergroups
        where sid = ]] .. MySQLite.SQLStr(steam_id) .. ";", function(data)
            if !data then
                MySQLite.query([[REPLACE INTO al_usergroups VALUES(]] ..
                MySQLite.SQLStr(steam_id) .. [[, ]] ..
                MySQLite.SQLStr(group) .. [[, ]] ..
                tonumber(t)  .. [[, ]] ..
                tonumber(os.time()) .. ");")
            end

            MySQLite.query([[UPDATE al_usergroups SET usergroup = ]] .. MySQLite.SQLStr(group) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(steam_id) .. ";")
            MySQLite.query([[UPDATE al_usergroups SET type = ]] .. tonumber(t) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(steam_id) .. ";")
            MySQLite.query([[UPDATE al_usergroups SET date = ]] .. tonumber(os.time()) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(steam_id) .. ";")
        end, function(e, q)
            print("Database Error:", e, q)
        end)
    end

    local ply = player.GetBySteamID(steam_id)
    if IsValid(ply) then
        ply:SetUserGroup(group)
        ply:SetNWInt("animelife.administration.usergroup.type", t)
        ply:SetNWInt("animelife.administration.usergroup.date", os.time())
    end
end

function RestoreUsergroup(ply)
    MySQLite.query([[
        SELECT usergroup, type, date
        FROM al_usergroups
        where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
            if !data then return end
            
            if data[1].usergroup then
                ply:SetUserGroup(data[1].usergroup)
            end

            if data[1].type then
                ply:SetNWInt("animelife.administration.usergroup.type", tonumber(data[1].type))
            end

            if data[1].date then
                ply:SetNWInt("animelife.administration.usergroup.date", tonumber(data[1].date))
            end
        end, function(e, q)
            print("Database Error:", e, q)
        end)
end

function PrintUsergroupList()
    MySQLite.query([[
        SELECT *
        FROM al_usergroups;]], function(data)
            if !data then return end
            
            PrintTable(data)
        end, function(e, q)
            print("Database Error:", e, q)
        end)
end

hook.Add("PlayerInitialSpawn", "animelife.administration.usergroups.db", function(ply)
    timer.Simple(10, function()
        if !IsValid(ply) or !ply:IsPlayer() then return end

        RestoreUsergroup(ply)
    end)
end)