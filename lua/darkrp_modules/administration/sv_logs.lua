util.AddNetworkString("animelife.logs.request_update")
util.AddNetworkString("animelife.logs.send_update")

module("logs", package.seeall)

List = List or {
    ["Death"] = {},
    ["Chat"] = {},
}

function Add(t, info)
    if !List[t] then
        List[t] = {}
    end

    List[t][#List[t] + 1] = info
end

-- hook.Add("PlayerDeath", "animelife.logs.deaths", function(victim, inflictor, attacker)
--     Add("Death", {
--         Time = os.time(),
--         VictimID = victim:SteamID(),
--         AttackerID = attacker:SteamID(),
--         VictimName = victim:Nick(),
--         AttackerName = attacker:Nick(),
--     })
-- end)

-- hook.Add("PlayerSay", "animelife.logs.chat", function(ply, msg, b_team)
--     Add("Chat", {
--         Time = os.time(),
--         PlayerID = ply:SteamID(),
--         PlayerName = ply:Nick(),
--         Message = msg,
--         Team = b_team
--     })
-- end)

function PrintLogs()
    PrintTable(List)
end

net.Receive("animelife.logs.request_update", function(len, ply)
    local t = net.ReadString()
    local data = List[t]
    data = util.TableToJSON(data)
    data = util.Compress(data)

    local len = data:len()
    net.Start("animelife.logs.send_update")
        net.WriteString(t)
        net.WriteInt(len, 32)
        net.WriteData(data, len)
    net.Send(ply)
end)