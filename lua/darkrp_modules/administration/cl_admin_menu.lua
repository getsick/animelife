local PANEL = {}

local background = Material("animelife/administration/background.png")
local icon_close = Material("animelife/administration/icon_close.png")
local hug_face = Material("animelife/administration/hug_face.png")
local leaderboard_background = Material("animelife/administration/leaderboard_background.png")
local box_stats = Material("animelife/administration/box_stats.png")
local box_warns = Material("animelife/administration/box_warns.png")
local box_chat = Material("animelife/administration/box_chat.png")
local icon_stats = Material("animelife/administration/icon_stats.png")
local icon_send = Material("animelife/administration/icon_send.png")
local icon_reports = Material("animelife/administration/icon_reports.png")
local chat_message = Material("animelife/administration/chat_message.png")
local chat_message_sent = Material("animelife/administration/chat_message_sent.png")
local icon_hammer = Material("animelife/administration/icon_hammer.png")
local icon_star = Material("animelife/administration/icon_star.png")

FONT_WEIGHT_BOLD = 700
FONT_WEIGHT_REGULAR = 400
FONT_WEIGHT_LIGHT = 300
FONT_WEIGHT_MEDIUM = 500
FONT_WEIGHT_SEMIBOLD = 600

surface.CreateFont("animelife.admin.title.semibold", {font = "Exo 2 SemiBold", size = 43, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
surface.CreateFont("animelife.admin.title.bold", {font = "Exo 2", size = 43, weight = FONT_WEIGHT_BOLD, extended = true})
surface.CreateFont("animelife.admin.title", {font = "Exo 2", size = 26, weight = FONT_WEIGHT_BOLD, extended = true})
surface.CreateFont("animelife.admin.date", {font = "Exo 2 SemiBold", size = 17, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
surface.CreateFont("animelife.admin.subtitle", {font = "Exo 2 SemiBold", size = 22, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
surface.CreateFont("animelife.admin.subtitle02", {font = "Exo 2 SemiBold", size = 24, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
surface.CreateFont("animelife.admin.subtitle03", {font = "Exo 2 SemiBold", size = 32, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
surface.CreateFont("animelife.admin.leaderboard.place", {font = "Exo 2", size = 12, weight = FONT_WEIGHT_BOLD, extended = true})
surface.CreateFont("animelife.admin.leaderboard.name", {font = "Exo 2", size = 14, weight = FONT_WEIGHT_BOLD, extended = true})
surface.CreateFont("animelife.admin.leaderboard.stats", {font = "Exo 2 SemiBold", size = 14, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
surface.CreateFont("animelife.admin.warn", {font = "Exo 2 SemiBold", size = 17, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
surface.CreateFont("animelife.admin.warn02", {font = "Exo 2 SemiBold", size = 14, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
surface.CreateFont("animelife.admin.report", {font = "Exo 2 SemiBold", size = 19, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
surface.CreateFont("animelife.admin.report02", {font = "Exo 2 SemiBold", size = 17, weight = FONT_WEIGHT_SEMIBOLD, extended = true})

GLOBALS_ADMINUI_CHAT = GLOBALS_ADMINUI_CHAT or {}

function PANEL:Init()
    self.CurrentTab = 1
    self.Warns = {}

    local x, y = 30, 96
    for i = 1, 2 do
        local tab_button = vgui.Create("DButton", self)
        tab_button:SetText("")
        tab_button:SetPos(ui.x(x), ui.y(y))
        tab_button:SetSize(ui.y(24), ui.y(24))
        tab_button.Paint = function(panel, w, h)
            local mat = i == 1 and icon_stats or icon_reports
            ui.Smooth(true, true)
                surface.SetDrawColor(self.CurrentTab == i and Color(132, 173, 174) or Color(85, 91, 110))
                surface.SetMaterial(mat)
                surface.DrawTexturedRect((w - mat:Width()) / 2, (h - mat:Height()) / 2, mat:Width(), mat:Height())
            ui.Smooth(false, true)
        end
        tab_button.DoClick = function()
            if i == 2 then
                self.StatsTab:SetVisible(false)
                self.ReportsTab:SetVisible(true)
            else
                self.StatsTab:SetVisible(true)
                self.ReportsTab:SetVisible(false)
            end

            self.CurrentTab = i
        end

        y = y + 24 + 24
    end

    self.StatsTab = vgui.Create("DPanel", self)
    self.StatsTab.Paint = function(panel, w, h)
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(leaderboard_background)
        surface.DrawTexturedRect(w - ui.x(372), 0, ui.x(372), h)

        draw.SimpleText("Лучшие за неделю", "animelife.admin.title", w - ui.x(138) - ui.x(201) + 4, 54, Color(18, 16, 28))

        surface.SetDrawColor(247, 248, 250)
        surface.DrawRect(w - ui.x(26) - ui.x(313), 84, ui.x(313), 3)

        draw.RoundedBox(6, w - ui.x(295) - ui.x(44), 86, ui.x(44), 3, Color(195, 226, 230))

        draw.SimpleText(os.date("%d %B %Y", os.time()), "animelife.admin.date", 73, 67, Color(207, 206, 212))

        local resolution_small = ScrW() < ui.BASE_RESOLUTION_WIDTH
        if resolution_small then
            draw.SimpleText("Привет!", "animelife.admin.title.semibold", 73, 81, Color(20, 18, 30))
        else
            draw.SimpleText("Привет,", "animelife.admin.title.semibold", 73, 81, Color(20, 18, 30))
            draw.SimpleText(LocalPlayer():Nick() .. "!", "animelife.admin.title.bold", 192, 81, Color(20, 18, 30))
        end

        surface.SetFont("animelife.admin.title.bold")

        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(hug_face)
        surface.DrawTexturedRect(192 + (resolution_small and 0 or surface.GetTextSize(LocalPlayer():Nick() .. "!")) + 9, 90, 30, 30)

        draw.SimpleText("Статистика", "animelife.admin.subtitle", 107, ui.y(173), Color(20, 18, 30))

        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(box_stats)
        surface.DrawTexturedRect(ui.x(58), ui.y(193), ui.x(776), ui.y(230))

        local x, y, i = 286, 238, 1
        local group_type = LocalPlayer():GetNWInt("animelife.administration.usergroup.type", 1)
        group_type = group_type == 1 and "наборный" or "донатный"
        local group_date = LocalPlayer():GetNWInt("animelife.administration.usergroup.date", 0)
        for _, v in pairs({
            {"На посту с", os.date("%d.%m.%y", group_date)},
            {"Разобрано репортов", LocalPlayer():GetNWInt("animelife.administration.report_count", 0)},
            {"Тип привилегии", group_type},
            {"Ваше место в топе", 1}
        }) do
            draw.SimpleText(v[1], "animelife.admin.subtitle02", ui.x(x), ui.y(y), Color(17, 19, 22), 1)
            draw.SimpleText(v[2], "animelife.admin.subtitle03", ui.x(x), ui.y(y) + ui.y(26), Color(144, 181, 182), 1)

            if i % 2 == 0 then
                y = y + 26 + 53
                x = 286
                i = 1
            else
                x = x + 286
                i = i + 1
            end
        end

        draw.SimpleText("Ваши предупреждения", "animelife.admin.subtitle", 107, ui.y(434), Color(20, 18, 30))

        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(box_warns)
        surface.DrawTexturedRect(ui.x(58), ui.y(456), ui.x(776), ui.y(393))

        if #self.Warns < 1 then
            draw.SimpleText("Всё хорошо. Предупреждений нет. <(￣︶￣)>", "animelife.admin.subtitle", ui.x(58) + (ui.x(776 / 2)), ui.y(456) + (ui.y(393 / 2)), Color(175, 175, 175), 1, 1)
        else
            if self.WarnsScroll.Items < 1 then
                draw.SimpleText("Предупреждения загружаются... ε=ε=ε=ε=┌(;￣▽￣)┘", "animelife.admin.subtitle", ui.x(58) + (ui.x(776 / 2)), ui.y(456) + (ui.y(393 / 2)), Color(175, 175, 175), 1, 1)
            end
        end
    end

    self.WarnsScroll = vgui.Create("DScrollPanel", self.StatsTab)
    self.WarnsScroll.Items = 0
    local v_bar = self.WarnsScroll:GetVBar()
    v_bar:SetWide(4)
    v_bar.Paint = function() end
    v_bar.btnUp.Paint = function() end
    v_bar.btnDown.Paint = function() end
    v_bar.btnGrip.Paint = function(panel, w, h)
        draw.RoundedBox(4, 0, 0, w, h, Color(0, 0, 0, 25))
    end

    timer.Simple(0.5, function()
        if !ValidPanel(self) then return end
        
        for _, warn in pairs(self.Warns) do
            local warn_item = vgui.Create("DPanel", self.WarnsScroll)
            warn_item:SetSize(ui.x(680), 51)
            warn_item:Dock(TOP)
            warn_item:DockMargin(0, 8, 0, 0)
            warn_item.Paint = function(panel, w, h)
                draw.RoundedBox(6, 0, 0, w, h, Color(149, 148, 154))

                draw.RoundedBoxEx(6, w - 212, 0, 212, h, Color(134, 133, 139), false, true, false, true)

                draw.SimpleText("Выдано за: " .. warn.Reason, "animelife.admin.warn", 40, h / 2, Color(255, 255, 255), nil, 1)
                draw.SimpleText("Истекает: " .. os.date("%d.%m.%Y", warn.Date), "animelife.admin.warn02", w - (212 / 2), h / 2, Color(255, 255, 255), 1, 1)
            end

            self.WarnsScroll.Items = self.WarnsScroll.Items + 1
        end
    end)

    // self.NameLabel = vgui.Create("DLabel", self.StatsTab)
    // self.NameLabel:SetText("Привет,")
    // self.NameLabel:SetFont("animelife.admin.title.semibold")
    // self.NameLabel:SetColor(Color(20, 18, 30))
    // self.NameLabel:SizeToContents()

    // self.NameLabelBold = vgui.Create("DLabel", self.StatsTab)
    // self.NameLabelBold:SetText(LocalPlayer():Nick() .. "!")
    // self.NameLabelBold:SetFont("animelife.admin.title.bold")
    // self.NameLabelBold:SetColor(Color(20, 18, 30))
    // self.NameLabelBold:SizeToContents()

    self.Leaderboard = vgui.Create("DScrollPanel", self.StatsTab)

    -- for i = 1, 15 do
    --     local leaderboard_item = vgui.Create("DPanel", self.Leaderboard)
    --     leaderboard_item:SetSize(251, 81)
    --     leaderboard_item:Dock(TOP)
    --     leaderboard_item:DockMargin(0, 11, 0, 0)
    --     leaderboard_item.Paint = function(panel, w, h)
    --         draw.SimpleText("#" .. i, "animelife.admin.leaderboard.place", 9 + 36, 4, Color(185, 185, 188))
    --         draw.SimpleText("Nameless Player", "animelife.admin.leaderboard.name", 9 + 36, 4 + 12, Color(18, 16, 28))
    --         draw.DrawText("Разобранных репортов: 56\nСредняя оценка: 5", "animelife.admin.leaderboard.stats", 9 + 36, 4 + 12 + 14, Color(18, 16, 28))

    --         surface.SetDrawColor(158, 158, 158)
    --         surface.DrawRect(0, 0, 36, 36)

    --         surface.SetDrawColor(245, 245, 251)
    --         surface.DrawRect(18, 6 + 36, 2, 39)
    --     end

    --     local leaderboard_avatar = vgui.Create("AvatarImage", leaderboard_item)
    --     leaderboard_avatar:SetPos(0, 0)
    --     leaderboard_avatar:SetSize(36, 36)
    --     leaderboard_avatar:SetSteamID("7656119"..tostring(7960265728+math.random(1, 200000000)), 64)
    -- end

    self.ReportsTab = vgui.Create("DPanel", self)
    self.ReportsTab:SetVisible(false)
    self.ReportsTab.Paint = function(panel, w, h)
        draw.SimpleText(os.date("%d %B %Y", os.time()), "animelife.admin.date", 73, 67, Color(207, 206, 212))

        draw.SimpleText("Жалобы", "animelife.admin.title.semibold", 73, 81, Color(20, 18, 30))

        if table.IsEmpty(GLOBALS_REPORTS) then
            draw.SimpleText("Жалоб не найдено. Возвращайся позже.", "animelife.admin.subtitle", w / 2, h / 2, Color(207, 206, 212), 1, 1)
        end
    end

    self.ReportScroll = vgui.Create("DScrollPanel", self.ReportsTab)
    self.ReportScroll.Panels = {}
    self:PaintScrollbar()

    for idx, report in SortedPairsByMemberValue(GLOBALS_REPORTS, "Time", true) do
        self:NewReport(report.Reporter, report.Target, report.Reason, report.Time, idx)
    end

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("")
    self.CloseButton.Paint = function(panel, w, h)
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(icon_close)
        surface.DrawTexturedRect(0, 0, w, h)
    end
    self.CloseButton.DoClick = function()
        if ValidPanel(self) then
            self:Remove()
            self = nil
        end
    end
end

function PANEL:PerformLayout(w, h)
    self.CloseButton:SetPos(w - 24 - 34, 30)
    self.CloseButton:SetSize(24, 24)

    self.ReportsTab:SetPos(ui.x(88), 0)
    self.ReportsTab:SetSize(ui.x(1254), h)

    self.ReportScroll:SetPos(0, 128)
    self.ReportScroll:SetSize(self.ReportsTab:GetWide(), self.ReportsTab:GetTall() - 128)

    self.StatsTab:SetPos(ui.x(88), 0)
    self.StatsTab:SetSize(ui.x(1254), h)

    self.WarnsScroll:SetPos(ui.x(102), ui.y(486))
    self.WarnsScroll:SetSize(ui.x(776) - ui.x(88), ui.y(325))

    self.Leaderboard:SetPos(w - 372, 124)
    self.Leaderboard:SetSize(372, h - 124)

    // self.NameLabel:SetPos(73, 81)
    // self.NameLabelBold:SetPos(self.NameLabel:GetWide() + 73 + 8, 81)
end

function PANEL:PaintScrollbar()
    local vertical_bar = self.ReportScroll:GetVBar()
    vertical_bar:SetWide(6)

    vertical_bar.Paint = function() end
    vertical_bar.btnGrip.Paint = function(pnl, w, h)
        draw.RoundedBox(6, 0, 0, w, h, Color(0, 0, 0, 150))
    end
    vertical_bar.btnUp.Paint = function() end
    vertical_bar.btnDown.Paint = function() end
end

function PANEL:NewReport(ply, target, reason, time, idx)
    local pnl = vgui.Create("DPanel", self.ReportScroll)
    pnl:SetSize(self.ReportScroll:GetWide() - 128, 128)
    pnl:Dock(TOP)
    pnl:DockMargin(64, 16, 64, 0)

    pnl.Info = {ply, target, reason, time, idx}
    pnl.Paint = function(panel, w, h)
        self:ReportPanelPaint(panel, w, h)
    end

    pnl.Think = function()
        if !istable(GLOBALS_REPORTS[idx]) then
            pnl:Remove()
        end
    end

    self.ReportScroll:AddItem(pnl)

    local x, y = ui.x(793), 18
    for i, b in pairs({"Телепортировать", "Ответить на жалобу", "Не интересно", "Телепортироваться к"}) do
        local btn = vgui.Create("DButton", pnl)
        btn:SetText("")
        btn:SetPos(x, y)
        btn:SetSize(ui.x(145), 41)
        btn.Paint = function(panel, w, h)
            draw.RoundedBox(8, 0, 0, w, h, Color(235, 117, 155))

            if i == 2 then
                if istable(GLOBALS_REPORTS[idx]) then
                    local taken = GLOBALS_REPORTS[idx].TakenBy
                    local text = b
                    if IsValid(taken) and taken == LocalPlayer() then
                        text = "Закрыть жалобу"
                    end
                    draw.SimpleText(text, "animelife.admin.report02", w / 2, h / 2, Color(255, 255, 255), 1, 1)
                    return
                end
            end

            draw.SimpleText(b, "animelife.admin.report02", w / 2, h / 2, Color(255, 255, 255), 1, 1)
        end
        btn.DoClick = function()
            if i == 3 then
                local taken = GLOBALS_REPORTS[idx].TakenBy
                if IsValid(taken) and taken == LocalPlayer() then
                    -- TODO: closing?
                else
                    self:RemoveReport(pnl)
                    
                    GLOBALS_REPORTS[idx] = nil
                end
            elseif i == 4 then
                RunConsoleCommand("al_teleport", LocalPlayer():SteamID(), target:SteamID())
            elseif i == 1 then
                local dmenu = DermaMenu(self)

                dmenu:AddOption(ply:Nick(), function()
                    RunConsoleCommand("al_teleport", ply:SteamID(), LocalPlayer():SteamID())
                end)

                dmenu:AddOption(target:Nick(), function()
                    RunConsoleCommand("al_teleport", target:SteamID(), LocalPlayer():SteamID())
                end)

                dmenu:Open()
            elseif i == 2 then
                local taken = GLOBALS_REPORTS[idx].TakenBy
                if IsValid(taken) and taken == LocalPlayer() then
                    net.Start("animelife.administration.closereport")
                    net.SendToServer()
                else
                    net.Start("animelife.administration.takereport")
                        net.WriteEntity(ply)
                        net.WriteEntity(target)
                        net.WriteString(reason)
                        net.WriteInt(time, 32)
                    net.SendToServer()
                end
            end
        end

        if i % 2 == 0 then
            y = y + 41 + 11
            x = ui.x(793)
        else
            x = x + ui.x(145) + ui.x(12)
        end
    end

    table.insert(self.ReportScroll.Panels, pnl)
end

function PANEL:RemoveReport(pnl)
    if ValidPanel(pnl) then
        pnl:Remove()
    end

    table.RemoveByValue(self.ReportScroll.Panels, pnl)
end

function PANEL:ReportPanelPaint(pnl, w, h)
    draw.RoundedBox(16, 0, 0, w, h, Color(255 ,255 ,255))

    surface.SetDrawColor(255, 255, 255)
    surface.SetMaterial(icon_hammer)
    surface.DrawTexturedRect(51, (h - 45) / 2, 45, 45)

    if IsValid(pnl.Info[2]) and IsValid(pnl.Info[1]) then
        draw.SimpleText(string.format("Жалоба на %s (%s)", pnl.Info[2]:Nick(), pnl.Info[2]:SteamID()), "animelife.admin.report", 156, 16, Color(164, 128, 151))
        draw.SimpleText(string.format("от: %s (%s)", pnl.Info[1]:Nick(), pnl.Info[1]:SteamID()), "animelife.admin.report", 156, 16 + 18, Color(184, 141, 167))
    end

    draw.DrawText("Что произошло: " .. pnl.Info[3] .. "\nЖалоба отправлена в " .. os.date("%H:%M", pnl.Info[4]), "animelife.admin.report02", 156, h - 58, Color(184, 141, 141), nil, TEXT_ALIGN_BOTTOM)

    local taken = GLOBALS_REPORTS[pnl.Info[5]].TakenBy
    if IsValid(taken) and taken:IsPlayer() then
        draw.SimpleText("На жалобу ответил: " .. taken:Nick(), "animelife.admin.subtitle", w / 2, h - 16, Color(0, 0, 0, 175), 1, TEXT_ALIGN_BOTTOM)
    end
end

function PANEL:Paint(w, h)
    ui.Smooth(true, true)
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(background)
        surface.DrawTexturedRect(0, 0, w, h)
    ui.Smooth(false, true)
end

vgui.Register("animelife.administration", PANEL, "EditablePanel")

function OpenAdminMenu(warns)
    local a = vgui.Create("animelife.administration")
    if istable(warns) then
        a.Warns = warns
    end
    a:SetSize(ui.x(1342), ui.y(903))
    a:Center()
    a:MakePopup()
    a:InvalidateLayout()
end

net.Receive("animelife.administration.menu", function()
    local len = net.ReadInt(32)
    local warns = net.ReadData(len)
    warns = util.Decompress(warns)
    warns = util.JSONToTable(warns)

    OpenAdminMenu(warns)
end)