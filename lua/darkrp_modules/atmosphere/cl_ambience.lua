local ambience_zones = {}

local function new_zone(map, snd, min, max)
    if !ambience_zones[map] then ambience_zones[map] = {} end

    table.insert(ambience_zones[map], {
        Filename = snd,
        Min = min,
        Max = max
    })

    return true
end

new_zone("rp_animelife", "tone_inside01", Vector(-11432, -19, 156), Vector(-10028, -1827, 516)) -- NOTE: metro should be first
new_zone("rp_animelife", "tone_inside02", Vector(-9589, -900, 4), Vector(-3184, 2154, 35))
new_zone("rp_animelife", "music_duel", Vector(-12648, 1360, 356), Vector(-11477, 2855, 914))
new_zone("rp_animelife", "tone_outside", Vector(0, 0, 0), Vector(0, 0, 0))

local function get_by_map()
    for m, z in pairs(ambience_zones) do
        if string.StartWith(game.GetMap(), m) then
            return z
        end
    end
end

local function order_zone_bounds()
    if table.IsEmpty(ambience_zones) then return end

    for m, z in pairs(ambience_zones) do
        if !string.StartWith(game.GetMap(), m) then continue end

        for _, f in ipairs(z) do
            OrderVectors(f.Min, f.Max)
        end
    end
end
order_zone_bounds()

local function in_zone_bounds(k)
    local z = get_by_map()
    if !istable(z) then return end

    for j, b in pairs(z) do
        local min, max = b.Min, b.Max
        if LocalPlayer():GetPos():WithinAABox(min, max) and j == k then
            return true
        end
    end

    return false
end

local function is_outdoors()
    local pos = LocalPlayer():GetShootPos() + Vector(0, 0, 64)
	local tr = util.TraceLine({start = pos, endpos = pos + Vector(0, 0, 16384), mask = MASK_SOLID})

    if tr.StartSolid then return false end
    if util.PointContents(tr.HitPos) == CONTENTS_WINDOW then return false end
    if tr.HitSky then return true end

    return false
end

local function is_within_station()
    return in_zone_bounds(1)
end

local function get_ambience()
    if table.IsEmpty(ambience_zones) then return end

    local ambience = {}
    for m, z in pairs(ambience_zones) do
        if !string.StartWith(game.GetMap(), m) then continue end

        for k, v in ipairs(z) do
            if (v.Min == v.Max) and is_outdoors() then
                -- this ambience is audible
                ambience[k] = true
                continue
            end

            if in_zone_bounds(k) then
                -- this ambience is audible
                ambience[k] = true
            end
        end
    end

    return ambience
end

local channels = {}
local mute_losefocus = GetConVar("snd_mute_losefocus")
hook.Add("InitPostEntity", "animelife.atmosphere.ambience", function()
    if !IsValid(LocalPlayer()) then return end

    local z = get_by_map()
    if !istable(z) then return end

    for k, f in ipairs(z) do
        channels[k] = nil

        sound.PlayFile("sound/animelife/ambience/" .. f.Filename .. ".mp3", "noplay", function(chan)
            if !IsValid(chan) then return end

            chan:SetVolume(0)
            chan:EnableLooping(true)
            chan:Play()

            channels[k] = chan
        end)
    end
end)

hook.Add("Think", "animelife.atmosphere.ambience", function()
    if !IsValid(LocalPlayer()) then return end

    local ambience = get_ambience()
    for k, b in pairs(channels) do
        if !IsValid(b) then continue end

        if b:GetState() ~= GMOD_CHANNEL_PLAYING then
            b:SetVolume(0)

            b:Play()
        end

        if mute_losefocus:GetBool() and !system.HasFocus() then
            b:SetVolume(0)
        else
            b:SetVolume(Lerp(0.05, b:GetVolume(), ambience[k] and 0.1 or 0))
        end
    end

    if is_within_station() then
        local ct = CurTime()
        if !LocalPlayer().NextMetroAnnouncement then
            LocalPlayer().NextMetroAnnouncement = ct + 300
        end
        if LocalPlayer().NextMetroAnnouncement < ct then
            local initial_pos = LocalPlayer():GetPos()
            initial_pos = initial_pos + LocalPlayer():GetUp() * 150 + LocalPlayer():GetRight() * -150

            sound.Play("animelife/announcements/metro_notification.mp3", initial_pos)
    
            timer.Simple(2, function()
                sound.Play("animelife/announcements/teine_trainarrival.mp3", initial_pos)
            end)

            LocalPlayer().NextMetroAnnouncement = ct + 600
        end
    end
end)