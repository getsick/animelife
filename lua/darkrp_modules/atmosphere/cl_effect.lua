local EFFECT = {}

EFFECT.Mat = Material("animelife/citizens/citizen_ask.png")

function EFFECT:Init(data)
	local pos = data:GetOrigin()

	local emitter = ParticleEmitter(pos)

	local iMax = data:GetScale() or 4
	for i = 1, iMax do
		local particle = emitter:Add(self.Mat, pos)
		particle:SetVelocity((Vector(0, 0, 0) + (VectorRand() * 1)) * math.random(5, 15))
		particle:SetDieTime(math.random(0.5, 1.5))
		particle:SetStartAlpha(255)
		particle:SetEndAlpha(0)
		particle:SetStartSize(2)
		particle:SetEndSize(0.1)
		particle:SetRoll(0)
		particle:SetRollDelta(0)
		particle:SetColor(255, 255, 255)
	end

	emitter:Finish()
end

function EFFECT:Think()
	return false
end

function EFFECT:Render()
end

effects.Register(EFFECT, "citizen_ask")