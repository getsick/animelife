local flux = util.Base64Decode("bG9jYWwgZmx1eCA9IHsgX3ZlcnNpb24gPSAiMC4xLjUiIH0KZmx1eC5fX2luZGV4ID0gZmx1eAoKZmx1eC50d2VlbnMgPSB7fQpmbHV4LmVhc2luZyA9IHsgbGluZWFyID0gZnVuY3Rpb24ocCkgcmV0dXJuIHAgZW5kIH0KCmxvY2FsIGVhc2luZyA9IHsKICBxdWFkICAgID0gInAgKiBwIiwKICBjdWJpYyAgID0gInAgKiBwICogcCIsCiAgcXVhcnQgICA9ICJwICogcCAqIHAgKiBwIiwKICBxdWludCAgID0gInAgKiBwICogcCAqIHAgKiBwIiwKICBleHBvICAgID0gIjIgXiAoMTAgKiAocCAtIDEpKSIsCiAgc2luZSAgICA9ICItbWF0aC5jb3MocCAqIChtYXRoLnBpICogLjUpKSArIDEiLAogIGNpcmMgICAgPSAiLShtYXRoLnNxcnQoMSAtIChwICogcCkpIC0gMSkiLAogIGJhY2sgICAgPSAicCAqIHAgKiAoMi43ICogcCAtIDEuNykiLAogIGVsYXN0aWMgPSAiLSgyXigxMCAqIChwIC0gMSkpICogbWF0aC5zaW4oKHAgLSAxLjA3NSkgKiAobWF0aC5waSAqIDIpIC8gLjMpKSIKfQoKbG9jYWwgbWFrZWZ1bmMgPSBmdW5jdGlvbihzdHIsIGV4cHIpCiAgcmV0dXJuIENvbXBpbGVTdHJpbmcoInJldHVybiBmdW5jdGlvbihwKSAiIC4uIHN0cjpnc3ViKCIlJGUiLCBleHByKSAuLiAiIGVuZCIsICJUZXN0RnVuY3Rpb24iKSgpCmVuZAoKZm9yIGssIHYgaW4gcGFpcnMoZWFzaW5nKSBkbwogIGZsdXguZWFzaW5nW2sgLi4gImluIl0gPSBtYWtlZnVuYygicmV0dXJuICRlIiwgdikKICBmbHV4LmVhc2luZ1trIC4uICJvdXQiXSA9IG1ha2VmdW5jKFtbCiAgICBwID0gMSAtIHAKICAgIHJldHVybiAxIC0gKCRlKQogIF1dLCB2KQogIGZsdXguZWFzaW5nW2sgLi4gImlub3V0Il0gPSBtYWtlZnVuYyhbWwogICAgcCA9IHAgKiAyCiAgICBpZiBwIDwgMSB0aGVuCiAgICAgIHJldHVybiAuNSAqICgkZSkKICAgIGVsc2UKICAgICAgcCA9IDIgLSBwCiAgICAgIHJldHVybiAuNSAqICgxIC0gKCRlKSkgKyAuNQogICAgZW5kCiAgXV0sIHYpCmVuZAoKCgpsb2NhbCB0d2VlbiA9IHt9CnR3ZWVuLl9faW5kZXggPSB0d2VlbgoKbG9jYWwgZnVuY3Rpb24gbWFrZWZzZXR0ZXIoZmllbGQpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBsb2NhbCBtdCA9IGdldG1ldGF0YWJsZSh4KQogICAgaWYgdHlwZSh4KSB+PSAiZnVuY3Rpb24iIGFuZCBub3QgKG10IGFuZCBtdC5fX2NhbGwpIHRoZW4KICAgICAgZXJyb3IoImV4cGVjdGVkIGZ1bmN0aW9uIG9yIGNhbGxhYmxlIiwgMikKICAgIGVuZAogICAgbG9jYWwgb2xkID0gc2VsZltmaWVsZF0KICAgIHNlbGZbZmllbGRdID0gb2xkIGFuZCBmdW5jdGlvbigpIG9sZCgpIHgoKSBlbmQgb3IgeAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgpsb2NhbCBmdW5jdGlvbiBtYWtlc2V0dGVyKGZpZWxkLCBjaGVja2ZuLCBlcnJtc2cpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBpZiBjaGVja2ZuIGFuZCBub3QgY2hlY2tmbih4KSB0aGVuCiAgICAgIGVycm9yKGVycm1zZzpnc3ViKCIlJHgiLCB0b3N0cmluZyh4KSksIDIpCiAgICBlbmQKICAgIHNlbGZbZmllbGRdID0geAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgp0d2Vlbi5lYXNlICA9IG1ha2VzZXR0ZXIoIl9lYXNlIiwKICAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uKHgpIHJldHVybiBmbHV4LmVhc2luZ1t4XSBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGVhc2luZyB0eXBlICckeCciKQp0d2Vlbi5kZWxheSA9IG1ha2VzZXR0ZXIoIl9kZWxheSIsCiAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbih4KSByZXR1cm4gdHlwZSh4KSA9PSAibnVtYmVyIiBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGRlbGF5IHRpbWU7IGV4cGVjdGVkIG51bWJlciIpCnR3ZWVuLm9uc3RhcnQgICAgID0gbWFrZWZzZXR0ZXIoIl9vbnN0YXJ0IikKdHdlZW4ub251cGRhdGUgICAgPSBtYWtlZnNldHRlcigiX29udXBkYXRlIikKdHdlZW4ub25jb21wbGV0ZSAgPSBtYWtlZnNldHRlcigiX29uY29tcGxldGUiKQoKCmZ1bmN0aW9uIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpCiAgbG9jYWwgc2VsZiA9IHNldG1ldGF0YWJsZSh7fSwgdHdlZW4pCiAgc2VsZi5vYmogPSBvYmoKICBzZWxmLnJhdGUgPSB0aW1lID4gMCBhbmQgMSAvIHRpbWUgb3IgMAogIHNlbGYucHJvZ3Jlc3MgPSB0aW1lID4gMCBhbmQgMCBvciAxCiAgc2VsZi5fZGVsYXkgPSAwCiAgc2VsZi5fZWFzZSA9ICJxdWFkb3V0IgogIHNlbGYudmFycyA9IHt9CiAgZm9yIGssIHYgaW4gcGFpcnModmFycykgZG8KICAgIGlmIHR5cGUodikgfj0gIm51bWJlciIgdGhlbgogICAgICBlcnJvcigiYmFkIHZhbHVlIGZvciBrZXkgJyIgLi4gayAuLiAiJzsgZXhwZWN0ZWQgbnVtYmVyIikKICAgIGVuZAogICAgc2VsZi52YXJzW2tdID0gdgogIGVuZAogIHJldHVybiBzZWxmCmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOmluaXQoKQogIGZvciBrLCB2IGluIHBhaXJzKHNlbGYudmFycykgZG8KICAgIGxvY2FsIHggPSBzZWxmLm9ialtrXQogICAgaWYgdHlwZSh4KSB+PSAibnVtYmVyIiB0aGVuCiAgICAgIGVycm9yKCJiYWQgdmFsdWUgb24gb2JqZWN0IGtleSAnIiAuLiBrIC4uICInOyBleHBlY3RlZCBudW1iZXIiKQogICAgZW5kCiAgICBzZWxmLnZhcnNba10gPSB7IHN0YXJ0ID0geCwgZGlmZiA9IHYgLSB4IH0KICBlbmQKICBzZWxmLmluaXRlZCA9IHRydWUKZW5kCgoKZnVuY3Rpb24gdHdlZW46YWZ0ZXIoLi4uKQogIGxvY2FsIHQKICBpZiBzZWxlY3QoIiMiLCAuLi4pID09IDIgdGhlbgogICAgdCA9IHR3ZWVuLm5ldyhzZWxmLm9iaiwgLi4uKQogIGVsc2UKICAgIHQgPSB0d2Vlbi5uZXcoLi4uKQogIGVuZAogIHQucGFyZW50ID0gc2VsZi5wYXJlbnQKICBzZWxmOm9uY29tcGxldGUoZnVuY3Rpb24oKSBmbHV4LmFkZChzZWxmLnBhcmVudCwgdCkgZW5kKQogIHJldHVybiB0CmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOnN0b3AoKQogIGZsdXgucmVtb3ZlKHNlbGYucGFyZW50LCBzZWxmKQplbmQKCgoKZnVuY3Rpb24gZmx1eC5ncm91cCgpCiAgcmV0dXJuIHNldG1ldGF0YWJsZSh7fSwgZmx1eCkKZW5kCgoKZnVuY3Rpb24gZmx1eDp0byhvYmosIHRpbWUsIHZhcnMpCiAgcmV0dXJuIGZsdXguYWRkKHNlbGYsIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpKQplbmQKCgpmdW5jdGlvbiBmbHV4OnVwZGF0ZShkZWx0YXRpbWUpCiAgZm9yIGkgPSAjc2VsZiwgMSwgLTEgZG8KICAgIGxvY2FsIHQgPSBzZWxmW2ldCiAgICBpZiB0Ll9kZWxheSA+IDAgdGhlbgogICAgICB0Ll9kZWxheSA9IHQuX2RlbGF5IC0gZGVsdGF0aW1lCiAgICBlbHNlCiAgICAgIGlmIG5vdCB0LmluaXRlZCB0aGVuCiAgICAgICAgZmx1eC5jbGVhcihzZWxmLCB0Lm9iaiwgdC52YXJzKQogICAgICAgIHQ6aW5pdCgpCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnN0YXJ0IHRoZW4KICAgICAgICB0Ll9vbnN0YXJ0KCkKICAgICAgICB0Ll9vbnN0YXJ0ID0gbmlsCiAgICAgIGVuZAogICAgICB0LnByb2dyZXNzID0gdC5wcm9ncmVzcyArIHQucmF0ZSAqIGRlbHRhdGltZQogICAgICBsb2NhbCBwID0gdC5wcm9ncmVzcwogICAgICBsb2NhbCB4ID0gcCA+PSAxIGFuZCAxIG9yIGZsdXguZWFzaW5nW3QuX2Vhc2VdKHApCiAgICAgIGZvciBrLCB2IGluIHBhaXJzKHQudmFycykgZG8KICAgICAgICB0Lm9ialtrXSA9IHYuc3RhcnQgKyB4ICogdi5kaWZmCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnVwZGF0ZSB0aGVuIHQuX29udXBkYXRlKCkgZW5kCiAgICAgIGlmIHAgPj0gMSB0aGVuCiAgICAgICAgZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgICAgICBpZiB0Ll9vbmNvbXBsZXRlIHRoZW4gdC5fb25jb21wbGV0ZSgpIGVuZAogICAgICBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmNsZWFyKG9iaiwgdmFycykKICBmb3IgdCBpbiBwYWlycyhzZWxmW29ial0pIGRvCiAgICBpZiB0LmluaXRlZCB0aGVuCiAgICAgIGZvciBrIGluIHBhaXJzKHZhcnMpIGRvIHQudmFyc1trXSA9IG5pbCBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmFkZCh0d2VlbikKICAtLSBBZGQgdG8gb2JqZWN0IHRhYmxlLCBjcmVhdGUgdGFibGUgaWYgaXQgZG9lcyBub3QgZXhpc3QKICBsb2NhbCBvYmogPSB0d2Vlbi5vYmoKICBzZWxmW29ial0gPSBzZWxmW29ial0gb3Ige30KICBzZWxmW29ial1bdHdlZW5dID0gdHJ1ZQogIC0tIEFkZCB0byBhcnJheQogIHRhYmxlLmluc2VydChzZWxmLCB0d2VlbikKICB0d2Vlbi5wYXJlbnQgPSBzZWxmCiAgcmV0dXJuIHR3ZWVuCmVuZAoKCmZ1bmN0aW9uIGZsdXg6cmVtb3ZlKHgpCiAgaWYgdHlwZSh4KSA9PSAibnVtYmVyIiB0aGVuCiAgICAtLSBSZW1vdmUgZnJvbSBvYmplY3QgdGFibGUsIGRlc3Ryb3kgdGFibGUgaWYgaXQgaXMgZW1wdHkKICAgIGxvY2FsIG9iaiA9IHNlbGZbeF0ub2JqCiAgICBzZWxmW29ial1bc2VsZlt4XV0gPSBuaWwKICAgIGlmIG5vdCBuZXh0KHNlbGZbb2JqXSkgdGhlbiBzZWxmW29ial0gPSBuaWwgZW5kCiAgICAtLSBSZW1vdmUgZnJvbSBhcnJheQogICAgc2VsZlt4XSA9IHNlbGZbI3NlbGZdCiAgICByZXR1cm4gdGFibGUucmVtb3ZlKHNlbGYpCiAgZW5kCiAgZm9yIGksIHYgaW4gaXBhaXJzKHNlbGYpIGRvCiAgICBpZiB2ID09IHggdGhlbgogICAgICByZXR1cm4gZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgIGVuZAogIGVuZAplbmQKCgoKbG9jYWwgYm91bmQgPSB7CiAgdG8gICAgICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgudG8oZmx1eC50d2VlbnMsIC4uLikgZW5kLAogIHVwZGF0ZSAgPSBmdW5jdGlvbiguLi4pIHJldHVybiBmbHV4LnVwZGF0ZShmbHV4LnR3ZWVucywgLi4uKSBlbmQsCiAgcmVtb3ZlICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgucmVtb3ZlKGZsdXgudHdlZW5zLCAuLi4pIGVuZCwKfQpzZXRtZXRhdGFibGUoYm91bmQsIGZsdXgpCgpyZXR1cm4gYm91bmQ=")
flux = CompileString(flux, "flux")()

GLOBALS_TALKINGNPC_DIALOGUES = {
    [1] = {
        "Если судьба действительно существует...",
        "Означает ли это, что ваше будущее определяется в тот же момент, когда вы рождаетесь?",
        "Если все ваши вчерашние дни накапливаются, чтобы создать ваше завтра...",
        "Является ли ваше будущее таким же окончательным, как и ваше прошлое?",
        "Я просто не знаю.",
        "Вот почему я все еще жива - потому что я не знаю. Вот почему я отчаянно цепляюсь за жизнь!"
    },
    [2] = {
        "Всякий раз, когда я замечаю симпатичную девушку или мальчика, у меня появляется склонность пялиться.",
        "Я ничего не могу с собой поделать - я просто пристально смотрю.",
        "На днях я поехала на велосипеде на железнодорожный вокзал...",
        "Я была в ванной, просто смотрела на себя в зеркало...",
    },
    [3] = {
        "Эй, у тебя есть минутка? Я хочу довериться тебе...",
        "Речь идет о преобладающем сейчас мировоззрении.",
        "Разница между пассивным и активным. Я имею в виду...",
        "Например, это разница между такими словами: ''Ой! Я убил его'', и: ''Да, это так. Я убил его''.",
        "Удивительно, но есть масса людей, которые не понимают разницы. Ты можешь в это поверить?!",
        "Но есть ОГРОМНАЯ разница между ''Ой!'' и 'Да, это так.'. Ты ведь понимаешь, о чем я говорю, верно?",
        "Любой может убить кого-нибудь в пылу момента. Но на удивление мало кто может сделать это с обдуманным намерением.",
        "Сделать этот выбор, а затем воплотить его в жизнь, намного сложнее, чем просто позволить своим эмоциям вести вас.",
        "Все дело в подготовке.",
        "Так что, может быть, вам интересно, какой смысл я пытаюсь донести?",
        "Ну, я и сама не совсем уверена. Может быть, тебе стоит попробовать читать между строк."
    },
    [4] = {
        "Хочешь знать, почему я ненавижу видеоигры?",
        "Возможно, вы не поверите, но когда-то у меня был лучший друг.",
        "На самом деле, он был моим *единственным* другом. Но в отличие от меня, у него было много друзей.",
        "Итак, однажды мы с ним были у него дома, когда появилась большая группа парней, чтобы потусоваться.",
        "Они только что купили последнюю крупнобюджетную видеоигру.",
        "Они сидели там, потерянные в игре, передавая контроллер взад и вперед, в течение нескольких часов.",
        "Но не я...",
        "Я просто сидела и смотрела. Правильно, я просто наблюдала! Что в этом плохого?! Мне было все равно!",
        "Просто наблюдая, я смогла определить все лучшие стратегии!",
        "Хотя на самом деле я никогда в нее не играла!",
        "И я тоже не планирую когда-либо играть в нее в будущем!"
    },
    [5] = {
        "Каждый день проходит спокойно. Каждый день полон радости. Счастливые дни с того момента, как ты просыпаешься, и до того, как ложишься спать.",
        "Желая только тех неизменных, бесконечных счастливых дней, дней скуки, повторяющихся во веки веков.",
        "Никаких отклонений, насколько могут видеть глаза. Жизнь без малейшего намека на ненормальность...",
        "Если бы мир был таким, я бы сочла *это* чертовски ненормальным."
    },
    [6] = {
        "Пока ты человек, всегда наступит момент, когда ты захочешь поступить по-другому.",
        "Даже игрок в бейсбол в высшей лиге...",
        "Даже футболист, путешествующий по Европе...",
        "Даже знаменитая певица, которая начинала свою карьеру в качестве дублерши в скетч-комедийном шоу...",
        "Она блестяще дебютирует, но вместо того, чтобы стремиться к большему, выходит замуж за какого-нибудь третьесортного актера и уходит на пенсию.",
        "У каждого человека есть сожаления, есть вещи, которые он хотел бы вернуть и изменить.",
        "Но не у меня!"
    },
    [7] = {
        "''Убийство'' - это слово, определение которого меняется со временем, с местом, с обществом.",
        "Если убийство является свидетельством ненормальности, то все величайшие герои истории должны были быть ненормальными.",
        "Иногда убийства, которые люди прощают, гораздо более ненормальны, чем те, которые они не прощают!",
    },
    [8] = {
        "С Новым Днем!",
        "Я из тех, что не хотят праздновать новый год. Я хочу праздновать рассвет каждого нового дня!",
        "Потому что каждый новый день заслуживает празднования! Так...",
        "Еще раз, с Новым Днем!",
    },
    [9] = {
        "Моя наименее любимая еда, безусловно, крабы.",
        "Я также не люблю креветки, яблоки, помидоры и приправленные икрой форели.",
        "Я вообще не люблю красную пищу.",
        "Красные продукты являются красными, потому что внутри них есть краб.",
        "На самом деле это заговор крабовой индустрии, чтобы попытаться заставить меня преодолеть свое отвращение!",
        "Я бы ни за что на это не купилась! Я никогда, никогда не буду есть красную пищу!",
        "Видите лиииии, наше общество наполнено различными скрытыми заговорами, которые ближе, чем вы думаете",
        "Убедитесь, что вы тоже будете осторожны.",
    },
    [10] = {
        "Вы не представляете, как трудно быть успешным.",
        "Я так завидую людям, которые не добиваются успеха и у которых ничего нет.",
        "Самым большим препятствием для того, чтобы бросать вызов новым вещам, безусловно, являются ваши прошлые успехи.",
        "Они всегда сдерживают меня, все время.",
        "Я так завидую людям, у которых ничего нет. Люди, у которых нет никаких ожиданий, уверены, что они счастливы.",
        "Я имею в виду, что они могут делать все, что захотят, ни о чем не беспокоясь.",
        "Черт, быть успешным - это тяжело!"
    },
}

local dialogue_anim = {arrow_alpha = 0, box_alpha = 0, arrow_y = -16, box_y = -32, name = "Какой-то гений с улицы", dialogue_path = 1, dialogue_progress = 1, time = 0, pos = 0}

surface.CreateFont("animelife.citizens.dialogue.name", {font = "Exo 2 SemiBold", size = 19, weight = 500, extended = true})
surface.CreateFont("animelife.citizens.dialogue.text", {font = "Exo 2 SemiBold", size = 21, weight = 500, extended = true})

local dialogue_holder = Material("animelife/citizens/dialogue_holder.png")
local dialogue_arrow = Material("animelife/citizens/dialogue_arrow.png")

function TalkTo(npc, name, path)
    if (ValidPanel(GLOBALS_F4MENU) and GLOBALS_F4MENU:IsVisible()) or (ValidPanel(GLOBALS_SCOREBOARD) and GLOBALS_SCOREBOARD:IsVisible()) then
        return
    end

    LocalPlayer().TalkingToNPC = npc

    dialogue_anim.name = name
    dialogue_anim.dialogue_path = path or math.random(1, #GLOBALS_TALKINGNPC_DIALOGUES)
    dialogue_anim.dialogue_progress = 1
    dialogue_anim.time = CurTime() + 1

    flux.to(dialogue_anim, 0.5, {box_alpha = 1, box_y = 0})
    :after(dialogue_anim, 0.5, {arrow_alpha = 1, arrow_y = 0})

    surface.PlaySound("dialog-pop-in.wav")

    GLOBALS_CITIZENS_DIALOGUE_VGUI = vgui.Create("EditablePanel")
    GLOBALS_CITIZENS_DIALOGUE_VGUI:SetSize(ScrW(), ScrH())
    GLOBALS_CITIZENS_DIALOGUE_VGUI:MakePopup()
    GLOBALS_CITIZENS_DIALOGUE_VGUI:SetCursor("hand")
    GLOBALS_CITIZENS_DIALOGUE_VGUI.Paint = function(self, w, h)
        if dialogue_anim.box_alpha > 0 then
            surface.SetDrawColor(255, 255, 255, 255 * dialogue_anim.box_alpha)
            surface.SetMaterial(dialogue_holder)
            surface.DrawTexturedRect((w - 702) / 2, 96 + dialogue_anim.box_y, 702, 274)
    
            surface.SetDrawColor(255, 255, 255, 255 * dialogue_anim.arrow_alpha)
            surface.SetMaterial(dialogue_arrow)
            surface.DrawTexturedRect((w - 38) / 2, 386 + dialogue_anim.arrow_y + (math.sin(CurTime() * 4) * 6), 38, 34)
    
            draw.SimpleText(dialogue_anim.name, "animelife.citizens.dialogue.name", w / 2, 96 + dialogue_anim.box_y + 12, Color(255, 255, 255, 255 * dialogue_anim.box_alpha), 1)

            local text = GLOBALS_TALKINGNPC_DIALOGUES[dialogue_anim.dialogue_path][dialogue_anim.dialogue_progress]
            if CurTime() > dialogue_anim.time then
                dialogue_anim.pos = dialogue_anim.pos + 2
                dialogue_anim.time = CurTime()

                if dialogue_anim.pos < utf8.len(text) then
                    surface.PlaySound("dialogue_letter.wav")
                end
            end
    
            text = utf8.sub(text, 0, dialogue_anim.pos)
            local dialogue_text = markup.Parse("<font=animelife.citizens.dialogue.text>" .. text .. "</font>", 480)
            dialogue_text:Draw(w / 2, 96 + dialogue_anim.box_y + 166, 1, 1, 255 * dialogue_anim.box_alpha)
        end
    
        flux.update(RealFrameTime())
    end
    GLOBALS_CITIZENS_DIALOGUE_VGUI.OnKeyCodePressed = function(self, key)
        if key == KEY_SPACE then
            self.OnMousePressed()
        end
    end
    GLOBALS_CITIZENS_DIALOGUE_VGUI.OnMousePressed = function()
        dialogue_anim.dialogue_progress = (dialogue_anim.dialogue_progress or 1) + 1
        dialogue_anim.pos = 1
        dialogue_anim.time = 0

        if dialogue_anim.dialogue_progress > #GLOBALS_TALKINGNPC_DIALOGUES[dialogue_anim.dialogue_path] then
            QuitNPCTalk()
        end

        surface.PlaySound("click.wav")
    end
end

function QuitNPCTalk()
    LocalPlayer().TalkingToNPC = nil

    dialogue_anim.box_alpha = 0
    dialogue_anim.box_y = -32

    if ValidPanel(GLOBALS_CITIZENS_DIALOGUE_VGUI) then
        GLOBALS_CITIZENS_DIALOGUE_VGUI:Remove()
        GLOBALS_CITIZENS_DIALOGUE_VGUI = nil
    end
end

hook.Add("CalcView", "animelife.citizens.dialogue", function(ply, pos, ang, fov)
    if IsValid(LocalPlayer().TalkingToNPC) then
        local npc = LocalPlayer().TalkingToNPC
        local head_bone = npc:LookupBone("ValveBiped.Bip01_Head1")
        local head_pos = npc:LocalToWorld(Vector(54, 0, 78))
        if isnumber(head_bone) then
            head_pos = npc:GetBonePosition(head_bone) + npc:GetUp() * 24 + npc:GetForward() * 54
        end

        return {
            origin = head_pos,
            angles = npc:LocalToWorldAngles(Angle(15, -180, 0)),
            drawviewer = false
        }
    end
end)

hook.Add("HUDPaint", "animelife.citizens.dialogue", function()
	if !render.SupportsPixelShaders_2_0() then return end
    if !IsValid(LocalPlayer().TalkingToNPC) then return end

    local passes = 3
    local h = ScrH() * .2

    DrawToyTown(passes, h)
end)