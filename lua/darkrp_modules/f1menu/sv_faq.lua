util.AddNetworkString("animelife.f1menu")

function GM:ShowHelp(ply)
    net.Start("animelife.f1menu")
    net.Send(ply)
end

hook.Add("PlayerInitialSpawn", "animelife.f1menu.hint", function(ply)
    timer.Simple(120, function()
        DarkRP.notify(ply, 0, 7, "Не уверены в чем-то? Ознакомьтесь с ответами на общие вопросы, нажав F1")
    end)
end)

if !timer.Exists("animelife.f1menu.hint") then
    timer.Create("animelife.f1menu.hint", 3600 * 2, 0, function()
        for _, ply in ipairs(player.GetHumans()) do
            DarkRP.notify(ply, 0, 7, "Не уверены в чем-то? Ознакомьтесь с ответами на общие вопросы, нажав F1")
        end
    end)
end