local PANEL = {}
local PlayerVoicePanels = {}

surface.CreateFont("animelife.voice", {font = "Exo 2 SemiBold", size = 16, weight = 500, extended = true})

function PANEL:Init()
	self.LabelName = vgui.Create("DLabel", self)
	self.LabelName:SetFont("animelife.voice")
	self.LabelName:Dock(FILL)
	self.LabelName:DockMargin(16 + 24 + 8, 0, 16, 0)
	self.LabelName:SetTextColor(color_white)

	self.Avatar = vgui.Create("AvatarImage", self)
	self.Avatar:SetPos(16, (42 - 24) / 2)
	self.Avatar:SetSize(24, 24)

	self.Color = color_transparent

	self:SetSize(180, 42)
	self:DockPadding(4, 4, 4, 4)
	self:DockMargin(2, 2, 2, 2)
	self:Dock(BOTTOM)
end

function PANEL:Setup(ply)
	self.ply = ply
	self.LabelName:SetText(ply:Nick())
	self.Avatar:SetPlayer(ply)
	
	self.Color = team.GetColor(ply:Team())
	
	self:InvalidateLayout()
end

function PANEL:Paint(w, h)
	if !IsValid(self.ply) then return end

    draw.RoundedBox( 16, 0, 0, w, h, Color( 65, 50, 22) )

    local add = self.ply:VoiceVolume() * 16
	draw.RoundedBox( 16, 1, 1, w - 2, h - 2, Color( 31 + add, 31 + add, 31 + add, 255 ) )
end

function PANEL:Think()
	if IsValid(self.ply) then
		self.LabelName:SetText(self.ply:Nick())
	end

	if self.fadeAnim then
		self.fadeAnim:Run()
	end
end

function PANEL:FadeOut(anim, delta, data)
	if anim.Finished then
		if IsValid(PlayerVoicePanels[self.ply]) then
			PlayerVoicePanels[self.ply]:Remove()
			PlayerVoicePanels[self.ply] = nil
			return
		end
	return end
	
	self:SetAlpha(255 - (255 * delta))
end

derma.DefineControl("VoiceNotify", "", PANEL, "DPanel")

function GM:PlayerStartVoice(ply)
	if !IsValid(g_VoicePanelList) then return end
	
	-- There'd be an extra one if voice_loopback is on, so remove it.
	GAMEMODE:PlayerEndVoice(ply)

	if IsValid(PlayerVoicePanels[ply]) then
		if PlayerVoicePanels[ply].fadeAnim then
			PlayerVoicePanels[ply].fadeAnim:Stop()
			PlayerVoicePanels[ply].fadeAnim = nil
		end

		PlayerVoicePanels[ply]:SetAlpha(255)

		return
	end

	if !IsValid(ply) then return end

	local pnl = g_VoicePanelList:Add("VoiceNotify")
	pnl:Setup(ply)
	
	PlayerVoicePanels[ply] = pnl
end

local function VoiceClean()
	for k, v in pairs(PlayerVoicePanels) do
		if !IsValid(k) then
			GAMEMODE:PlayerEndVoice(k)
		end
	end
end
timer.Create("VoiceClean", 10, 0, VoiceClean)

function GM:PlayerEndVoice(ply)
	if IsValid(PlayerVoicePanels[ply]) then
		if PlayerVoicePanels[ply].fadeAnim then return end

		PlayerVoicePanels[ply].fadeAnim = Derma_Anim("FadeOut", PlayerVoicePanels[ply], PlayerVoicePanels[ply].FadeOut)
		PlayerVoicePanels[ply].fadeAnim:Start(2)
	end
end

local function CreateVoiceVGUI()
    if g_VoicePanelList then
        g_VoicePanelList:Remove()
        g_VoicePanelList = nil
    end

	g_VoicePanelList = vgui.Create( "DPanel" )

	g_VoicePanelList:ParentToHUD()
	g_VoicePanelList:SetPos( ScrW() - 200, 100 )
	g_VoicePanelList:SetSize( 180, ScrH() - 200 )
	g_VoicePanelList:SetPaintBackground( false )
end
hook.Add("InitPostEntity", "CreateVoiceVGUI", CreateVoiceVGUI)