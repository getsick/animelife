local meta = FindMetaTable("Player")

function meta:SetVIP(bool, duration)
    self:SetNWBool("animelife.memberplus", bool)

    if donation and donation.SaveVIPState then
        donation.SaveVIPState(self, !bool, duration)
    end
end

hook.Add("playerGetSalary", "animelife.memberplus", function(ply, amount)
    if ply:IsVIP() then
        -- player gets twice (almost) the cake
        local new = amount + math.ceil(amount - (amount * 0.45))
        return false, DarkRP.getPhrase("payday_message", "¥" .. new), new
    end
end)

-- VIP Saving
MySQLite.begin()

MySQLite.query([[
    CREATE TABLE IF NOT EXISTS al_donation_vip(
        sid TINYTEXT NOT NULL PRIMARY KEY,
        date MEDIUMINT NOT NULL,
        duration TINYINT NOT NULL
    );
]])

module("donation", package.seeall)

function SaveVIPState(ply, rm, duration)
    -- it was fun while it lasted
    -- taking away all the priviliges
    if rm then
        MySQLite.query([[DELETE FROM al_donation_vip WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")

        return
    end

    if !duration then
        duration = 0 -- forever
    end

    MySQLite.query([[SELECT date
    FROM al_donation_vip
    where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
        if !data then
            MySQLite.query([[REPLACE INTO al_donation_vip VALUES(]] ..
            MySQLite.SQLStr(ply:SteamID()) .. [[, ]] ..
            tonumber(os.time()) .. [[, ]] ..
            duration .. ");")
        end

        MySQLite.query([[UPDATE al_donation SET date = ]] .. tonumber(os.time()) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
        MySQLite.query([[UPDATE al_donation SET duration = ]] .. tonumber(duration) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
    end, function(e, q) 
        print("Database Error:", e, q)
    end)
end

function RestoreVIPState(ply)
    MySQLite.query([[
        SELECT date
        FROM al_donation_vip
        where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
            if !data then return end

            ply:SetNWBool("animelife.memberplus", true)
        end, function(e, q)
            print("Database Error:", e, q)
        end)
end

hook.Add("PlayerInitialSpawn", "animelife.donation.viprestore", function(ply)
    timer.Simple(4, function()
        if !IsValid(ply) or !ply:IsPlayer() then return end

        RestoreVIPState(ply)
    end)
end)

if !timer.Exists("animelife.donation.vipchecker") then
    timer.Create("animelife.donation.vipchecker", 5, 0, function()
        MySQLite.query([[
            SELECT *
            FROM al_donation_vip;]], function(data)
                if !data then return end

                for _, r in pairs(data) do
                    local duration = tonumber(r.duration)
                    if duration == 0 then continue end

                    local date = tonumber(r.date)

                    -- it's over boys
                    if (date + (duration * 60 * 1440)) < os.time() then
                        local sid = r.sid
                        local ply = player.GetBySteamID(sid)
                        if IsValid(ply) and ply:IsPlayer() then
                            ply:SetVIP(false)
                        else
                            -- player offline, delete it anyway
                            MySQLite.query([[DELETE FROM al_donation_vip WHERE sid = ]] .. MySQLite.SQLStr(r.sid) .. ";")
                        end
                    end
                end
            end, function(e, q)
                print("Database Error:", e, q)
            end)
    end)
end