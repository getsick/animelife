local meta = FindMetaTable("Player")

function meta:IsVIP()
    return self:GetNWBool("animelife.memberplus", false)
end