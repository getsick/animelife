local meta = FindMetaTable("Player")

function meta:GetDonationPoints()
    if self.IGSFunds then
        return self:IGSFunds() or 0
    end

    return self:GetNWInt("animelife.donation.points", 0)
end