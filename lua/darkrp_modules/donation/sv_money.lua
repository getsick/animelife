local meta = FindMetaTable("Player")

function meta:SetDonationPoints(pts)
    self:SetNWInt("animelife.donation.points", pts)

    if donation then
        donation.SaveMoney(self)
    end
end