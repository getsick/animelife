hook.Add("IGS.PaymentStatusUpdated", "animelife.donation.igsupdate", function(ply, data)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    
    -- update our local database values, we use them for leaderboards
    ply:SetDonationPoints(ply:GetDonationPoints())
end)

-- never asked for this, kinda sucks that I have to remove this feature this way
hook.Add("Initialize", "animelife.donation.igsmoment", function()
    if IGS then
        function IGS.Notify(ply, ...)
        end
    end
end)