module("donation", package.seeall)

Levels = {
    [1] = {
        Min = 500,
    },
    [2] = {
        Min = 1000,
    },
    [3] = {
        Min = 2500,
    },
    [4] = {
        Min = 5000,
    },
    [5] = {
        Min = 10000,
    },
}

Supporters = {
    "STEAM_0:0:78203148",
}

function IsSupporter(sid)
    return table.HasValue(Supporters, sid)
end

function GetLevel(ply)
    local donated = 0
    for pos, lv in pairs(Levels) do
        -- TODO: fix out of bounds array
        if donated >= lv.Min and donated < Levels[pos + 1].Min then
            return pos
        end
    end

    return -1
end