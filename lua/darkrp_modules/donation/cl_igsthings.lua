local empty_function = function() end
function RemoveIGSUI()
    -- remove default gmdonate ui
    if IGS and IGS.UI then -- promise me you won't be buggin me in singleplayer
        if IGS.UI ~= empty_function then
            IGS.UI = empty_function
        end
    end
end

hook.Add("OnContextMenuOpen", "animelife.donation.igsui", function()
    -- damn, this should be an option!!
    for k, v in pairs(g_ContextMenu:GetChildren()) do
        if v:GetClassName() == "Panel" and v:GetName() == "DIconLayout" then
            for _, icon in pairs(v:GetChildren()) do
                for _, btn in pairs(icon:GetChildren()) do
                    if btn:GetName() == "DImage" and btn:GetImage() == "icon16/money_add.png" then
                        icon:Remove()
                        break
                    end
                end
            end
            break
        end
    end
end)