MySQLite.begin()

MySQLite.query([[
    CREATE TABLE IF NOT EXISTS al_donation(
        sid TINYTEXT NOT NULL PRIMARY KEY,
        rpname TINYTEXT NOT NULL,
        points INTEGER NOT NULL
    );
]])

module("donation", package.seeall)

function SaveMoney(ply)
    MySQLite.query([[SELECT points
    FROM al_donation
    where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
        if !data then
            MySQLite.query([[REPLACE INTO al_donation VALUES(]] ..
            MySQLite.SQLStr(ply:SteamID()) .. [[, ]] ..
            MySQLite.SQLStr(ply:Nick()) .. [[, ]] ..
            0 .. ");")
        end

        MySQLite.query([[UPDATE al_donation SET rpname = ]] .. MySQLite.SQLStr(ply:Nick()) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
        MySQLite.query([[UPDATE al_donation SET points = ]] .. tonumber(ply:GetDonationPoints()) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
    end, function(e, q) 
        print("Database Error:", e, q)
    end)
end

function RestoreMoney(ply)
    MySQLite.query([[
        SELECT points
        FROM al_donation
        where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
            if !data then return end

            if data[1].points then
                ply:SetNWInt("animelife.donation.points", tonumber(data[1].points))
            end
        end, function(e, q)
            print("Database Error:", e, q)
        end)
end

hook.Add("PlayerInitialSpawn", "animelife.donation.db", function(ply)
    timer.Simple(3, function()
        if !IsValid(ply) or !ply:IsPlayer() then return end

        RestoreMoney(ply)
    end)
end)