GLOBALS_LEADERBOARDS = GLOBALS_LEADERBOARDS or {}

net.Receive("animelife.leaderboards.update", function()
    local t = net.ReadString()
    local len = net.ReadInt(32)
    local data = net.ReadData(len)
    data = util.Decompress(data)
    data = util.JSONToTable(data)

    GLOBALS_LEADERBOARDS[t] = data
end)