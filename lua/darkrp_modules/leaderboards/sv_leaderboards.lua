util.AddNetworkString("animelife.leaderboards.update")

module("leaderboards", package.seeall)

function leaderboards:GetSortedByMoney(rows, fb)
    rows = rows or 10

    local players = MySQLite.query([[
        SELECT * FROM darkrp_player
        WHERE LENGTH(uid) > 15
        ORDER BY wallet DESC
        LIMIT ]] .. rows, function(d)
            fb(d)
    end, function(e, q)
        print(e, q)
    end)

    return {}
end

function leaderboards:GetSortedByLevel(rows, fb)
    rows = rows or 10

    local players = MySQLite.query([[
        SELECT sid, rpname, level FROM al_levelee
        WHERE LENGTH(sid) > 8
        ORDER BY level DESC
        LIMIT ]] .. rows, function(d)
            fb(d)
    end, function(e, q)
        print(e, q)
    end)

    return {}
end

function leaderboards:GetSortedByDonate(rows, fb)
    rows = rows or 10

    local players = MySQLite.query([[
        SELECT sid, rpname, points FROM al_donation
        WHERE LENGTH(sid) > 8
        ORDER BY points DESC
        LIMIT ]] .. rows, function(d)
            fb(d)
    end, function(e, q)
        print(e, q)
    end)

    return {}
end

function leaderboards:GetSortedByPVPWins(rows, fb)
    rows = rows or 10

    local players = MySQLite.query([[
        SELECT sid, rpname, wins FROM al_pvpbattle
        WHERE LENGTH(sid) > 8
        ORDER BY wins DESC
        LIMIT ]] .. rows, function(d)
            fb(d)
    end, function(e, q)
        print(e, q)
    end)

    return {}
end

local function leaderboard_update(ply, t)
    if t == "money" then
        local wallet_lb = leaderboards:GetSortedByMoney(10, function(data)
            data = util.TableToJSON(data)
            data = util.Compress(data)

            local len = data:len()
            net.Start("animelife.leaderboards.update")
                net.WriteString("wallet")
                net.WriteInt(len, 32)
                net.WriteData(data, len)
            net.Send(ply)
        end)
    elseif t == "level" then
        local level_lb = leaderboards:GetSortedByLevel(10, function(data)
            data = data or {}
            data = util.TableToJSON(data)
            data = util.Compress(data)

            local len = data:len()
            net.Start("animelife.leaderboards.update")
                net.WriteString("level")
                net.WriteInt(len, 32)
                net.WriteData(data, len)
            net.Send(ply)
        end)
    elseif t == "donate" then
        local donate_lb = leaderboards:GetSortedByDonate(10, function(data)
            data = data or {}
            data = util.TableToJSON(data)
            data = util.Compress(data)

            local len = data:len()
            net.Start("animelife.leaderboards.update")
                net.WriteString("donate")
                net.WriteInt(len, 32)
                net.WriteData(data, len)
            net.Send(ply)
        end)
    elseif t == "pvpbattle" then
        local report_lb = leaderboards:GetSortedByPVPWins(10, function(data)
            data = data or {}
            data = util.TableToJSON(data)
            data = util.Compress(data)

            local len = data:len()
            net.Start("animelife.leaderboards.update")
                net.WriteString("pvpbattle")
                net.WriteInt(len, 32)
                net.WriteData(data, len)
            net.Send(ply)
        end)
    end
end

if !timer.Exists("animelife.leaderboards.update") then
    timer.Create("animelife.leaderboards.update", 60, 0, function()
        leaderboard_update(player.GetAll(), "money")
        leaderboard_update(player.GetAll(), "level")
        leaderboard_update(player.GetAll(), "donate")
        leaderboard_update(player.GetAll(), "pvpbattle")
    end)
end

hook.Add("PlayerInitialSpawn", "animelife.leaderboards.update", function(ply)
    timer.Simple(20, function()
        if !IsValid(ply) or !ply:IsPlayer() then return end

        leaderboard_update(ply, "money")
        leaderboard_update(ply, "level")
        leaderboard_update(ply, "donate")
        leaderboard_update(ply, "pvpbattle")
    end)
end)