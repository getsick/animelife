util.AddNetworkString("animelife.groups.invitation")
util.AddNetworkString("animelife.groups.accept")
util.AddNetworkString("animelife.groups.makeinvite")

module("groups", package.seeall)

function Invite(ply, send_to)
    if table.HasValue(send_to, ply:SteamID()) then
        table.RemoveByValue(send_to, ply:SteamID())
    end

    -- ply:SetNWString("animelife.groups.invitations", util.TableToJSON(send_to)) -- why did I even use networking for this in the first place?
    ply.AnimeLifeGroupInvitations = send_to

    -- net.Start("animelife.groups.invitation")
    --     net.WriteEntity(ply)
    -- net.Send(send_to)

    for _, receiver in pairs(player.GetHumans()) do
        if table.HasValue(send_to, receiver:SteamID()) then
            receiver:ChatPrint(ply:GetName() .. " приглашает вас в группу. Введите /partyaccept, чтобы принять предложение.")
            receiver.LastGroupInvitor = ply
        end
    end
end

function Accept(ply, invitor)
    local inv_list = invitor.AnimeLifeGroupInvitations
    if !inv_list or table.IsEmpty(inv_list) then return end

    if !table.HasValue(inv_list, ply:IsBot() and "NULL" or ply:SteamID()) then
        return
    end

    local inv_token = string.Replace(invitor:SteamID(), "STEAM_", "GROUP_")
    if inv_token ~= invitor:GetNWString("animelife.group") then
        invitor:SetNWString("animelife.group", inv_token)
    end

    ply:SetNWString("animelife.group", invitor:GetNWString("animelife.group"))

    -- print("GROUPS:", ply, "accepted invitation from", invitor)
    -- print("GROUPS:", ply, "inv token:", ply:GetNWString("animelife.group"))
    -- print("GROUPS:", invitor, "inv token:", inv_token)
end

local function count_players_in_group(group)
    local c = 0
    for _, ply in ipairs(player.GetAll()) do
        if ply:GetNWString("animelife.group") == group then
            c = c + 1
        end
    end

    return c
end

function Leave(ply)
    ply:SetNWString("animelife.group", "-fds-fsd-f-dsf-sd-fds-fds-f-dsf-s-df")
    ply.AnimeLifeGroupInvitations = nil
end

net.Receive("animelife.groups.accept", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local invitor = net.ReadEntity()
    if !IsValid(invitor) then return end

    Accept(ply, invitor)
end)

net.Receive("animelife.groups.makeinvite", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local send_to = net.ReadString()
    send_to = util.JSONToTable(send_to)
    Invite(ply, send_to)
end)

hook.Add("PlayerSay", "animelife.groups.accept", function(ply, msg)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    if msg == "/partyaccept" or msg == "!partyaccept" then
        Accept(ply, ply.LastGroupInvitor)

        return ""
    elseif msg == "/partyleave" or msg == "!partyleave" then
        Leave(ply)
        ply:ChatPrint("Вы вышли из группы.")

        return ""
    end
end)

hook.Add("EntityTakeDamage", "animelife.groups.damage", function(target, dmg)
    if IsValid(target) and target:IsPlayer() then
    local attacker = dmg:GetAttacker()
        if IsValid(attacker) and attacker:IsPlayer() then
            if attacker:GetNWString("animelife.group") == "-fds-fsd-f-dsf-sd-fds-fds-f-dsf-s-df" then return end
            if attacker:GetNWString("animelife.group", "invalidgroupname") == target:GetNWString("animelife.group") then
                return true
            end
        end
    end
end)