local PANEL = {}
local PANEL_BG_COLOR = Color(39, 29, 33)
local PANEL_MATERIAL_BACKGROUND = Material("animelife/levelee/remake/header.png")
local PANEL_MATERIAL_LOGO = Material("animelife/levelee/remake/logo.png")
local PANEL_MATERIAL_BOOKS = Material("animelife/levelee/remake/books.png")
local PANEL_MATERIAL_BUTTON = Material("animelife/levelee/remake/button.png")
local PANEL_MATERIAL_CLOSE = Material("animelife/levelee/remake/icon_close.png")

surface.CreateFont("animelife.levelee.xpexchange.med", {font = "Exo 2 SemiBold", size = 21, weight = 600, extended = true})
surface.CreateFont("animelife.levelee.xpexchange.big", {font = "Exo 2 Bold", size = 32, weight = 700, extended = true})
surface.CreateFont("animelife.levelee.xpexchange.sml", {font = "Exo 2 SemiBold", size = 16, weight = 600, extended = true})

local flux = util.Base64Decode("bG9jYWwgZmx1eCA9IHsgX3ZlcnNpb24gPSAiMC4xLjUiIH0KZmx1eC5fX2luZGV4ID0gZmx1eAoKZmx1eC50d2VlbnMgPSB7fQpmbHV4LmVhc2luZyA9IHsgbGluZWFyID0gZnVuY3Rpb24ocCkgcmV0dXJuIHAgZW5kIH0KCmxvY2FsIGVhc2luZyA9IHsKICBxdWFkICAgID0gInAgKiBwIiwKICBjdWJpYyAgID0gInAgKiBwICogcCIsCiAgcXVhcnQgICA9ICJwICogcCAqIHAgKiBwIiwKICBxdWludCAgID0gInAgKiBwICogcCAqIHAgKiBwIiwKICBleHBvICAgID0gIjIgXiAoMTAgKiAocCAtIDEpKSIsCiAgc2luZSAgICA9ICItbWF0aC5jb3MocCAqIChtYXRoLnBpICogLjUpKSArIDEiLAogIGNpcmMgICAgPSAiLShtYXRoLnNxcnQoMSAtIChwICogcCkpIC0gMSkiLAogIGJhY2sgICAgPSAicCAqIHAgKiAoMi43ICogcCAtIDEuNykiLAogIGVsYXN0aWMgPSAiLSgyXigxMCAqIChwIC0gMSkpICogbWF0aC5zaW4oKHAgLSAxLjA3NSkgKiAobWF0aC5waSAqIDIpIC8gLjMpKSIKfQoKbG9jYWwgbWFrZWZ1bmMgPSBmdW5jdGlvbihzdHIsIGV4cHIpCiAgcmV0dXJuIENvbXBpbGVTdHJpbmcoInJldHVybiBmdW5jdGlvbihwKSAiIC4uIHN0cjpnc3ViKCIlJGUiLCBleHByKSAuLiAiIGVuZCIsICJUZXN0RnVuY3Rpb24iKSgpCmVuZAoKZm9yIGssIHYgaW4gcGFpcnMoZWFzaW5nKSBkbwogIGZsdXguZWFzaW5nW2sgLi4gImluIl0gPSBtYWtlZnVuYygicmV0dXJuICRlIiwgdikKICBmbHV4LmVhc2luZ1trIC4uICJvdXQiXSA9IG1ha2VmdW5jKFtbCiAgICBwID0gMSAtIHAKICAgIHJldHVybiAxIC0gKCRlKQogIF1dLCB2KQogIGZsdXguZWFzaW5nW2sgLi4gImlub3V0Il0gPSBtYWtlZnVuYyhbWwogICAgcCA9IHAgKiAyCiAgICBpZiBwIDwgMSB0aGVuCiAgICAgIHJldHVybiAuNSAqICgkZSkKICAgIGVsc2UKICAgICAgcCA9IDIgLSBwCiAgICAgIHJldHVybiAuNSAqICgxIC0gKCRlKSkgKyAuNQogICAgZW5kCiAgXV0sIHYpCmVuZAoKCgpsb2NhbCB0d2VlbiA9IHt9CnR3ZWVuLl9faW5kZXggPSB0d2VlbgoKbG9jYWwgZnVuY3Rpb24gbWFrZWZzZXR0ZXIoZmllbGQpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBsb2NhbCBtdCA9IGdldG1ldGF0YWJsZSh4KQogICAgaWYgdHlwZSh4KSB+PSAiZnVuY3Rpb24iIGFuZCBub3QgKG10IGFuZCBtdC5fX2NhbGwpIHRoZW4KICAgICAgZXJyb3IoImV4cGVjdGVkIGZ1bmN0aW9uIG9yIGNhbGxhYmxlIiwgMikKICAgIGVuZAogICAgbG9jYWwgb2xkID0gc2VsZltmaWVsZF0KICAgIHNlbGZbZmllbGRdID0gb2xkIGFuZCBmdW5jdGlvbigpIG9sZCgpIHgoKSBlbmQgb3IgeAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgpsb2NhbCBmdW5jdGlvbiBtYWtlc2V0dGVyKGZpZWxkLCBjaGVja2ZuLCBlcnJtc2cpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBpZiBjaGVja2ZuIGFuZCBub3QgY2hlY2tmbih4KSB0aGVuCiAgICAgIGVycm9yKGVycm1zZzpnc3ViKCIlJHgiLCB0b3N0cmluZyh4KSksIDIpCiAgICBlbmQKICAgIHNlbGZbZmllbGRdID0geAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgp0d2Vlbi5lYXNlICA9IG1ha2VzZXR0ZXIoIl9lYXNlIiwKICAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uKHgpIHJldHVybiBmbHV4LmVhc2luZ1t4XSBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGVhc2luZyB0eXBlICckeCciKQp0d2Vlbi5kZWxheSA9IG1ha2VzZXR0ZXIoIl9kZWxheSIsCiAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbih4KSByZXR1cm4gdHlwZSh4KSA9PSAibnVtYmVyIiBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGRlbGF5IHRpbWU7IGV4cGVjdGVkIG51bWJlciIpCnR3ZWVuLm9uc3RhcnQgICAgID0gbWFrZWZzZXR0ZXIoIl9vbnN0YXJ0IikKdHdlZW4ub251cGRhdGUgICAgPSBtYWtlZnNldHRlcigiX29udXBkYXRlIikKdHdlZW4ub25jb21wbGV0ZSAgPSBtYWtlZnNldHRlcigiX29uY29tcGxldGUiKQoKCmZ1bmN0aW9uIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpCiAgbG9jYWwgc2VsZiA9IHNldG1ldGF0YWJsZSh7fSwgdHdlZW4pCiAgc2VsZi5vYmogPSBvYmoKICBzZWxmLnJhdGUgPSB0aW1lID4gMCBhbmQgMSAvIHRpbWUgb3IgMAogIHNlbGYucHJvZ3Jlc3MgPSB0aW1lID4gMCBhbmQgMCBvciAxCiAgc2VsZi5fZGVsYXkgPSAwCiAgc2VsZi5fZWFzZSA9ICJxdWFkb3V0IgogIHNlbGYudmFycyA9IHt9CiAgZm9yIGssIHYgaW4gcGFpcnModmFycykgZG8KICAgIGlmIHR5cGUodikgfj0gIm51bWJlciIgdGhlbgogICAgICBlcnJvcigiYmFkIHZhbHVlIGZvciBrZXkgJyIgLi4gayAuLiAiJzsgZXhwZWN0ZWQgbnVtYmVyIikKICAgIGVuZAogICAgc2VsZi52YXJzW2tdID0gdgogIGVuZAogIHJldHVybiBzZWxmCmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOmluaXQoKQogIGZvciBrLCB2IGluIHBhaXJzKHNlbGYudmFycykgZG8KICAgIGxvY2FsIHggPSBzZWxmLm9ialtrXQogICAgaWYgdHlwZSh4KSB+PSAibnVtYmVyIiB0aGVuCiAgICAgIGVycm9yKCJiYWQgdmFsdWUgb24gb2JqZWN0IGtleSAnIiAuLiBrIC4uICInOyBleHBlY3RlZCBudW1iZXIiKQogICAgZW5kCiAgICBzZWxmLnZhcnNba10gPSB7IHN0YXJ0ID0geCwgZGlmZiA9IHYgLSB4IH0KICBlbmQKICBzZWxmLmluaXRlZCA9IHRydWUKZW5kCgoKZnVuY3Rpb24gdHdlZW46YWZ0ZXIoLi4uKQogIGxvY2FsIHQKICBpZiBzZWxlY3QoIiMiLCAuLi4pID09IDIgdGhlbgogICAgdCA9IHR3ZWVuLm5ldyhzZWxmLm9iaiwgLi4uKQogIGVsc2UKICAgIHQgPSB0d2Vlbi5uZXcoLi4uKQogIGVuZAogIHQucGFyZW50ID0gc2VsZi5wYXJlbnQKICBzZWxmOm9uY29tcGxldGUoZnVuY3Rpb24oKSBmbHV4LmFkZChzZWxmLnBhcmVudCwgdCkgZW5kKQogIHJldHVybiB0CmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOnN0b3AoKQogIGZsdXgucmVtb3ZlKHNlbGYucGFyZW50LCBzZWxmKQplbmQKCgoKZnVuY3Rpb24gZmx1eC5ncm91cCgpCiAgcmV0dXJuIHNldG1ldGF0YWJsZSh7fSwgZmx1eCkKZW5kCgoKZnVuY3Rpb24gZmx1eDp0byhvYmosIHRpbWUsIHZhcnMpCiAgcmV0dXJuIGZsdXguYWRkKHNlbGYsIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpKQplbmQKCgpmdW5jdGlvbiBmbHV4OnVwZGF0ZShkZWx0YXRpbWUpCiAgZm9yIGkgPSAjc2VsZiwgMSwgLTEgZG8KICAgIGxvY2FsIHQgPSBzZWxmW2ldCiAgICBpZiB0Ll9kZWxheSA+IDAgdGhlbgogICAgICB0Ll9kZWxheSA9IHQuX2RlbGF5IC0gZGVsdGF0aW1lCiAgICBlbHNlCiAgICAgIGlmIG5vdCB0LmluaXRlZCB0aGVuCiAgICAgICAgZmx1eC5jbGVhcihzZWxmLCB0Lm9iaiwgdC52YXJzKQogICAgICAgIHQ6aW5pdCgpCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnN0YXJ0IHRoZW4KICAgICAgICB0Ll9vbnN0YXJ0KCkKICAgICAgICB0Ll9vbnN0YXJ0ID0gbmlsCiAgICAgIGVuZAogICAgICB0LnByb2dyZXNzID0gdC5wcm9ncmVzcyArIHQucmF0ZSAqIGRlbHRhdGltZQogICAgICBsb2NhbCBwID0gdC5wcm9ncmVzcwogICAgICBsb2NhbCB4ID0gcCA+PSAxIGFuZCAxIG9yIGZsdXguZWFzaW5nW3QuX2Vhc2VdKHApCiAgICAgIGZvciBrLCB2IGluIHBhaXJzKHQudmFycykgZG8KICAgICAgICB0Lm9ialtrXSA9IHYuc3RhcnQgKyB4ICogdi5kaWZmCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnVwZGF0ZSB0aGVuIHQuX29udXBkYXRlKCkgZW5kCiAgICAgIGlmIHAgPj0gMSB0aGVuCiAgICAgICAgZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgICAgICBpZiB0Ll9vbmNvbXBsZXRlIHRoZW4gdC5fb25jb21wbGV0ZSgpIGVuZAogICAgICBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmNsZWFyKG9iaiwgdmFycykKICBmb3IgdCBpbiBwYWlycyhzZWxmW29ial0pIGRvCiAgICBpZiB0LmluaXRlZCB0aGVuCiAgICAgIGZvciBrIGluIHBhaXJzKHZhcnMpIGRvIHQudmFyc1trXSA9IG5pbCBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmFkZCh0d2VlbikKICAtLSBBZGQgdG8gb2JqZWN0IHRhYmxlLCBjcmVhdGUgdGFibGUgaWYgaXQgZG9lcyBub3QgZXhpc3QKICBsb2NhbCBvYmogPSB0d2Vlbi5vYmoKICBzZWxmW29ial0gPSBzZWxmW29ial0gb3Ige30KICBzZWxmW29ial1bdHdlZW5dID0gdHJ1ZQogIC0tIEFkZCB0byBhcnJheQogIHRhYmxlLmluc2VydChzZWxmLCB0d2VlbikKICB0d2Vlbi5wYXJlbnQgPSBzZWxmCiAgcmV0dXJuIHR3ZWVuCmVuZAoKCmZ1bmN0aW9uIGZsdXg6cmVtb3ZlKHgpCiAgaWYgdHlwZSh4KSA9PSAibnVtYmVyIiB0aGVuCiAgICAtLSBSZW1vdmUgZnJvbSBvYmplY3QgdGFibGUsIGRlc3Ryb3kgdGFibGUgaWYgaXQgaXMgZW1wdHkKICAgIGxvY2FsIG9iaiA9IHNlbGZbeF0ub2JqCiAgICBzZWxmW29ial1bc2VsZlt4XV0gPSBuaWwKICAgIGlmIG5vdCBuZXh0KHNlbGZbb2JqXSkgdGhlbiBzZWxmW29ial0gPSBuaWwgZW5kCiAgICAtLSBSZW1vdmUgZnJvbSBhcnJheQogICAgc2VsZlt4XSA9IHNlbGZbI3NlbGZdCiAgICByZXR1cm4gdGFibGUucmVtb3ZlKHNlbGYpCiAgZW5kCiAgZm9yIGksIHYgaW4gaXBhaXJzKHNlbGYpIGRvCiAgICBpZiB2ID09IHggdGhlbgogICAgICByZXR1cm4gZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgIGVuZAogIGVuZAplbmQKCgoKbG9jYWwgYm91bmQgPSB7CiAgdG8gICAgICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgudG8oZmx1eC50d2VlbnMsIC4uLikgZW5kLAogIHVwZGF0ZSAgPSBmdW5jdGlvbiguLi4pIHJldHVybiBmbHV4LnVwZGF0ZShmbHV4LnR3ZWVucywgLi4uKSBlbmQsCiAgcmVtb3ZlICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgucmVtb3ZlKGZsdXgudHdlZW5zLCAuLi4pIGVuZCwKfQpzZXRtZXRhdGFibGUoYm91bmQsIGZsdXgpCgpyZXR1cm4gYm91bmQ=")
flux = CompileString(flux, "flux")()

function PANEL:Init()
    self.Paint = self.PaintMainSection

    self.PointsColor = Color(224, 255, 184)

    self.AscendButton = vgui.Create("DButton", self)
    self.AscendButton:SetText("")
    self.AscendButton.Paint = self.PaintAscendButton
    self.AscendButton.DoClick = function()
        net.Start("animelife.levelee.transfer")
        net.SendToServer()

        flux.to(self.PointsColor, 0.15, {r = 255, g = 222, b = 184})
        :after(self.PointsColor, 1, {r = 224, g = 255, b = 184})
        :delay(0.15)

        surface.PlaySound("click02.wav")
    end

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("")
    self.CloseButton.Paint = self.PaintCloseButton
    self.CloseButton.DoClick = function()
        if ValidPanel(self) then
            self:Remove()
        end
    end

    self.NavBarButtons = {}
    self.CurrentStore = 1

    for i, v in pairs({[1] = "Оружие", [2] = "Скины", [3] = "Аксессуары"}) do
        local btn = vgui.Create("DButton", self)
        btn:SetText("")
        btn.Paint = function(pnl, w, h)
            draw.SimpleText(v, "animelife.levelee.xpexchange.sml", w / 2, h / 2, self.CurrentStore == i and Color(255, 255, 255) or Color(219, 219, 219), 1, 1)
        end
        btn.DoClick = function()
            self.CurrentStore = i
            self:GenerateItems(i)
        end

        table.insert(self.NavBarButtons, btn)
    end

    self.Scroll = vgui.Create("DScrollPanel", self)
    self:PaintScrollbar()
    self.Layout = vgui.Create("DIconLayout", self.Scroll)
    self.Layout:SetBorder(32)
    self.Layout:SetSpaceX(16)
    self.Layout:SetSpaceY(24)

    self.Layout.Items = {}
    self:GenerateItems(self.CurrentStore)
end

function PANEL:PerformLayout(w, h)
    self.AscendButton:SetPos((w - 151) / 2, 344)
    self.AscendButton:SetSize(151, 41)

    self.CloseButton:SetPos(w - 24 - 46, 40)
    self.CloseButton:SetSize(24, 24)

    -- position buttons
    local by = 533
    for _, btn in pairs(self.NavBarButtons) do
        btn:SetSize(275, 49)
        btn:SetPos(0, by)

        by = by + 49
    end

    self.Scroll:SetPos(275, 435)
    self.Scroll:SetSize(w - 275, h - 435)

    self.Layout:Dock(FILL)

    for _, pnl in pairs(self.Layout.Items) do
        pnl:SetSize(250, 188)
    end
end

function PANEL:GenerateItems(store_id)
    -- remove old ones
    for _, pnl in pairs(self.Layout.Items) do
        if ValidPanel(pnl) then
            pnl:Remove()
        end
    end
    self.Layout.Items = {}

    -- create new ones
    for _, season in pairs(levelee.SeasonalStore or {}) do
        if !season.Enable then continue end
        for _, item in pairs(season.Items[store_id] or {}) do
            local pnl = vgui.Create("animelife.globalstore.mini", self.Layout)
            pnl:SetSize(250, 188)
            pnl:SetStoreItem(item)
            pnl:SetStoreCategory(store_id)
            table.insert(self.Layout.Items, pnl)
        end
    end
end

function PANEL:PaintScrollbar()
    local vertical_bar = self.Scroll:GetVBar()
    vertical_bar:SetWide(6)

    vertical_bar.Paint = function() end
    vertical_bar.btnGrip.Paint = function(pnl, w, h)
        draw.RoundedBox(6, 0, 0, w, h, Color(0, 0, 0, 150))
    end
    vertical_bar.btnUp.Paint = function() end
    vertical_bar.btnDown.Paint = function() end
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(PANEL_BG_COLOR)
    surface.DrawRect(0, 0, w, h)
end

function PANEL:PaintMainSection(w, h)
    ui.Smooth(true, true)
        ui.DrawMaterial(PANEL_MATERIAL_BACKGROUND, 0, 0, w, 435, Color(255, 255, 255))
        ui.DrawMaterial(PANEL_MATERIAL_LOGO, (w - 358) / 2, 49, 358, 119, Color(255, 255, 255))

        local books_y = math.sin(CurTime() * 3) * 5
        local mx, my = gui.MouseX() / h * 50, gui.MouseY() / w * 50
        local ex, ey = self:LocalToScreen(w, 432)
        render.SetScissorRect(0, 0, ex, ey, true)
            ui.DrawMaterial(PANEL_MATERIAL_BOOKS, mx, my + books_y, w, 456, Color(255, 255, 255, 25))
        render.SetScissorRect(0, 0, 0, 0, false)
    ui.Smooth(false, true)

    draw.SimpleText("Накоплено опыта:", "animelife.levelee.xpexchange.med", w / 2, 211, Color(255, 255, 255), 1)
    local xp = LocalPlayer():GetTreeXP() or 0
    draw.SimpleText(string.Comma(xp), "animelife.levelee.xpexchange.big", w / 2, 211 + 21, self.PointsColor, 1)

    local lvl = LocalPlayer():GetTreeLevel() or 1
    draw.SimpleText("Ваш уровень: " .. lvl, "animelife.levelee.xpexchange.med", w / 2, 211 + 21 + 42, Color(255, 255, 255), 1)

    local wanted_xp = levelee.ascend_formula(lvl + 1)
    local text = "Обменять ваши " .. string.Comma(wanted_xp) .. " опыта и перейти на следующий уровень?"
    if xp < wanted_xp then
        text = "Требуется " .. string.Comma(wanted_xp) .. " опыта для перехода на следующий уровень."
    end
    draw.SimpleText(text, "animelife.levelee.xpexchange.sml", w / 2, 211 + 21 + 76, Color(255, 255, 255), 1)

    surface.SetDrawColor(39, 29, 33)
    surface.DrawRect(0, 435, w, h - 435)

    surface.SetDrawColor(0, 0, 0, 150)
    surface.DrawRect(0, 435, 275, h - 435)

    draw.SimpleText("Сезонный магазин", "animelife.levelee.xpexchange.big", 275 / 2, 476, Color(255, 255, 255), 1)

    flux.update(RealFrameTime())
end

function PANEL:PaintAscendButton(w, h)
    ui.DrawMaterial(PANEL_MATERIAL_BUTTON, 0, 0, w, h, Color(255, 255, 255))

    draw.SimpleText("влить в уровень", "animelife.levelee.xpexchange.sml", w / 2 + 1, h / 2 - 2, Color(0, 0, 0, 75), 1, 1)
    draw.SimpleText("влить в уровень", "animelife.levelee.xpexchange.sml", w / 2, h / 2 - 3, Color(255, 255, 255), 1, 1)
end

function PANEL:PaintCloseButton(w, h)
    ui.DrawMaterial(PANEL_MATERIAL_CLOSE, 0, 0, w, h, Color(255, 255, 255))
end

vgui.Register("animelife.levelee.gui", PANEL, "EditablePanel")

function OpenLeveleeGUI()
    GLOBALS_LEVELEE_GUI = vgui.Create("animelife.levelee.gui")
    GLOBALS_LEVELEE_GUI:SetSize(ScrW(), ScrH())
    GLOBALS_LEVELEE_GUI:MakePopup()
    GLOBALS_LEVELEE_GUI:SetAlpha(0)
    GLOBALS_LEVELEE_GUI:AlphaTo(255, 0.15)
end