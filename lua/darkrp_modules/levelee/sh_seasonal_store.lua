module("levelee", package.seeall)

SeasonalStore = {
    ["winter 2k21-2k22"] = {
        Enable = true,
        Items = {
            [1] = {"weapon_partypopper", "weapon_pumpkinslayer", "weapon_tomahawk"},
            [2] = {"playermodel_yowane", "playermodel_mikuchristmas"},
            [3] = {"accessories_santahat", "accessories_winterholiday", "accessories_snowman", "accessories_holidaygift", "accessories_icecube"},
        }
    },
    ["spring 2k22"] = {
        Enable = false,
        Items = {}
    },
    ["summer 2k22"] = {
        Enable = false,
        Items = {}
    },
    ["autumn 2k22"] = {
        Enable = false,
        Items = {}
    }
}