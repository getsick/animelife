local meta = FindMetaTable("Player")

module("levelee", package.seeall)

ascend_formula = function(n)
    return ((n - 1) + n) ^ 2
end

function meta:GetTreeXP()
    return self:GetNWInt("animelife.levelee.xp", 0)
end

function meta:GetTreeLevel()
    return self:GetNWInt("animelife.levelee.level", 1)
end

local colors = {
    ["blue"] = Color(72, 203, 255),
    ["green"] = Color(183, 255, 91),
    ["yellow"] = Color(252, 255, 91),
    ["orange"] = Color(255, 203, 91),
    ["red"] = Color(255, 91, 91),
}
function ColorByLevel(lvl)
    if lvl >= 0 and lvl <= 49 then
        return colors["green"]
    elseif lvl >= 50 and lvl <= 99 then
        return colors["blue"]
    elseif lvl >= 100 and lvl <= 199 then
        return colors["yellow"]
    elseif lvl >= 200 and lvl <= 300 then
        return colors["orange"]
    elseif lvl >= 301 then
        return colors["red"]
    end

    return colors["green"]
end