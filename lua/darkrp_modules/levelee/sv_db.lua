MySQLite.begin()

MySQLite.query([[
    CREATE TABLE IF NOT EXISTS al_levelee(
        sid TINYTEXT NOT NULL PRIMARY KEY,
        rpname TINYTEXT NOT NULL,
        level INTEGER NOT NULL,
        xp INTEGER NOT NULL
    );
]])

module("levelee", package.seeall)

function SaveData(ply)
    MySQLite.query([[SELECT level, xp
    FROM al_levelee
    where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
        if !data then
            MySQLite.query([[REPLACE INTO al_levelee VALUES(]] ..
            MySQLite.SQLStr(ply:SteamID()) .. [[, ]] ..
            MySQLite.SQLStr(ply:Nick()) .. [[, ]] ..
            0  .. [[, ]] ..
            0 .. ");")
        end

        MySQLite.query([[UPDATE al_levelee SET rpname = ]] .. MySQLite.SQLStr(ply:Nick()) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
        MySQLite.query([[UPDATE al_levelee SET level = ]] .. tonumber(ply:GetTreeLevel()) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
        MySQLite.query([[UPDATE al_levelee SET xp = ]] .. tonumber(ply:GetTreeXP()) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";")
    end, function(e, q) 
        print("Database Error:", e, q)
    end)
end

function RestoreData(ply)
    MySQLite.query([[
        SELECT level, xp
        FROM al_levelee
        where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
            if !data then return end
            
            if data[1].level then
                ply:SetTreeLevel(tonumber(data[1].level), true)
            end

            if data[1].xp then
                ply:SetNWInt("animelife.levelee.xp", tonumber(data[1].xp))
            end
        end, function(e, q)
            print("Database Error:", e, q)
        end)
end

hook.Add("PlayerInitialSpawn", "animelife.levelee.db", function(ply)
    -- this might get overwritten, so we need to restore data as fast as we can
    timer.Simple(0.5, function()
        if !IsValid(ply) or !ply:IsPlayer() then return end

        RestoreData(ply)
    end)
end)