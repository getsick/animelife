local meta = FindMetaTable("Player")

util.AddNetworkString("animelife.levelee.transfer")

module("levelee", package.seeall)

XPPerMinute = 10

function meta:SetTreeLevel(lv, db_call)
    self:SetNWInt("animelife.levelee.level", lv)

    if db_call then return end

    SaveData(self)
end

function meta:AddTreeXP(xp, db_call)
    local last_xp = self:GetNWInt("animelife.levelee.xp", 0)
    self:SetNWInt("animelife.levelee.xp", last_xp + xp)

    if db_call then return end

    SaveData(self)
end

function meta:TransferXPToLevel()
    local xp = self:GetNWInt("animelife.levelee.xp", 0)

    -- nowhere to upgrade
    if xp <= 0 then return end

    local lv = self:GetNWInt("animelife.levelee.level", 0)
    local should_ascend = xp >= ascend_formula(lv + 1)
    if should_ascend then
        self:SetTreeLevel(lv + 1)

        local returned_xp = 0
        if xp > ascend_formula(lv + 1) then
            returned_xp = xp - ascend_formula(lv + 1)
        end
    
        -- reset xp var
        self:SetNWInt("animelife.levelee.xp", returned_xp)
        
        return
    end
end

net.Receive("animelife.levelee.transfer", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    ply:TransferXPToLevel()
end)

if !timer.Exists("animelife.levelee.addxp") then
    timer.Create("animelife.levelee.addxp", 60, 0, function()
        for _, ply in pairs(player.GetHumans()) do
            if ply:TimeConnected() < 60 then continue end -- restore process might fuck up, so it's better to use this
            
            local xp = ply:IsVIP() and XPPerMinute * 2 or XPPerMinute
            ply:AddTreeXP(xp)
        end
    end)
end