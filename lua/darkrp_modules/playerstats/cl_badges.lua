module("badges", package.seeall)

List = {
    ["member_plus"] = function(ply) return ply:IsVIP() end,
    ["superadmin"] = function(ply) return ply:IsUserGroup("superadmin") or ply:IsUserGroup("root") end,
    ["curator"] = function(ply) return ply:IsUserGroup("Curator") end,
    ["admin"] = function(ply) return ply:IsUserGroup("Admin") end,
    ["eventer"] = function(ply) return ply:IsUserGroup("Ivent") end,
    ["operator"] = function(ply) return ply:IsUserGroup("Operator") end,
    ["sponsor"] = function(ply) return ply:IsUserGroup("Sponsor") end,
}

function HasBadge(ply, badge)
    return List[badge](ply)
end

function AnyBadges(ply)
    for badge, _ in pairs(List) do
        if HasBadge(ply, badge) then
            return true
        end
    end

    return false
end

local materials = {}
for badge, _ in pairs(List) do
    local badge_mat = Material("animelife/scoreboard/badges/" .. badge .. ".png")
    materials[badge] = badge_mat
end

function GetMaterial(badge)
    return materials[badge]
end