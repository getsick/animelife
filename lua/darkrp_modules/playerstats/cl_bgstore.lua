local PANEL = {}

local logo = Material("animelife/bgstore/logo.png")
local detail_blobs = Material("animelife/bgstore/detail_blobs.png")
local icon_close = Material("animelife/bgstore/icon_close.png")

local flux = util.Base64Decode("bG9jYWwgZmx1eCA9IHsgX3ZlcnNpb24gPSAiMC4xLjUiIH0KZmx1eC5fX2luZGV4ID0gZmx1eAoKZmx1eC50d2VlbnMgPSB7fQpmbHV4LmVhc2luZyA9IHsgbGluZWFyID0gZnVuY3Rpb24ocCkgcmV0dXJuIHAgZW5kIH0KCmxvY2FsIGVhc2luZyA9IHsKICBxdWFkICAgID0gInAgKiBwIiwKICBjdWJpYyAgID0gInAgKiBwICogcCIsCiAgcXVhcnQgICA9ICJwICogcCAqIHAgKiBwIiwKICBxdWludCAgID0gInAgKiBwICogcCAqIHAgKiBwIiwKICBleHBvICAgID0gIjIgXiAoMTAgKiAocCAtIDEpKSIsCiAgc2luZSAgICA9ICItbWF0aC5jb3MocCAqIChtYXRoLnBpICogLjUpKSArIDEiLAogIGNpcmMgICAgPSAiLShtYXRoLnNxcnQoMSAtIChwICogcCkpIC0gMSkiLAogIGJhY2sgICAgPSAicCAqIHAgKiAoMi43ICogcCAtIDEuNykiLAogIGVsYXN0aWMgPSAiLSgyXigxMCAqIChwIC0gMSkpICogbWF0aC5zaW4oKHAgLSAxLjA3NSkgKiAobWF0aC5waSAqIDIpIC8gLjMpKSIKfQoKbG9jYWwgbWFrZWZ1bmMgPSBmdW5jdGlvbihzdHIsIGV4cHIpCiAgcmV0dXJuIENvbXBpbGVTdHJpbmcoInJldHVybiBmdW5jdGlvbihwKSAiIC4uIHN0cjpnc3ViKCIlJGUiLCBleHByKSAuLiAiIGVuZCIsICJUZXN0RnVuY3Rpb24iKSgpCmVuZAoKZm9yIGssIHYgaW4gcGFpcnMoZWFzaW5nKSBkbwogIGZsdXguZWFzaW5nW2sgLi4gImluIl0gPSBtYWtlZnVuYygicmV0dXJuICRlIiwgdikKICBmbHV4LmVhc2luZ1trIC4uICJvdXQiXSA9IG1ha2VmdW5jKFtbCiAgICBwID0gMSAtIHAKICAgIHJldHVybiAxIC0gKCRlKQogIF1dLCB2KQogIGZsdXguZWFzaW5nW2sgLi4gImlub3V0Il0gPSBtYWtlZnVuYyhbWwogICAgcCA9IHAgKiAyCiAgICBpZiBwIDwgMSB0aGVuCiAgICAgIHJldHVybiAuNSAqICgkZSkKICAgIGVsc2UKICAgICAgcCA9IDIgLSBwCiAgICAgIHJldHVybiAuNSAqICgxIC0gKCRlKSkgKyAuNQogICAgZW5kCiAgXV0sIHYpCmVuZAoKCgpsb2NhbCB0d2VlbiA9IHt9CnR3ZWVuLl9faW5kZXggPSB0d2VlbgoKbG9jYWwgZnVuY3Rpb24gbWFrZWZzZXR0ZXIoZmllbGQpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBsb2NhbCBtdCA9IGdldG1ldGF0YWJsZSh4KQogICAgaWYgdHlwZSh4KSB+PSAiZnVuY3Rpb24iIGFuZCBub3QgKG10IGFuZCBtdC5fX2NhbGwpIHRoZW4KICAgICAgZXJyb3IoImV4cGVjdGVkIGZ1bmN0aW9uIG9yIGNhbGxhYmxlIiwgMikKICAgIGVuZAogICAgbG9jYWwgb2xkID0gc2VsZltmaWVsZF0KICAgIHNlbGZbZmllbGRdID0gb2xkIGFuZCBmdW5jdGlvbigpIG9sZCgpIHgoKSBlbmQgb3IgeAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgpsb2NhbCBmdW5jdGlvbiBtYWtlc2V0dGVyKGZpZWxkLCBjaGVja2ZuLCBlcnJtc2cpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBpZiBjaGVja2ZuIGFuZCBub3QgY2hlY2tmbih4KSB0aGVuCiAgICAgIGVycm9yKGVycm1zZzpnc3ViKCIlJHgiLCB0b3N0cmluZyh4KSksIDIpCiAgICBlbmQKICAgIHNlbGZbZmllbGRdID0geAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgp0d2Vlbi5lYXNlICA9IG1ha2VzZXR0ZXIoIl9lYXNlIiwKICAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uKHgpIHJldHVybiBmbHV4LmVhc2luZ1t4XSBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGVhc2luZyB0eXBlICckeCciKQp0d2Vlbi5kZWxheSA9IG1ha2VzZXR0ZXIoIl9kZWxheSIsCiAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbih4KSByZXR1cm4gdHlwZSh4KSA9PSAibnVtYmVyIiBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGRlbGF5IHRpbWU7IGV4cGVjdGVkIG51bWJlciIpCnR3ZWVuLm9uc3RhcnQgICAgID0gbWFrZWZzZXR0ZXIoIl9vbnN0YXJ0IikKdHdlZW4ub251cGRhdGUgICAgPSBtYWtlZnNldHRlcigiX29udXBkYXRlIikKdHdlZW4ub25jb21wbGV0ZSAgPSBtYWtlZnNldHRlcigiX29uY29tcGxldGUiKQoKCmZ1bmN0aW9uIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpCiAgbG9jYWwgc2VsZiA9IHNldG1ldGF0YWJsZSh7fSwgdHdlZW4pCiAgc2VsZi5vYmogPSBvYmoKICBzZWxmLnJhdGUgPSB0aW1lID4gMCBhbmQgMSAvIHRpbWUgb3IgMAogIHNlbGYucHJvZ3Jlc3MgPSB0aW1lID4gMCBhbmQgMCBvciAxCiAgc2VsZi5fZGVsYXkgPSAwCiAgc2VsZi5fZWFzZSA9ICJxdWFkb3V0IgogIHNlbGYudmFycyA9IHt9CiAgZm9yIGssIHYgaW4gcGFpcnModmFycykgZG8KICAgIGlmIHR5cGUodikgfj0gIm51bWJlciIgdGhlbgogICAgICBlcnJvcigiYmFkIHZhbHVlIGZvciBrZXkgJyIgLi4gayAuLiAiJzsgZXhwZWN0ZWQgbnVtYmVyIikKICAgIGVuZAogICAgc2VsZi52YXJzW2tdID0gdgogIGVuZAogIHJldHVybiBzZWxmCmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOmluaXQoKQogIGZvciBrLCB2IGluIHBhaXJzKHNlbGYudmFycykgZG8KICAgIGxvY2FsIHggPSBzZWxmLm9ialtrXQogICAgaWYgdHlwZSh4KSB+PSAibnVtYmVyIiB0aGVuCiAgICAgIGVycm9yKCJiYWQgdmFsdWUgb24gb2JqZWN0IGtleSAnIiAuLiBrIC4uICInOyBleHBlY3RlZCBudW1iZXIiKQogICAgZW5kCiAgICBzZWxmLnZhcnNba10gPSB7IHN0YXJ0ID0geCwgZGlmZiA9IHYgLSB4IH0KICBlbmQKICBzZWxmLmluaXRlZCA9IHRydWUKZW5kCgoKZnVuY3Rpb24gdHdlZW46YWZ0ZXIoLi4uKQogIGxvY2FsIHQKICBpZiBzZWxlY3QoIiMiLCAuLi4pID09IDIgdGhlbgogICAgdCA9IHR3ZWVuLm5ldyhzZWxmLm9iaiwgLi4uKQogIGVsc2UKICAgIHQgPSB0d2Vlbi5uZXcoLi4uKQogIGVuZAogIHQucGFyZW50ID0gc2VsZi5wYXJlbnQKICBzZWxmOm9uY29tcGxldGUoZnVuY3Rpb24oKSBmbHV4LmFkZChzZWxmLnBhcmVudCwgdCkgZW5kKQogIHJldHVybiB0CmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOnN0b3AoKQogIGZsdXgucmVtb3ZlKHNlbGYucGFyZW50LCBzZWxmKQplbmQKCgoKZnVuY3Rpb24gZmx1eC5ncm91cCgpCiAgcmV0dXJuIHNldG1ldGF0YWJsZSh7fSwgZmx1eCkKZW5kCgoKZnVuY3Rpb24gZmx1eDp0byhvYmosIHRpbWUsIHZhcnMpCiAgcmV0dXJuIGZsdXguYWRkKHNlbGYsIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpKQplbmQKCgpmdW5jdGlvbiBmbHV4OnVwZGF0ZShkZWx0YXRpbWUpCiAgZm9yIGkgPSAjc2VsZiwgMSwgLTEgZG8KICAgIGxvY2FsIHQgPSBzZWxmW2ldCiAgICBpZiB0Ll9kZWxheSA+IDAgdGhlbgogICAgICB0Ll9kZWxheSA9IHQuX2RlbGF5IC0gZGVsdGF0aW1lCiAgICBlbHNlCiAgICAgIGlmIG5vdCB0LmluaXRlZCB0aGVuCiAgICAgICAgZmx1eC5jbGVhcihzZWxmLCB0Lm9iaiwgdC52YXJzKQogICAgICAgIHQ6aW5pdCgpCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnN0YXJ0IHRoZW4KICAgICAgICB0Ll9vbnN0YXJ0KCkKICAgICAgICB0Ll9vbnN0YXJ0ID0gbmlsCiAgICAgIGVuZAogICAgICB0LnByb2dyZXNzID0gdC5wcm9ncmVzcyArIHQucmF0ZSAqIGRlbHRhdGltZQogICAgICBsb2NhbCBwID0gdC5wcm9ncmVzcwogICAgICBsb2NhbCB4ID0gcCA+PSAxIGFuZCAxIG9yIGZsdXguZWFzaW5nW3QuX2Vhc2VdKHApCiAgICAgIGZvciBrLCB2IGluIHBhaXJzKHQudmFycykgZG8KICAgICAgICB0Lm9ialtrXSA9IHYuc3RhcnQgKyB4ICogdi5kaWZmCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnVwZGF0ZSB0aGVuIHQuX29udXBkYXRlKCkgZW5kCiAgICAgIGlmIHAgPj0gMSB0aGVuCiAgICAgICAgZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgICAgICBpZiB0Ll9vbmNvbXBsZXRlIHRoZW4gdC5fb25jb21wbGV0ZSgpIGVuZAogICAgICBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmNsZWFyKG9iaiwgdmFycykKICBmb3IgdCBpbiBwYWlycyhzZWxmW29ial0pIGRvCiAgICBpZiB0LmluaXRlZCB0aGVuCiAgICAgIGZvciBrIGluIHBhaXJzKHZhcnMpIGRvIHQudmFyc1trXSA9IG5pbCBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmFkZCh0d2VlbikKICAtLSBBZGQgdG8gb2JqZWN0IHRhYmxlLCBjcmVhdGUgdGFibGUgaWYgaXQgZG9lcyBub3QgZXhpc3QKICBsb2NhbCBvYmogPSB0d2Vlbi5vYmoKICBzZWxmW29ial0gPSBzZWxmW29ial0gb3Ige30KICBzZWxmW29ial1bdHdlZW5dID0gdHJ1ZQogIC0tIEFkZCB0byBhcnJheQogIHRhYmxlLmluc2VydChzZWxmLCB0d2VlbikKICB0d2Vlbi5wYXJlbnQgPSBzZWxmCiAgcmV0dXJuIHR3ZWVuCmVuZAoKCmZ1bmN0aW9uIGZsdXg6cmVtb3ZlKHgpCiAgaWYgdHlwZSh4KSA9PSAibnVtYmVyIiB0aGVuCiAgICAtLSBSZW1vdmUgZnJvbSBvYmplY3QgdGFibGUsIGRlc3Ryb3kgdGFibGUgaWYgaXQgaXMgZW1wdHkKICAgIGxvY2FsIG9iaiA9IHNlbGZbeF0ub2JqCiAgICBzZWxmW29ial1bc2VsZlt4XV0gPSBuaWwKICAgIGlmIG5vdCBuZXh0KHNlbGZbb2JqXSkgdGhlbiBzZWxmW29ial0gPSBuaWwgZW5kCiAgICAtLSBSZW1vdmUgZnJvbSBhcnJheQogICAgc2VsZlt4XSA9IHNlbGZbI3NlbGZdCiAgICByZXR1cm4gdGFibGUucmVtb3ZlKHNlbGYpCiAgZW5kCiAgZm9yIGksIHYgaW4gaXBhaXJzKHNlbGYpIGRvCiAgICBpZiB2ID09IHggdGhlbgogICAgICByZXR1cm4gZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgIGVuZAogIGVuZAplbmQKCgoKbG9jYWwgYm91bmQgPSB7CiAgdG8gICAgICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgudG8oZmx1eC50d2VlbnMsIC4uLikgZW5kLAogIHVwZGF0ZSAgPSBmdW5jdGlvbiguLi4pIHJldHVybiBmbHV4LnVwZGF0ZShmbHV4LnR3ZWVucywgLi4uKSBlbmQsCiAgcmVtb3ZlICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgucmVtb3ZlKGZsdXgudHdlZW5zLCAuLi4pIGVuZCwKfQpzZXRtZXRhdGFibGUoYm91bmQsIGZsdXgpCgpyZXR1cm4gYm91bmQ=")
flux = CompileString(flux, "flux")()

FONT_WEIGHT_BOLD = 700
FONT_WEIGHT_REGULAR = 400
FONT_WEIGHT_LIGHT = 300
FONT_WEIGHT_MEDIUM = 500
FONT_WEIGHT_SEMIBOLD = 600

local function setup_fonts()
    surface.CreateFont("animelife.bgstore.category", {font = "Exo 2", size = ScreenScale(29) / 3, weight = FONT_WEIGHT_BOLD, extended = true})
    surface.CreateFont("animelife.bgstore.category.shadow", {font = "Exo 2", size = ScreenScale(29) / 3, weight = FONT_WEIGHT_BOLD, blursize = 4, extended = true})
    surface.CreateFont("animelife.bgstore.category.button", {font = "Exo 2 SemiBold", size = ScreenScale(17) / 3, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
    surface.CreateFont("animelife.bgstore.item", {font = "Exo 2", size = ScreenScale(17) / 3, weight = FONT_WEIGHT_BOLD, extended = true})
    surface.CreateFont("animelife.bgstore.price", {font = "Exo 2", size = ScreenScale(43) / 3, weight = FONT_WEIGHT_BOLD, extended = true})
end
setup_fonts()

local color_grip = Color(250, 103, 120, 45)
local color_background = Color(31, 31, 31)
local color_button = Color(250, 103, 120)
local color_storebg = Color(246, 244, 244)
local color_fade = Color(4, 4, 4, 15)

function PANEL:Init()
    self:SetAlpha(0)
    self:AlphaTo(255, 0.25)

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetPos(ScrW() - ui.x(32) - ui.x(34), ui.y(35))
    self.CloseButton:SetSize(ui.x(32), ui.y(32))
    self.CloseButton:SetText("")
    self.CloseButton.Paint = function(panel, w, h)
        ui.Smooth(true, true)
            surface.SetDrawColor(color_white)
            surface.SetMaterial(icon_close)
            surface.DrawTexturedRect(0, 0, w, h)
        ui.Smooth(false, true)
    end
    self.CloseButton.DoClick = function()
        if ValidPanel(self) then
            self:Remove()
            self = nil
        end
    end

    self.ScrollList = vgui.Create("DScrollPanel", self)
    local v_bar = self.ScrollList:GetVBar()
    v_bar:SetWide(6)
    v_bar.Paint = function() end
    v_bar.btnUp.Paint = function() end
    v_bar.btnDown.Paint = function() end
    v_bar.btnGrip.Paint = function(panel, w, h)
        draw.RoundedBox(6, 0, 0, w, h, color_grip)
    end

    local x, y, i = 270, 39, 1
    for key, cat in pairs(playerstats.BackgroundStore) do
        for index, item in pairs(cat) do
            if item.Hidden then continue end
            if ispanel(item) then continue end

            local item_panel = vgui.Create("DPanel", self.ScrollList)
            item_panel:SetPos(ui.x(x), ui.y(y))
            item_panel:SetSize(ui.x(429), ui.y(166))
            item_panel:SetCursor("hand")
            item_panel.Button = nil
            item_panel.Background = nil
            item_panel.Parameters = {alpha = 0, price_x = 16, button_y = 5}
            item_panel.Paint = function(panel, w, h)
                draw.RoundedBox(16, 0, 0, w, h, color_background)

                draw.SimpleText("Фон загружается...", "animelife.bgstore.category.button", w / 2, h / 2, ColorAlpha(color_white, 75), 1, 1)
            end
            item_panel.PaintOver = function(panel, w, h)
                draw.SimpleText(item.Name, "animelife.bgstore.item", 35, 20, ColorAlpha(color_white, 95))

                draw.SimpleText("¥" .. string.Comma(item.Price or 0), "animelife.bgstore.price", panel.Parameters.price_x, h - 26, ColorAlpha(color_white, 150 * panel.Parameters.alpha), nil, 4)

                draw.RoundedBox(6, w - 114 - 37, h - 40 - 23 + panel.Parameters.button_y, 114, 40, ColorAlpha(color_button, 255 * panel.Parameters.alpha))
                draw.SimpleText(LocalPlayer():HasBackground(index) and "Установить" or "Купить", "animelife.bgstore.category.button", w - (114 / 2) - 37, h - 20 - 23 + panel.Parameters.button_y, ColorAlpha(color_white, 255 * panel.Parameters.alpha), 1, 1 )
            end
            item_panel.OnCursorEntered = function(panel)
                flux.to(panel.Parameters, 0.8, {alpha = 1, price_x = 35, button_y = 0})
            end
            item_panel.OnCursorExited = function(panel)
                if panel.Button:IsHovered() then return end
                flux.to(panel.Parameters, 0.8, {alpha = 0, price_x = 16, button_y = 5})
            end

            item_panel.Background = vgui.Create("DHTML", item_panel)
            item_panel.Background:Dock(FILL)
            item_panel.Background:SetMouseInputEnabled(false)
            item_panel.Background:SetHTML([[<style>
            html,body{padding:0;margin:0}
            img{
                border-radius: 16px;
                width: 100%;
                height: 100%;
                object-fit: cover;
                filter: saturate(135%) brightness(80%);
            }
            </style><img src="]] .. item.Link .. [[" width="100%" height="100%">]])

            item_panel.Button = vgui.Create("DButton", item_panel)
            item_panel.Button:SetPos(item_panel:GetWide() - 114 - 37, item_panel:GetTall() - 40 - 23)
            item_panel.Button:SetSize(114, 40)
            item_panel.Button:SetText("")
            item_panel.Button.Paint = function() end
            // item_panel.Button:SetVisible(false)
            item_panel.Button.DoClick = function()
                net.Start("animelife.playerstats.background_buy")
                    net.WriteString(key)
                    net.WriteInt(index, 16)
                net.SendToServer()

                surface.PlaySound("click.wav")
            end

            if i % 3 == 0 then
                x = 270
                y = y + 166 + 27 
            else
                x = x + 429 + 46
            end

            i = i + 1
        end
    end
end

function PANEL:PerformLayout(w, h)
    self.ScrollList:SetPos(0, h - ui.y(630))
    self.ScrollList:SetSize(w, ui.y(630))
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(color_storebg)
    surface.DrawRect(0, 0, w, h)

    -- is where the items are
    surface.SetDrawColor(color_fade)
    surface.DrawRect(0, h - ui.y(630), w, ui.y(630))

    ui.Smooth(true, true)
        surface.SetDrawColor(color_white)
        surface.SetMaterial(logo)
        surface.DrawTexturedRect((w - ui.y(533)) / 2, ui.y(127), ui.y(533), ui.y(208))

        surface.SetDrawColor(color_white)
        surface.SetMaterial(detail_blobs)
        surface.DrawTexturedRect((w - ui.y(1045)) / 2, ui.y(44) + math.sin(CurTime()) * ui.y(32), ui.y(1045), ui.y(297))
    ui.Smooth(false, true)

    self:SeekResolutionChange(function(pnl)
        pnl:Remove()
        pnl = nil 

        setup_fonts()
    end)

    flux.update(RealFrameTime())
end

vgui.Register("animelife.bgstore", PANEL, "EditablePanel")

function OpenBackgroundStore()
    local s = vgui.Create("animelife.bgstore")
    s:SetSize(ScrW(), ScrH())
    s:MakePopup()
end