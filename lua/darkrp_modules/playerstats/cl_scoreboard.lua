local flux = util.Base64Decode("bG9jYWwgZmx1eCA9IHsgX3ZlcnNpb24gPSAiMC4xLjUiIH0KZmx1eC5fX2luZGV4ID0gZmx1eAoKZmx1eC50d2VlbnMgPSB7fQpmbHV4LmVhc2luZyA9IHsgbGluZWFyID0gZnVuY3Rpb24ocCkgcmV0dXJuIHAgZW5kIH0KCmxvY2FsIGVhc2luZyA9IHsKICBxdWFkICAgID0gInAgKiBwIiwKICBjdWJpYyAgID0gInAgKiBwICogcCIsCiAgcXVhcnQgICA9ICJwICogcCAqIHAgKiBwIiwKICBxdWludCAgID0gInAgKiBwICogcCAqIHAgKiBwIiwKICBleHBvICAgID0gIjIgXiAoMTAgKiAocCAtIDEpKSIsCiAgc2luZSAgICA9ICItbWF0aC5jb3MocCAqIChtYXRoLnBpICogLjUpKSArIDEiLAogIGNpcmMgICAgPSAiLShtYXRoLnNxcnQoMSAtIChwICogcCkpIC0gMSkiLAogIGJhY2sgICAgPSAicCAqIHAgKiAoMi43ICogcCAtIDEuNykiLAogIGVsYXN0aWMgPSAiLSgyXigxMCAqIChwIC0gMSkpICogbWF0aC5zaW4oKHAgLSAxLjA3NSkgKiAobWF0aC5waSAqIDIpIC8gLjMpKSIKfQoKbG9jYWwgbWFrZWZ1bmMgPSBmdW5jdGlvbihzdHIsIGV4cHIpCiAgcmV0dXJuIENvbXBpbGVTdHJpbmcoInJldHVybiBmdW5jdGlvbihwKSAiIC4uIHN0cjpnc3ViKCIlJGUiLCBleHByKSAuLiAiIGVuZCIsICJUZXN0RnVuY3Rpb24iKSgpCmVuZAoKZm9yIGssIHYgaW4gcGFpcnMoZWFzaW5nKSBkbwogIGZsdXguZWFzaW5nW2sgLi4gImluIl0gPSBtYWtlZnVuYygicmV0dXJuICRlIiwgdikKICBmbHV4LmVhc2luZ1trIC4uICJvdXQiXSA9IG1ha2VmdW5jKFtbCiAgICBwID0gMSAtIHAKICAgIHJldHVybiAxIC0gKCRlKQogIF1dLCB2KQogIGZsdXguZWFzaW5nW2sgLi4gImlub3V0Il0gPSBtYWtlZnVuYyhbWwogICAgcCA9IHAgKiAyCiAgICBpZiBwIDwgMSB0aGVuCiAgICAgIHJldHVybiAuNSAqICgkZSkKICAgIGVsc2UKICAgICAgcCA9IDIgLSBwCiAgICAgIHJldHVybiAuNSAqICgxIC0gKCRlKSkgKyAuNQogICAgZW5kCiAgXV0sIHYpCmVuZAoKCgpsb2NhbCB0d2VlbiA9IHt9CnR3ZWVuLl9faW5kZXggPSB0d2VlbgoKbG9jYWwgZnVuY3Rpb24gbWFrZWZzZXR0ZXIoZmllbGQpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBsb2NhbCBtdCA9IGdldG1ldGF0YWJsZSh4KQogICAgaWYgdHlwZSh4KSB+PSAiZnVuY3Rpb24iIGFuZCBub3QgKG10IGFuZCBtdC5fX2NhbGwpIHRoZW4KICAgICAgZXJyb3IoImV4cGVjdGVkIGZ1bmN0aW9uIG9yIGNhbGxhYmxlIiwgMikKICAgIGVuZAogICAgbG9jYWwgb2xkID0gc2VsZltmaWVsZF0KICAgIHNlbGZbZmllbGRdID0gb2xkIGFuZCBmdW5jdGlvbigpIG9sZCgpIHgoKSBlbmQgb3IgeAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgpsb2NhbCBmdW5jdGlvbiBtYWtlc2V0dGVyKGZpZWxkLCBjaGVja2ZuLCBlcnJtc2cpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBpZiBjaGVja2ZuIGFuZCBub3QgY2hlY2tmbih4KSB0aGVuCiAgICAgIGVycm9yKGVycm1zZzpnc3ViKCIlJHgiLCB0b3N0cmluZyh4KSksIDIpCiAgICBlbmQKICAgIHNlbGZbZmllbGRdID0geAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgp0d2Vlbi5lYXNlICA9IG1ha2VzZXR0ZXIoIl9lYXNlIiwKICAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uKHgpIHJldHVybiBmbHV4LmVhc2luZ1t4XSBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGVhc2luZyB0eXBlICckeCciKQp0d2Vlbi5kZWxheSA9IG1ha2VzZXR0ZXIoIl9kZWxheSIsCiAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbih4KSByZXR1cm4gdHlwZSh4KSA9PSAibnVtYmVyIiBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGRlbGF5IHRpbWU7IGV4cGVjdGVkIG51bWJlciIpCnR3ZWVuLm9uc3RhcnQgICAgID0gbWFrZWZzZXR0ZXIoIl9vbnN0YXJ0IikKdHdlZW4ub251cGRhdGUgICAgPSBtYWtlZnNldHRlcigiX29udXBkYXRlIikKdHdlZW4ub25jb21wbGV0ZSAgPSBtYWtlZnNldHRlcigiX29uY29tcGxldGUiKQoKCmZ1bmN0aW9uIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpCiAgbG9jYWwgc2VsZiA9IHNldG1ldGF0YWJsZSh7fSwgdHdlZW4pCiAgc2VsZi5vYmogPSBvYmoKICBzZWxmLnJhdGUgPSB0aW1lID4gMCBhbmQgMSAvIHRpbWUgb3IgMAogIHNlbGYucHJvZ3Jlc3MgPSB0aW1lID4gMCBhbmQgMCBvciAxCiAgc2VsZi5fZGVsYXkgPSAwCiAgc2VsZi5fZWFzZSA9ICJxdWFkb3V0IgogIHNlbGYudmFycyA9IHt9CiAgZm9yIGssIHYgaW4gcGFpcnModmFycykgZG8KICAgIGlmIHR5cGUodikgfj0gIm51bWJlciIgdGhlbgogICAgICBlcnJvcigiYmFkIHZhbHVlIGZvciBrZXkgJyIgLi4gayAuLiAiJzsgZXhwZWN0ZWQgbnVtYmVyIikKICAgIGVuZAogICAgc2VsZi52YXJzW2tdID0gdgogIGVuZAogIHJldHVybiBzZWxmCmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOmluaXQoKQogIGZvciBrLCB2IGluIHBhaXJzKHNlbGYudmFycykgZG8KICAgIGxvY2FsIHggPSBzZWxmLm9ialtrXQogICAgaWYgdHlwZSh4KSB+PSAibnVtYmVyIiB0aGVuCiAgICAgIGVycm9yKCJiYWQgdmFsdWUgb24gb2JqZWN0IGtleSAnIiAuLiBrIC4uICInOyBleHBlY3RlZCBudW1iZXIiKQogICAgZW5kCiAgICBzZWxmLnZhcnNba10gPSB7IHN0YXJ0ID0geCwgZGlmZiA9IHYgLSB4IH0KICBlbmQKICBzZWxmLmluaXRlZCA9IHRydWUKZW5kCgoKZnVuY3Rpb24gdHdlZW46YWZ0ZXIoLi4uKQogIGxvY2FsIHQKICBpZiBzZWxlY3QoIiMiLCAuLi4pID09IDIgdGhlbgogICAgdCA9IHR3ZWVuLm5ldyhzZWxmLm9iaiwgLi4uKQogIGVsc2UKICAgIHQgPSB0d2Vlbi5uZXcoLi4uKQogIGVuZAogIHQucGFyZW50ID0gc2VsZi5wYXJlbnQKICBzZWxmOm9uY29tcGxldGUoZnVuY3Rpb24oKSBmbHV4LmFkZChzZWxmLnBhcmVudCwgdCkgZW5kKQogIHJldHVybiB0CmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOnN0b3AoKQogIGZsdXgucmVtb3ZlKHNlbGYucGFyZW50LCBzZWxmKQplbmQKCgoKZnVuY3Rpb24gZmx1eC5ncm91cCgpCiAgcmV0dXJuIHNldG1ldGF0YWJsZSh7fSwgZmx1eCkKZW5kCgoKZnVuY3Rpb24gZmx1eDp0byhvYmosIHRpbWUsIHZhcnMpCiAgcmV0dXJuIGZsdXguYWRkKHNlbGYsIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpKQplbmQKCgpmdW5jdGlvbiBmbHV4OnVwZGF0ZShkZWx0YXRpbWUpCiAgZm9yIGkgPSAjc2VsZiwgMSwgLTEgZG8KICAgIGxvY2FsIHQgPSBzZWxmW2ldCiAgICBpZiB0Ll9kZWxheSA+IDAgdGhlbgogICAgICB0Ll9kZWxheSA9IHQuX2RlbGF5IC0gZGVsdGF0aW1lCiAgICBlbHNlCiAgICAgIGlmIG5vdCB0LmluaXRlZCB0aGVuCiAgICAgICAgZmx1eC5jbGVhcihzZWxmLCB0Lm9iaiwgdC52YXJzKQogICAgICAgIHQ6aW5pdCgpCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnN0YXJ0IHRoZW4KICAgICAgICB0Ll9vbnN0YXJ0KCkKICAgICAgICB0Ll9vbnN0YXJ0ID0gbmlsCiAgICAgIGVuZAogICAgICB0LnByb2dyZXNzID0gdC5wcm9ncmVzcyArIHQucmF0ZSAqIGRlbHRhdGltZQogICAgICBsb2NhbCBwID0gdC5wcm9ncmVzcwogICAgICBsb2NhbCB4ID0gcCA+PSAxIGFuZCAxIG9yIGZsdXguZWFzaW5nW3QuX2Vhc2VdKHApCiAgICAgIGZvciBrLCB2IGluIHBhaXJzKHQudmFycykgZG8KICAgICAgICB0Lm9ialtrXSA9IHYuc3RhcnQgKyB4ICogdi5kaWZmCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnVwZGF0ZSB0aGVuIHQuX29udXBkYXRlKCkgZW5kCiAgICAgIGlmIHAgPj0gMSB0aGVuCiAgICAgICAgZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgICAgICBpZiB0Ll9vbmNvbXBsZXRlIHRoZW4gdC5fb25jb21wbGV0ZSgpIGVuZAogICAgICBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmNsZWFyKG9iaiwgdmFycykKICBmb3IgdCBpbiBwYWlycyhzZWxmW29ial0pIGRvCiAgICBpZiB0LmluaXRlZCB0aGVuCiAgICAgIGZvciBrIGluIHBhaXJzKHZhcnMpIGRvIHQudmFyc1trXSA9IG5pbCBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmFkZCh0d2VlbikKICAtLSBBZGQgdG8gb2JqZWN0IHRhYmxlLCBjcmVhdGUgdGFibGUgaWYgaXQgZG9lcyBub3QgZXhpc3QKICBsb2NhbCBvYmogPSB0d2Vlbi5vYmoKICBzZWxmW29ial0gPSBzZWxmW29ial0gb3Ige30KICBzZWxmW29ial1bdHdlZW5dID0gdHJ1ZQogIC0tIEFkZCB0byBhcnJheQogIHRhYmxlLmluc2VydChzZWxmLCB0d2VlbikKICB0d2Vlbi5wYXJlbnQgPSBzZWxmCiAgcmV0dXJuIHR3ZWVuCmVuZAoKCmZ1bmN0aW9uIGZsdXg6cmVtb3ZlKHgpCiAgaWYgdHlwZSh4KSA9PSAibnVtYmVyIiB0aGVuCiAgICAtLSBSZW1vdmUgZnJvbSBvYmplY3QgdGFibGUsIGRlc3Ryb3kgdGFibGUgaWYgaXQgaXMgZW1wdHkKICAgIGxvY2FsIG9iaiA9IHNlbGZbeF0ub2JqCiAgICBzZWxmW29ial1bc2VsZlt4XV0gPSBuaWwKICAgIGlmIG5vdCBuZXh0KHNlbGZbb2JqXSkgdGhlbiBzZWxmW29ial0gPSBuaWwgZW5kCiAgICAtLSBSZW1vdmUgZnJvbSBhcnJheQogICAgc2VsZlt4XSA9IHNlbGZbI3NlbGZdCiAgICByZXR1cm4gdGFibGUucmVtb3ZlKHNlbGYpCiAgZW5kCiAgZm9yIGksIHYgaW4gaXBhaXJzKHNlbGYpIGRvCiAgICBpZiB2ID09IHggdGhlbgogICAgICByZXR1cm4gZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgIGVuZAogIGVuZAplbmQKCgoKbG9jYWwgYm91bmQgPSB7CiAgdG8gICAgICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgudG8oZmx1eC50d2VlbnMsIC4uLikgZW5kLAogIHVwZGF0ZSAgPSBmdW5jdGlvbiguLi4pIHJldHVybiBmbHV4LnVwZGF0ZShmbHV4LnR3ZWVucywgLi4uKSBlbmQsCiAgcmVtb3ZlICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgucmVtb3ZlKGZsdXgudHdlZW5zLCAuLi4pIGVuZCwKfQpzZXRtZXRhdGFibGUoYm91bmQsIGZsdXgpCgpyZXR1cm4gYm91bmQ=")
flux = CompileString(flux, "flux")()

include("scoreboard/cl_scoreboard_background.lua")
include("scoreboard/cl_scoreboard_profile.lua")


local PANEL = {}

local animelife_logo = Material("animelife/scoreboard/revamp/logo.png")
local header = Material("animelife/scoreboard/header.png")
local base_avatar = Material("animelife/scoreboard/base_avatar.png")
local pen = Material("animelife/scoreboard/icon_pen.png")
local thumb_like = Material("animelife/scoreboard/thumb_like.png")
local thumb_dislike = Material("animelife/scoreboard/thumb_dislike.png")
local achi_test = Material("animelife/scoreboard/achievements/0.png")
local ping = Material("animelife/scoreboard/icon_ping.png")
local level = Material("animelife/scoreboard/icon_level.png")

FONT_WEIGHT_BOLD = 700
FONT_WEIGHT_REGULAR = 400
FONT_WEIGHT_LIGHT = 300
FONT_WEIGHT_MEDIUM = 500
FONT_WEIGHT_SEMIBOLD = 600

local function setup_fonts()
    surface.CreateFont("animelife.scoreboard.playernamebig", {font = "Exo 2", size = ScreenScale(39) / 3, weight = FONT_WEIGHT_BOLD, extended = true})
    surface.CreateFont("animelife.scoreboard.playernamebig.shadow", {font = "Exo 2", size = ScreenScale(39) / 3, weight = FONT_WEIGHT_BOLD, blursize = 2, extended = true})
    surface.CreateFont("animelife.scoreboard.status", {font = "Exo 2 SemiBold", size = ScreenScale(20) / 3, weight = FONT_WEIGHT_MEDIUM, extended = true})
    surface.CreateFont("animelife.scoreboard.status.shadow", {font = "Exo 2 SemiBold", size = ScreenScale(20) / 3, weight = FONT_WEIGHT_MEDIUM, blursize = 2, extended = true})
    surface.CreateFont("animelife.scoreboard.likes", {font = "Exo 2", size = ScreenScale(17) / 3, weight = FONT_WEIGHT_BOLD, extended = true})
    surface.CreateFont("animelife.scoreboard.likes.shadow", {font = "Exo 2", size = ScreenScale(17) / 3, weight = FONT_WEIGHT_BOLD, blursize = 2, extended = true})
    surface.CreateFont("animelife.scoreboard.stats", {font = "Exo 2", size = 22, weight = FONT_WEIGHT_BOLD, extended = true})
    surface.CreateFont("animelife.scoreboard.stats.numbers", {font = "Exo 2 Medium", size = ScreenScale(22) / 3, weight = FONT_WEIGHT_MEDIUM, extended = true})
    surface.CreateFont("animelife.scoreboard.admin", {font = "Exo 2 Medium", size = ScreenScale(17) / 3, weight = FONT_WEIGHT_MEDIUM, extended = true})
    surface.CreateFont("animelife.scoreboard.category", {font = "Exo 2 SemiBold", size = ScreenScale(22) / 3, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
    surface.CreateFont("animelife.scoreboard.achievement", {font = "Exo 2 SemiBold", size = 17, weight = FONT_WEIGHT_SEMIBOLD, extended = true})
    surface.CreateFont("animelife.scoreboard.score", {font = "Exo 2 Medium", size = 19, weight = FONT_WEIGHT_MEDIUM, extended = true})
    surface.CreateFont("animelife.scoreboard.level", {font = "Exo 2 Medium", size = 17, weight = FONT_WEIGHT_MEDIUM, extended = true})
end
setup_fonts()

local color_admin = Color(255, 237, 221)
local color_panel = Color(16, 16, 16, 65)
local color_level = Color(183, 255, 91)
local color_grip = Color(221, 205, 231)

function PANEL:Init()
    self.CurrentPlayer = LocalPlayer()

    -- Scoreboard Side
    self.ScoreboardScroll = vgui.Create("DScrollPanel", self)
    self.ScoreboardScroll.Players = {}
    self.ScoreboardScroll.NextUpdate = 0
    self.ScoreboardScroll.Think = function(panel)
        if RealTime() > panel.NextUpdate then
            for ply in pairs(panel.Players or {}) do
                if !IsValid(ply) then
                    panel:RemovePlayer(ply)
                end
            end

            local sorted = player.GetAll()
            table.sort(sorted, function(a, b)
                return a:Team() < b:Team()
            end)
            for c, ply in ipairs(sorted) do
                if panel.Players[ply] == nil then
                    panel:AddPlayer(ply, c)
                end
            end

            panel:InvalidateLayout()

            panel.NextUpdate = RealTime() + 3
        end
    end
    self.ScoreboardScroll.AddPlayer = function(panel, ply, id)
        local player_panel = vgui.Create("DPanel", panel)
        // player_panel:SetPos(18, score_y)
        player_panel:Dock(TOP)
        player_panel:DockMargin(18, 6, 18, 0)
        player_panel:SetSize(panel:GetWide() - 36, 52)
        player_panel:SetCursor("hand")
        player_panel.OnMousePressed = function()
            if !IsValid(ply) then return end
            
            self.CurrentPlayer = ply

            self.ProfileBase:SetPlayer(self.CurrentPlayer)
        end
        player_panel.Paint = function(panel, w, h)
            draw.RoundedBox(6, 0, 0, w, h, color_panel)
            if ply == LocalPlayer() then
                draw.RoundedBox(6, 0, 0, w, h, Color(0, 0, 0, 150 * math.sin(CurTime() * 2)))
            end

            local lvl = 1
            local nick = "Disconnected Player"
            local nick_color = color_white
            if IsValid(ply) then
                nick = ply:Nick()
                lvl = ply:GetTreeLevel() or 1

                if ply:IsAdmin() then
                    nick_color = color_admin
                end
            end

            draw.SimpleText(lvl, "animelife.scoreboard.level", 32, h / 2 + 1, color_white, nil, 1)

            surface.SetFont("animelife.scoreboard.level")
            local lw = surface.GetTextSize(lvl)

            local col = color_level
            if levelee and levelee.ColorByLevel then
                col = levelee.ColorByLevel(lvl)
            end
            surface.SetDrawColor(col)
            surface.SetMaterial(level)
            surface.DrawTexturedRect(32 + lw + 8, (h - 16) / 2 + 2, 5, 16)

            draw.SimpleText(nick, "animelife.scoreboard.score", 32 + lw + 27, h / 2, nick_color, nil, 1)

            if IsValid(ply) then
                local job = team.GetName(ply:Team())
                local class = classchooser.List[ply:Team()]
                if istable(class) then
                    local actual_class = ply:GetNWInt("animelife.classchooser.class", -1)
                    if actual_class ~= -1 then
                        if istable(class[actual_class]) then
                            job = job .. " (" .. class[actual_class].Name .. ")"
                        end
                    end
                end

                draw.SimpleText(job, "animelife.scoreboard.score", w / 2, h / 2, team.GetColor(ply:Team()), 1, 1)
            end

            surface.SetDrawColor(255, 255, 255)
            surface.SetMaterial(ping)
            surface.DrawTexturedRect(w - 32 - 12, ( h - 12 ) / 2, 12, 12)

            local ping = 0
            if IsValid(ply) then
                ping = ply:Ping()
            end
            
            draw.SimpleText(ping .. "ms", "animelife.scoreboard.score", w - 32 - 12 - 4, h / 2, color_white, 2, 1)
        end

        panel.Players[ply] = player_panel
        panel:AddItem(player_panel)
    end
    self.ScoreboardScroll.PopulatePlayers = function(panel)
        panel:Clear()
        panel.Players = {}

        local sorted = player.GetAll()
        table.sort(sorted, function(a, b)
            return a:Team() < b:Team()
        end)
        for c, ply in ipairs(sorted) do
            if panel.Players[ply] == nil then
                panel:AddPlayer(ply, c)
            end
        end

        panel:InvalidateLayout()
        panel.NextUpdate = CurTime() + 3
    end
    self.ScoreboardScroll.RemovePlayer = function(panel, ply)
        if ValidPanel(panel.Players[ply]) then
            panel.Players[ply]:Remove()
            panel.Players[ply] = nil
        end
    end
    local vertical_bar = self.ScoreboardScroll:GetVBar()
    vertical_bar:SetWide(4)
    vertical_bar.Paint = function() end
    vertical_bar.btnUp.Paint = function() end
    vertical_bar.btnDown.Paint = function() end
    vertical_bar.btnGrip.Paint = function(panel, w, h)
        draw.RoundedBox(2, 0, 0, w, h, color_grip)
    end

    -- Profile Side
    self.ProfileBase = vgui.Create("animelife.scoreboard.profile.panel", self)
    self.ProfileBase:SetPlayer(LocalPlayer())
end

function PANEL:PerformLayout(w, h)
    self.ScoreboardScroll:SetPos(0, 0)
    self.ScoreboardScroll:SetSize(w - ui.x(716), ui.y(1011))

    self.ProfileBase:SetSize(ui.x(716), h)
    self.ProfileBase:SetPos(w - ui.x(716), 0)
end

local matBlurScreen = Material( "pp/blurscreen" )
local function blur(panel, amount) 
	local x, y = panel:LocalToScreen( 0, 0 )
	surface.SetDrawColor( 255, 255, 255 )
	surface.SetMaterial( matBlurScreen )
	for i = 1, 2 do
		matBlurScreen:SetFloat('$blur', (i / 6) * (amount ~= nil and amount or 6))
		matBlurScreen:Recompute()
		render.UpdateScreenEffectTexture()
		
		surface.DrawTexturedRect(x * -1, y * -1, ScrW(), ScrH())
	end
end

function PANEL:Paint(w, h)
    local x, y = self:LocalToScreen(0), self:LocalToScreen(0)
    render.SetScissorRect(x, y, x + self:LocalToScreen(w - ui.x(716)), y + self:LocalToScreen(h), true)
        blur(self, 6)
    render.SetScissorRect(0, 0, 0, 0, false)

    surface.SetDrawColor(0, 0, 0, 225)
    surface.DrawRect(0, 0, w - ui.x(716), h)

    local w, h = animelife_logo:Width(), animelife_logo:Height()

    surface.SetDrawColor(255, 255, 255, 75)
    surface.SetMaterial(animelife_logo)
    surface.DrawTexturedRect(17, ScrH() - h - 2, w, h)

    self:SeekResolutionChange(function(pnl)
        pnl:Remove()
        pnl = nil 

        setup_fonts()
    end)
end

vgui.Register("animelife.scoreboard", PANEL, "EditablePanel")

hook.Add("HUDShouldDraw", "htrhrt", function(e)
    -- TODO: Don't draw any HUD if scoreboard is open
    if e == "CHudHealth" then return false end
end)

function GM:ScoreboardShow()
    if ( !ValidPanel( GLOBALS_SCOREBOARD ) ) then
        GLOBALS_SCOREBOARD = vgui.Create("animelife.scoreboard")
        GLOBALS_SCOREBOARD:SetSize(ScrW(), ScrH())
        GLOBALS_SCOREBOARD:SetAlpha(0)
        GLOBALS_SCOREBOARD:AlphaTo(255, 0.1)
        GLOBALS_SCOREBOARD:SetVisible(true)
    else
        GLOBALS_SCOREBOARD:SetVisible( true )
        GLOBALS_SCOREBOARD:SetAlpha(0)
        GLOBALS_SCOREBOARD:AlphaTo(255, 0.1)
    end

    GLOBALS_SCOREBOARD.ScoreboardScroll:PopulatePlayers()

    RestoreCursorPosition()
    gui.EnableScreenClicker( true )
end

function GM:ScoreboardHide()
    if ( !ValidPanel( GLOBALS_SCOREBOARD ) ) then
        GLOBALS_SCOREBOARD = vgui.Create("animelife.scoreboard")
        GLOBALS_SCOREBOARD:SetSize(ScrW(), ScrH())
        GLOBALS_SCOREBOARD:SetVisible(false)
    else
        GLOBALS_SCOREBOARD:SetVisible( false )
    end

    RememberCursorPosition()
    gui.EnableScreenClicker( false )
    CloseDermaMenus()
end