local meta = FindMetaTable("Player")

function meta:GetDislikes()
    return self:GetNWString("animelife.playerstats.dislikes", "")
end

function meta:GetLikes()
    return self:GetNWString("animelife.playerstats.likes", "")
end

function meta:GetStatus()
    return self:GetNWString("animelife.playerstats.status", "Нет статуса.")
end

function meta:GetBackground()
    return self:GetNWInt("animelife.playerstats.background", 1)
end

function meta:GetBackgrounds()
    return self:GetNWString("animelife.playerstats.background.list", "")
end

function meta:HasBackground(num)
    return table.HasValue(string.Split(self:GetBackgrounds(), ","), tostring(num))
end

GLOBALS_BACKGROUNDS = {
    ["scoreboard"] = {},
    ["store"] = {}
}

for f = 1, 39 do
    local mat = Material("animelife/scoreboard/backgrounds/" .. f .. ".png")
    // table.insert(GLOBALS_BACKGROUNDS["scoreboard"], mat)
    GLOBALS_BACKGROUNDS["scoreboard"][tonumber(f)] = mat
end

for f = 2, 39 do
    local mat = Material("animelife/bgstore/list/" .. f .. ".png")
    GLOBALS_BACKGROUNDS["store"][tonumber(f)] = mat
end

GLOBALS_BACKGROUND_LINKS = {
    [2] = "https://i.ibb.co/X271z5j/91341540-p0-ccexpress.jpg",
    [3] = "https://i.ibb.co/FnNKSq6/92161881-p0.jpg",
    [4] = "https://i.ibb.co/DrFdJdY/95145339-p0-ccexpress.jpg",
    [5] = "https://i.ibb.co/wQnbxTn/89618492-p0-ccexpress.jpg",
    [6] = "https://i.ibb.co/74MrRYZ/94819769-p0-ccexpress.jpg",
    [7] = "https://i.ibb.co/NC8m736/90324324-p0-ccexpress.jpg",
    [8] = "https://i.ibb.co/Pw1Tr89/92191625-p1-ccexpress.jpg",
    [9] = "https://i.ibb.co/F3CypfC/93990522-p0-ccexpress.jpg",
    [10] = "https://i.ibb.co/JCksNWQ/94483207-p0.jpg",
    [11] = "https://i.ibb.co/n35CK5W/92290091-p0.jpg",
    [12] = "https://i.ibb.co/9cS2ZKx/89143847-p0.jpg",
    [13] = "https://i.ibb.co/K24Yq9d/95327436-p0.jpg",
    [14] = "https://i.ibb.co/ZT2MBwJ/92387831-p0.jpg",
    [15] = "https://i.ibb.co/Ht5b4vw/89418550-p0.jpg",
    [16] = "https://i.ibb.co/Jt0q78P/92079701-p0-ccexpress.jpg",
    [17] = "https://mir-s3-cdn-cf.behance.net/project_modules/fs/9afe0493484903.5e66500f8dea4.gif",
}