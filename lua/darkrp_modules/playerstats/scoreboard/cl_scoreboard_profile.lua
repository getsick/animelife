
local PANEL = {}

local background_color = Color(30, 33, 40)
local block_color = Color(22, 25, 32)
local subtitle_color = Color(220, 218, 218)
local color_grey = Color(228, 228, 228)

local gradient = Material("animelife/scoreboard/revamp/detail_gradient.png")

function PANEL:Init()
    self.Background = vgui.Create("animelife.scoreboard.profile.background", self)

    self.BadgesPanel = vgui.Create("DPanel", self)
    self.BadgesPanel.Paint = function(panel, w, h)
        draw.RoundedBox(16, 0, 0, w, h, block_color)

        local x = 29
        for badge, _ in pairs(badges.List) do
            if badges.HasBadge(self.Player, badge) then
                local mat = badges.GetMaterial(badge)
                ui.Smooth(true, true)
                    surface.SetDrawColor(255, 255, 255)
                    surface.SetMaterial(mat)
                    surface.DrawTexturedRect(ui.x(x), (h - ui.y(46)) / 2, ui.y(139), ui.y(46))
                ui.Smooth(false, true)

                x = x + 139 + 12
            end
        end
    end

    self.AdminPanel = vgui.Create("DPanel", self)
    self.AdminPanel.Paint = function(panel, w, h)
        draw.RoundedBox(16, 0, 0, w, h, block_color)

        draw.SimpleText("Количество банов: " .. self.Player:GetNWInt("animelife.administration.ban_count", 0), "animelife.scoreboard.stats.numbers", ui.x(29), ui.y(16), color_grey)
        draw.SimpleText("Количество джайлов: " .. self.Player:GetNWInt("animelife.administration.jail_count", 0), "animelife.scoreboard.stats.numbers", ui.x(29), ui.y(16) + ui.y(22) + ui.y(6), color_grey)
    end

    self.ActButton = vgui.Create("DButton", self.AdminPanel)
    self.ActButton:SetText("")
    self.ActButton.Paint = function(panel, w, h)
        draw.RoundedBox(12, 0, 0, w, h, ColorAlpha(color_grey, 1))

        draw.SimpleText("действия над игроком", "animelife.scoreboard.admin", w / 2, h / 2, Color(255, 255, 255), 1, 1)
    end
    self.ActButton.DoClick = function()
        if !IsValid(self.Player) then return end

        local dmenu = DermaMenu(nil, GLOBALS_SCOREBOARD)

        dmenu:AddOption("скопировать SteamID", function()
            SetClipboardText(self.Player:SteamID())
        end):SetIcon("icon16/bullet_blue.png")

        if self.Player ~= LocalPlayer() then
            if !self.Player:IsMuted() then
                dmenu:AddOption("отключить голос (локально)", function()
                    self.Player:SetMuted(true)
                end):SetIcon("icon16/bullet_blue.png")
            else
                dmenu:AddOption("включить голос (локально)", function()
                    self.Player:SetMuted(false)
                end):SetIcon("icon16/bullet_blue.png")
            end
        end

        if LocalPlayer():IsAdmin() then
            for keycode, act in SortedPairsByMemberValue(administration.Actions or {}, "Name") do
                if !LocalPlayer():HasActionAccess(keycode) then continue end
                if isfunction(act.ShouldShow) then
                    if !act.ShouldShow(self.Player) then continue end
                end
                if act.ShowPlayers then
                    local opt = dmenu:AddSubMenu(act.Name)
                    for _, ply in ipairs(player.GetAll()) do
                        opt:AddOption(string.format("%s [%s]", ply:Nick(), ply:SteamID()), function()
                            net.Start("animelife.administration.action")
                                net.WriteString(keycode)
                                net.WriteEntity(self.Player)
                                net.WriteEntity(ply)
                            net.SendToServer()
                        end)
                    end

                    continue
                end
                if act.ShowTeams then
                    local opt = dmenu:AddSubMenu(act.Name)
                    for idx, job in pairs(team.GetAllTeams()) do
                        opt:AddOption(string.format("%s: %s", idx, job.Name), function()
                            net.Start("animelife.administration.setteam")
                                net.WriteEntity(self.Player)
                                net.WriteInt(idx, 16)
                            net.SendToServer()
                        end):SetColor(Color(job.Color.r - 64, job.Color.g - 64, job.Color.b - 64))
                    end

                    continue
                end
                if act.ShowUsergroups then
                    local opt = dmenu:AddSubMenu(act.Name)
                    for name, usergroup in pairs(administration.Usergroups) do
                        opt:AddOption(name, function()
                            net.Start("animelife.administration.action2")
                                net.WriteString(keycode)
                                net.WriteEntity(self.Player)
                                net.WriteString(name)
                            net.SendToServer()
                        end)
                    end

                    continue
                end
                dmenu:AddOption(act.Name, function()
                    if keycode == "ban" then
                        OpenBanPanel(self.Player)
                        return
                    elseif keycode == "jail" then
                        OpenJailPanel(self.Player)
                        return
                    elseif keycode == "addwarn" then
                        OpenWarnPanel(self.Player)
                        return
                    end

                    if act.TakesOneArgument then
                        Derma_StringRequest(self.Player:Nick() .. " - " .. act.Name, act.Text, "", function(argument)
                            net.Start("animelife.administration.action2")
                                net.WriteString(keycode)
                                net.WriteEntity(self.Player)
                                net.WriteString(argument)
                            net.SendToServer()
                        end)
                        return
                    end

                    net.Start("animelife.administration.action")
                        net.WriteString(keycode)
                        net.WriteEntity(self.Player)
                    net.SendToServer()
                end):SetIcon("icon16/" .. act.Icon .. ".png")
            end
        end

        dmenu:Open()
        RegisterDermaMenuForClose(dmenu)
    end

    self.AchievementsLabel = vgui.Create("DLabel", self)
    self.AchievementsLabel:SetText("Достижения")
    self.AchievementsLabel:SetFont("animelife.scoreboard.category")
    self.AchievementsLabel:SetColor(color_white)
    self.AchievementsLabel:SizeToContents()

    local kaomoji = "｡ﾟ･ (>﹏<) ･ﾟ｡"
    self.AchiCounterLabel = vgui.Create("DLabel", self)
    self.AchiCounterLabel:SetText("[" .. kaomoji .. "] открыто: 0")
    self.AchiCounterLabel:SetFont("animelife.scoreboard.admin")
    self.AchiCounterLabel:SetColor(color_white)
    self.AchiCounterLabel:SizeToContents()
    self.AchiCounterLabel.Think = function(panel)
        local achi_count = #achievements:GetAllCompleted(self.Player)
        if achi_count > 0 then
            kaomoji = "(〃＾▽＾〃)"
        else
            kaomoji = "｡ﾟ･ (>﹏<) ･ﾟ｡"
        end
        
        panel:SetText("[" .. kaomoji .. "] открыто: " .. achi_count)
    end

    self.AchievementScroll = vgui.Create("DScrollPanel", self)
    local vertical_bar = self.AchievementScroll:GetVBar()
    vertical_bar:SetWide(6)
    vertical_bar.Paint = function() end
    vertical_bar.btnGrip.Paint = function(pnl, w, h)
        draw.RoundedBox(6, 0, 0, w, h, Color(192, 133, 133, 5))
    end
    vertical_bar.btnDown.Paint = function() end
    vertical_bar.btnUp.Paint = function() end
end

function PANEL:PerformLayout(w, h)
    local cur_y = 27
    self.Background:SetSize(ui.x(666), ui.y(228))
    self.Background:SetPos((w - ui.x(666)) / 2, ui.y(cur_y))
    cur_y = cur_y + 228 + 19

    if self.BadgesPanel:IsVisible() then
        self.BadgesPanel:SetSize(ui.x(666), ui.y(64))
        self.BadgesPanel:SetPos((w - ui.x(666)) / 2, ui.y(cur_y))
        self.BadgesPanel:SetVisible(badges.AnyBadges(self.Player))
        cur_y = cur_y + 64 + 19
    end

    if self.AdminPanel:IsVisible() then
        self.AdminPanel:SetSize(ui.x(666), ui.y(88) + ui.y(40) + ui.y(29))
        self.AdminPanel:SetPos((w - ui.x(666)) / 2, ui.y(cur_y))
        cur_y = cur_y + 88 + 40 + 29 + 19
    end

    self.ActButton:SetSize(ui.x(180), ui.y(40))
    self.ActButton:SetPos(ui.x(29), ui.y(88))

    self.AchievementsLabel:SetPos((w - self.AchievementsLabel:GetWide()) / 2, ui.y(cur_y))
    self.AchiCounterLabel:SetPos((w - self.AchiCounterLabel:GetWide()) / 2, ui.y(cur_y) + ui.y(24))
    cur_y = cur_y + 24 + 24 + 19

    self.AchievementScroll:SetSize(w, h - ui.y(cur_y))
    self.AchievementScroll:SetPos(0, ui.y(cur_y))

    if !self.AchievementScroll.Panels then
        local x, y = 24, 0
        for i, achi in pairs(achievements.List) do
            local achi_button = vgui.Create("DButton", self.AchievementScroll)
            achi_button:SetPos(x, y)
            achi_button:SetSize(100, 100)
            achi_button:SetText("")
            achi_button.Paint = function(panel, w, h)
                surface.SetDrawColor(255, 255, 255, achievements:GetStatus(self.Player, i) and 255 or 12)
                surface.SetMaterial(achi.Icon)
                surface.DrawTexturedRect(0, 0, w, h)
            end
            achi_button.OnCursorEntered = function(panel)
                self.Tooltip = {panel.x, panel.y, achi.Name, achi.Description}
            end
            achi_button.OnCursorExited = function()
                self.Tooltip = nil
            end
    
            if x > self.AchievementScroll:GetWide() - 256 then
                x = 24
                y = y + 100 + 8
            else
                x = x + 100 + 8
            end

            self.AchievementScroll.Panels = true
        end
    end
end

function PANEL:SetPlayer(ply)
    self.Background:SetPlayer(ply)

    if ValidPanel(self.AdminPanel) then
        self.AdminPanel:SetVisible(ply == LocalPlayer() or LocalPlayer():IsAdmin())
        self:InvalidateLayout()
    end

    if ValidPanel(self.BadgesPanel) then
        self.BadgesPanel:SetVisible(badges.AnyBadges(ply))
        self:InvalidateLayout()
    end

    self.Player = ply
end

function PANEL:Paint(w, h)
    if !IsValid(self.Player) then
        -- player disconnected, try to find someone for a replace
        self:SetPlayer(table.Random(player.GetAll()))
        return
    end

    surface.SetDrawColor(background_color)
    surface.DrawRect(0, 0, w, h)

    surface.SetDrawColor(color_white)
    surface.SetMaterial(gradient)
    surface.DrawTexturedRect(0, 0, w, h)
end

function PANEL:PaintOver(w, h)
    if istable(self.Tooltip) then
        local sx, sy = self.AchievementScroll:GetPos()
        local ax, ay = math.max(72, self.Tooltip[1]), self.Tooltip[2] + sy
        local description = markup.Parse("<font=animelife.scoreboard.achievement><colour=225,225,255>" .. self.Tooltip[4].. "</colour></font>", 236)
        local desc_h = description:GetHeight()
        desc_h = desc_h + 25 + 4 + 10 + 8
        surface.SetDrawColor(50, 50, 51, 255)
        surface.DrawRect(ax - 60, ay - desc_h, 236, desc_h)

        surface.SetDrawColor(255, 255, 255, 6)
        surface.DrawRect(ax - 60, ay - desc_h, 236, 25)

        draw.SimpleText(self.Tooltip[3], "animelife.scoreboard.achievement", ax - 60 + 16, ay - desc_h + 4, Color(255, 255, 255))

        description:Draw(ax - 60 + 16, ay - desc_h + 25 + 10)
        -- draw.SimpleText(panel.Tooltip[4], "animelife.scoreboard.achievement", ax - 60 + 16, ay - 65 + 25 + 10, Color(225, 225, 225))
    end
end

vgui.Register("animelife.scoreboard.profile.panel", PANEL, "DPanel")