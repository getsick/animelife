local flux = util.Base64Decode("bG9jYWwgZmx1eCA9IHsgX3ZlcnNpb24gPSAiMC4xLjUiIH0KZmx1eC5fX2luZGV4ID0gZmx1eAoKZmx1eC50d2VlbnMgPSB7fQpmbHV4LmVhc2luZyA9IHsgbGluZWFyID0gZnVuY3Rpb24ocCkgcmV0dXJuIHAgZW5kIH0KCmxvY2FsIGVhc2luZyA9IHsKICBxdWFkICAgID0gInAgKiBwIiwKICBjdWJpYyAgID0gInAgKiBwICogcCIsCiAgcXVhcnQgICA9ICJwICogcCAqIHAgKiBwIiwKICBxdWludCAgID0gInAgKiBwICogcCAqIHAgKiBwIiwKICBleHBvICAgID0gIjIgXiAoMTAgKiAocCAtIDEpKSIsCiAgc2luZSAgICA9ICItbWF0aC5jb3MocCAqIChtYXRoLnBpICogLjUpKSArIDEiLAogIGNpcmMgICAgPSAiLShtYXRoLnNxcnQoMSAtIChwICogcCkpIC0gMSkiLAogIGJhY2sgICAgPSAicCAqIHAgKiAoMi43ICogcCAtIDEuNykiLAogIGVsYXN0aWMgPSAiLSgyXigxMCAqIChwIC0gMSkpICogbWF0aC5zaW4oKHAgLSAxLjA3NSkgKiAobWF0aC5waSAqIDIpIC8gLjMpKSIKfQoKbG9jYWwgbWFrZWZ1bmMgPSBmdW5jdGlvbihzdHIsIGV4cHIpCiAgcmV0dXJuIENvbXBpbGVTdHJpbmcoInJldHVybiBmdW5jdGlvbihwKSAiIC4uIHN0cjpnc3ViKCIlJGUiLCBleHByKSAuLiAiIGVuZCIsICJUZXN0RnVuY3Rpb24iKSgpCmVuZAoKZm9yIGssIHYgaW4gcGFpcnMoZWFzaW5nKSBkbwogIGZsdXguZWFzaW5nW2sgLi4gImluIl0gPSBtYWtlZnVuYygicmV0dXJuICRlIiwgdikKICBmbHV4LmVhc2luZ1trIC4uICJvdXQiXSA9IG1ha2VmdW5jKFtbCiAgICBwID0gMSAtIHAKICAgIHJldHVybiAxIC0gKCRlKQogIF1dLCB2KQogIGZsdXguZWFzaW5nW2sgLi4gImlub3V0Il0gPSBtYWtlZnVuYyhbWwogICAgcCA9IHAgKiAyCiAgICBpZiBwIDwgMSB0aGVuCiAgICAgIHJldHVybiAuNSAqICgkZSkKICAgIGVsc2UKICAgICAgcCA9IDIgLSBwCiAgICAgIHJldHVybiAuNSAqICgxIC0gKCRlKSkgKyAuNQogICAgZW5kCiAgXV0sIHYpCmVuZAoKCgpsb2NhbCB0d2VlbiA9IHt9CnR3ZWVuLl9faW5kZXggPSB0d2VlbgoKbG9jYWwgZnVuY3Rpb24gbWFrZWZzZXR0ZXIoZmllbGQpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBsb2NhbCBtdCA9IGdldG1ldGF0YWJsZSh4KQogICAgaWYgdHlwZSh4KSB+PSAiZnVuY3Rpb24iIGFuZCBub3QgKG10IGFuZCBtdC5fX2NhbGwpIHRoZW4KICAgICAgZXJyb3IoImV4cGVjdGVkIGZ1bmN0aW9uIG9yIGNhbGxhYmxlIiwgMikKICAgIGVuZAogICAgbG9jYWwgb2xkID0gc2VsZltmaWVsZF0KICAgIHNlbGZbZmllbGRdID0gb2xkIGFuZCBmdW5jdGlvbigpIG9sZCgpIHgoKSBlbmQgb3IgeAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgpsb2NhbCBmdW5jdGlvbiBtYWtlc2V0dGVyKGZpZWxkLCBjaGVja2ZuLCBlcnJtc2cpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBpZiBjaGVja2ZuIGFuZCBub3QgY2hlY2tmbih4KSB0aGVuCiAgICAgIGVycm9yKGVycm1zZzpnc3ViKCIlJHgiLCB0b3N0cmluZyh4KSksIDIpCiAgICBlbmQKICAgIHNlbGZbZmllbGRdID0geAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgp0d2Vlbi5lYXNlICA9IG1ha2VzZXR0ZXIoIl9lYXNlIiwKICAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uKHgpIHJldHVybiBmbHV4LmVhc2luZ1t4XSBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGVhc2luZyB0eXBlICckeCciKQp0d2Vlbi5kZWxheSA9IG1ha2VzZXR0ZXIoIl9kZWxheSIsCiAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbih4KSByZXR1cm4gdHlwZSh4KSA9PSAibnVtYmVyIiBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGRlbGF5IHRpbWU7IGV4cGVjdGVkIG51bWJlciIpCnR3ZWVuLm9uc3RhcnQgICAgID0gbWFrZWZzZXR0ZXIoIl9vbnN0YXJ0IikKdHdlZW4ub251cGRhdGUgICAgPSBtYWtlZnNldHRlcigiX29udXBkYXRlIikKdHdlZW4ub25jb21wbGV0ZSAgPSBtYWtlZnNldHRlcigiX29uY29tcGxldGUiKQoKCmZ1bmN0aW9uIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpCiAgbG9jYWwgc2VsZiA9IHNldG1ldGF0YWJsZSh7fSwgdHdlZW4pCiAgc2VsZi5vYmogPSBvYmoKICBzZWxmLnJhdGUgPSB0aW1lID4gMCBhbmQgMSAvIHRpbWUgb3IgMAogIHNlbGYucHJvZ3Jlc3MgPSB0aW1lID4gMCBhbmQgMCBvciAxCiAgc2VsZi5fZGVsYXkgPSAwCiAgc2VsZi5fZWFzZSA9ICJxdWFkb3V0IgogIHNlbGYudmFycyA9IHt9CiAgZm9yIGssIHYgaW4gcGFpcnModmFycykgZG8KICAgIGlmIHR5cGUodikgfj0gIm51bWJlciIgdGhlbgogICAgICBlcnJvcigiYmFkIHZhbHVlIGZvciBrZXkgJyIgLi4gayAuLiAiJzsgZXhwZWN0ZWQgbnVtYmVyIikKICAgIGVuZAogICAgc2VsZi52YXJzW2tdID0gdgogIGVuZAogIHJldHVybiBzZWxmCmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOmluaXQoKQogIGZvciBrLCB2IGluIHBhaXJzKHNlbGYudmFycykgZG8KICAgIGxvY2FsIHggPSBzZWxmLm9ialtrXQogICAgaWYgdHlwZSh4KSB+PSAibnVtYmVyIiB0aGVuCiAgICAgIGVycm9yKCJiYWQgdmFsdWUgb24gb2JqZWN0IGtleSAnIiAuLiBrIC4uICInOyBleHBlY3RlZCBudW1iZXIiKQogICAgZW5kCiAgICBzZWxmLnZhcnNba10gPSB7IHN0YXJ0ID0geCwgZGlmZiA9IHYgLSB4IH0KICBlbmQKICBzZWxmLmluaXRlZCA9IHRydWUKZW5kCgoKZnVuY3Rpb24gdHdlZW46YWZ0ZXIoLi4uKQogIGxvY2FsIHQKICBpZiBzZWxlY3QoIiMiLCAuLi4pID09IDIgdGhlbgogICAgdCA9IHR3ZWVuLm5ldyhzZWxmLm9iaiwgLi4uKQogIGVsc2UKICAgIHQgPSB0d2Vlbi5uZXcoLi4uKQogIGVuZAogIHQucGFyZW50ID0gc2VsZi5wYXJlbnQKICBzZWxmOm9uY29tcGxldGUoZnVuY3Rpb24oKSBmbHV4LmFkZChzZWxmLnBhcmVudCwgdCkgZW5kKQogIHJldHVybiB0CmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOnN0b3AoKQogIGZsdXgucmVtb3ZlKHNlbGYucGFyZW50LCBzZWxmKQplbmQKCgoKZnVuY3Rpb24gZmx1eC5ncm91cCgpCiAgcmV0dXJuIHNldG1ldGF0YWJsZSh7fSwgZmx1eCkKZW5kCgoKZnVuY3Rpb24gZmx1eDp0byhvYmosIHRpbWUsIHZhcnMpCiAgcmV0dXJuIGZsdXguYWRkKHNlbGYsIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpKQplbmQKCgpmdW5jdGlvbiBmbHV4OnVwZGF0ZShkZWx0YXRpbWUpCiAgZm9yIGkgPSAjc2VsZiwgMSwgLTEgZG8KICAgIGxvY2FsIHQgPSBzZWxmW2ldCiAgICBpZiB0Ll9kZWxheSA+IDAgdGhlbgogICAgICB0Ll9kZWxheSA9IHQuX2RlbGF5IC0gZGVsdGF0aW1lCiAgICBlbHNlCiAgICAgIGlmIG5vdCB0LmluaXRlZCB0aGVuCiAgICAgICAgZmx1eC5jbGVhcihzZWxmLCB0Lm9iaiwgdC52YXJzKQogICAgICAgIHQ6aW5pdCgpCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnN0YXJ0IHRoZW4KICAgICAgICB0Ll9vbnN0YXJ0KCkKICAgICAgICB0Ll9vbnN0YXJ0ID0gbmlsCiAgICAgIGVuZAogICAgICB0LnByb2dyZXNzID0gdC5wcm9ncmVzcyArIHQucmF0ZSAqIGRlbHRhdGltZQogICAgICBsb2NhbCBwID0gdC5wcm9ncmVzcwogICAgICBsb2NhbCB4ID0gcCA+PSAxIGFuZCAxIG9yIGZsdXguZWFzaW5nW3QuX2Vhc2VdKHApCiAgICAgIGZvciBrLCB2IGluIHBhaXJzKHQudmFycykgZG8KICAgICAgICB0Lm9ialtrXSA9IHYuc3RhcnQgKyB4ICogdi5kaWZmCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnVwZGF0ZSB0aGVuIHQuX29udXBkYXRlKCkgZW5kCiAgICAgIGlmIHAgPj0gMSB0aGVuCiAgICAgICAgZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgICAgICBpZiB0Ll9vbmNvbXBsZXRlIHRoZW4gdC5fb25jb21wbGV0ZSgpIGVuZAogICAgICBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmNsZWFyKG9iaiwgdmFycykKICBmb3IgdCBpbiBwYWlycyhzZWxmW29ial0pIGRvCiAgICBpZiB0LmluaXRlZCB0aGVuCiAgICAgIGZvciBrIGluIHBhaXJzKHZhcnMpIGRvIHQudmFyc1trXSA9IG5pbCBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmFkZCh0d2VlbikKICAtLSBBZGQgdG8gb2JqZWN0IHRhYmxlLCBjcmVhdGUgdGFibGUgaWYgaXQgZG9lcyBub3QgZXhpc3QKICBsb2NhbCBvYmogPSB0d2Vlbi5vYmoKICBzZWxmW29ial0gPSBzZWxmW29ial0gb3Ige30KICBzZWxmW29ial1bdHdlZW5dID0gdHJ1ZQogIC0tIEFkZCB0byBhcnJheQogIHRhYmxlLmluc2VydChzZWxmLCB0d2VlbikKICB0d2Vlbi5wYXJlbnQgPSBzZWxmCiAgcmV0dXJuIHR3ZWVuCmVuZAoKCmZ1bmN0aW9uIGZsdXg6cmVtb3ZlKHgpCiAgaWYgdHlwZSh4KSA9PSAibnVtYmVyIiB0aGVuCiAgICAtLSBSZW1vdmUgZnJvbSBvYmplY3QgdGFibGUsIGRlc3Ryb3kgdGFibGUgaWYgaXQgaXMgZW1wdHkKICAgIGxvY2FsIG9iaiA9IHNlbGZbeF0ub2JqCiAgICBzZWxmW29ial1bc2VsZlt4XV0gPSBuaWwKICAgIGlmIG5vdCBuZXh0KHNlbGZbb2JqXSkgdGhlbiBzZWxmW29ial0gPSBuaWwgZW5kCiAgICAtLSBSZW1vdmUgZnJvbSBhcnJheQogICAgc2VsZlt4XSA9IHNlbGZbI3NlbGZdCiAgICByZXR1cm4gdGFibGUucmVtb3ZlKHNlbGYpCiAgZW5kCiAgZm9yIGksIHYgaW4gaXBhaXJzKHNlbGYpIGRvCiAgICBpZiB2ID09IHggdGhlbgogICAgICByZXR1cm4gZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgIGVuZAogIGVuZAplbmQKCgoKbG9jYWwgYm91bmQgPSB7CiAgdG8gICAgICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgudG8oZmx1eC50d2VlbnMsIC4uLikgZW5kLAogIHVwZGF0ZSAgPSBmdW5jdGlvbiguLi4pIHJldHVybiBmbHV4LnVwZGF0ZShmbHV4LnR3ZWVucywgLi4uKSBlbmQsCiAgcmVtb3ZlICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgucmVtb3ZlKGZsdXgudHdlZW5zLCAuLi4pIGVuZCwKfQpzZXRtZXRhdGFibGUoYm91bmQsIGZsdXgpCgpyZXR1cm4gYm91bmQ=")
flux = CompileString(flux, "flux")()

local PANEL = {}

local avatar_shadow = Material("animelife/scoreboard/revamp/avatar_shadow.png")
local pen = Material("animelife/scoreboard/icon_pen.png")
local thumb_like = Material("animelife/scoreboard/thumb_like.png")
local thumb_dislike = Material("animelife/scoreboard/thumb_dislike.png")

local color_grey = Color(228, 228, 228)
local color_noimg = Color(31, 31, 31)
local color_shadow = Color(0, 0, 0, 255 * 0.35)

function PANEL:Init()
    self.Container = vgui.Create("DPanel", self)
    self.Container.Paint = function(pnl, w, h)
        draw.RoundedBox(16, 0, 0, w, h, color_noimg)
    end
    self.Container.PaintOver = function(pnl, w, h)
        surface.SetDrawColor(color_white)
        surface.SetMaterial(avatar_shadow)
        surface.DrawTexturedRect(ui.x(58), ui.y(37), ui.y(155), ui.y(155))

        if ValidPanel(self.ProfileAvatar) then
            render.ClearStencil()
            render.SetStencilEnable(true)
            render.SetStencilWriteMask(1)
            render.SetStencilTestMask(1)
            render.SetStencilFailOperation(STENCILOPERATION_REPLACE)
            render.SetStencilPassOperation(STENCILOPERATION_ZERO)
            render.SetStencilZFailOperation(STENCILOPERATION_ZERO)
            render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_NEVER)
            render.SetStencilReferenceValue(1)
                draw.NoTexture()
                drawRoundedRectangle(ui.x(62), ui.y(41), ui.y(147), ui.y(147), 16, color_white, color_white, 0, 0.25, nil)
            render.SetStencilFailOperation(STENCILOPERATION_ZERO)
            render.SetStencilPassOperation(STENCILOPERATION_REPLACE)
            render.SetStencilZFailOperation(STENCILOPERATION_ZERO)
            render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_EQUAL)
            render.SetStencilReferenceValue(1)
                self.ProfileAvatar:PaintManual()
            render.SetStencilEnable( false )
            render.ClearStencil()
        end

        local nick = self.Player:Nick()
        local steam_id = self.Player:SteamID()
        local row_y = 68
        draw.SimpleText(nick, "animelife.scoreboard.playernamebig.shadow", ui.x(230), ui.y(row_y) + 1, color_shadow)
        draw.SimpleText(nick, "animelife.scoreboard.playernamebig", ui.x(230), ui.y(row_y), color_white)
        row_y = row_y + 32 + 9

        local status = self.Player:GetStatus()
        draw.SimpleText(status, "animelife.scoreboard.status.shadow", ui.x(230), ui.y(row_y) + 1, color_shadow)
        draw.SimpleText(status, "animelife.scoreboard.status", ui.x(230), ui.y(row_y), color_white)

        if ValidPanel(self.StatusEdit) then
            surface.SetFont("animelife.scoreboard.status")
            self.StatusEdit:SetPos(ui.x(230) + surface.GetTextSize(status) + ui.x(7), ui.y(row_y) + 4)
        end

        row_y = row_y + 18 + 9

        local likes, dislikes = #string.Split(self.Player:GetLikes(), ",") - 1, #string.Split(self.Player:GetDislikes(), ",") - 1
        draw.SimpleText(likes, "animelife.scoreboard.likes.shadow", ui.x(250), ui.y(row_y) + 4 + 1, color_shadow)
        draw.SimpleText(likes, "animelife.scoreboard.likes", ui.x(250), ui.y(row_y) + 4, color_grey)

        surface.SetFont("animelife.scoreboard.likes")
        local dx = ui.x(250) + surface.GetTextSize(likes) + ui.x(10)
        if ValidPanel(self.DislikeButton) then
            self.DislikeButton:SetPos(dx, ui.y(row_y) + 4)
        end

        if ValidPanel(self.LikeButton) then
            self.LikeButton:SetPos(ui.x(230), ui.y(row_y) + 4)
        end

        draw.SimpleText(dislikes, "animelife.scoreboard.likes.shadow", dx + ui.x(22), ui.y(row_y) + 4 + 1, color_shadow)
        draw.SimpleText(dislikes, "animelife.scoreboard.likes", dx + ui.x(22), ui.y(row_y) + 4, color_grey)
    end

    self.ActualBackground = vgui.Create("DHTML", self.Container)
    self.ActualBackground:SetVisible(false)

    self.ProfileAvatar = vgui.Create("AvatarImage", self.Container)
    self.ProfileAvatar:SetPaintedManually(true)
    self.ProfileAvatar:SetCursor("hand")
    self.ProfileAvatar.OnMousePressed = function()
        if IsValid(self.CurrentPlayer) then
            self.CurrentPlayer:ShowProfile()
        end
    end

    self.StatusEdit = vgui.Create("DButton", self.Container)
    self.StatusEdit:SetText("")
    self.StatusEdit.Color = {r = 255, g = 255, b = 255}
    self.StatusEdit.Paint = function(panel, w, h)
        ui.Smooth(true, true)
            surface.SetDrawColor(panel.Color.r, panel.Color.g, panel.Color.b)
            surface.SetMaterial(pen)
            surface.DrawTexturedRect(0, 0, w, h)
        ui.Smooth(false, true)
    end
    self.StatusEdit.OnCursorEntered = function(panel)
        flux.to(panel.Color, 0.5, {r = 221, g = 205, b = 231})
    end
    self.StatusEdit.OnCursorExited = function(panel)
        flux.to(panel.Color, 0.5, {r = 255, g = 255, b = 255})
    end
    self.StatusEdit.DoClick = function()
        local preview = LocalPlayer().GetStatus and LocalPlayer():GetStatus() or "Нет статуса."
        Derma_StringRequest("Смена статуса", "Введите ваш новый статус", preview, function(str)
            net.Start("animelife.playerstats.status_change")
                net.WriteString(str)
            net.SendToServer()
        end)
    end

    self.LikeButton = vgui.Create("DButton", self.Container)
    self.LikeButton:SetText("")
    self.LikeButton.Color = {r = 255, g = 255, b = 255}
    self.LikeButton.Paint = function(panel, w, h)
        ui.Smooth(true, true)
            surface.SetDrawColor(panel.Color.r, panel.Color.g, panel.Color.b)
            surface.SetMaterial(thumb_like)
            surface.DrawTexturedRect(0, 0, w, h)
        ui.Smooth(false, true)
    end
    self.LikeButton.OnCursorEntered = function(panel)
        flux.to(panel.Color, 0.5, {r = 183, g = 241, b = 165})
    end
    self.LikeButton.OnCursorExited = function(panel)
        flux.to(panel.Color, 0.5, {r = 255, g = 255, b = 255})
    end
    self.LikeButton.DoClick = function()
        net.Start("animelife.playerstats.like")
            net.WriteEntity(self.Player)
        net.SendToServer()
    end

    self.DislikeButton = vgui.Create("DButton", self.Container)
    self.DislikeButton:SetText("")
    self.DislikeButton.Color = {r = 255, g = 255, b = 255}
    self.DislikeButton.Paint = function(panel, w, h)
        ui.Smooth(true, true)
            surface.SetDrawColor(panel.Color.r, panel.Color.g, panel.Color.b)
            surface.SetMaterial(thumb_dislike)
            surface.DrawTexturedRect(0, 0, w, h)
        ui.Smooth(false, true)
    end
    self.DislikeButton.OnCursorEntered = function(panel)
        flux.to(panel.Color, 0.5, {r = 247, g = 127, b = 151})
    end
    self.DislikeButton.OnCursorExited = function(panel)
        flux.to(panel.Color, 0.5, {r = 255, g = 255, b = 255})
    end
    self.DislikeButton.DoClick = function()
        net.Start("animelife.playerstats.dislike")
            net.WriteEntity(self.Player)
        net.SendToServer()
    end
end

function PANEL:PerformLayout(w, h)
    self.Container:SetSize(w, h)
    self.ActualBackground:Dock(FILL)

    self.ProfileAvatar:SetPos(ui.x(62), ui.y(41))
    self.ProfileAvatar:SetSize(ui.y(147), ui.y(147))

    self.StatusEdit:SetPos(ui.x(562), ui.y(107))
    self.StatusEdit:SetSize(ui.y(14), ui.y(14))

    self.LikeButton:SetPos(230 + 24, 60 + 32 + 9 + 32 + 32)
    self.LikeButton:SetSize(ui.y(16), ui.y(18))

    self.DislikeButton:SetPos(230 + 24, 60 + 32 + 9 + 32 + 32)
    self.DislikeButton:SetSize(ui.y(17), ui.y(19))
end

function PANEL:SetPlayer(ply)
    self.ProfileAvatar:SetPlayer(ply, 184)

    if ValidPanel(self.StatusEdit) then
        self.StatusEdit:SetVisible(ply == LocalPlayer())
    end

    self:SetBackground(ply)

    self.Player = ply
end

function PANEL:SetBackground(ply)
    local bg = ply:GetNWString("animelife.playerstats.background", 1) -- lmao why did i make it a string tho?
    self.LastBackground = bg
    bg = tonumber(bg)
    if bg >= 2 and bg <= #GLOBALS_BACKGROUND_LINKS then -- in range
        if ValidPanel(self.ActualBackground) then
            local link = GLOBALS_BACKGROUND_LINKS[bg]
            self.ActualBackground:SetHTML([[<style>
            html,body{padding:0;margin:0}
            img{
                border-radius: 16px;
                width: 100%;
                height: 100%;
                object-fit: cover;
                filter: saturate(135%) brightness(50%);
            }
            </style><img src="]] .. link .. [[" width="100%" height="100%">]])
            self.ActualBackground:SetVisible(true)
        end
    else
        if ValidPanel(self.ActualBackground) then
            self.ActualBackground:SetVisible(false)
        end
    end
end

function PANEL:Paint(w, h)
    local cur_bg = self.Player:GetNWString("animelife.playerstats.background", 1)
    if self.LastBackground ~= cur_bg then
        self:SetBackground(self.Player)
    end

    flux.update(RealFrameTime())
end

vgui.Register("animelife.scoreboard.profile.background", PANEL, "DPanel")