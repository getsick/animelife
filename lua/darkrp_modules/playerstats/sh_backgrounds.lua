module("playerstats", package.seeall)

BackgroundStore = {
    ["Все категории"] = {
    },
    ["Анимированные"] = {},
}

for i = 2, #GLOBALS_BACKGROUND_LINKS do
    if i == 17 then
        BackgroundStore["Анимированные"][i] = {
            Name = "АНИМИРОВАННЫЕ",
            Category = "Анимированные",
            Link = GLOBALS_BACKGROUND_LINKS[i],
            Price = 1100000
        }

        continue
    end

    BackgroundStore["Все категории"][i] = {
        Name = "",
        Category = "Все категории",
        Link = GLOBALS_BACKGROUND_LINKS[i],
        Price = 110000
    }
end