util.AddNetworkString("animelife.dailybonus.takereward")

sql.Query("CREATE TABLE IF NOT EXISTS animelife_daily(SteamID TINYTEXT, LastJoin INT, Level TINYINT, BonusGiven TINYINT)")

local meta = FindMetaTable("Player")

function meta:SetDailyLevel(lvl)
    self:SetNWInt("animelife.dailybonus.level", lvl)
end

function meta:SetDailyTaken(b)
    self:SetNWBool("animelife.dailybonus.taken", b)
end

local function save(ply, lvl)
    local query = sql.Query("SELECT * FROM animelife_daily WHERE SteamID = " .. SQLStr(ply:SteamID()))
    if istable(query) and #query > 0 then
        sql.Query("UPDATE animelife_daily SET LastJoin = " .. os.time() .. ", Level = " .. lvl .. ", BonusGiven = " .. 0 .. " WHERE SteamID = " .. SQLStr(ply:SteamID()))
    else
        sql.Query("INSERT INTO animelife_daily(SteamID, LastJoin, Level, BonusGiven) VALUES(" .. SQLStr(ply:SteamID()) .. ", " .. os.time() .. ", " .. lvl .. ", " .. 0 .. ")")
    end
end

hook.Add("PlayerInitialSpawn", "animelife.dailybonus", function(ply)
    timer.Simple(5, function()
        if IsValid(ply) and ply:IsPlayer() then
            local data = sql.Query("SELECT * FROM animelife_daily WHERE SteamID = " .. SQLStr(ply:SteamID()))
            if data ~= nil and data ~= false then
                local ply_time = tonumber(data[1].LastJoin)
                local current_time = os.time()
                local diff = os.difftime(current_time, ply_time)

                ply:SetDailyLevel(tonumber(data[1].Level))

                local day_time = 86400

                if diff >= day_time and diff < (day_time * 2) then
                    local next_lvl = ply:GetDailyLevel() + 1
                    if next_lvl <= 7 then
                        ply:SetDailyLevel(next_lvl)
                    else
                        ply:SetDailyLevel(1)
                    end

                    ply:SetDailyTaken(false)
                    
                    save(ply, ply:GetDailyLevel())
                else
                    if diff >= (day_time * 2) then
                        save(ply, 1)
                    end

                    ply:SetDailyTaken(false)
                end
            else
                save(ply, 1)
                ply:SetDailyTaken(false)
            end
        end
    end)
end)

net.Receive("animelife.dailybonus.takereward", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local reward_taken = sql.Query("SELECT BonusGiven FROM animelife_daily WHERE SteamID = " .. SQLStr(ply:SteamID()))
    if !reward_taken or reward_taken == nil then return end
    if !istable(reward_taken) then return end
    if tonumber(reward_taken[1].BonusGiven) == 1 then 
        DarkRP.notify(ply, 1, 5, "Вы уже забрали бонус за этот день.")
        return 
    end

    local day = ply:GetDailyLevel() or 1
    sql.Query("UPDATE animelife_daily SET BonusGiven = " .. 1 .. " WHERE SteamID = " .. SQLStr(ply:SteamID()))

    dailybonus.Bonuses[day].Function(ply)

    ply:SetDailyTaken(true)
end)