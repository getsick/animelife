--[[
  Table:
    [day_of_week] = {
        Function (function) = give function
        Name (string) = displayed text in menu
        Icon (material) = icon displayed in menu
        VIP (boolean) = do x2 for vip
    }
]]

module("dailybonus", package.seeall)

local icon_money = Material("animelife/dailybonus/icon_money.png")
local icon_weapon = Material("animelife/dailybonus/icon_weapon.png")
local icon_pet = Material("animelife/dailybonus/icon_pet.png")
local icon_heart = Material("animelife/dailybonus/icon_heart.png")

Bonuses = {
    [1] = {
        Name = "5000¥",
        VIP = true,
        Icon = icon_money,
        Function = function(ply)
            ply:addMoney(ply:IsVIP() and 5000 * 2 or 5000)
        end
    },
    [2] = {
        Name = "10000¥",
        Icon = icon_money,
        Function = function(ply)
            ply:addMoney(10000)
        end
    },
    [3] = {
        Name = "15000¥",
        Icon = icon_money,
        VIP = true,
        Function = function(ply)
            ply:addMoney(ply:IsVIP() and 15000 * 2 or 15000)
        end
    },
    [4] = {
        Name = "2500 XP",
        Icon = icon_heart,
        Function = function(ply)
            ply:AddTreeXP(5000)
        end
    },
    [5] = {
        Name = "5000 XP",
        Icon = icon_heart,
        VIP = true,
        Function = function(ply)
            ply:AddTreeXP(ply:IsVIP() and 5000 * 2 or 5000)
        end
    },
    [6] = {
        Name = "7500 XP",
        Icon = icon_heart,
        Function = function(ply)
            ply:AddTreeXP(7500)
        end
    },
    [7] = {
        Name = "10000 XP",
        Icon = icon_heart,
        VIP = true,
        Function = function(ply)
            ply:AddTreeXP(ply:IsVIP() and 10000 * 2 or 10000)
        end
    },
}

local meta = FindMetaTable("Player")

function meta:GetDailyLevel()
    return self:GetNWInt("animelife.dailybonus.level", 1)
end

function meta:GetDailyTaken()
    return self:GetNWBool("animelife.dailybonus.taken", true)
end