local PANEL = {}
local PANEL_BG_COLOR = Color(37, 37, 37)
local PANEL_MATERIAL_BACKGROUND_DETAIL = Material("animelife/dailybonus/detail_background.png")
local PANEL_MATERIAL_HEADER = Material("animelife/dailybonus/background_header.png")
local PANEL_MATERIAL_DAY = Material("animelife/dailybonus/icon_day.png")
local PANEL_MATERIAL_BUTTON = Material("animelife/dailybonus/button_get.png")
local PANEL_MATERIAL_CLOSE = Material("animelife/dailybonus/icon_close.png")
local PANEL_MATERIAL_GIFTS = Material("animelife/dailybonus/detail_gifts.png")
local PANEL_MATERIAL_DAY_INFO = Material("animelife/dailybonus/day_info.png")
local PANEL_MATERIAL_VIP = Material("animelife/dailybonus/vip.png")

surface.CreateFont("animelife.dailybonus.button", {font = "Exo 2 SemiBold", size = 21, weight = 600, extended = true})
surface.CreateFont("animelife.dailybonus.day", {font = "Exo 2 SemiBold", size = 43, weight = 600, extended = true})
surface.CreateFont("animelife.dailybonus.day_shadow", {font = "Exo 2 SemiBold", size = 43, weight = 600, blursize = 2, extended = true})
surface.CreateFont("animelife.dailybonus.title", {font = "Exo 2 SemiBold", size = 28, weight = 600, extended = true})
surface.CreateFont("animelife.dailybonus.title_shadow", {font = "Exo 2 SemiBold", size = 28, weight = 600, blursize = 2, extended = true})
surface.CreateFont("animelife.dailybonus.day_small", {font = "Exo 2 SemiBold", size = 21, weight = 600, extended = true})
surface.CreateFont("animelife.dailybonus.bonus_name", {font = "Exo 2 SemiBold", size = 26, weight = 600, extended = true})

function PANEL:Init()
    self.PointsColor = Color(224, 255, 184)

    self.GetButton = vgui.Create("DButton", self)
    self.GetButton:SetText("")
    self.GetButton.Paint = self.GetButtonPaint
    self.GetButton.DoClick = function()
        surface.PlaySound("click02.wav")

        if LocalPlayer():GetDailyTaken() then return end
        net.Start("animelife.dailybonus.takereward")
        net.SendToServer()
    end

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("")
    self.CloseButton.Paint = self.PaintCloseButton
    self.CloseButton.DoClick = function()
        if ValidPanel(self) then
            self:Remove()
        end
    end
end

function PANEL:PerformLayout(w, h)
    self.GetButton:SetPos((w - ui.y(519)) / 2, h - ui.y(61) - ui.y(49))
    self.GetButton:SetSize(ui.y(519), ui.y(61))

    self.CloseButton:SetPos(w - 24 - 56, 54)
    self.CloseButton:SetSize(24, 24)
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(PANEL_BG_COLOR)
    surface.DrawRect(0, 0, w, h)

    ui.DrawMaterial(PANEL_MATERIAL_BACKGROUND_DETAIL, 0, 0, w, h, Color(255, 255, 255, 45))

    ui.DrawMaterial(PANEL_MATERIAL_HEADER, (w - 788) / 2, 37, 788, 209, Color(255, 255, 255))

    ui.DrawMaterial(PANEL_MATERIAL_GIFTS, (w - ui.y(637)) / 2, h - ui.y(171), ui.y(637), ui.y(171), Color(255, 255, 255, 175))

    draw.SimpleText("Ежедневные бонусы", "animelife.dailybonus.title_shadow", w / 2, 78 + 1, Color(0, 0, 0, 75), 1)
    draw.SimpleText("Ежедневные бонусы", "animelife.dailybonus.title", w / 2, 78, Color(255, 255, 255), 1)

    local day = LocalPlayer():GetDailyLevel() or 1

    draw.SimpleText("День " .. day, "animelife.dailybonus.day_shadow", w / 2, 139 + 1, Color(0, 0, 0, 75), 1)
    draw.SimpleText("День " .. day, "animelife.dailybonus.day", w / 2, 139, Color(218, 244, 255), 1)

    local x = (w - ((28 + 10) * 7)) / 2
    local info_x, info_y = (w - ui.x((250 + 23) * 3)) / 2, 291
    for i = 1, 7 do
        ui.DrawMaterial(PANEL_MATERIAL_DAY, x, 196, 28, 12, i > day and Color(86, 86, 86) or Color(81, 182, 255))

        ui.Smooth(true, true)
            draw.SimpleText("День " .. i, "animelife.dailybonus.day_small", info_x + ui.x(125), info_y, Color(217, 217, 217), 1)
            ui.DrawMaterial(PANEL_MATERIAL_DAY_INFO, info_x, info_y + ui.y(40), ui.y(250), ui.y(131), Color(255, 255, 255, i == day and 225 or 75))

            local bonus_info = dailybonus.Bonuses[i]
            draw.SimpleText(bonus_info.Name, "animelife.dailybonus.bonus_name", info_x + ui.x(125), info_y + ui.y(114) + ui.y(21), Color(255, 255, 255), 1)

            ui.DrawMaterial(bonus_info.Icon, info_x + ui.x(98), info_y + ui.y(35) + ui.y(21), ui.y(54), ui.y(54), Color(255, 255, 255))

            if bonus_info.VIP then
                ui.DrawMaterial(PANEL_MATERIAL_VIP, info_x + ui.y(250) - ui.x(54) - ui.x(16), info_y + ui.y(100), 54, 13, Color(255, 255, 255))
            end
        ui.Smooth(false, true)

        if i % 3 ~= 0 then
            info_x = info_x + ui.x(250) + ui.x(23)
        else
            info_y = info_y + ui.y(131) + ui.y(14) + ui.y(40)

            if i == 6 then
                info_x = (w - ui.x(250 + 23)) / 2
            else
                info_x = (w - ui.x((250 + 23) * 3)) / 2
            end
        end

        x = x + 28 + 10
    end
end

function PANEL:GetButtonPaint(w, h)
    ui.Smooth(true, true)
        ui.DrawMaterial(PANEL_MATERIAL_BUTTON, 0, 0, w, h, Color(255, 255, 255))
    ui.Smooth(false, true)

    local day = LocalPlayer():GetDailyLevel() or 1
    local text = "забрать награду за " .. day .. " день"
    if LocalPlayer():GetDailyTaken() then
        text = "вы уже получили награду за " .. day .. " день"
    end
    draw.SimpleText(text, "animelife.dailybonus.button", w / 2 + 1, h / 2 - 2, Color(0, 0, 0, 75), 1, 1)
    draw.SimpleText(text, "animelife.dailybonus.button", w / 2, h / 2 - 3, Color(255, 255, 255), 1, 1)
end

function PANEL:PaintCloseButton(w, h)
    ui.DrawMaterial(PANEL_MATERIAL_CLOSE, 0, 0, w, h, Color(255, 255, 255))
end

vgui.Register("animelife.daily.gui", PANEL, "EditablePanel")

function OpenDailyBonusGUI()
    GLOBALS_DAILY_GUI = vgui.Create("animelife.daily.gui")
    GLOBALS_DAILY_GUI:SetSize(860, ScrH())
    GLOBALS_DAILY_GUI:Center()
    GLOBALS_DAILY_GUI:MakePopup()
    GLOBALS_DAILY_GUI:SetAlpha(0)
    GLOBALS_DAILY_GUI:AlphaTo(255, 0.15)
end