CreateClientConVar("animelife_buildmode", "0", false, true, "Change building mode")

local buttons = {
    {"Магазин фонов", function()
        OpenBackgroundStore()
    end},
    {"Магазин префиксов и статусов", function()
        OpenPrefixStore()
    end},
    {"Общий магазин", function()
        RunConsoleCommand("al_globalstore")
    end},
    {"Древо опыта", function()
        OpenLeveleeGUI()
    end},
    {"Ежедневные бонусы", function()
        OpenDailyBonusGUI()
    end},
    {"Создать группу", function()
        OpenInviteMenu()
    end},
    {"Вызвать полицию", function()
        Derma_StringRequest("Вызвать полицию", "Введите причину вызова. Сотрудникам полиции будет отправлена метка на ваше местоположение.", "", function(res)
            RunConsoleCommand("say", "/911 " .. res)
        end)
    end, function()
        return !LocalPlayer():isCP()
    end},
    {"Я донатил, хочу подарки", function()
        OpenDonateReturnPanel()
    end, function()
        if !donation then return false end
        
        return donation.IsSupporter(LocalPlayer():SteamID())
    end},
    {"Пожаловаться на игрока", function()
        OpenReportGUI()
    end},
    {"Меню движений и танцев", function()
        RunConsoleCommand("animelife_mmdgui")
    end},
    {"Третье лицо", function()
        RunConsoleCommand("thirdperson_enhanced_toggle")
    end},
    {"Включить режим строительства", function()
        if IsValid(itemdrop.PropModel) then
            RunConsoleCommand("animelife_itemdrop_stop")
        end
        
        -- local val = LocalPlayer():GetInfoNum("animelife_buildmode", 0)
        RunConsoleCommand("animelife_buildmode", 1)
    end, function()
        return !tobool(LocalPlayer():GetInfoNum("animelife_buildmode", 0))
    end},
    {"Выключить режим строительства", function()
        if IsValid(itemdrop.PropModel) then
            RunConsoleCommand("animelife_itemdrop_stop")
        end
        
        -- local val = LocalPlayer():GetInfoNum("animelife_buildmode", 0)
        RunConsoleCommand("animelife_buildmode", 0)
    end, function()
        return tobool(LocalPlayer():GetInfoNum("animelife_buildmode", 0))
    end},
    {"!spacer", function()
    end, function()
        return LocalPlayer():Team() == TEAM_MUSILY
    end},
    {"Положить оружие в инвентарь", function()
        RunConsoleCommand("al_invweapon")
    end, function()
        local wep = LocalPlayer():GetActiveWeapon()
        local function valid(class)
            for _, item in pairs(InventoryItems or {}) do
                if item.Class == class then
                    return true
                end
            end

            return false
        end

        return IsValid(wep) and valid(wep:GetClass())
    end},
    {"Купить патроны на текущее оружие", function()
        RunConsoleCommand("al_buyuniammo")
    end, function()
        local wep = LocalPlayer():GetActiveWeapon()
        return IsValid(wep) and wep:GetClass() ~= "weapon_physcannon" and wep:Clip1() >= 0
    end},
    {"Проиграть трек", function()
        Derma_StringRequest("Проиграть трек", "Вставьте ссылку на трек Soundcloud или путь до .mp3", "", function(res)
            RunConsoleCommand("animelife_boombox", res)
        end)
    end, function()
        return LocalPlayer():Team() == TEAM_MUSILY
    end},
    {"Остановить трек", function()
        RunConsoleCommand("animelife_boombox_stop")
    end, function()
        return LocalPlayer():Team() == TEAM_MUSILY and IsValid(LocalPlayer().AudioStation)
    end},
    {"Разорвать связи", function()
        RunConsoleCommand("al_yandere_tiescut")
    end, function()
        local valid_yandere = IsValid(LocalPlayer():GetNWEntity("animelife.jobabilities.yandere"))
        local valid_cutie = IsValid(LocalPlayer():GetNWEntity("animelife.jobabilities.yandere.cutie"))
        return valid_yandere or valid_cutie
    end},
    {"Начать комендантский час", function()
        RunConsoleCommand("say", "/lockdown")
    end, function()
        return !GetGlobalBool("DarkRP_Lockdown") and LocalPlayer():Team() == TEAM_IMPERATOR
    end},
    {"Закончить комендантский час", function()
        RunConsoleCommand("say", "/unlockdown")
    end, function()
        return GetGlobalBool("DarkRP_Lockdown") and LocalPlayer():Team() == TEAM_IMPERATOR
    end},
    {"Поставить доску с законами", function()
        RunConsoleCommand("say", "/placelaws")
    end, function()
        return LocalPlayer():Team() == TEAM_IMPERATOR
    end},
    {"Добавить закон", function()
        Derma_StringRequest("Добавить закон", "Введите новый закон", "", function(res)
            RunConsoleCommand("say", "/addlaw " .. res)
        end)
    end, function()
        return LocalPlayer():Team() == TEAM_IMPERATOR
    end},
    {"Удалить закон", function()
        Derma_StringRequest("Удалить закон", "Введите номер закона, который вы хотите удалить", "", function(res)
            RunConsoleCommand("say", "/removelaw " .. res)
        end)
    end, function()
        return LocalPlayer():Team() == TEAM_IMPERATOR
    end},
    {"Запросить обыск", function()
        local dmenu = DermaMenu(nil, g_ContextMenu)

        for _, ply in pairs(player.GetHumans()) do
            if ply == LocalPlayer() then continue end
            dmenu:AddOption(ply:Nick() .. " [" .. team.GetName(ply:Team()) .. "]", function()
                Derma_StringRequest(ply:Nick() .. ": Запросить обыск", "Введите причину обыска", "", function(res)
                    RunConsoleCommand("say", "/warrant " .. string.Split(ply:Nick(), " ")[1] .. " " .. res)
                end)
            end)
        end

        dmenu:Open()
    end, function()
        return LocalPlayer():isCP()
    end},
    {"Отозвать обыск", function()
        local dmenu = DermaMenu(nil, g_ContextMenu)

        for _, ply in pairs(player.GetHumans()) do
            if ply == LocalPlayer() then continue end
            dmenu:AddOption(ply:Nick() .. " [" .. team.GetName(ply:Team()) .. "]", function()
                RunConsoleCommand("say", "/unwarrant " .. string.Split(ply:Nick(), " ")[1])
            end)
        end

        dmenu:Open()
    end, function()
        return LocalPlayer():isCP()
    end},
    {"Начать розыск", function()
        local dmenu = DermaMenu(nil, g_ContextMenu)

        for _, ply in pairs(player.GetHumans()) do
            if ply == LocalPlayer() then continue end
            dmenu:AddOption(ply:Nick() .. " [" .. team.GetName(ply:Team()) .. "]", function()
                Derma_StringRequest(ply:Nick() .. ": Начать розыск", "Введите причину розыска", "", function(res)
                    RunConsoleCommand("say", "/wanted " .. string.Split(ply:Nick(), " ")[1] .. " " .. res)
                end)
            end)
        end

        dmenu:Open()
    end, function()
        return LocalPlayer():isCP()
    end},
    {"Снять розыск", function()
        local dmenu = DermaMenu(nil, g_ContextMenu)

        for _, ply in pairs(player.GetHumans()) do
            if ply == LocalPlayer() then continue end
            if !ply:getDarkRPVar("wanted") then continue end
            dmenu:AddOption(ply:Nick() .. " [" .. team.GetName(ply:Team()) .. "]", function()
                RunConsoleCommand("say", "/unwanted " .. string.Split(ply:Nick(), " ")[1])
            end)
        end

        dmenu:Open()
    end, function()
        return LocalPlayer():isCP()
    end},
    {"Установить цену заказа", function()
        Derma_StringRequest("Изменить цену за заказ", "Введите новую цену", "", function(res)
            RunConsoleCommand("say", "/hitprice " .. res)
        end)
    end, function()
        return LocalPlayer():Team() == TEAM_KILLER
    end},
    {"Изменить положение аксессуаров", function()
        inventory.accessories.EnterEditingMode(LocalPlayer())
    end, function()
        return LocalPlayer().ClientsideModels and !table.IsEmpty(LocalPlayer().ClientsideModels)
    end},
    {"Открыть логи", function()
        RunConsoleCommand("say", "!blogs")
    end, function()
        return LocalPlayer():IsAdmin()
    end},
    {"Открыть админ-панель", function()
        RunConsoleCommand("say", "!menu")
    end, function()
        return LocalPlayer():IsAdmin()
    end},
}

local function open_context()
    local derma_menu = DermaMenu(false, g_ContextMenu)

    for _, b in pairs(buttons) do
        if isfunction(b[3]) and !b[3]() then
            continue    
        end

        if b[1] == "!spacer" then
            derma_menu:AddSpacer()
            continue
        end

        derma_menu:AddOption(b[1], b[2])
    end

    derma_menu:Open(16, 0)
    derma_menu:CenterVertical()
end
concommand.Add("animelife_context", open_context)

local function get_players_around()
    local players_around = {}

    for _, ent in ipairs(ents.FindInSphere(LocalPlayer():GetPos(), 256)) do
        if !IsValid(ent) or !ent:IsPlayer() then continue end
        if ent == LocalPlayer() then continue end

        table.insert(players_around, ent)
    end

    return players_around
end

local function open_new_context()
    if ValidPanel(GLOBALS_MENUCONTEXT) then
        GLOBALS_MENUCONTEXT:Remove()
        GLOBALS_MENUCONTEXT = nil
    end

    GLOBALS_MENUCONTEXT = vgui.Create("animelife.menucontext", g_ContextMenu)
    GLOBALS_MENUCONTEXT:SetPos(0, ScrH() - 330)
    GLOBALS_MENUCONTEXT:SetSize(ScrW(), 261 + 70)
    -- GLOBALS_MENUCONTEXT:MakePopup()

    for _, b in pairs(buttons) do
        if isfunction(b[3]) and !b[3]() then
            continue
        end

        if b[1] == "!spacer" then
            continue
        end

        GLOBALS_MENUCONTEXT:AddButton(b[1], b[2])
    end

    if ValidPanel(GLOBALS_MENUCONTEXT_EXTRA) then
        GLOBALS_MENUCONTEXT_EXTRA:Remove()
        GLOBALS_MENUCONTEXT_EXTRA = nil
    end

    local around_players = get_players_around()
    if #around_players > 0 then
        GLOBALS_MENUCONTEXT_EXTRA = vgui.Create("animelife.menucontext.player", g_ContextMenu)
        GLOBALS_MENUCONTEXT_EXTRA:SetSize(170, 256)
        GLOBALS_MENUCONTEXT_EXTRA:SetPos(24, ScrH() - 256 - 80)
        for _, ply in pairs(around_players) do
            GLOBALS_MENUCONTEXT_EXTRA:AddPlayer(ply)
        end
        GLOBALS_MENUCONTEXT_EXTRA:InvalidateLayout(true)
    end

    if ValidPanel(GLOBALS_MENUCONTEXT_PARTY) then
        GLOBALS_MENUCONTEXT_PARTY:Remove()
        GLOBALS_MENUCONTEXT_PARTY = nil
    end

    local in_party = LocalPlayer():GetNWString("animelife.group", NULL) ~= NULL and LocalPlayer():GetNWString("animelife.group") ~= "-fds-fsd-f-dsf-sd-fds-fds-f-dsf-s-df"
    if in_party then
        GLOBALS_MENUCONTEXT_PARTY = vgui.Create("animelife.menucontext.party", g_ContextMenu)
        GLOBALS_MENUCONTEXT_PARTY:SetSize(128, 48)
        GLOBALS_MENUCONTEXT_PARTY:SetPos(24, ScrH() - 140)
    end
end
concommand.Add("animelife_context2", open_new_context)

hook.Add("OnContextMenuOpen", "animelife.contextmenu", function()
    LocalPlayer():ConCommand("animelife_context2")
end)