local emotes = {
    -- Stops currently running dance
    "Остановить_танец",
    -- Full Dance With Music
    "original_dance1", "original_dance2", "original_dance3", "original_dance4", "original_dance5", "original_dance6", "original_dance7",
    "original_dance8", "original_dance9", "original_dance10", "original_dance11", "original_dance12", "original_dance13", "original_dance14",
    -- Fortnite Emotes
    "f_Accolades", "f_Acrobatic_Superhero", "f_AerobicChamp", "f_AfroHouse", "f_AirHorn", "f_ArmUp", "f_Banana", "f_BandOfTheFort",
    "f_Basketball", "f_Battle_Horn", "f_Bendi", "f_Blow_Kiss", "f_Boogie_Down", "f_Breakboy", "f_Calculated", "f_Capoeira", "f_Cheerleader",
    "f_Chicken", "f_Chug", "f_ClapperBoard", "f_Conga", "f_Cool_Robot", "f_Cowbell", "f_CowboyDance", "f_Crackshot", "f_CrazyFeet", "f_Cross_Legs",
    "f_Cry", "f_Dance_NoBones", "f_Dance_Shoot", "f_Dance_SwipeIt", "f_Dancing_Girl", "f_Disagree", "f_DJ_Drop", "f_DreamFeet", "f_Drum_Major",
    "f_Dust_Off_Hands", "f_Dust_Off_Shoulders", "f_Eastern_Bloc", "f_Eating_Popcorn", "f_ElectroSwing", "f_Epic_Sax_Guy",
    "f_FancyFeet", "f_FancyWorkout", "f_Festivus", "f_FireStick", "f_Fistpump_Celebration", "f_Flamenco", "f_FlippnSexy", "f_Floppy",
    "f_Fresh", "f_Funk_Time", "f_GlowStickDance", "f_Golfer_Clap", "f_GrooveJam", "f_Halloween_Candy", "f_HandSignals", "f_Happy_Wave",
    "f_Headbanger", "f_Head_Bounce", "f_Heelclick", "f_HillBilly_Shuffle", "f_HiLoWave", "f_HiLowWave", "f_HipHop_01", "f_Hip_Hop",
    "f_Hip_Hop_Gestures_1", "f_Hip_Hop_S7", "f_Hitchhiker", "f_Hooked", "f_Hotstuff", "f_Hula", "f_HulaHoop", "f_I_Break_You",
    "f_IceKing", "f_IDontKnow", "f_Jammin", "f_Jazz_Dance", "f_Jazz_Hands", "f_Juggler", "f_Kitty_Cat", "f_KoreanEagle", "f_Kpop_02",
    "f_Kpop_03", "f_KPop_Dance03", "f_Laugh", "f_Lazy_Shuffle", "f_LivingLarge", "f_Llama", "f_Look_At_This", "f_Loser_Dance", "f_Luchador",
    "f_Make_It_Rain_V2", "f_Maracas", "f_Marat", "f_MartialArts", "f_Mask_Off", "f_Mello", "f_Mic_Drop", 
    -- MMD Emotes
    "mmd_deepbluetown1", "mmd_deepbluetown2", "mmd_deepbluetown3", "mmd_deepbluetown4", "mmd_deepbluetown5", "mmd_deepbluetown6", "mmd_deepbluetown7", 
    "mmd_deepbluetown8", "mmd_deepbluetown9", "mmd_deepbluetown10", "mmd_deepbluetown11", "mmd_deepbluetown12", "mmd_deepbluetown13", "mmd_deepbluetown14",
    "mmd_deepbluetown15", "mmd_marine1", "mmd_marine2", "mmd_marine3", "mmd_marine4", "mmd_marine5", "mmd_marine6", "mmd_marine7", "mmd_marine8", "mmd_marine9",
    "mmd_marine10", "mmd_marine11", "mmd_marine12", "mmd_marine13", "mmd_marine14", "mmd_marine15", "mmd_suki_yuki1", "mmd_suki_yuki2",
    "mmd_suki_yuki3", "mmd_suki_yuki4", "mmd_suki_yuki5", "mmd_suki_yuki6", "mmd_suki_yuki7", "mmd_suki_yuki8", "mmd_suki_yuki9",
    "mmd_suki_yuki10", "mmd_suki_yuki11", "mmd_suki_yuki12", "mmd_suki_yuki13", "mmd_suki_yuki14", "mmd_suki_yuki15"
}

local function display_text(src)
    -- make command names more understandable in menu
    src = string.Replace(src, "f_", "")
    src = string.Split(src, "_")
    src = table.concat(src, " ", 1, #src)

    return src
end

local function ModernMMDGUI()
    if ValidPanel(GLOBALS_MMDGUI) then
        GLOBALS_MMDGUI:Remove()
        GLOBALS_MMDGUI = nil
        return
    end

    GLOBALS_MMDGUI = vgui.Create("animelife.mmdgui", g_ContextMenu)
    GLOBALS_MMDGUI:SetSize(ScrW(), ScrH() / 2)
end
concommand.Add("animelife_mmdgui", ModernMMDGUI)

-- Register panel
local PANEL = {}

function PANEL:Init()
    self.HorizontalScroll = vgui.Create("DHorizontalScroller", self)

    self.ButtonLayout = vgui.Create("DIconLayout", self.HorizontalScroll)
    self.ButtonLayout:SetLayoutDir(LEFT)

    for _, cmd in pairs(emotes) do
        local btn = vgui.Create("DButton", self.ButtonLayout)
        btn:SetText("")
        btn:SetSize(ui.x(150), ui.y(36))
        btn.Alpha = math.Rand(0.25, 1)
        btn.Paint = function(pnl, w, h)
            surface.SetDrawColor(0, 0, 0, pnl:IsHovered() and 225 or 75 * pnl.Alpha)
            surface.DrawRect(0, 0, w, h)

            draw.SimpleText(display_text(cmd), "animelife.menucontext", w / 2, h / 2, color_white, 1, 1)
        end
        btn.DoClick = function()
            if cmd == "Остановить_танец" then
                RunConsoleCommand("stopdance")
                return    
            end
            
            if string.StartWith(cmd, "original_dance") then
                local time = LocalPlayer():SequenceDuration(LocalPlayer():LookupSequence(cmd))
                local ang = LocalPlayer():EyeAngles()
                StartMMDCommand(LocalPlayer(), time, ang, cmd)
            else
                RunConsoleCommand(cmd)
            end
        end
    end
end

function PANEL:PerformLayout(w, h)
    self.HorizontalScroll:Dock(FILL)

    self.ButtonLayout:SetSize(w, h)
    self.ButtonLayout:SetPos(0, 0)
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(0, 0, 0, 225)
    surface.DrawRect(0, 0, w, h)
end

vgui.Register("animelife.mmdgui", PANEL, "EditablePanel")