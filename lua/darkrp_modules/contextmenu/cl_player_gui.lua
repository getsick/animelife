local PANEL = {}

local ACTIONS = {
    [1] = {
        Name = "Передать деньги",
        Function = function(ply)
            Derma_StringRequest("Передать деньги " .. ply:Nick(), "Введите количество денег для передачи (игрок должен находиться под прицелом)", "", function(arg)
                RunConsoleCommand("say", "/give " .. arg)
            end)
        end
    },
    [2] = {
        Name = "Дать доступ к пропам",
        Function = function(ply)
            -- RunConsoleCommand("FPP_SetBuddy", ply:UserID(), "1", "1", "1", "1", "1")
            FPP.SetBuddyMenu(ply:SteamID(), ply:Nick())
        end
    },
    [3] = {
        Name = "Выдать лицензию на оружие",
        ShouldShow = function()
            return LocalPlayer():isCP()
        end,
        Function = function(ply)
            RunConsoleCommand("say", "/givelicense")
        end
    },
    [4] = {
        Name = "Открыть профиль Steam",
        Function = function(ply)
            ply:ShowProfile()
        end
    },
    [5] = {
        Name = "Выключить голос (локально)",
        ShouldShow = function(ply)
            return !ply:IsMuted()
        end,
        Function = function(ply)
            ply:SetMuted(true)
        end
    },
    [6] = {
        Name = "Включить голос (локально)",
        ShouldShow = function(ply)
            return ply:IsMuted()
        end,
        Function = function(ply)
            ply:SetMuted(false)
        end
    },
    [7] = {
        Name = "Вызвать на дуэль",
        Function = function(ply)
            OpenDuelInviteGUI(ply)
        end
    },
}

function PANEL:Init()
    self.Players = {}

    self.Scroll = vgui.Create("DScrollPanel", self)
    self.Scroll.Buttons = {}
end

function PANEL:AddPlayer(ply)
    table.insert(self.Players, ply)

    local btn = vgui.Create("DButton", self.Scroll)
    btn:SetText("")
    btn:Dock(TOP)
    btn.Paint = function(pnl, w, h)
        surface.SetDrawColor(31, 31, 31, pnl:IsHovered() and 255 or 170)
        surface.DrawRect(0, 0, w, h)

        local nick = "Disconnected"
        if IsValid(ply) and ply:IsPlayer() then
            nick = ply:Nick()
        end

        draw.SimpleText(nick, "animelife.menucontext", w / 2, h / 2, color_white, 1, 1)
    end
    btn.Think = function()
        if !IsValid(ply) then
            btn:Remove()
            table.RemoveByValue(self.Players, ply)
            table.RemoveByValue(self.Scroll.Buttons, btn)
        else
            if ply:GetPos():DistToSqr(LocalPlayer():GetPos()) > 256^2 then
                btn:Remove()
                table.RemoveByValue(self.Players, ply)
                table.RemoveByValue(self.Scroll.Buttons, btn)
            end
        end
    end
    btn.DoClick = function()
        local dmenu = DermaMenu()

        for _, act in pairs(ACTIONS) do
            if isfunction(act.ShouldShow) and !act.ShouldShow(ply) then continue end
            dmenu:AddOption(act.Name, function()
                act.Function(ply)
            end)
        end

        dmenu:Open()
    end

    self.Scroll:AddItem(btn)

    table.insert(self.Scroll.Buttons, btn)
end

function PANEL:GetPlayers()
    return self.Players
end

function PANEL:PerformLayout(w, h)
    self:SetTall(math.min(#self.Scroll.Buttons * 30, 128))
    local x = self:GetPos()
    local party_y = 0
    if ValidPanel(GLOBALS_MENUCONTEXT_PARTY) then
        party_y = GLOBALS_MENUCONTEXT_PARTY:GetTall() + 30
    end
    self:SetPos(x, ScrH() - h - party_y - 80)

    self.Scroll:SetPos(0, 0)
    self.Scroll:SetSize(w, h)

    local v_bar = self.Scroll:GetVBar()
    v_bar:SetWide(4)

    for _, btn in pairs(self.Scroll.Buttons) do
        btn:SetSize(170, 30)
    end
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(0, 0, 0, 220)
    surface.DrawRect(0, 0, w, h)
end

function PANEL:Think()
    if table.IsEmpty(self:GetPlayers()) then
        self:Remove()
        self = nil
    end
end

vgui.Register("animelife.menucontext.player", PANEL, "EditablePanel")