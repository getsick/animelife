local price = 150 -- yen per purchase

concommand.Add("al_buyuniammo", function(ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local wep = ply:GetActiveWeapon()
    if !IsValid(wep) or !wep:IsWeapon() then return end
    if wep:GetClass() == "weapon_physcannon" then return end

    local ammo = wep:GetPrimaryAmmoType()
    local left = wep:Clip1()
    if left < 0 then return end
    if !ply:canAfford(price) then 
        DarkRP.notify(ply, 1, 3, "Сожалею, не по карману.")
        return 
    end

    local amount = 100
    if ammo == 25 or ammo == 7 then
        amount = 20
    elseif ammo == 6 or ammo == 26 then
        amount = 30
    end

    ply:GiveAmmo(amount, ammo)

    ply:addMoney(-price)
end)