local PANEL = {}

local color_shadow = Color(0, 0, 0, 75)

function PANEL:Init()
    self.LeaveButton = vgui.Create("DButton", self)
    self.LeaveButton:SetText("")
    self.LeaveButton.Paint = function(pnl, w, h)
        surface.SetDrawColor(31, 31, 31)
        surface.DrawRect(0, 0, w, h)

        draw.SimpleText("Выйти", "animelife.menucontext", w / 2, h / 2, Color(255, 255, 255), 1, 1)
    end
    self.LeaveButton.DoClick = function()
        RunConsoleCommand("say", "/partyleave")

        self:Remove()
        self = nil
    end
end

function PANEL:PerformLayout(w, h)
    self.LeaveButton:Dock(FILL)
    self.LeaveButton:DockMargin(0, 24, 0, 0)
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(0, 0, 0, 220)
    surface.DrawRect(0, 0, w, h)

    draw.SimpleText("Вы состоите в группе", "animelife.menucontext", w / 2 + 2, 4, color_shadow, 1)
    draw.SimpleText("Вы состоите в группе", "animelife.menucontext", w / 2, 4, color_white, 1)
end

vgui.Register("animelife.menucontext.party", PANEL, "EditablePanel")