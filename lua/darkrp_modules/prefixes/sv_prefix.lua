util.AddNetworkString("animelife.prefixes.buy")

MySQLite.begin()

MySQLite.query([[
    CREATE TABLE IF NOT EXISTS al_prefixstatus(
        sid TINYTEXT NOT NULL PRIMARY KEY,
        prefix TINYINT NOT NULL,
        status TINYINT NOT NULL,
        prefixes TINYTEXT NOT NULL,
        statuses TINYTEXT NOT NULL
    );
]])

local meta = FindMetaTable("Player")

function meta:AddPrefix(num, save)
    local last = self:GetNWString("animelife.prefixes.list", "")
    local new = last .. "," .. num
    self:SetNWString("animelife.prefixes.list", new)

    if save then
        MySQLite.query([[SELECT prefix
        FROM al_prefixstatus
        where sid = ]] .. MySQLite.SQLStr(self:SteamID()) .. ";", function(data)
            if !data then
                MySQLite.query([[REPLACE INTO al_prefixstatus VALUES(]] ..
                MySQLite.SQLStr(self:SteamID()) .. [[, ]] ..
                0 .. [[, ]] ..
                0  .. [[, ]] ..
                MySQLite.SQLStr(new) .. [[, ]] ..
                MySQLite.SQLStr("") .. ");")
            end

            MySQLite.query([[UPDATE al_prefixstatus SET prefixes = ]] .. MySQLite.SQLStr(new) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(self:SteamID()) .. ";")
        end, function(e, q)
            print("Database Error:", e, q)
        end)
    end
end

function meta:AddStatus(num, save)
    local last = self:GetNWString("animelife.status.list", "")
    local new = last .. "," .. num
    self:SetNWString("animelife.status.list", new)

    if save then
        MySQLite.query([[SELECT status
        FROM al_prefixstatus
        where sid = ]] .. MySQLite.SQLStr(self:SteamID()) .. ";", function(data)
            if !data then
                MySQLite.query([[REPLACE INTO al_prefixstatus VALUES(]] ..
                MySQLite.SQLStr(self:SteamID()) .. [[, ]] ..
                0 .. [[, ]] ..
                0  .. [[, ]] ..
                MySQLite.SQLStr("") .. [[, ]] ..
                MySQLite.SQLStr(new) .. ");")
            end

            MySQLite.query([[UPDATE al_prefixstatus SET statuses = ]] .. MySQLite.SQLStr(new) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(self:SteamID()) .. ";")
        end, function(e, q)
            print("Database Error:", e, q)
        end)
    end
end

function meta:SetPrefix(id)
    self:SetNWInt("animelife.customizations.prefix", id)

    if !id then id = 0 end

    MySQLite.query([[SELECT status
    FROM al_prefixstatus
    where sid = ]] .. MySQLite.SQLStr(self:SteamID()) .. ";", function(data)
        if !data then
            MySQLite.query([[REPLACE INTO al_prefixstatus VALUES(]] ..
            MySQLite.SQLStr(self:SteamID()) .. [[, ]] ..
            tonumber(id) .. [[, ]] ..
            0  .. [[, ]] ..
            MySQLite.SQLStr("") .. [[, ]] ..
            MySQLite.SQLStr("") .. ");")
        end

        MySQLite.query([[UPDATE al_prefixstatus SET prefix = ]] .. tonumber(id) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(self:SteamID()) .. ";")
    end, function(e, q)
        print("Database Error:", e, q)
    end)
end

function meta:SetOverheadStatus(id)
    self:SetNWInt("animelife.customizations.status", id)

    if !id then id = 0 end
    
    MySQLite.query([[SELECT status
    FROM al_prefixstatus
    where sid = ]] .. MySQLite.SQLStr(self:SteamID()) .. ";", function(data)
        if !data then
            MySQLite.query([[REPLACE INTO al_prefixstatus VALUES(]] ..
            MySQLite.SQLStr(self:SteamID()) .. [[, ]] ..
            0 .. [[, ]] ..
            tonumber(id)  .. [[, ]] ..
            MySQLite.SQLStr("") .. [[, ]] ..
            MySQLite.SQLStr("") .. ");")
        end

        MySQLite.query([[UPDATE al_prefixstatus SET status = ]] .. tonumber(id) .. [[ WHERE sid = ]] .. MySQLite.SQLStr(self:SteamID()) .. ";")
    end, function(e, q)
        print("Database Error:", e, q)
    end)
end

net.Receive("animelife.prefixes.buy", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end

    local t = net.ReadInt(3)
    local e = net.ReadInt(32)
    if t > 2 or t < 1 then return end

    local val = prefix.List[t == 1 and "chat" or "status"][e]
    if !istable(val) then return end

    -- if val.Expires then
    --     if !prefix.CanBuy(t == 1 and "chat" or "status", prefix) then
    --         ply:ChatPrint("Предмет недоступен к покупке.")
    --         return
    --     end
    -- end

    if t == 2 then
        if !ply:HasStatus(e) then
            if !ply:canAfford(val.Price) then
                ply:ChatPrint("Недостаточно средств для покупки [" .. val.Name .. "]!")
                return
            end

            ply:AddStatus(e, true)

            ply:addMoney(-val.Price)

            achievements:MarkCompleted(ply, 10)
        else
            -- ply:SetNWInt("animelife.customizations.status", e)
            local set = ply:GetOverheadStatus() ~= e
            ply:SetOverheadStatus(set and e or false)

            DarkRP.notify(ply, 0, 3, (set and "Установлен" or "Снят") .. " статус " .. val.Name)
        end
    else
        if !ply:HasPrefix(e) then
            if !ply:canAfford(val.Price) then
                ply:ChatPrint("Недостаточно средств для покупки [" .. val.Name .. "]!")
                return
            end

            ply:AddPrefix(e, true)

            ply:addMoney(-val.Price)

            achievements:MarkCompleted(ply, 10)
        else
            -- ply:SetNWInt("animelife.customizations.prefix", e)
            local set = ply:GetPrefix() ~= e
            ply:SetPrefix(set and e or false)

            DarkRP.notify(ply, 0, 3, (set and "Установлен" or "Снят") .. " префикс [" .. val.Name .. "]")
        end
    end
end)

function RestorePrefixStatusData(ply)
    MySQLite.query([[
        SELECT prefix, status, prefixes, statuses
        FROM al_prefixstatus
        where sid = ]] .. MySQLite.SQLStr(ply:SteamID()) .. ";", function(data)
            if !data then return end
            
            if data[1].prefix and tonumber(data[1].prefix) ~= 0 then
                ply:SetNWInt("animelife.customizations.prefix", tonumber(data[1].prefix))
            end

            if data[1].status and tonumber(data[1].status) ~= 0 then
                ply:SetNWInt("animelife.customizations.status", tonumber(data[1].status))
            end

            if data[1].prefixes then
                ply:SetNWString("animelife.prefixes.list", data[1].prefixes)
            end

            if data[1].statuses then
                ply:SetNWString("animelife.status.list", data[1].statuses)
            end
        end, function(e, q)
            print("Database Error:", e, q)
        end)
end

hook.Add("PlayerInitialSpawn", "animelife.prefixstatus.db", function(ply)
    timer.Simple(5, function()
        if !IsValid(ply) or !ply:IsPlayer() then return end

        RestorePrefixStatusData(ply)
    end)
end)