module("prefix", package.seeall)

surface.CreateFont("animelife.OverHUD.Name", {font = "Exo 2", size = 75, weight = 700, extended = true})
surface.CreateFont("animelife.OverHUD.Job", {font = "Exo 2", size = 42, weight = 700, extended = true})

local volume = Material("animelife/hud/talking.png")

hook.Add("PostDrawTranslucentRenderables", "animelife.RenderPrefixStatus", function()
    if LocalPlayer():GetNWBool("animelife.administration.jail", false) then
        return
    end
    
    if LocalPlayer():GetNWBool("animelife.administration.ban", false) then
        return
    end

    for _, ply in ipairs(player.GetAll()) do
        if !IsValid(ply) or !ply:Alive() then continue end
        -- if !ply.GetOverheadStatus or !ply:GetOverheadStatus() then continue end
        if ply:GetViewEntity() == LocalPlayer() then continue end
        if ply:GetNWBool("animelife.jobabilities.mimic_nodraw", false) then continue end
        if ply:GetNWBool("animelife.administration.ban", false) then continue end
        if ply:GetNoDraw() then continue end

        local bone_head = ply:LookupBone("ValveBiped.Bip01_Head1") or 1
        bone_head = ply:GetBonePosition(bone_head)
        local pos = (util.IsValidModel(ply:GetModel()) and bone_head or Vector(0, 0, 64)) + Vector(0, 0, 17)
        local ang = Angle(0, EyeAngles().y - 90, 90)

        local dist = ply:GetPos():DistToSqr(LocalPlayer():GetViewEntity():GetPos())
        if dist > 256^2 then continue end
        local alpha = 1 - (1 * (dist / 256^2))
        local name = ply:Nick()
        local job = team.GetName(ply:Team())

        local class = classchooser.List[ply:Team()]
        if istable(class) then
            local actual_class = ply:GetNWInt("animelife.classchooser.class", -1)
            if actual_class ~= -1 then
                if istable(class[actual_class]) then
                    job = team.GetName(ply:Team()) .. " (" .. class[actual_class].Name .. ")"
                end
            end
        end

        local masked_ent = ply:GetNWEntity("animelife.jobabilities.mimic_masked")
        if IsValid(masked_ent) then
            name = masked_ent:Nick()
            job = team.GetName(masked_ent:Team())
        end
        
        if ply:GetNWBool("animelife.administration.jail", false) then
            name = name .. " [В джайле]"
        end

        cam.Start3D2D(pos, ang, 0.05)
            local beg_y = 0
            local status = ply:GetOverheadStatus()
            if isnumber(status) then
                status = List["status"][status]
                local status_text = StatusFormat(status.Name)
                local status_color = isfunction(status.Color) and status.Color() or status.Color
                status_color = ColorAlpha(status_color, 255 * alpha)

                draw.SimpleText(status_text, "animelife.OverHUD.Job", 2, 54 + 2, Color(0, 0, 0, 150 * alpha), 1)
                draw.SimpleText(status_text, "animelife.OverHUD.Job", 0, 54, status_color, 1)

                beg_y = beg_y - 75
            end
            
            if ply:getDarkRPVar("HasGunlicense") then
                draw.SimpleText("Есть лицензия на оружие", "animelife.OverHUD.Job", 2, beg_y + 54 + 2, Color(0, 0, 0, 150 * alpha), 1)
                draw.SimpleText("Есть лицензия на оружие", "animelife.OverHUD.Job", 0, beg_y + 54, Color(255, 255, 255, 255 * alpha), 1)

                beg_y = beg_y - 75
            end

            if ply:getDarkRPVar("wanted") then
                local reason = tostring(LocalPlayer():getDarkRPVar("wantedReason"))
                draw.SimpleText("В розыске: " .. reason, "animelife.OverHUD.Job", 2, beg_y + 54 + 2, Color(0, 0, 0, 150 * alpha), 1)
                draw.SimpleText("В розыске: " .. reason, "animelife.OverHUD.Job", 0, beg_y + 54, Color(255, 194, 194, 255 * alpha), 1)

                beg_y = beg_y - 75
            end

            if ply:IsSpeaking() then
                surface.SetDrawColor(255, 255, 255, 255 * alpha)
                surface.SetMaterial(volume)
                surface.DrawTexturedRect(-64 / 2, beg_y - 64, 64, 64)
            end

            draw.SimpleText(name, "animelife.OverHUD.Name", 2, beg_y + 2, Color(0, 0, 0, 150 * alpha), 1)
            draw.SimpleText(name, "animelife.OverHUD.Name", 0, beg_y, Color(255, 255, 255, 255 * alpha), 1)
            draw.SimpleText(job, "animelife.OverHUD.Job", 2, beg_y + 75 + 6 + 2, Color(0, 0, 0, 150 * alpha), 1)
            draw.SimpleText(job, "animelife.OverHUD.Job", 0, beg_y + 75 + 6, Color(245, 184, 255, 255 * alpha), 1)

            local job_table = LocalPlayer().getJobTable and LocalPlayer():getJobTable() or nil
            if job_table then
                local is_robot = job_table.category == "Прочее" and job_table.subcategory == "Роботы"
                if is_robot then
                    draw.SimpleText("Здоровье: " .. ply:Health(), "animelife.OverHUD.Job", -64 + 2, beg_y - 56 + 2, Color(0, 0, 0, 150 * alpha), 2)
                    draw.SimpleText("Здоровье: " .. ply:Health(), "animelife.OverHUD.Job", -64, beg_y - 56, Color(255, 255, 255, 255 * alpha), 2)

                    draw.SimpleText("Броня: " .. ply:Armor() .. "%", "animelife.OverHUD.Job", 64 + 2, beg_y - 56 + 2, Color(0, 0, 0, 150 * alpha))
                    draw.SimpleText("Броня: " .. ply:Armor() .. "%", "animelife.OverHUD.Job", 64, beg_y - 56, Color(255, 255, 255, 255 * alpha))

                    surface.SetDrawColor(51, 51, 51, 255 * alpha)
                    surface.DrawRect(-275 / 2, beg_y - 125, 275, 32)

                    if !ply.OverheadHealthAnim then
                        ply.OverheadHealthAnim = {
                            LastHealth = ply:Health(),
                            DisappearIn = 0,
                            Diff = 0
                        }
                    else
                        local hw = 275 * (ply:Health() / ply:GetMaxHealth())
                        surface.SetDrawColor(255, 255, 255, 255 * alpha)
                        surface.DrawRect(-275 / 2, beg_y - 125, hw, 32)
        
                        local dw = 275 * (ply.OverheadHealthAnim.Diff / ply:GetMaxHealth())
        
                        if ply.OverheadHealthAnim.LastHealth ~= ply:Health() then
                            ply.OverheadHealthAnim.DisappearIn = CurTime() + 0.25
                            ply.OverheadHealthAnim.Diff = (ply.OverheadHealthAnim.LastHealth - ply:Health())
                            ply.OverheadHealthAnim.LastHealth = ply:Health()
                        end
        
                        local box_alpha = (ply.OverheadHealthAnim.DisappearIn - CurTime()) / 0.25
                        surface.SetDrawColor(255, 75, 75, (255 * alpha) * box_alpha)
                        surface.DrawRect(-275 / 2 + hw, beg_y - 125, dw, 32)
                    end
                end
            end
        cam.End3D2D()
    end
end)