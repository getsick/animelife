local meta = FindMetaTable("Player")

module("prefix", package.seeall)

List = {
    ["chat"] = {
        [1] = {
            Name = "Новичок",
            Color = Color(0, 204, 255),
            Description = [[Для новичков.]],
            Price = 50000,
            Expires = {18585954, 19548473},
        },
        [2] = {
            Name = "Крутой",
            Color = Color(245, 184, 255),
            Description = [[Только для крутых.]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [3] = {
            Name = "Богатый",
            Color = Color(255, 102, 0),
            Description = [[Покажи всем насколько ты богат!]],
            Price = 500000,
            Expires = {18585954, 19548473},
        },
        [4] = {
            Name = "Ультра-богатый",
            Color = Color(255, 102, 0),
            Description = [[Тебе все мало?]],
            Price = 1500000,
            Expires = {18585954, 19548473},
        },
        [5] = {
            Name = "Миллиардер",
            Color = function() return HSVToColor((CurTime() * 25) % 360, 0.5, 1) end,
            Description = [[Может хватит?]],
            Price = 3000000,
            Expires = {18585954, 19548473},
        },
        [6] = {
            Name = "Анархист",
            Color = Color(255, 51, 0),
            Description = [[Для самых непослушных.]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [7] = {
            Name = "Гароу",
            Color = Color(204, 102, 255),
            Description = [[Злодей? Герой?]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [8] = {
            Name = "Повелитель",
            Color = Color(204, 102, 255),
            Description = [[Он же оверлорд.]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [9] = {
            Name = "zxc",
            Color = Color(102, 0, 102),
            Description = [[У меня нет проблем, кроме моей башки.]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [10] = {
            Name = "plusultra",
            Color = Color(0, 204, 255),
            Description = [[PLUS ULTRA!]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [11] = {
            Name = "Vagabond",
            Color = Color(51, 204, 51),
            Description = [[Бродяга.]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [12] = {
            Name = "1000-7",
            Color = Color(204, 0, 255),
            Description = [[Я умер, прости.]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [13] = {
            Name = "Лжец",
            Color = Color(255, 255, 102),
            Description = [[Притворщик.]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [14] = {
            Name = "Моб",
            Color = Color(204, 204, 255),
            Description = [[Психо?]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [15] = {
            Name = "Филантроп",
            Color = Color(27, 24, 237),
            Description = [[На филантропычах.]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [16] = {
            Name = "Микрочел",
            Color = Color(27, 24, 237),
            Description = [[Микрочелик. Микрочелик.]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [17] = {
            Name = "♂ Dungeon master",
            Color = Color(51, 204, 255),
            Description = [[♂♂♂]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [18] = {
            Name = "♂ Van",
            Color = Color(51, 204, 255),
            Description = [[♂♂♂]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [19] = {
            Name = "♂ Billy",
            Color = Color(51, 204, 255),
            Description = [[♂♂♂]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },
        [20] = {
            Name = "14.1",
            Color = Color(27, 24, 237),
            Description = [[14.1]],
            Price = 150000,
            Expires = {18585954, 19548473},
        },

    },
    ["status"] = {
        [1] = {
            Name = "Патрик",
            Color = function() return HSVToColor((CurTime() * 25) % 360, 0.5, 1) end,
            Description = [[Глупец или гений?]],
            Price = 800000,
        },
        [2] = {
            Name = "Моб",
            Color = Color(204, 204, 255),
            Description = [[Психо!]],
            Price = 150000,
        },
        [3] = {
            Name = "zxc",
            Color = Color(102, 0, 102),
            Description = [[У меня нет проблем, кроме моей башки.]],
            Price = 150000,
        },
        [4] = {
            Name = "Гароу",
            Color = Color(204, 102, 255),
            Description = [[Злодей? Герой?]],
            Price = 150000,
        },
        [5] = {
            Name = "Очень Крутой",
            Color = Color(102, 255, 102),
            Description = [[Только для очень крутых.]],
            Price = 250000,
        },
        [6] = {
            Name = "Вечно счастливый",
            Color = Color(102, 255, 102),
            Description = [[Настроение.]],
            Price = 150000,
        },
        [7] = {
            Name = "Вечно грустный",
            Color = Color(255, 102, 255),
            Description = [[Настроение.]],
            Price = 150000,
        },
        [8] = {
            Name = "Пудж",
            Color = Color(102, 51, 0),
            Description = [[Я как шаверма. Если неповезло, будешь страдать.]],
            Price = 150000,
        },
        [9] = {
            Name = "godlike",
            Color = Color(204, 0, 0),
            Description = [[]],
            Price = 150000,
        },
        [10] = {
            Name = "abuser",
            Color = Color(204, 0, 102),
            Description = [[Для самых непослушных нарушителей.]],
            Price = 150000,
        },
        [11] = {
            Name = "Банан",
            Color = Color(255, 255, 0),
            Description = [[Если ты не банан, даже не вздумай это покупать.]],
            Price = 150000,
        },
        [12] = {
            Name = "Сенсей",
            Color = Color(223, 24, 237),
            Description = [[Самый старший и уважаемый.]],
            Price = 150000,
        },
        [13] = {
            Name = "Семпай",
            Color = Color(223, 24, 237),
            Description = [[Наставник Кохая.]],
            Price = 150000,
        },
        [14] = {
            Name = "Отаку",
            Color = Color(223, 24, 237),
            Description = [[Сейчас я буду читать всю мангу!]],
            Price = 150000,
        },
        [15] = {
            Name = "Кохай",
            Color = Color(223, 24, 237),
            Description = [[Ученик Семпая.]],
            Price = 150000,
        },
        [16] = {
            Name = "♂ Ass we can",
            Color = Color(51, 204, 255),
            Description = [[♂♂♂]],
            Price = 150000,
        },
    },
}

function CanBuy(t, idx)
    local e = List[t][idx]
    local from, to = e.Expires[1], e.Expires[2]
    return os.time() >= from and os.time() <= to
end

function StatusFormat(status)
    if !status then return "" end
    return status
end

function meta:GetOverheadStatus()
    return self:GetNWInt("animelife.customizations.status", false)
end

function meta:GetPrefix()
    return self:GetNWInt("animelife.customizations.prefix", false)
end

function meta:HasPrefix(num)
    local prefix_list = self:GetNWString("animelife.prefixes.list", "")
    return table.HasValue(string.Split(prefix_list, ","), tostring(num))
end

function meta:HasStatus(num)
    local status_list = self:GetNWString("animelife.status.list", "")
    return table.HasValue(string.Split(status_list, ","), tostring(num))
end