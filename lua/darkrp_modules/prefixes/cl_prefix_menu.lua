local PANEL = {}

local background = Material("animelife/prefixes/background.png")
local icon_info = Material("animelife/prefixes/icon_info.png")
local icon_color = Material("animelife/prefixes/icon_color.png")
local icon_price = Material("animelife/prefixes/icon_price.png")
local icon_buy = Material("animelife/prefixes/icon_buy.png")
local icon_use = Material("animelife/prefixes/icon_use.png")
local icon_disable = Material("animelife/prefixes/icon_disable.png")

local function setup_fonts()
    surface.CreateFont("animelife.prefix.store.title", {font = "Exo 2 Bold", size = ScreenScale(19) / 3, weight = 700, extended = true})
    surface.CreateFont("animelife.prefix.store.info", {font = "Exo 2 Medium", size = ScreenScale(19) / 3, weight = 500, extended = true})
    surface.CreateFont("animelife.prefix.store.button", {font = "Exo 2 SemiBold", size = ScreenScale(19) / 3, weight = 600, extended = true})
    surface.CreateFont("animelife.prefix.store.item.name", {font = "Exo 2 Bold", size = ScreenScale(20) / 3, weight = 700, extended = true})
    surface.CreateFont("animelife.prefix.store.item.desc", {font = "Exo 2 SemiBold", size = ScreenScale(16) / 3, weight = 600, extended = true})
end
setup_fonts()

function PANEL:Init()
    setup_fonts()

    self:SetAlpha(0)
    self:AlphaTo(255, 0.25)

    self.CurrentTab = 1

    self.CloseButton = vgui.Create("DButton", self)
    self.CloseButton:SetText("")
    self.CloseButton.Paint = function() end
    self.CloseButton.DoClick = function()
        if ValidPanel(self) then
            self:Remove()
            self = nil
        end
    end

    local x, y = (ScrW() - (2 * ui.y(213) + ui.y(45))) / 2, ui.y(80)
    for i = 1, 2 do
        local cat_btn = vgui.Create("DButton", self)
        cat_btn:SetPos(x, y)
        cat_btn:SetSize(ui.y(213), ui.y(52))
        cat_btn:SetText("")
        cat_btn.Paint = function(panel, w, h)
            draw.RoundedBox(128, 0, 0, w, h, self.CurrentTab == i and Color(164, 117, 255) or Color(203, 179, 250))

            draw.SimpleText(i == 1 and "Префиксы" or "Статусы", "animelife.prefix.store.button", w / 2, h / 2, Color(255, 255, 255), 1, 1)
        end
        cat_btn.DoClick = function()
            self.CurrentTab = i

            if i == 2 then
                self.ScrollListPrefix:SetVisible(false)
                self.ScrollListStatus:SetVisible(true)
            else
                self.ScrollListPrefix:SetVisible(true)
                self.ScrollListStatus:SetVisible(false)
            end
        end

        x = x + ui.y(213) + ui.y(45)
    end

    self.ScrollListPrefix = vgui.Create("DScrollPanel", self)
    local v_bar = self.ScrollListPrefix:GetVBar()
    v_bar:SetWide(4)

    self.ScrollListStatus = vgui.Create("DScrollPanel", self)
    self.ScrollListStatus:SetVisible(false)
    v_bar = self.ScrollListStatus:GetVBar()
    v_bar:SetWide(4)

    for i = 1, 2 do
        x, y = ui.y(53), ui.y(22)
        for n, p in pairs(prefix.List[i == 1 and "chat" or "status"]) do
            local item_panel = vgui.Create("DPanel", i == 1 and self.ScrollListPrefix or self.ScrollListStatus)
            item_panel:SetPos(x, y)
            item_panel:SetSize(ui.y(508), ui.y(88))
            item_panel.Paint = function(panel, w, h)
                draw.RoundedBox(16, ui.y(6), ui.y(4), w - ui.y(6), h - ui.y(4), Color(255, 255, 255))

                ui.Smooth(true, true)
                    surface.SetDrawColor(isfunction(p.Color) and p.Color() or p.Color)
                    surface.SetMaterial(icon_color)
                    surface.DrawTexturedRect(0, 0, ui.y(20), ui.y(20))

                    surface.SetFont("animelife.prefix.store.item.name")

                    surface.SetDrawColor(255, 255, 255)
                    surface.SetMaterial(icon_price)
                    surface.DrawTexturedRect(surface.GetTextSize(p.Name) + ui.y(35) + ui.y(8) + ui.y(6), ui.y(23) + ui.y(4), ui.y(16), ui.y(16))
                ui.Smooth(false, true)

                draw.SimpleText(p.Name, "animelife.prefix.store.item.name", ui.y(35) + ui.y(6), ui.y(22) + ui.y(4), Color(97, 96, 126))
                draw.SimpleText(string.Comma(p.Price) .. "¥", "animelife.prefix.store.item.name", surface.GetTextSize(p.Name) + ui.y(35) + ui.y(8) + ui.y(16) + ui.y(4) + ui.y(6), ui.y(22) + ui.y(4), Color(244, 149, 201))
                draw.SimpleText(p.Description, "animelife.prefix.store.item.desc", ui.y(35) + ui.y(6), ui.y(45) + ui.y(4), Color(202, 202, 210))
            end

            local buy_btn = vgui.Create("DButton", item_panel)
            buy_btn:SetText("")
            buy_btn:SetPos(item_panel:GetWide() - ui.y(36) - ui.y(40), ui.y(25))
            buy_btn:SetSize(ui.y(40), ui.y(40))
            buy_btn.Paint = function(panel, w, h)
                if i == 1 and LocalPlayer():HasPrefix(n) then
                    ui.Smooth(true, true)
                        surface.SetDrawColor(255, 255, 255)
                        surface.SetMaterial(LocalPlayer():GetPrefix() == n and icon_disable or icon_use)
                        surface.DrawTexturedRect(0, 0, w, h)
                    ui.Smooth(false, true)
                    return
                end

                if i == 2 and LocalPlayer():HasStatus(n) then
                    ui.Smooth(true, true)
                        surface.SetDrawColor(255, 255, 255)
                        surface.SetMaterial(LocalPlayer():GetOverheadStatus() == n and icon_disable or icon_use)
                        surface.DrawTexturedRect(0, 0, w, h)
                    ui.Smooth(false, true)
                    return
                end

                ui.Smooth(true, true)
                    surface.SetDrawColor(255, 255, 255)
                    surface.SetMaterial(icon_buy)
                    surface.DrawTexturedRect(0, 0, w, h)
                ui.Smooth(false, true)
            end
            buy_btn.DoClick = function()
                -- if ValidPanel(self) then
                --     self:Remove()
                --     self = nil
                -- end

                net.Start("animelife.prefixes.buy")
                    net.WriteInt(i, 3)
                    net.WriteInt(n, 32)
                net.SendToServer()

                surface.PlaySound("click.wav")
            end

            y = y + ui.y(88) + ui.y(12)
        end
    end
end

function PANEL:PerformLayout(w, h)
    self.ScrollListPrefix:SetPos((w - ui.y(619)) / 2, ui.y(240))
    self.ScrollListPrefix:SetSize(ui.y(619), h - ui.y(240))

    self.ScrollListStatus:SetPos((w - ui.y(619)) / 2, ui.y(240))
    self.ScrollListStatus:SetSize(ui.y(619), h - ui.y(240))

    self.CloseButton:SetPos((w - ui.y(619)) / 2 + ui.y(619) - ui.y(21) - ui.y(24), ui.y(18))
    self.CloseButton:SetSize(ui.y(24), ui.y(24))
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(0, 0, 0, 150)
    surface.DrawRect(0, 0, w, h)

    ui.Smooth(true, true)
        surface.SetDrawColor(255, 255, 255)
        surface.SetMaterial(background)
        surface.DrawTexturedRect((w - ui.y(619)) / 2, 0, ui.y(619), ui.y(1080))

        draw.SimpleText("Префиксы и статусы", "animelife.prefix.store.title", w / 2, ui.y(33), Color(79, 78, 112), 1)

        surface.SetMaterial(icon_info)
        surface.DrawTexturedRect((w - ui.y(619)) / 2 + ui.y(125), ui.y(160), ui.y(41), ui.y(41))
    ui.Smooth(false, true)

    draw.SimpleText(self.CurrentTab == 1 and "Префиксы отображаются перед ником в чате" or
    "Статус будет подвешен над вашей головой", "animelife.prefix.store.info", (w - ui.y(619)) / 2 + ui.y(182), ui.y(171), Color(216, 216, 222))

    self:SeekResolutionChange(function(pnl)
        pnl:Remove()
        pnl = nil 

        setup_fonts()
    end)
end

vgui.Register("animelife.prefix.store", PANEL, "EditablePanel")

function OpenPrefixStore()
    local f = vgui.Create("animelife.prefix.store")
    f:SetSize(ScrW(), ScrH())
    f:MakePopup()
end