-- "implement your own"
BranchSupportsExtHTML = BranchSupportsExtHTML == nil and true
function IsChromiumBranch()
    local html_test = false 
    pcall(function()
        html_test = vgui.Create("Chromium")
    end)
    if !html_test then
        BranchSupportsExtHTML = false
        return false
    end

    html_test:Remove()

    return true
end

local flux = util.Base64Decode("bG9jYWwgZmx1eCA9IHsgX3ZlcnNpb24gPSAiMC4xLjUiIH0KZmx1eC5fX2luZGV4ID0gZmx1eAoKZmx1eC50d2VlbnMgPSB7fQpmbHV4LmVhc2luZyA9IHsgbGluZWFyID0gZnVuY3Rpb24ocCkgcmV0dXJuIHAgZW5kIH0KCmxvY2FsIGVhc2luZyA9IHsKICBxdWFkICAgID0gInAgKiBwIiwKICBjdWJpYyAgID0gInAgKiBwICogcCIsCiAgcXVhcnQgICA9ICJwICogcCAqIHAgKiBwIiwKICBxdWludCAgID0gInAgKiBwICogcCAqIHAgKiBwIiwKICBleHBvICAgID0gIjIgXiAoMTAgKiAocCAtIDEpKSIsCiAgc2luZSAgICA9ICItbWF0aC5jb3MocCAqIChtYXRoLnBpICogLjUpKSArIDEiLAogIGNpcmMgICAgPSAiLShtYXRoLnNxcnQoMSAtIChwICogcCkpIC0gMSkiLAogIGJhY2sgICAgPSAicCAqIHAgKiAoMi43ICogcCAtIDEuNykiLAogIGVsYXN0aWMgPSAiLSgyXigxMCAqIChwIC0gMSkpICogbWF0aC5zaW4oKHAgLSAxLjA3NSkgKiAobWF0aC5waSAqIDIpIC8gLjMpKSIKfQoKbG9jYWwgbWFrZWZ1bmMgPSBmdW5jdGlvbihzdHIsIGV4cHIpCiAgcmV0dXJuIENvbXBpbGVTdHJpbmcoInJldHVybiBmdW5jdGlvbihwKSAiIC4uIHN0cjpnc3ViKCIlJGUiLCBleHByKSAuLiAiIGVuZCIsICJUZXN0RnVuY3Rpb24iKSgpCmVuZAoKZm9yIGssIHYgaW4gcGFpcnMoZWFzaW5nKSBkbwogIGZsdXguZWFzaW5nW2sgLi4gImluIl0gPSBtYWtlZnVuYygicmV0dXJuICRlIiwgdikKICBmbHV4LmVhc2luZ1trIC4uICJvdXQiXSA9IG1ha2VmdW5jKFtbCiAgICBwID0gMSAtIHAKICAgIHJldHVybiAxIC0gKCRlKQogIF1dLCB2KQogIGZsdXguZWFzaW5nW2sgLi4gImlub3V0Il0gPSBtYWtlZnVuYyhbWwogICAgcCA9IHAgKiAyCiAgICBpZiBwIDwgMSB0aGVuCiAgICAgIHJldHVybiAuNSAqICgkZSkKICAgIGVsc2UKICAgICAgcCA9IDIgLSBwCiAgICAgIHJldHVybiAuNSAqICgxIC0gKCRlKSkgKyAuNQogICAgZW5kCiAgXV0sIHYpCmVuZAoKCgpsb2NhbCB0d2VlbiA9IHt9CnR3ZWVuLl9faW5kZXggPSB0d2VlbgoKbG9jYWwgZnVuY3Rpb24gbWFrZWZzZXR0ZXIoZmllbGQpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBsb2NhbCBtdCA9IGdldG1ldGF0YWJsZSh4KQogICAgaWYgdHlwZSh4KSB+PSAiZnVuY3Rpb24iIGFuZCBub3QgKG10IGFuZCBtdC5fX2NhbGwpIHRoZW4KICAgICAgZXJyb3IoImV4cGVjdGVkIGZ1bmN0aW9uIG9yIGNhbGxhYmxlIiwgMikKICAgIGVuZAogICAgbG9jYWwgb2xkID0gc2VsZltmaWVsZF0KICAgIHNlbGZbZmllbGRdID0gb2xkIGFuZCBmdW5jdGlvbigpIG9sZCgpIHgoKSBlbmQgb3IgeAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgpsb2NhbCBmdW5jdGlvbiBtYWtlc2V0dGVyKGZpZWxkLCBjaGVja2ZuLCBlcnJtc2cpCiAgcmV0dXJuIGZ1bmN0aW9uKHNlbGYsIHgpCiAgICBpZiBjaGVja2ZuIGFuZCBub3QgY2hlY2tmbih4KSB0aGVuCiAgICAgIGVycm9yKGVycm1zZzpnc3ViKCIlJHgiLCB0b3N0cmluZyh4KSksIDIpCiAgICBlbmQKICAgIHNlbGZbZmllbGRdID0geAogICAgcmV0dXJuIHNlbGYKICBlbmQKZW5kCgp0d2Vlbi5lYXNlICA9IG1ha2VzZXR0ZXIoIl9lYXNlIiwKICAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uKHgpIHJldHVybiBmbHV4LmVhc2luZ1t4XSBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGVhc2luZyB0eXBlICckeCciKQp0d2Vlbi5kZWxheSA9IG1ha2VzZXR0ZXIoIl9kZWxheSIsCiAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbih4KSByZXR1cm4gdHlwZSh4KSA9PSAibnVtYmVyIiBlbmQsCiAgICAgICAgICAgICAgICAgICAgICAgICAiYmFkIGRlbGF5IHRpbWU7IGV4cGVjdGVkIG51bWJlciIpCnR3ZWVuLm9uc3RhcnQgICAgID0gbWFrZWZzZXR0ZXIoIl9vbnN0YXJ0IikKdHdlZW4ub251cGRhdGUgICAgPSBtYWtlZnNldHRlcigiX29udXBkYXRlIikKdHdlZW4ub25jb21wbGV0ZSAgPSBtYWtlZnNldHRlcigiX29uY29tcGxldGUiKQoKCmZ1bmN0aW9uIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpCiAgbG9jYWwgc2VsZiA9IHNldG1ldGF0YWJsZSh7fSwgdHdlZW4pCiAgc2VsZi5vYmogPSBvYmoKICBzZWxmLnJhdGUgPSB0aW1lID4gMCBhbmQgMSAvIHRpbWUgb3IgMAogIHNlbGYucHJvZ3Jlc3MgPSB0aW1lID4gMCBhbmQgMCBvciAxCiAgc2VsZi5fZGVsYXkgPSAwCiAgc2VsZi5fZWFzZSA9ICJxdWFkb3V0IgogIHNlbGYudmFycyA9IHt9CiAgZm9yIGssIHYgaW4gcGFpcnModmFycykgZG8KICAgIGlmIHR5cGUodikgfj0gIm51bWJlciIgdGhlbgogICAgICBlcnJvcigiYmFkIHZhbHVlIGZvciBrZXkgJyIgLi4gayAuLiAiJzsgZXhwZWN0ZWQgbnVtYmVyIikKICAgIGVuZAogICAgc2VsZi52YXJzW2tdID0gdgogIGVuZAogIHJldHVybiBzZWxmCmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOmluaXQoKQogIGZvciBrLCB2IGluIHBhaXJzKHNlbGYudmFycykgZG8KICAgIGxvY2FsIHggPSBzZWxmLm9ialtrXQogICAgaWYgdHlwZSh4KSB+PSAibnVtYmVyIiB0aGVuCiAgICAgIGVycm9yKCJiYWQgdmFsdWUgb24gb2JqZWN0IGtleSAnIiAuLiBrIC4uICInOyBleHBlY3RlZCBudW1iZXIiKQogICAgZW5kCiAgICBzZWxmLnZhcnNba10gPSB7IHN0YXJ0ID0geCwgZGlmZiA9IHYgLSB4IH0KICBlbmQKICBzZWxmLmluaXRlZCA9IHRydWUKZW5kCgoKZnVuY3Rpb24gdHdlZW46YWZ0ZXIoLi4uKQogIGxvY2FsIHQKICBpZiBzZWxlY3QoIiMiLCAuLi4pID09IDIgdGhlbgogICAgdCA9IHR3ZWVuLm5ldyhzZWxmLm9iaiwgLi4uKQogIGVsc2UKICAgIHQgPSB0d2Vlbi5uZXcoLi4uKQogIGVuZAogIHQucGFyZW50ID0gc2VsZi5wYXJlbnQKICBzZWxmOm9uY29tcGxldGUoZnVuY3Rpb24oKSBmbHV4LmFkZChzZWxmLnBhcmVudCwgdCkgZW5kKQogIHJldHVybiB0CmVuZAoKCmZ1bmN0aW9uIHR3ZWVuOnN0b3AoKQogIGZsdXgucmVtb3ZlKHNlbGYucGFyZW50LCBzZWxmKQplbmQKCgoKZnVuY3Rpb24gZmx1eC5ncm91cCgpCiAgcmV0dXJuIHNldG1ldGF0YWJsZSh7fSwgZmx1eCkKZW5kCgoKZnVuY3Rpb24gZmx1eDp0byhvYmosIHRpbWUsIHZhcnMpCiAgcmV0dXJuIGZsdXguYWRkKHNlbGYsIHR3ZWVuLm5ldyhvYmosIHRpbWUsIHZhcnMpKQplbmQKCgpmdW5jdGlvbiBmbHV4OnVwZGF0ZShkZWx0YXRpbWUpCiAgZm9yIGkgPSAjc2VsZiwgMSwgLTEgZG8KICAgIGxvY2FsIHQgPSBzZWxmW2ldCiAgICBpZiB0Ll9kZWxheSA+IDAgdGhlbgogICAgICB0Ll9kZWxheSA9IHQuX2RlbGF5IC0gZGVsdGF0aW1lCiAgICBlbHNlCiAgICAgIGlmIG5vdCB0LmluaXRlZCB0aGVuCiAgICAgICAgZmx1eC5jbGVhcihzZWxmLCB0Lm9iaiwgdC52YXJzKQogICAgICAgIHQ6aW5pdCgpCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnN0YXJ0IHRoZW4KICAgICAgICB0Ll9vbnN0YXJ0KCkKICAgICAgICB0Ll9vbnN0YXJ0ID0gbmlsCiAgICAgIGVuZAogICAgICB0LnByb2dyZXNzID0gdC5wcm9ncmVzcyArIHQucmF0ZSAqIGRlbHRhdGltZQogICAgICBsb2NhbCBwID0gdC5wcm9ncmVzcwogICAgICBsb2NhbCB4ID0gcCA+PSAxIGFuZCAxIG9yIGZsdXguZWFzaW5nW3QuX2Vhc2VdKHApCiAgICAgIGZvciBrLCB2IGluIHBhaXJzKHQudmFycykgZG8KICAgICAgICB0Lm9ialtrXSA9IHYuc3RhcnQgKyB4ICogdi5kaWZmCiAgICAgIGVuZAogICAgICBpZiB0Ll9vbnVwZGF0ZSB0aGVuIHQuX29udXBkYXRlKCkgZW5kCiAgICAgIGlmIHAgPj0gMSB0aGVuCiAgICAgICAgZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgICAgICBpZiB0Ll9vbmNvbXBsZXRlIHRoZW4gdC5fb25jb21wbGV0ZSgpIGVuZAogICAgICBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmNsZWFyKG9iaiwgdmFycykKICBmb3IgdCBpbiBwYWlycyhzZWxmW29ial0pIGRvCiAgICBpZiB0LmluaXRlZCB0aGVuCiAgICAgIGZvciBrIGluIHBhaXJzKHZhcnMpIGRvIHQudmFyc1trXSA9IG5pbCBlbmQKICAgIGVuZAogIGVuZAplbmQKCgpmdW5jdGlvbiBmbHV4OmFkZCh0d2VlbikKICAtLSBBZGQgdG8gb2JqZWN0IHRhYmxlLCBjcmVhdGUgdGFibGUgaWYgaXQgZG9lcyBub3QgZXhpc3QKICBsb2NhbCBvYmogPSB0d2Vlbi5vYmoKICBzZWxmW29ial0gPSBzZWxmW29ial0gb3Ige30KICBzZWxmW29ial1bdHdlZW5dID0gdHJ1ZQogIC0tIEFkZCB0byBhcnJheQogIHRhYmxlLmluc2VydChzZWxmLCB0d2VlbikKICB0d2Vlbi5wYXJlbnQgPSBzZWxmCiAgcmV0dXJuIHR3ZWVuCmVuZAoKCmZ1bmN0aW9uIGZsdXg6cmVtb3ZlKHgpCiAgaWYgdHlwZSh4KSA9PSAibnVtYmVyIiB0aGVuCiAgICAtLSBSZW1vdmUgZnJvbSBvYmplY3QgdGFibGUsIGRlc3Ryb3kgdGFibGUgaWYgaXQgaXMgZW1wdHkKICAgIGxvY2FsIG9iaiA9IHNlbGZbeF0ub2JqCiAgICBzZWxmW29ial1bc2VsZlt4XV0gPSBuaWwKICAgIGlmIG5vdCBuZXh0KHNlbGZbb2JqXSkgdGhlbiBzZWxmW29ial0gPSBuaWwgZW5kCiAgICAtLSBSZW1vdmUgZnJvbSBhcnJheQogICAgc2VsZlt4XSA9IHNlbGZbI3NlbGZdCiAgICByZXR1cm4gdGFibGUucmVtb3ZlKHNlbGYpCiAgZW5kCiAgZm9yIGksIHYgaW4gaXBhaXJzKHNlbGYpIGRvCiAgICBpZiB2ID09IHggdGhlbgogICAgICByZXR1cm4gZmx1eC5yZW1vdmUoc2VsZiwgaSkKICAgIGVuZAogIGVuZAplbmQKCgoKbG9jYWwgYm91bmQgPSB7CiAgdG8gICAgICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgudG8oZmx1eC50d2VlbnMsIC4uLikgZW5kLAogIHVwZGF0ZSAgPSBmdW5jdGlvbiguLi4pIHJldHVybiBmbHV4LnVwZGF0ZShmbHV4LnR3ZWVucywgLi4uKSBlbmQsCiAgcmVtb3ZlICA9IGZ1bmN0aW9uKC4uLikgcmV0dXJuIGZsdXgucmVtb3ZlKGZsdXgudHdlZW5zLCAuLi4pIGVuZCwKfQpzZXRtZXRhdGFibGUoYm91bmQsIGZsdXgpCgpyZXR1cm4gYm91bmQ=")
flux = CompileString(flux, "flux")()

local background = Material("animelife/chromium/background.png")
local detail_underwater = Material("animelife/chromium/detail_underwater.png")
local button_ok = Material("animelife/chromium/button_ok.png")
local icon_error = Material("animelife/chromium/icon_error_msg.png")

surface.CreateFont("animelife.chromium.title", {font = "Exo 2 SemiBold", weight = 600, size = ScreenScale(72) / 3, extended = true})
surface.CreateFont("animelife.chromium.title2", {font = "Exo 2 SemiBold", weight = 600, size = ScreenScale(32) / 3, extended = true})
surface.CreateFont("animelife.chromium.subtitle", {font = "Exo 2 SemiBold", weight = 600, size = ScreenScale(28) / 3, extended = true})
surface.CreateFont("animelife.chromium.explanation", {font = "Exo 2 Medium", weight = 500, size = ScreenScale(24) / 3, extended = true})
surface.CreateFont("animelife.chromium.button", {font = "Exo 2 SemiBold", weight = 500, size = ScreenScale(24) / 3, extended = true})

local explanation_text = [[
animelife работает лучше и стабильнее на бета-версиях с поддержкой chromium. Настоятельно рекомендуем попробовать.



<colour=255, 219, 92>
Погружение вместе с chromium решает проблемы с проигрыванием видео, чатом, некоторыми вкладками меню F4, а также много всего прочего.
</colour>
]]
local explanation_text_shadow = string.Replace(explanation_text, "<colour=255, 219, 92>", "")
explanation_text_shadow = string.Replace(explanation_text_shadow, "</colour>", "")

function ShowChromiumNotify()
    surface.PlaySound("dialog-pop-in.wav")

    local notify = vgui.Create("EditablePanel")
    notify:SetSize(ScrW(), ScrH())
    notify:SetAlpha(0)
    notify:AlphaTo(255, 0.1)
    notify:MakePopup()
    notify.Animations = {
        Background = {width = 1, alpha = 1, next_update = 0},
        Button = {space = 10},
        Icons = {alpha = 1, add_y = 0, x = 625, next_update = 0}
    }
    notify.Paint = function(panel, w, h)
        ui.Smooth(true, true)
            surface.SetDrawColor(255, 255, 255, 75 * panel.Animations.Background.alpha)
            surface.SetMaterial(background)
            surface.DrawTexturedRect((w - (ui.x(981) * panel.Animations.Background.width)) / 2, -4, ui.x(981) * panel.Animations.Background.width, ui.y(1088))

            surface.SetDrawColor(255, 255, 255)
            surface.SetMaterial(background)
            surface.DrawTexturedRect((w - ui.x(981)) / 2, -4, ui.x(981), ui.y(1088))

            surface.SetMaterial(detail_underwater)
            surface.DrawTexturedRect(ui.x(859), ui.y(71), ui.x(657), ui.y(609))
        ui.Smooth(false, true)

        draw.SimpleText("О, у вас stable", "animelife.chromium.title", ui.x(591), ui.y(316) + 4, Color(0, 0, 0, 75))
        draw.SimpleText("О, у вас stable", "animelife.chromium.title", ui.x(591), ui.y(316), Color(255, 255, 255))
        draw.SimpleText("(￣ω￣;)", "animelife.chromium.title2", ui.x(591) + ui.x(346), ui.y(316) + ui.y(32) + 4, Color(0, 0, 0, 75))
        draw.SimpleText("(￣ω￣;)", "animelife.chromium.title2", ui.x(591) + ui.x(346), ui.y(316) + ui.y(32), Color(255, 255, 255))
        draw.SimpleText("Попробуйте chromium!", "animelife.chromium.subtitle", ui.x(591), ui.y(316) + ui.y(78) + 4, Color(0, 0, 0, 75))
        draw.SimpleText("Попробуйте chromium!", "animelife.chromium.subtitle", ui.x(591), ui.y(316) + ui.y(78), Color(255, 255, 255))

        local explanation_markup = markup.Parse("<font=animelife.chromium.explanation>" .. explanation_text .. "</font>", ui.x(604))
        local explanation_markup_shadow = markup.Parse("<font=animelife.chromium.explanation><colour=0, 0, 0>" .. explanation_text_shadow .. "</colour></font>", ui.x(604))
        explanation_markup:Draw(ui.x(635), ui.y(479))
        explanation_markup_shadow:Draw(ui.x(635), ui.y(479) + 4, nil, nil, 75)

        surface.SetDrawColor(255, 255, 255, 255 * panel.Animations.Icons.alpha)
        surface.SetMaterial(icon_error)
        surface.DrawTexturedRect(panel.Animations.Icons.x, ScrH() + 34 + panel.Animations.Icons.add_y, 30, 34)

        if panel.Animations.Background.next_update < CurTime() then
            panel.Animations.Background.width = 1
            panel.Animations.Background.alpha = 1

            flux.to(panel.Animations.Background, 0.5, {alpha = 0, width = 1.2})

            panel.Animations.Background.next_update = CurTime() + 1
        end

        if panel.Animations.Icons.next_update < CurTime() then
            panel.Animations.Icons.x = math.random(625, 1200)
            panel.Animations.Icons.alpha = 1
            panel.Animations.Icons.add_y = 0

            flux.to(panel.Animations.Icons, 1, {alpha = 0, add_y = -128})

            panel.Animations.Icons.next_update = CurTime() + 1.25
        end

        flux.update(RealFrameTime())
    end

    local ok_button = vgui.Create("DButton", notify)
    ok_button:SetSize(ui.x(858), ui.y(81))
    ok_button:SetPos(ui.x(512), ui.y(751))
    ok_button:SetText("")
    ok_button.Paint = function(panel, w, h)
        ui.Smooth(true, true)
            surface.SetDrawColor(255, 255, 255)
            surface.SetMaterial(button_ok)
            surface.DrawTexturedRect(0, 0, w, h)
        ui.Smooth(false, true)

        local text = "это понятно"
        local x = (w - (utf8.len(text) * 1 * notify.Animations.Button.space)) / 2
        for i = 1, utf8.len(text) do
            local char = utf8.sub(text, i, i)
            draw.SimpleText(char, "animelife.chromium.button", x, h / 2, Color(0, 0, 0, 75), 1, 1)
            draw.SimpleText(char, "animelife.chromium.button", x, h / 2 - 2, Color(255, 255, 255), 1, 1)

            x = x + notify.Animations.Button.space
        end
    end
    ok_button.OnCursorEntered = function()
        flux.to(notify.Animations.Button, 0.25, {space = 16})
        surface.PlaySound("generic-hover-softer.wav")
    end
    ok_button.OnCursorExited = function()
        flux.to(notify.Animations.Button, 0.25, {space = 10})
    end
    ok_button.DoClick = function()
        notify:Remove()

        surface.PlaySound("dialog-pop-out.wav")
        surface.PlaySound("generic-select-softer.wav")
    end
end

-- if !IsChromiumBranch() then return end
-- ShowChromiumNotify()