util.AddNetworkString("animelife.classchooser.choose")

local function get_players(class)
    local count = 0
    for _, ply in pairs(player.GetAll()) do
        if ply:Team() == t then
            if ply:GetNWInt("animelife.classchooser.class", -1) == class then
                count = count + 1
            end
        end
    end

    return count
end

net.Receive("animelife.classchooser.choose", function(len, ply)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !classchooser.List[ply:Team()] then return end
    if ply:GetNWInt("animelife.classchooser.class", -1) ~= -1 then return end

    local list = classchooser.List[ply:Team()]
    local class = net.ReadInt(16)
    if !list[class] then return end
    if get_players(class) >= list[class].Limit then return end

    ply:SetNWInt("animelife.classchooser.class", class)

    ply:SetRunSpeed(list[class].Stats.speed)
    ply:SetMaxHealth(list[class].Stats.hp)
    ply:SetHealth(ply:GetMaxHealth())
    ply:SetArmor(list[class].Stats.armor)

    if istable(list[class].Model) then
        ply:SetModel(table.Random(list[class].Model))
    end

    if list[class].ModelScale then
        ply:SetModelScale(list[class].ModelScale)
    end

    local weps = list[class].Weapons
    for _, wep in pairs(weps) do
        ply:Give(wep)
    end
end)

hook.Add("PlayerSpawn", "animelife.classchooser.respawn", function(ply)
    timer.Simple(0.5, function()
        if !IsValid(ply) or !ply:IsPlayer() then return end
        local class_list = classchooser.List[ply:Team()]
        if !class_list then return end
        local class = ply:GetNWInt("animelife.classchooser.class", -1)
        if class ~= -1 then
            ply:SetRunSpeed(class_list[class].Stats.speed)
            ply:SetMaxHealth(class_list[class].Stats.hp)
            ply:SetHealth(ply:GetMaxHealth())
            ply:SetArmor(class_list[class].Stats.armor)

            if istable(class_list[class].Model) then
                ply:SetModel(table.Random(class_list[class].Model))
            end

            if class_list[class].ModelScale then
                ply:SetModelScale(class_list[class].ModelScale)
            end

            local weps = class_list[class].Weapons
            for _, wep in pairs(weps) do
                ply:Give(wep)
            end
        end
    end)
end)

hook.Add("PlayerChangedTeam", "animelife.classchooser.changeteam", function(ply)
    if ply:GetNWInt("animelife.classchooser.class", -1) ~= -1 then
        ply:SetNWInt("animelife.classchooser.class", -1)
    end
end)