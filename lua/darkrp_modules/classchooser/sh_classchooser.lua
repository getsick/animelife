module("classchooser", package.seeall)

List = {}

timer.Simple(0, function()
    List = {
        [TEAM_NEET] = {
            [1] = {
                Name = "Каратель",
                Model = {
                    "models/player/dewobedil/shadow_person/default_p.mdl"
                },
                Description = "Действует в одиночку.",
                Weapons = {"arrest_stick", "unarrest_stick", "stungun_new", "door_ram", "weaponchecker", "weapon_cuff_elastic", "tfa_ins2_glock_19", "tfa_ins2_cq300"},
                Stats = {hp = 150, armor = 100, speed = 260},
                Limit = 1
            },
            [2] = {
                Name = "Снайпер",
                Model = {
                    "models/gfl/drhunter/nyto_b/nyto_b_pm.mdl",
                },
                Description = "Его выстрел смертелен.",
                Weapons = {"arrest_stick", "unarrest_stick", "door_ram", "stungun_new", "weaponchecker", "weapon_cuff_elastic", "tfa_ins2_glock_19", "tfa_ins2_gol"},
                Stats = {hp = 100, armor = 100, speed = 260},
                Limit = 1
            },
            [3] = {
                Name = "Широяша",
                Model = {
                    "models/void_archieves/honkai_impact/rstar/void_archieves/void_archieves.mdl",
                },
                Description = "Может разрезать даже сталь.",
                Weapons = {"arrest_stick", "unarrest_stick", "door_ram", "stungun_new", "weaponchecker", "weapon_cuff_elastic", "tfa_cso_jaydagger"},
                Stats = {hp = 100, armor = 100, speed = 440},
                Limit = 1
            },
            [4] = {
                Name = "Танк",
                Model = {
                    "models/persona_nk/sho/sho_p4.mdl",
                },
                Description = "Его крепкое тело даже пули\nпробивают с трудом.",
                Weapons = {"arrest_stick", "unarrest_stick", "door_ram", "stungun_new", "weaponchecker", "weapon_cuff_elastic", "tfa_ins2_spas12"},
                Stats = {hp = 200, armor = 100, speed = 260},
                Limit = 1
            },
        },
        [TEAM_YAKUZA] = {
            [1] = {
                Name = "Оябун",
                Description = "Глава клана. Страшный человек.",
                Model = {
                    "models/player/voikanaa/kazuma_kiryu.mdl"
                },
                Weapons = {"weapon_cuff_rope", "unarrest_stick"},
                Stats = {hp = 150, armor = 0, speed = 260},
                Limit = 1
            },
            [2] = {
                Name = "Кедай",
                Model = {
                    "models/survivors/survivor_gambler.mdl",
                },
                Description = "Самые приближенные.",
                Weapons = {"weapon_cuff_rope", "ah_lockpick"},
                Stats = {hp = 120, armor = 0, speed = 260},
                Limit = 2
            },
            [3] = {
                Name = "Сятэй",
                Model = {
                    "models/fireteam/yak.mdl",
                },
                Description = "Рядовой член Якудзы.",
                Weapons = {},
                Stats = {hp = 100, armor = 0, speed = 260},
                Limit = 6
            },
            [4] = {
                Name = "Шибу",
                Model = {
                    "models/player/dewobedil/persona5/munehisa_iwai/default_p.mdl",
                },
                Description = "Стальное тело клана.",
                Weapons = {"weapon_cuff_rope"},
                Stats = {hp = 200, armor = 100, speed = 260},
                Limit = 1
            },
            
        }
    }
end)