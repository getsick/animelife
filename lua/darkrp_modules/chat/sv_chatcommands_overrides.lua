local function OOC(ply, args)
    if not GAMEMODE.Config.ooc then
        DarkRP.notify(ply, 1, 4, DarkRP.getPhrase("disabled", DarkRP.getPhrase("ooc"), ""))
        return ""
    end

    local DoSay = function(text)
        if text == "" then
            DarkRP.notify(ply, 1, 4, DarkRP.getPhrase("invalid_x", DarkRP.getPhrase("arguments"), ""))
            return ""
        end
        local col = team.GetColor(ply:Team())
        local col2 = Color(255, 255, 255, 255)
        if not ply:Alive() then
            col2 = Color(255, 200, 200, 255)
            col = col2
        end

        local prefix_color = Color(255, 255, 255)
        local prfx = ""
        if isnumber(ply:GetPrefix()) then
            local p = prefix.List["chat"][ply:GetPrefix()]
            
            prefix_color = p.Color
            if isfunction(prefix_color) then
                prefix_color = p.Color()
                prefix_color = Color(prefix_color.r, prefix_color.g, prefix_color.b, prefix_color.a or 255)
            end
            prfx = p.Name
        end

        local phrase = DarkRP.getPhrase("ooc")
        local name = ply:Nick()
        for _, v in ipairs(player.GetAll()) do
            -- DarkRP.talkToPerson(v, col, "(" .. phrase .. ") " .. name, col2, text, ply)

            net.Start("animelife.chatcommand.chat")
                net.WriteInt(1, 16)
                net.WriteString(name)
                net.WriteString(text)
                net.WriteColor(col)
                net.WriteInt(ply:EntIndex(), 32)
            net.Send(v)
        end
    end
    return args, DoSay
end
DarkRP.defineChatCommand("/", OOC, true, 1.5)
DarkRP.defineChatCommand("a", OOC, true, 1.5)
DarkRP.defineChatCommand("ooc", OOC, true, 1.5)

local function Whisper(ply, args)
    local DoSay = function(text)
        if text == "" then
            DarkRP.notify(ply, 1, 4, DarkRP.getPhrase("invalid_x", DarkRP.getPhrase("arguments"), ""))
            return ""
        end
        -- DarkRP.talkToRange(ply, "(" .. DarkRP.getPhrase("whisper") .. ") " .. ply:Nick(), text, GAMEMODE.Config.whisperDistance)

        for _, v in pairs(ents.FindInSphere(ply:GetPos(), GAMEMODE.Config.whisperDistance)) do
            if !v:IsPlayer() then continue end

            net.Start("animelife.chatcommand.chat")
                net.WriteInt(2, 16)
                net.WriteString(ply:Nick())
                net.WriteString(text)
                net.WriteColor(team.GetColor(ply:Team()))
                net.WriteInt(ply:EntIndex(), 32)
            net.Send(v)
        end
    end
    return args, DoSay
end
DarkRP.defineChatCommand("w", Whisper, 1.5)

local function Yell(ply, args)
    local DoSay = function(text)
        if text == "" then
            DarkRP.notify(ply, 1, 4, DarkRP.getPhrase("invalid_x", DarkRP.getPhrase("arguments"), ""))
            return ""
        end
        -- DarkRP.talkToRange(ply, "(" .. DarkRP.getPhrase("yell") .. ") " .. ply:Nick(), text, GAMEMODE.Config.yellDistance)

        for _, v in pairs(ents.FindInSphere(ply:GetPos(), GAMEMODE.Config.yellDistance)) do
            if !v:IsPlayer() then continue end

            net.Start("animelife.chatcommand.chat")
                net.WriteInt(3, 16)
                net.WriteString(ply:Nick())
                net.WriteString(text)
                net.WriteColor(team.GetColor(ply:Team()))
                net.WriteInt(ply:EntIndex(), 32)
            net.Send(v)
        end
    end
    return args, DoSay
end
DarkRP.defineChatCommand("y", Yell, 1.5)

local function Me(ply, args)
    if args == "" then
        DarkRP.notify(ply, 1, 4, DarkRP.getPhrase("invalid_x", DarkRP.getPhrase("arguments"), ""))
        return ""
    end

    local DoSay = function(text)
        if text == "" then
            DarkRP.notify(ply, 1, 4, DarkRP.getPhrase("invalid_x", DarkRP.getPhrase("arguments"), ""))
            return ""
        end
        if GAMEMODE.Config.alltalk then
            local col = team.GetColor(ply:Team())
            local name = ply:Nick()
            for _, target in ipairs(player.GetAll()) do
                DarkRP.talkToPerson(target, col, name .. " " .. text)
            end
        else
            -- DarkRP.talkToRange(ply, ply:Nick() .. " " .. text, "", GAMEMODE.Config.meDistance)
            for _, v in pairs(ents.FindInSphere(ply:GetPos(), GAMEMODE.Config.meDistance)) do
                if !v:IsPlayer() then continue end

                net.Start("animelife.chatcommand.chat")
                    net.WriteInt(4, 16)
                    net.WriteString(ply:Nick())
                    net.WriteString(text)
                net.Send(v)
            end
        end
    end
    return args, DoSay
end
DarkRP.defineChatCommand("me", Me, 1.5)

local function PM(ply, args)
    local namepos = string.find(args, " ")
    if not namepos then
        DarkRP.notify(ply, 1, 4, DarkRP.getPhrase("invalid_x", DarkRP.getPhrase("arguments"), ""))
        return ""
    end

    local name = string.sub(args, 1, namepos - 1)
    local msg = string.sub(args, namepos + 1)

    if msg == "" then
        DarkRP.notify(ply, 1, 4, DarkRP.getPhrase("invalid_x", DarkRP.getPhrase("arguments"), ""))
        return ""
    end

    local target = DarkRP.findPlayer(name)
    if target == ply then return "" end

    if target then
        local col = team.GetColor(ply:Team())
        local pname = ply:Nick()
        local col2 = Color(255, 255, 255, 255)
        -- DarkRP.talkToPerson(target, col, "(PM) " .. pname, col2, msg, ply)
        -- DarkRP.talkToPerson(ply, col, "(PM) " .. pname, col2, msg, ply)
        for _, v in pairs({target, ply}) do
            if !IsValid(v) or !v:IsPlayer() then continue end

            net.Start("animelife.chatcommand.chat")
                net.WriteInt(5, 16)
                net.WriteString(pname)
                net.WriteString(msg)
                net.WriteColor(col)
            net.Send(v)
        end
    else
        DarkRP.notify(ply, 1, 4, DarkRP.getPhrase("could_not_find", tostring(name)))
    end

    return ""
end
DarkRP.defineChatCommand("pm", PM, 1.5)

local function CombineRequest(ply, args)
    if (ply.NextCombineRequest or 0) > SysTime() then
        DarkRP.notify(ply, 1, 4, "Вы недавно вызывали полицию. Подождите ещё.")
        return ""
    end

    if args == "" then
        DarkRP.notify(ply, 1, 4, DarkRP.getPhrase("invalid_x", DarkRP.getPhrase("arguments"), ""))
        return ""
    end

    local DoSay = function(text)
        if text == "" then
            DarkRP.notify(ply, 1, 4, DarkRP.getPhrase("invalid_x", DarkRP.getPhrase("arguments"), ""))
            return
        end

        local col = team.GetColor(ply:Team())
        local col2 = Color(255, 0, 0, 255)
        local phrase = DarkRP.getPhrase("request")
        local name = ply:Nick()
        local pos = ply:GetPos() + Vector(0, 0, 64)
        DarkRP.notify(ply, 0, 5, "Вы вызвали полицию на ваше местоположение.")
        for _, v in ipairs(player.GetAll()) do
            if v:isCP() then
                markers:CreateAt(v, pos, text, Color(255, 75, 75), 300)
                DarkRP.notify(v, 0, 8, name .. " вызывает полицию: " .. text)
            end
        end

        ply.NextCombineRequest = SysTime() + (4 * 60)
    end
    return args, DoSay
end
for _, cmd in ipairs{"cr", "911", "999", "112", "000"} do
    DarkRP.defineChatCommand(cmd, CombineRequest, 1.5)
end

local function MayorBroadcast(ply, args)
    DarkRP.notify(ply, 1, 4, "Мы отключили эту команду. 人(_ _*)")
    return ""
end
DarkRP.defineChatCommand("broadcast", MayorBroadcast, 1.5)

local function CreateCheque(ply, args)
    DarkRP.notify(ply, 1, 4, "Мы отключили эту команду. 人(_ _*)")
    return ""
end

DarkRP.defineChatCommand("cheque", CreateCheque, 0.3)
DarkRP.defineChatCommand("check", CreateCheque, 0.3)