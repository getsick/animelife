local PANEL = {}

surface.CreateFont("animelife.chatbox.emojigui", {font = "Exo 2", size = 16, extended = true})

function PANEL:Init()
    self.Scroll = vgui.Create("DScrollPanel", self)

    local v_bar = self.Scroll:GetVBar()
    v_bar:SetWide(4)

    self.Layout = vgui.Create("DIconLayout", self.Scroll)
    self.Layout:SetSpaceX(8)
    self.Layout:SetSpaceY(8)
    self.Layout:SetBorder(0)

    for emote, link in pairs(chatbox.Emojis) do
        local preview = vgui.Create("DHTML", self.Layout)
        preview:SetSize(32, 32)
        preview:SetHTML('<style>html,body{padding:0;margin:0}</style><img src="' .. link .. '" width="100%" height="100%">')

        local btn = vgui.Create("DButton", preview)
        btn:SetSize(preview:GetSize())
        btn:SetText("")
        btn.Paint = function() end
        btn.DoClick = function()
            if ValidPanel(self.InputPanel) then
                local old = self.InputPanel:GetText()
                local new = old .. ":" .. emote .. ":"
        
                self.InputPanel:SetText(new)
                self.InputPanel:SetCaretPos(string.len(new))

                self:SetVisible(false)
            end
        end
    end
end

function PANEL:SetInputPanel(pnl)
    self.InputPanel = pnl
end

function PANEL:PerformLayout(w, h)
    self.Scroll:SetPos(0, 32)
    self.Scroll:SetSize(self:GetWide(), self:GetTall() - 32)

    self.Layout:Dock(FILL)
end

function PANEL:Paint(w, h)
    draw.RoundedBox(6, 0, 0, w, h, Color(0, 0, 0, 225))

    draw.SimpleText("Эмодзи", "animelife.chatbox.emojigui", 16, 8, Color(218, 218, 218))
end

vgui.Register("animelife.chatbox.emoji", PANEL, "EditablePanel")