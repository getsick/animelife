local PANEL = {}

surface.CreateFont("animelife.chat.pm.player", {font = "Roboto", size = 16, extended = true})

function PANEL:Init()
    self:SetText("")

    self.Avatar = vgui.Create("AvatarImage", self)

    self.Player = nil
end

function PANEL:SetPlayer(ply)
    self.Avatar:SetPlayer(ply, 64)
    self.Player = ply
end

function PANEL:PerformLayout(w, h)
    self.Avatar:SetSize(38, 38)
    self.Avatar:SetPos(18, (h - self.Avatar:GetTall()) / 2)
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(0, 0, 0)
    surface.DrawRect(0, 0, w, h)

    surface.SetDrawColor(0, 0, 0, 50)
    surface.DrawOutlinedRect(0, 0, w, h, 1)

    if IsValid(self.Player) and self.Player:IsPlayer() then
        draw.SimpleText(self.Player:Nick(), "animelife.chat.pm.player", 18 + 38 + 14, h / 2 + 1, Color(0, 0, 0, 75), nil, 1)
        draw.SimpleText(self.Player:Nick(), "animelife.chat.pm.player", 18 + 38 + 14, h / 2, Color(255, 255, 255), nil, 1)
    end
end

vgui.Register("animelife.pm.player", PANEL, "DButton")