CreateClientConVar("al_chat_showadvert", "1", true)

module("chatbox", package.seeall)

function chatbox:Initialize(bool)
    local x, y = 58, ScrH() - 375
    local offx, offy = cookie.GetNumber("animelife.chat_offsetx", 0), 
    cookie.GetNumber("animelife.chat_offsety", 0)

    _G.GLOBALS_CHATBOX = vgui.Create("animelife.chatbox")
    _G.GLOBALS_CHATBOX:SetPos(58 + offx, ScrH() - 375 - offy)
    _G.GLOBALS_CHATBOX:SetSize(585, 325)

    if bool then
        _G.GLOBALS_CHATBOX:Hide()
    end
end

function chatbox:CheckChatboxValidness()
    if !ValidPanel(_G.GLOBALS_CHATBOX) then 
        self:Initialize(false) 
    end
end

function GM:StartChat(t)
    chatbox:CheckChatboxValidness()

    if t then
        _G.GLOBALS_CHATBOX:Show("Local")
    else
        _G.GLOBALS_CHATBOX:Show("Server")
    end

    return true
end

function GM:FinishChat()
    chatbox:CheckChatboxValidness()

    _G.GLOBALS_CHATBOX:Hide()
end

function GM:ChatText(id, name, msg, chat_type, t)
    chatbox:CheckChatboxValidness()

    local ply = player.GetByID(id)
    local taipu = t or "Server"
    local color = Color(255, 255, 255)
    local team_color = Color(255, 255, 255)
    local display_name = ply.Nick and ply:Nick() or name
    local prefix = nil

    local should_block = GetConVar("al_chat_offensivewordsblock"):GetInt() > 0
    if should_block then
        local found, rep = FindOffensiveWords(msg)
        if found then
            msg = rep
        end
    end

    if chat_type == "chat" then
        if IsValid(ply) then
            team_color = ply:Team() == 1 and Color(194, 194, 194) or team.GetColor(ply:Team())
            prefix = ply:GetPrefix()
        end

        _G.GLOBALS_CHATBOX:AddChat(taipu, display_name, id, msg, team_color, prefix)
        chat.PlaySound() -- TODO: Replace default chat sound with something more pleasant
    elseif chat_type == "joinleave" then
        -- Do nothing. Yet.
    else
        _G.GLOBALS_CHATBOX:AddText(msg, color)
    end
end

if !chatAddText then chatAddText = chat.AddText end
function chat.AddText(...)
    chatbox:CheckChatboxValidness()

    local text = {}

    for _, obj in pairs({...}) do
        if type(obj) == "table" then
            if _G.GLOBALS_CHATBOX.TextPanel and _G.GLOBALS_CHATBOX.TextPanel.InsertColorChange then
                _G.GLOBALS_CHATBOX.TextPanel:InsertColorChange(obj.r, obj.g, obj.b, obj.a or 255)
            end

            table.insert(text, Color(obj.r, obj.g, obj.b, obj.a or 255))
        elseif type(obj) == "string" then
            if _G.GLOBALS_CHATBOX.TextPanel and _G.GLOBALS_CHATBOX.TextPanel.InsertMessage then
                _G.GLOBALS_CHATBOX.TextPanel:InsertMessage(tostring(obj))
            end

            table.insert(text, tostring(obj))
        end
    end

    if _G.GLOBALS_CHATBOX.TextPanel and _G.GLOBALS_CHATBOX.TextPanel.InsertMessage then
        _G.GLOBALS_CHATBOX.TextPanel:InsertMessage("\n")
        _G.GLOBALS_CHATBOX.TextPanel:ScrollToEnd()

        if _G.GLOBALS_CHATBOX.TextPanel.Fading == nil then return end
        if !_G.GLOBALS_CHATBOX.TextPanel.Fading then
            _G.GLOBALS_CHATBOX.TextPanel:ChangeOpacity(1)
        end
    end

    chatAddText(...)

    return text
end

function GM:OnPlayerChat(ply, msg, team_only, dead)
    if IsValid(ply) and ply:IsPlayer() then
        local name
        if ply.Nick then
            name = ply:Nick()
        else
            name = "(Unknown Player)"
        end

        self:ChatText(ply:EntIndex(), name, msg, "chat")
    else
        self:ChatText(0, "(Unknown Player)", msg, "chat")
    end

    return true
end

hook.Add("PlayerBindPress", "animelife.chatbox.override", function(ply, bind, pressed)
    if !pressed then return end

    if bind == "messagemode" or bind == "messagemode2" then
        GAMEMODE:StartChat(bind == "messagemode2")
        return true
    end
end)

hook.Add("HUDShouldDraw", "animelife.chatbox.default", function(e)
    if e == "CHudChat" then
        return false
    end
end)

function _G.chat.GetChatBoxPos()
    if IsValid(_G.GLOBALS_CHATBOX) then
        return _G.GLOBALS_CHATBOX:GetPos()
    end

    return 0, 0
end

net.Receive("animelife.roll.message", function()
    local nick = net.ReadString()
    local result = net.ReadString()

    chat.AddText(Color(194, 194, 194), nick, Color(208, 255, 177), " выпало " .. result)
end)

net.Receive("animelife.chatcommand.chat", function()
    local t = net.ReadInt(16)
    if t >= 1 and t <= 3 then
        local name = net.ReadString()
        local text = net.ReadString()
        local col = net.ReadColor()
        local idx = net.ReadInt(32)

        GAMEMODE:ChatText(idx, name, text, "chat", t == 1 and "Global" or (t == 2 and "Whisper" or (t == 3 and "Yell" or "")))
    elseif t == 4 then
        local name = net.ReadString()
        local text = net.ReadString()

        chat.AddText(Color(240, 255, 204), name .. " " .. text)
    elseif t == 5 then
        local name = net.ReadString()
        local text = net.ReadString()
        local col = net.ReadColor()

        chat.AddText(Color(255, 231, 199), "[Личное сообщение] ", col, name, Color(255, 255, 255), ": " .. text)
    end

    chat.PlaySound()
end)