module("location", package.seeall)

Locations = {}

-- Returns location name
function GetName(loc)
    local fallback = "Unknown Location"

    if Locations[game.GetMap()][loc] then
        return (Locations[game.GetMap()][loc].Name or fallback)
    end

    return fallback
end

-- Returns location index based on given position
function Get(pos)
    for i = 1, #Locations[game.GetMap()] do
        if pos:WithinAABox(Locations[game.GetMap()][i].Min, Locations[game.GetMap()][i].Max) then
            return i
        end
    end

    return -1
end

-- [Locations]
Locations["rp_animelife_v5"] = {
    [1] = {
        Min = Vector(-8091, 5034, 28),
        Max = Vector(-5197, 3323, 1554),
        Name = "Городское управление"
    },
    [2] = {
        Min = Vector(-11863, -1622, -33),
        Max = Vector(-9049, 425, 1042),
        Name = "Станция метро"
    },
    [3] = {
        Min = Vector(-6017, -1819, 38),
        Max = Vector(-7845, -4117, 1063),
        Name = "Кинотеатр"
    },
    [4] = {
        Min = Vector(-4496, -2634, -1),
        Max = Vector(-5997, -4528, 743),
        Name = "Club Sunshine"
    },
    [5] = {
        Min = Vector(-3659, -845, -60),
        Max = Vector(-2464, 296, 730),
        Name = "Полицейский участок"
    },
    [6] = {
        Min = Vector(-7446, -459, 45),
        Max = Vector(-4233, 586, 641),
        Name = "Парк"
    },
}

Locations["under_construction"] = Locations["rp_animelife_v5"]
-- [#Locations]