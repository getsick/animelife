local canvas_white = Color(255, 255, 255)
local pixel_black = Color(0, 0, 0)
local pixel_red = Color(255, 0, 0)
local pixel_blue = Color(0, 0, 255)
local pixel_green = Color(0, 255, 0)
local pixel_white = Color(255, 255, 255)
local pixel_porsche = Color(234, 183, 106)
local pixel_pearl = Color(236, 218, 200)
local pixel_springfrost = Color(119, 237, 63)
local pixel_pantonegreen = Color(21, 169, 55)
local pixel_cello = Color(29, 67, 91)
local pixel_cherrywood = Color(99, 21, 31)
local pixel_cgred = Color(226, 57, 58)
local pixel_celestialblue = Color(64, 146, 217)
local pixel_aquamarineblue = Color(92, 241, 231)
local pixel_pinksherbet = Color(247, 128, 163)
local pixel_yellow = Color(255, 255, 0)
local pixel_purple = Color(162, 0, 255)

module("pixeldraw", package.seeall)

canvas_list = canvas_list or {}
palette_colors = {
    pixel_black, pixel_white, pixel_red, pixel_blue, pixel_yellow, pixel_purple, pixel_green, pixel_springfrost, pixel_pantonegreen, pixel_cello, pixel_cherrywood, pixel_cgred, pixel_celestialblue, pixel_aquamarineblue, pixel_pinksherbet, pixel_porsche, pixel_pearl
}

function pixeldraw:CreateCanvas(width, height, bg_color, pixel_size)
    if !width then error('you have to declare a width') end
    if !height then error('you have to declare a height') end
    if !bg_color then bg_color = canvas_white end
    if !pixel_size then pixel_size = 32 end

    local canvas = {
        Width = width,
        Height = height,
        BackgroundColor = bg_color,
        PixelSize = pixel_size
    }

    local idx = table.insert(canvas_list, canvas)
    -- print('Creating canvas', idx, width, height, bg_color)

    return idx
end

function pixeldraw:GetCanvas(id)
    return canvas_list[id]
end

function pixeldraw:PlacePixel(canvas, x, y, color)
    if !canvas then error('canvas is null, quitting.') end
    if !x then error('x seems to be invalid') end
    if !y then error('y seems to be invalid') end
    if !color then color = pixel_black end

    if isnumber(canvas) and !canvas_list[canvas] then -- assuming its an index
        error('couldnt find given canvas')
    end

    canvas = canvas_list[canvas] -- should be valid at this point?

    if !canvas.Pixels then canvas.Pixels = {} end

    local pixels = canvas.Pixels

    -- are we going out of bounds?
    if (x < 0 or y < 0) or (x > canvas.Width or y > canvas.Height) then
        return false
    end

    -- accept only snapped positions
    if x % canvas.PixelSize ~= 0 or y % canvas.PixelSize ~= 0 then
        return false
    end

    local function find_pixel_by_xy(x, y)
        for k, v in ipairs(pixels) do
            if v.x == x and v.y == y then
                return k, v
            end
        end
    end

    -- do nothing for equally perfect pixels
    local lk_index, lk_pixel = find_pixel_by_xy(x, y)
    if lk_pixel and !table.IsEmpty(lk_pixel) then
        if lk_pixel.color == color then
            -- they're the same, skipping
            return false
        end

        if lk_pixel.color ~= color then
            -- color changed, skipping
            canvas.Pixels[lk_index].color = color

            return true -- still counts as `success`
        end

        if lk_pixel.color ~= color and color == canvas.BackgroundColor then
            -- we don't want to store useless pixels (since they're the same color as bg)
            -- skipping.

            return false
        end
    end

    canvas.Pixels[#canvas.Pixels + 1] = {
        x = x, y = y , color = color
    }

    -- print('Insert pixel', x, y, color)

    return true -- yay, we did it
end

-- canvas, dimensions, [Optional] panel, [Optional] pos, [Optional] ang, [Optional] scale
function pixeldraw:CursorPos(canvas, dimensions, panel, pos, ang, scale)
    local cx, cy = 0, 0
    if dimensions == 2 then -- 2D
        if panel and ValidPanel(panel) then
            cx, cy = panel:CursorPos()
        else -- drawing on hud?
            cx, cy = gui.MouseX(), gui.MouseY()
        end
    else -- 3D
        cx, cy = self:ProjectOnto3D(pos, ang, scale)
    end

    local canvas = canvas_list[canvas]
    if !canvas then error('couldnt find given canvas') end
    local pixel_size = canvas.PixelSize

    -- make offsets depending on pixel size
    cx, cy = math.Round(cx / pixel_size) * pixel_size, math.Round(cy / pixel_size) * pixel_size

    -- clip it so that it won't go out of bounds
    if cx < 0 then
        cx = 0
    elseif cx > (canvas.Width - pixel_size) then
        cx = canvas.Width - pixel_size
    end

    if cy < 0 then
        cy = 0
    elseif cy > (canvas.Height - pixel_size) then
        cy = canvas.Height - pixel_size
    end

    return cx, cy
end

function pixeldraw:Render(canvas, x, y, w, h, cursor_color, panel, pos, ang, scale)
    local mx, my = self:CursorPos(canvas, pos and 3 or 2, panel, pos, ang, scale)
    local canvas_info = canvas_list[canvas]
    local pixel_size = canvas_info.PixelSize

    surface.SetDrawColor(canvas_info.BackgroundColor)
    surface.DrawRect(x, y, w, h)

    -- render pixels
    if canvas_info.Pixels then
        for _, px in ipairs(canvas_info.Pixels) do
            surface.SetDrawColor(px.color)
            surface.DrawRect(px.x, px.y, pixel_size, pixel_size)
        end
    end

    local cursor_color = ColorAlpha(cursor_color, 150)

    surface.SetDrawColor(cursor_color)
    surface.DrawRect(x + mx, y + my, pixel_size, pixel_size)
end

function pixeldraw:RenderPalette(x, y, size)
    local rx = x
    for i = 1, #palette_colors do
        local clr = palette_colors[i]
        surface.SetDrawColor(clr)
        surface.DrawRect(rx, y, size, size)

        surface.SetDrawColor(pixel_black)
        surface.DrawOutlinedRect(rx, y, size, size, 4)

        rx = rx + size + 16
    end
end

function pixeldraw:ProjectOnto3D(pos, ang, scale)
    local mx, my = 0, 0
    if !vgui.CursorVisible() or vgui.IsHoveringWorld() then
		local tr = LocalPlayer():GetEyeTrace()
		local eye_pos = tr.StartPos
		local eye_normal

		if vgui.CursorVisible() and vgui.IsHoveringWorld() then
			eye_normal = gui.ScreenToVector(gui.MousePos())
		else
			eye_normal = tr.Normal
		end

		local plane_normal = ang:Up()

		local hit_pos = util.IntersectRayWithPlane(eye_pos, eye_normal, pos, plane_normal)
		if hit_pos then
            local diff = pos - hit_pos
            local x = diff:Dot(-ang:Forward()) / scale
            local y = diff:Dot(-ang:Right()) / scale

            mx = x
            my = y
		end
	end

    return mx, my
end

-- PIXELDRAW_TEST = vgui.Create("DFrame")
-- PIXELDRAW_TEST:SetSize(1024, 1024)
-- PIXELDRAW_TEST:Center()
-- PIXELDRAW_TEST:MakePopup()
-- PIXELDRAW_TEST.Canvas = pixeldraw:CreateCanvas(1024, 1024, Color(31, 31, 31), 24)
-- PIXELDRAW_TEST.CurrentColor = pixel_black
-- PIXELDRAW_TEST.Paint = function(pnl, w, h)
--     local canvas = pnl.Canvas
--     if canvas then
--         pixeldraw:Render(canvas, 0, 0, w, h, pnl.CurrentColor, pnl)
--         pixeldraw:RenderPalette(32, 32, 32)
--     end
-- end
-- PIXELDRAW_TEST.OnMousePressed = function(pnl)
--     local canvas = pnl.Canvas
--     if canvas then
--         local x, y = pixeldraw:CursorPos(canvas, 2, pnl)
--         pixeldraw:PlacePixel(canvas, x, y, pnl.CurrentColor)

--         local px, py, psize = 32, 32, 32
--         for i = 1, #palette_colors do
--             -- in button bounds
--             if (x >= px - 16 and y >= py - 16) and (x <= px + psize + 16 and y <= py + psize + 16) then
--                 local clr = palette_colors[i]

--                 pnl.CurrentColor = clr
--             end

--             px = px + psize + 16
--         end
--     end
-- end