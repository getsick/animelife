local xpcall = xpcall
local push_filter_min = render.PushFilterMin
local push_filter_mag = render.PushFilterMag
local pop_filter_min = render.PopFilterMin
local pop_filter_mag = render.PopFilterMag
local screen_scale = ScreenScale
local round = math.Round
local min = math.min
local scr_w = ScrW
local scr_h = ScrH
local set_draw_color = surface.SetDrawColor
local set_material = surface.SetMaterial
local draw_textured_rect = surface.DrawTexturedRect

module("ui", package.seeall)

BASE_RESOLUTION_WIDTH = 1920
BASE_RESOLUTION_HEIGHT = 1080

function Smooth(bool, resolution_based)
    xpcall(function()
        local run = resolution_based and (ScrW() ~= BASE_RESOLUTION_WIDTH or ScrH() ~= BASE_RESOLUTION_HEIGHT) or true
        if bool then
            if run then
                push_filter_min(TEXFILTER.ANISOTROPIC)
                push_filter_mag(TEXFILTER.ANISOTROPIC)
            end
        else
            if run then
                pop_filter_min()
                pop_filter_mag()
            end
        end
    end, function(thrown_err)
        print('{AnimeLife UI} Smoothing disabled:', thrown_err)
    end)
end

-- Drawing
function GenerateShadowFont() end -- TODO
function DrawShadowText() end -- TODO

function DrawMaterial(mat, x, y, w, h, col)
    set_draw_color(col)
    set_material(mat)
    draw_textured_rect(x, y, w, h)
end

-- Resolution scaling
function x(num)
    if !num then return 0 end
    if scr_w() == BASE_RESOLUTION_WIDTH then return num end
    return (screen_scale(num) / 3)
end

function y(num)
    if !num then return 0 end
    if scr_h() == BASE_RESOLUTION_HEIGHT then return num end
    return round(num * min(scr_w(), scr_h()) / BASE_RESOLUTION_HEIGHT)
end

-- Panels
local meta = FindMetaTable("Panel")

function meta:SeekResolutionChange(func)
    if !self.LastResolutionValues then
        self.LastResolutionValues = {scr_w(), scr_h()}
    end

    if scr_w() ~= self.LastResolutionValues[1] or scr_h() ~= self.LastResolutionValues[2] then
        func(self)

        self.LastResolutionValues[1] = scr_w()
        self.LastResolutionValues[2] = scr_h()
    end
end