local marker_white = Color(255, 255, 255)
local marker_outline = Color(0, 0, 0)

surface.CreateFont("animelife.worldmarkers", {font = "Exo 2 SemiBold", size = 16, weight = 500, extended = true})

module("markers", package.seeall)

markers = markers or {}

function markers:CreateAt(origin, text, color, duration)
    if !origin then error('trying to create marker yet the position is null') end
    if !text then text = "Marker" end
    if !color then color = marker_white end
    if !duration then duration = 60 end

    local idx = table.insert(markers, {
        Origin = origin, Text = text, Color = color, Duration = duration, Time = CurTime()
    })

    return idx
end

function markers:GetList()
    return markers
end

function markers:GetMarker(idx)
    return markers[idx]
end

function markers:Remove(idx)
    table.remove(markers, idx)

    return true
end

function markers:Draw(idx)
    local marker = self:GetMarker(idx)
    if marker == nil then return end

    local pos = marker.Origin
    if !isvector(pos) then self:Remove(idx) end -- how come?

    local dist = LocalPlayer():GetPos():Distance(pos)
    dist = math.floor(dist * 1.905 / 100) -- convert to meters

    -- grab 2d values from vector
    pos = pos:ToScreen()

    draw.SimpleTextOutlined(("%s [%s м]"):format(marker.Text, dist), "animelife.worldmarkers", pos.x, pos.y, marker.Color, 1, 1, 1, marker_outline)
end

function markers:DrawAll()
    local marker_list = self:GetList()
    if table.IsEmpty(marker_list) then return end
    for k, _ in ipairs(marker_list) do
        self:Draw(k)
    end
end

hook.Add("HUDPaint", "animelife.worldmarkers.default_draw", function()
    if !IsValid(LocalPlayer()) then return end

    -- default action
    markers:DrawAll()
end)

hook.Add("Think", "animelife.worldmarkers.remover", function()
    if !IsValid(LocalPlayer()) then return end

    local marker_list = markers:GetList()
    if table.IsEmpty(marker_list) then return end
    for k, v in ipairs(marker_list) do
        if (v.Time + v.Duration) < CurTime() then
            markers:Remove(k)
            continue
        end

        if v.Origin:DistToSqr(LocalPlayer():GetPos()) < 128^2 then
            markers:Remove(k)
            continue
        end
    end
end)

net.Receive("animelife.worldmarkers.receive", function()
    local pos = net.ReadVector()
    local text = net.ReadString()
    local color = net.ReadColor()
    local duration = net.ReadInt(14)
    
    markers:CreateAt(pos, text, color, duration)
end)