local pixel_black = Color(0, 0, 0)

MySQLite.begin()
MySQLite.query([[
    CREATE TABLE IF NOT EXISTS al_pixeldraw(
        canvas TINYTEXT NOT NULL PRIMARY KEY,
        data LONGTEXT NOT NULL
    );
]])

module("pixeldraw", package.seeall)

canvas_list = canvas_list or {}

function pixeldraw:NewCanvas(index, width, height, bg_color, pixel_size)
    if !width then error('you have to declare a width') end
    if !height then error('you have to declare a height') end
    if !bg_color then bg_color = canvas_white end
    if !pixel_size then pixel_size = 32 end

    local canvas = {
        Width = width,
        Height = height,
        BackgroundColor = bg_color,
        PixelSize = pixel_size
    }

    canvas_list[index] = canvas

    return index
end

function pixeldraw:PlacePixel(canvas, x, y, color)
    if !canvas then error('canvas is null, quitting.') end
    if !x then error('x seems to be invalid') end
    if !y then error('y seems to be invalid') end
    if !color then color = pixel_black end

    if isnumber(canvas) and !canvas_list[canvas] then -- assuming its an index
        error('couldnt find given canvas')
    end

    canvas = canvas_list[canvas] -- should be valid at this point?

    if !canvas.Pixels then canvas.Pixels = {} end

    local pixels = canvas.Pixels

    -- are we going out of bounds?
    if (x < 0 or y < 0) or (x > canvas.Width or y > canvas.Height) then
        return false
    end

    -- accept only snapped positions
    if x % canvas.PixelSize ~= 0 or y % canvas.PixelSize ~= 0 then
        return false
    end

    local function find_pixel_by_xy(x, y)
        for k, v in ipairs(pixels) do
            if v.x == x and v.y == y then
                return k, v
            end
        end
    end

    -- do nothing for equally perfect pixels
    local lk_index, lk_pixel = find_pixel_by_xy(x, y)
    if lk_pixel and !table.IsEmpty(lk_pixel) then
        if lk_pixel.color == color then
            -- they're the same, skipping
            return false
        end

        if lk_pixel.color ~= color then
            -- color changed, skipping
            canvas.Pixels[lk_index].color = color

            return true -- still counts as `success`
        end

        if lk_pixel.color ~= color and color == canvas.BackgroundColor then
            -- we don't want to store useless pixels (since they're the same color as bg)
            -- skipping.

            return false
        end
    end

    canvas.Pixels[#canvas.Pixels + 1] = {
        x = x, y = y , color = color
    }

    return true -- yay, we did it
end

function pixeldraw:SaveCanvas(canvas)
    if !canvas then error('invalid canvas') end
    if !canvas_list[canvas] then error('invalid canvas') end

    local pixels = canvas_list[canvas].Pixels
    if pixels == nil or table.IsEmpty(pixels) then
        return
    end

    pixels = util.TableToJSON(pixels)

    MySQLite.query([[SELECT data
    FROM al_pixeldraw
    where canvas = ]] .. MySQLite.SQLStr(canvas) .. ";", function(data)
        if !data then
            MySQLite.query([[REPLACE INTO al_pixeldraw VALUES(]] ..
            MySQLite.SQLStr(canvas) .. [[, ]] ..
            MySQLite.SQLStr(pixels) .. ");")
        end

        MySQLite.query([[UPDATE al_pixeldraw SET data = ]] .. MySQLite.SQLStr(pixels) .. [[ WHERE canvas = ]] .. MySQLite.SQLStr(canvas) .. ";")
    end, function(e, q) 
        print("Database Error:", e, q)
    end)
end

function pixeldraw:RestoreCanvasData(canvas)
    if !canvas then error('invalid canvas') end
    if !canvas_list[canvas] then error('invalid canvas') end

    MySQLite.query([[
        SELECT data
        FROM al_pixeldraw
        where canvas = ]] .. MySQLite.SQLStr(canvas) .. ";", function(data)
            if !data then return end
            
            if data[1].data then
                canvas_list[canvas].Pixels = util.JSONToTable(data[1].data)
            end
        end, function(e, q)
            print("Database Error:", e, q)
        end)
end