util.AddNetworkString("animelife.worldmarkers.receive")

module("markers", package.seeall)

function markers:CreateAt(ply, origin, text, color, duration)
    if !IsValid(ply) or !ply:IsPlayer() then return end
    if !isvector(origin) then return end
    if !isstring(text) then return end
    if !color then color = Color(255, 255, 255) end
    if !duration then duration = 300 end

    net.Start("animelife.worldmarkers.receive")
        net.WriteVector(origin)
        net.WriteString(text)
        net.WriteColor(color)
        net.WriteInt(duration, 14)
    net.Send(ply)
end