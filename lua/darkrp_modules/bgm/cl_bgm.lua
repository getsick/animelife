CreateClientConVar("al_bgm_volume", "0.06", true)
CreateClientConVar("al_bgm_preference", "", true)
CreateClientConVar("al_bgm_enable", "1", true)

module("backgroundmusic", package.seeall)

math.randomseed(os.time())

List = {
    "17_issho_ni_arukou", "18_arigatou_yoroshiku_ne", "19_new_waltz", "20_kaze_to_yubikiri",
    "21_kono_kara_noyouni", "22_thoughts_difficult_to_tell", "23_asobi_no_jikan",
    "24_ecstacy", "25_route135", "26_hurry_up", "27_swing_on_the_beach", "28_funny_cat",
    "29_blue_in_bossa", "30_natsu_no_ookoku", "31_delightful_day", "32_suichuu_megane",
    "33_umi_to_sora_to_taiyou_to_sunset"
}
IsPlaying = IsPlaying or false
SoundObjects = SoundObjects or {}

function backgroundmusic:IsTriggered()
    -- return ply:GetNWBool("animelife.bgm.trigger", false)
    -- return LocalPlayer().BGMTrigger
    return true
end

function backgroundmusic:Enabled()
    return GetConVar("al_bgm_enable"):GetInt() > 0
end

function backgroundmusic:ShouldPlay()
    -- TODO: Add volume check, intro check and bgm on/off check
    return self:IsTriggered() and self:Enabled() and !IsPlaying and table.IsEmpty(SoundObjects)
end

function backgroundmusic:GetVolume()
    return GetConVar("al_bgm_volume"):GetFloat() or 0.1
end

function backgroundmusic:GetPreferredTrack()
    return GetConVar("al_bgm_preference"):GetString() or ""
end

function backgroundmusic:PlayRandom()
    local path = List[math.random(1, #List)]
    path = "sound/animelife/bgm/" .. path .. ".mp3"
    local preferred_track = self:GetPreferredTrack()
    if preferred_track ~= "" then
        path = "sound/animelife/bgm/" .. preferred_track .. ".mp3"
    end

    if timer.Exists("animelife.bgm.stop_timer") then
        timer.Remove("animelife.bgm.stop_timer")
    end

    sound.PlayFile(path, "", function(chan)
        if IsValid(chan) then
            chan:SetVolume(self:GetVolume())

            IsPlaying = true

            table.insert(SoundObjects, chan)

            timer.Create("animelife.bgm.stop_timer", chan:GetLength() + 1, 1, function()
                self:Stop()

                IsPlaying = false

                timer.Remove("animelife.bgm.stop_timer")
            end)
        end
    end)
end

function backgroundmusic:Stop()
    -- if IsValid(SoundObject) then
    --     SoundObject:Stop()
    --     SoundObject = nil
    -- end

    for _, obj in pairs(SoundObjects) do
        if IsValid(obj) then
            obj:Stop()
        end

        table.RemoveByValue(SoundObjects, obj)
    end

    if timer.Exists("animelife.bgm.stop_timer") then
        timer.Remove("animelife.bgm.stop_timer")
    end

    IsPlaying = false
end

function backgroundmusic:ChangeVolumeInstant(vol)
    for _, obj in pairs(SoundObjects) do
        if IsValid(obj) then
            obj:SetVolume(vol)
        end
    end
end

if !timer.Exists("animelife.bgm.auto_play") then
    timer.Create("animelife.bgm.auto_play", math.random(15, 30), 0, function()
        if !backgroundmusic:ShouldPlay() then return end

        -- somehow still happens up to this day
        -- mostly when you're initial spawning
        if table.Count(SoundObjects) > 1 then
            backgroundmusic:Stop()
        end

        backgroundmusic:PlayRandom()
    end)
end