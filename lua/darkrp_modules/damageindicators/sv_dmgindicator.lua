util.AddNetworkString("animelife.damageindicator")
util.AddNetworkString("animelife.deathindicator")

hook.Add("EntityTakeDamage", "animelife.damageindicator", function(target, dmg)
    if IsValid(target) and target:IsPlayer() then
        local attacker = dmg:GetAttacker()
        if IsValid(attacker) and attacker:IsPlayer() and dmg:IsBulletDamage() then
            local damage = dmg:GetDamage()
            if damage < 1 then return end

            net.Start("animelife.damageindicator")
                net.WriteVector(target:GetPos() + Vector(0, 0, 72))
                net.WriteInt(damage, 32)
            net.Send(attacker)
        end
    end
end)

hook.Add("PlayerDeath", "animelife.deathindicator", function(victim, inflictor, attacker)
    if IsValid(victim) and victim:IsPlayer() then
        if IsValid(attacker) and attacker:IsPlayer() then
            if victim == attacker then return end
            net.Start("animelife.deathindicator")
                net.WriteInt(victim:LastHitGroup(), 5)
                net.WriteString(victim:Nick())
            net.Send(attacker)
        end
    end
end)